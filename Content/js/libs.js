import '../../node_modules/featherlight/release/featherlight.min.css';
import '../../node_modules/select2/dist/css/select2.min.css';
import '../../node_modules/qtip2/dist/jquery.qtip.min.css';
import '../../node_modules/jquery-loadmask/jquery.loadmask.css';
import '../../node_modules/nprogress/nprogress.css';
import '../../node_modules/chartist/dist/chartist.min.css';
import '../../node_modules/vue-directive-tooltip/css/index.css'; //https://hekigan.github.io/vue-directive-tooltip/
/*Keen-ui components in use*/
import '../../node_modules/keen-ui/dist/keen-ui.css';
/*Main app CSS*/
import '../less/app.less';

global.$ = window.jQuery = require('jquery');
require('jstorage');
require('featherlight');
//require('trunk8');
require('qtip2');
require('select2');
require('parsleyjs');
//require('jquery.scrollintoview');
require('jquery-simplecolorpicker');
require('jquery-idletimeout');
require('jquery-toast-plugin');
require('../../node_modules/linkifyjs/dist/linkify.js');
require('../../node_modules/linkifyjs/dist/linkify-string.js');
require('../../node_modules/linkifyjs/dist/linkify-html.js');
require('../../node_modules/linkifyjs/dist/linkify-jquery.js');
require('../../node_modules/jquery-loadmask/jquery.loadmask.js');
global.Handlebars = require('handlebars');
global.Sammy = $.sammy = require('sammy');
require('../../node_modules/sammy/lib/min/plugins/sammy.handlebars-latest.min.js');
require('../../node_modules/sammy/lib/min/plugins/sammy.title-latest.min.js');
global._ = require('underscore');
global.s = require('underscore.string');
global.crosstab = require('crosstab');
global.NProgress = require('NProgress');
//global.autosize = require('autosize');
global.moment = require('moment');
global.Modernizr = require('modernizr');
global.axios = require('axios');
global.Vue = require('vue');
global.Mousetrap = require('mousetrap');
global.Chartist = require('chartist');
require('chartist-plugin-tooltips');
require("babel-polyfill");
global.appMain = require('./app/app-main').default;
global.appApi = require('./views/AppIntegrationApi'); // Webpack will automatically convert this to window(default), read more here: https://webpack.js.org/configuration/node
global.CareflowApp = require('./careflow-app').default; //Handlebars
//global.powerBiclient = require('./app/plugins/powerbi');
require('./app/plugins/diffMatchPatch');
//require('./app/plugins/plugins-and-helpers');

/*Vue.js components - globally register*/
import KeenUI from 'keen-ui';
Vue.use(KeenUI);

import Tooltip from 'vue-directive-tooltip';
Vue.use(Tooltip, { delay: 0, class: 'u-ui-tooltip-dark' }); //https://hekigan.github.io/vue-directive-tooltip/ - used in Tasks List component only - TODO: find a better option

/*Raygun app reporting setup*/
global.rg4js = require('raygun4js');

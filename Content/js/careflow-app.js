var hbTemplates = require.context('./handlebarsTemplates/', true, /\.htm$/);

export default hbTemplates.keys().reduce(function(result, templatePath) {
    var templatePathNormalized = templatePath.slice(2), //remove "./" from path
        virtualTemplatePath = 'Content/js/handlebarsTemplates/' + templatePathNormalized;

    result.Templates[virtualTemplatePath] = hbTemplates(templatePath);

    return result;
}, { Templates: {} });
/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var UnsubscribeFromGroupSubscription = UnsubscribeFromGroupSubscription || {

    callApi: function (memberAreaId, subscriptionId) {
        var verb = "POST";
        var methodUrl = "Subscription/UnsubscribeFromGroupSubscription";
        var parameters = {
			"SubscriptionId": subscriptionId,
			"MemberAreaId": memberAreaId
		};
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry); //returns data, textStatus, jqXHR
    }
};

export default UnsubscribeFromGroupSubscription;

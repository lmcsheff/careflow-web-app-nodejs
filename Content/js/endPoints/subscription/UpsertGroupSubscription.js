﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var UpsertGroupSubscription = UpsertGroupSubscription || {

    callApi: function (obj) {
        var verb = "POST";
        var methodUrl = "Subscription/UpsertGroupSubscription";
        var parameters = obj;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry); //returns data, textStatus, jqXHR
    },

    responseHandler: function (data, textStatus, jqXHR) {
        UpsertGroupSubscription.data = data;
        UpsertGroupSubscription.textStatus = textStatus;
    }

};

export default UpsertGroupSubscription;
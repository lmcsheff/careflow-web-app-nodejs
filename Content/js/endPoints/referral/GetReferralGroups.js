﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetReferralGroups = GetReferralGroups || {

    callApi: function (networkId) {

        var verb = "GET";
        var methodUrl = "referral/GetReferralGroups?networkId=" + networkId + "";
        var isRetry = null;
        var parameters = null;
               
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler : function (data, textStatus, jqXHR) {        
        GetReferralGroups.data = data;
        GetReferralGroups.textStatus = textStatus;
    }
           
};

export default GetReferralGroups;

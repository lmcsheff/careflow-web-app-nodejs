﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

/*Gets a list of referrals for a patient, based on the given network identifier, patient identifier and request model.*/

let GetNetworkReferralList = GetNetworkReferralList || {

    callApi: function (accessGroupId, patientId, params) {

        let verb = "POST";
        let methodUrl = "Networks/" + accessGroupId + "/Patients/" + patientId +"/Referrals/List";
        let isRetry = null;
        let parameters = params;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    }

};

export default GetNetworkReferralList;

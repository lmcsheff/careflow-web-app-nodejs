/*Cross-Network - Allows a user with the ViewReferralTeams permission to view the teams that accept referrals in a network, and any networks affiliated with it./*

/*Components*/
import CareflowApi from '../careflowApp/CareflowApi';

export function getReferralsTeams(networkExternalAccessGroupId) {

    if (!networkExternalAccessGroupId) return Promise.reject();

    const url = `/Networks/${networkExternalAccessGroupId}/Referrals/Teams`;
    const verb = "GET";
    const isRetry = null;
    const parameters = null;

    return CareflowApi.MakeRequest(verb, url, parameters, isRetry);

}

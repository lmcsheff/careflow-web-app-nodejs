﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetReferralList = GetReferralList || {

    callApi: function (accessGroupId, params) {
        
        var verb = "POST";
        var methodUrl = "Teams/"+accessGroupId+"/Referrals/List";
        var isRetry = null;
        var parameters = params;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetReferralGroups.data = data;
        GetReferralGroups.textStatus = textStatus;
    }

};

export default GetReferralList;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var SendReferral = SendReferral || {

    callApi: function (sendingMemberAreaId, receivingMemberAreaId, patientExternalIdentifier, startedFromContentItemId, referralFields) {

        var verb = "POST";
        var methodUrl = "referral/SendReferral";
        var parameters = {
            SendingMemberAreaId: sendingMemberAreaId,
            ReceivingMemberAreaId: receivingMemberAreaId,
            PatientExternalIdentifier: patientExternalIdentifier,
            StartedFromContentItemId: startedFromContentItemId,
            ReferralFields: referralFields
        };
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        SendReferral.data = data;
        SendReferral.textStatus = textStatus;
        SendReferral.jqXHR = jqXHR;

    }

};

export default SendReferral;

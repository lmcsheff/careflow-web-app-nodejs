/*V12 Cross network referrals./*

/*Components*/
import CareflowApi from '../careflowApp/CareflowApi';

export function upsertPatientReferral(receivingExternalAccessGroupId, sendingExternalAccessGroupId, patientExternalIdentifier, referralFields) {
    const url = `/Teams/${receivingExternalAccessGroupId}/Referrals`;
    const verb = "POST";
    const isRetry = null;
    const parameters = {
        SendingTeamId: sendingExternalAccessGroupId,
        PatientId: patientExternalIdentifier,
        Fields: referralFields
    };
    return CareflowApi.MakeRequest(verb, url, parameters, isRetry);
}

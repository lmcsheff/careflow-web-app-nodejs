﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var UpdateReferralStatus = UpdateReferralStatus || {

    callApi: function (conversationTrackableID, commentText, targetMemberAreaId, updatedReferralStatusId) {

        var verb = "POST";
        var methodUrl = "referral/UpdateReferralStatus";
        var parameters = {
            "ConversationTrackableID": conversationTrackableID,
            "CommentText": commentText,
            "UpdatedReferralStatusId": updatedReferralStatusId,
            "TargetMemberAreaId": targetMemberAreaId
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {        
        UpdateReferralStatus.textStatus = textStatus;
        UpdateReferralStatus.jqXHR = jqXHR;
    }

};

export default UpdateReferralStatus;

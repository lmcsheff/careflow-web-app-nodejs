/*Allows a user with the Group Owner, Group Administrator, or Super User role to view the Team Handover Roles available for their team, and the which have been selected.*/

import CareflowApi from 'Endpoints/careflowApp/CareflowApi';

export function callApi(accessGroupExternalIdentifier, skip, take) {

    let verb = "GET";
    let methodUrl = '/Teams/' + accessGroupExternalIdentifier  + '/Profiles?skip=' + skip + '&take=' + take;
    let parameters = null;
    let isRetry = null;

    return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
}

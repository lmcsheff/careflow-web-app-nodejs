/*Allows a user with the Group Owner, Group Administrator, or Super User role to select the Team Handover Roles for their team, and the order in which they will be listed.*/

import CareflowApi from 'Endpoints/careflowApp/CareflowApi';
import { isArray } from 'util';

export function callApi(accessGroupExternalIdentifier, teamRoleExternalIdentifiers) {

    if (!teamRoleExternalIdentifiers || !Array.isArray(teamRoleExternalIdentifiers)) return Promise.reject();

    let verb = "POST";
    let methodUrl = '/Teams/' + accessGroupExternalIdentifier + '/Profiles';
    let parameters = {
        ProfileExternalIdentifiers: teamRoleExternalIdentifiers
    };
    let isRetry = null;

    return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
}

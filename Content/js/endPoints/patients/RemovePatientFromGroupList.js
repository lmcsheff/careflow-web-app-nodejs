﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var RemovePatientFromGroupList = RemovePatientFromGroupList || {

    callApi: function (memberAreaId, patientExternalIdentifier) {
        var verb = "POST";
        var methodUrl = "patients/RemovePatientFromGroupList";
        var parameters = {
			MemberAreaId: memberAreaId,
            PatientExternalIdentifier: patientExternalIdentifier,
            RelatedContentItemId: null
        };
        var isRetry = null;
        var data = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        RemovePatientFromGroupList.data = data;
    }

};

export default RemovePatientFromGroupList;

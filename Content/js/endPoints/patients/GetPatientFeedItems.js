﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetPatientFeedItems = GetPatientFeedItems || {
    callApi: function (externalIdentifier, networkId, skip, take) {
        var verb = "GET";
        var methodUrl = "patients/GetPatientFeedItems?externalIdentifier=" + externalIdentifier + "&networkId=" + networkId + "&skip=" + skip + "&take=" + take + "";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetPatientFeedItems.data = data;
        GetPatientFeedItems.textStatus = textStatus;
    }
};

export default GetPatientFeedItems;
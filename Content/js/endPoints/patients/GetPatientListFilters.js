﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetPatientListFilters = GetPatientListFilters || {

    callApi: function (memberAreaId) {
        
        var verb = "GET";
        var methodUrl = "Patients/GetPatientListFilters";
        var parameters = {
            "GroupID": memberAreaId    
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetPatientListFilters.data = data;
        GetPatientListFilters.textStatus = textStatus;
    }

};

export default GetPatientListFilters;
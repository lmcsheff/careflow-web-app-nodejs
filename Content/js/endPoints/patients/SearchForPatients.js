﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var SearchForPatients = SearchForPatients || {

    callApi: function (searchTerm, networkId, skip, take) {

        var verb = "GET";
        var methodUrl = "/patients/SearchForPatients?searchTerm=" + searchTerm + "&networkId=" + networkId + "&skip=" + skip + "&take=" + take +"";
        var parameters = null;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler : function (data, textStatus, jqXHR) {
        SearchForPatients.data = data;
        SearchForPatients.textStatus = textStatus;
       }

};

export default SearchForPatients;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetPatientsOnGroupList = GetPatientsOnGroupList || {
    callApi: function (memberAreaId, includeParamaters, itemsPerPage, page, sortBy) {
        return $.Deferred(function () {//Needed to sort handovers - Legacy app does not use native promise to consume, jQ based, so use $deferred :(
            itemsPerPage = itemsPerPage || null;
            sortBy = sortBy || null;

            var verb = "POST";
            var methodUrl = "Groups/" + memberAreaId + "/Patients";
            var parameters = {
                "Include": includeParamaters, //[ClinicalTags,HandoverNotes,JustCurrentGroupClinicalTags,RecentContent|null]
                "ItemsPerPage": itemsPerPage,
                "Page": page,
                "SortBy": sortBy //PatientName|Location|ClinicianName|null
            };
            var isRetry = null;

            //Order Handover data correctly on response, do here at API level to avoid repeating at every call - https://devmvc.careflowapp.com/#/49/Teams/195/Patients
            CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry).then((response, textStatus, jQXHr) => {
                if (response.Data.length) {
                    //Sort by SBAR keys
                    response.Data.forEach(function (item, index, array) {
                        if (item.HandoverNotes && item.HandoverNotes.length && item.HandoverNotes[0].Notes) {
                            item.HandoverNotes[0].Notes = _.sortBy(item.HandoverNotes[0].Notes, function (item) { return item.Position; });
                        }
                    });
                }
                this.resolve(response, textStatus, jQXHr);
            }).fail((jqXHR, textStatus, errorThrown) => {//makeRequest uses jQ so deferred (fail not catch method)
                this.reject(jqXHR, textStatus, errorThrown);
            });
        });
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetPatientsOnGroupList.data = data;
        GetPatientsOnGroupList.textStatus = textStatus;
    }
};

export default GetPatientsOnGroupList;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var AddPatientToGroupList = AddPatientToGroupList || {

    callApi: function (memberAreaId, patientExternalIdentifier) {
        var verb = "POST";
        var methodUrl = "patients/AddPatientToGroupList";
        var parameters = {
            MemberAreaId: memberAreaId,
            PatientExternalIdentifier: patientExternalIdentifier,
            RelatedContentItemId: null
        };
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        AddPatientToGroupList.data = data;
    }

};

export default AddPatientToGroupList;

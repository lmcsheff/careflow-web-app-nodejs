/* Utils */
import CareflowApi from 'Endpoints/careflowApp/CareflowApi';

var GetPatientHandoverNotesHistoryByProfile = GetPatientHandoverNotesHistoryByProfile || {
    callApi: function(model, skip, take) {
        //Needed to sort handovers - Legacy app does not use native promise to consume, jQ based, so use $deferred :(
        return $.Deferred(function() {
            var verb = "POST";
            var methodUrl = "patients/GetPatientHandoverNotesHistory?skip=" + skip + "&take=" + take + "";
            var parameters = model;
            var isRetry = null;

            //Order Handover data correctly on response, do here at API level to avoid repeating at every call - https://devmvc.careflowapp.com/#/49/Teams/195/Patients
            CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry).then((response, textStatus, jQXHr) => {
                if (response.Data.length) {
                    //Sort by SBAR keys
                    response.Data.forEach(function(item, index, array) {
                        if (item.Notes.length) {
                            item.Notes = _.sortBy(item.Notes, function(item) { return item.Position; });
                        }
                    });
                }

                this.resolve(response, textStatus, jQXHr);
            }).fail((jqXHR, textStatus, errorThrown) => { //makeRequest uses jQ so deferred (fail not catch method)
                this.reject(jqXHR, textStatus, errorThrown);
            });
        });
    }
};

export default GetPatientHandoverNotesHistoryByProfile;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetPatient = GetPatient || {
    callApi: function(externalIdentifier, networkId, includeParamaters) {
        //Needed to sort handovers - Legacy app does not use native promise to consume, jQ based, so use $deferred :(
        return $.Deferred(function() {
            var verb = "GET";
            var methodUrl = "patients/GetPatient?externalIdentifier=" + externalIdentifier + "&networkId=" + networkId + "&include=" + includeParamaters;
            var parameters = null;
            var isRetry = null;

            //Order Handover data correctly on response, do here at API level to avoid repeating at every call - https://devmvc.careflowapp.com/#/49/Teams/195/Patients
            CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry).then((response, textStatus, jQXHr) => {

                //Sort by SBAR keys
                if (response.Data.HandoverNotes && response.Data.HandoverNotes.length) {
                    response.Data.HandoverNotes.forEach(x => {
                        x.Notes = _.sortBy(x.Notes, function(item) { return item.Position; });
                    });

                }

                this.resolve(response, textStatus, jQXHr);

            }).fail((jqXHR, textStatus, errorThrown) => { //makeRequest uses jQ so deferred (fail not catch method)
                this.reject(jqXHR, textStatus, errorThrown);
            });
        });
    }
};

export default GetPatient;

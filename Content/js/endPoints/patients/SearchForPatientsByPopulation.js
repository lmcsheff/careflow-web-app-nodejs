﻿/* Utils */
import CareflowApi from 'Endpoints/careflowApp/CareflowApi';

var SearchForPatientsByPopulation = SearchForPatientsByPopulation || {

    callApi: function(networkId, clinician, site, area, skip, take) {

        var verb = "GET";
        var methodUrl = "patients/SearchForPatientsByPopulation?networkId=" + networkId + "&clinician=" + encodeURIComponent(clinician) + "&site=" + encodeURIComponent(site) + "&area=" + encodeURIComponent(area) + "&skip=" + skip + "&take=" + take + "";
        var parameters = null;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function(data, textStatus, jqXHR) {
        SearchForPatientsByPopulation.data = data;
        SearchForPatientsByPopulation.textStatus = textStatus;

    }

};

export default SearchForPatientsByPopulation;

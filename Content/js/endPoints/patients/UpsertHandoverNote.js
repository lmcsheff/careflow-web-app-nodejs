﻿/* Utils */
import CareflowApi from 'Endpoints/careflowApp/CareflowApi';

var UpsertHandoverNote = UpsertHandoverNote || {

    callApi: function(groupId, situationBody, backgroundBody, assessmentBody, recommendationBody, patientExternalIdentifier, teamRoleExternalIdentifier) {

        var verb = "POST";
        var methodUrl = "patients/upserthandovernote";
        var parameters = {
            "GroupId": groupId,
            "Notes": [
                { "Key": "Situation", "Position": 0, "Value": situationBody.trimEnd() },
                { "Key": "Background", "Position": 1, "Value": backgroundBody.trimEnd() },
                { "Key": "Assessment", "Position": 2, "Value": assessmentBody.trimEnd() },
                { "Key": "Recommendation", "Position": 3, "Value": recommendationBody.trimEnd() }
            ],
            "PatientExternalIdentifier": patientExternalIdentifier,
            "ProfileExternalIdentifier": teamRoleExternalIdentifier || null
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function(data, textStatus, jqXHR) {
        UpsertHandoverNote.textStatus = textStatus;
        UpsertHandoverNote.jqXHR = jqXHR;
    }

};

export default UpsertHandoverNote;

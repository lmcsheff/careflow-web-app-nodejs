/*https://doccom.atlassian.net/browse/DOC-3671*/
/* Utils */
import staticKeys from 'Endpoints/CareflowApp/Static';
import Dispatch from 'Endpoints/CareflowApp/Dispatch';

export function authenticateHandoverToken(handoverToken, type = 'handover_token')  {

    const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    const verb = "POST";
    const methodUrl = '/handovertoken'; //internal API

    const headers = { 'CSRF-Token': token, 'Content-Type': 'application/json' }
    const parameters = {
        token: handoverToken,
        type: type

    }
	return Dispatch.Call(verb, methodUrl, parameters, headers, token, true);
}
﻿/* Utils */
import Dispatch from './Dispatch';

var AuthenticationLogIn = AuthenticationLogIn || {
	callApi: function (email, password) {
	
		var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        var verb = "POST";
        var methodUrl = '/authenticate'; //internal API
        
        var headers = {'CSRF-Token':token, 'Content-Type':'application/json' }
        var parameters = {			
				response_type: 'token',
				redirect_uri: 'http://www.careflowconnect.com',
				client_id: 'DocComMobile',
				emailAddress: email,
				password: password
			
        }
        return Dispatch.Call(verb, methodUrl, parameters, headers, token, true);
    }
};

export default AuthenticationLogIn;

﻿/* Utils */
import staticKeys from './Static';
import PubSub from './PubSub';

/* STORAGE: Use this namespace to interact with jStorage,*/
var Vault = Vault || {
    setItem: function(key, value) {
        var apiKey = staticKeys.getStaticKey(key);
        if (apiKey) {
            return $.jStorage.set(apiKey, value);
        } else return console.error("key not set");
    },

    getItem: function(key) {
        //if (typeof $.jStorage.get(staticKeys.getStaticKey(key)) === "object" || _.isArray($.jStorage.get(staticKeys.getStaticKey(key)))) {
        //    return JSON.parse(JSON.stringify($.jStorage.get(staticKeys.getStaticKey(key))));
        //} else {
        //    return $.jStorage.get(staticKeys.getStaticKey(key));
        //}
        return JSON.parse(JSON.stringify($.jStorage.get(staticKeys.getStaticKey(key))));
    },

    deleteItem: function(key) {
        var apiKey = staticKeys.getStaticKey(key);
        if (apiKey) {
            return $.jStorage.deleteKey(apiKey);
        }
    },

    setTTL: function(key, ttl) {
        return $.jStorage.setTTL(key, parseInt(ttl)); // milliseconds
    },

    getTTL: function(key) {
        return $.jStorage.getTTL(key); // TTL in milliseconds or 0
    },

    flush: function() {
        $.jStorage.flush(); //Clears the cache.
        sessionStorage.clear(); //Clear SessionStorage
        localStorage.clear();
    },

    index: function() {
        return $.jStorage.index(); //Returns all the keys currently in use as an array.
    },

    /*Handle native Session Storage API.*/

    setSessionItem: function(key, value) {
        sessionStorage[key] = JSON.stringify(value);

		if (key === "ActiveGroupMemberAreaId") {
			VueEventBus.$emit('Vault.ActiveGroupMemberAreaIdChanged', key, value);
            return PubSub.publish("CareflowApp.ActiveGroupMemberAreaIdChanged", {});
        }
		if (key === "ActiveNetworkId") {
			VueEventBus.$emit('Vault.ActiveNetworkChanged', key, value);
            return PubSub.publish("CareflowApp.ActiveNetworkChanged", {});
        }
    },
    getSessionItem: function(key) {
        //return sessionStorage.getItem(key);
        return sessionStorage[key] && JSON.parse(sessionStorage[key]);
    },

    deleteSessionItem: function(key) {
        sessionStorage.removeItem(key);

		if (key === "ActiveGroupMemberAreaId") {
			VueEventBus.$emit('Vault.ActiveGroupMemberAreaIdChanged', key, false);
            return PubSub.publish("CareflowApp.ActiveGroupMemberAreaIdChanged", {});
        }
		if (key === "ActiveNetworkId") {
			VueEventBus.$emit('Vault.ActiveNetworkChanged', key, false);
            return PubSub.publish("CareflowApp.ActiveNetworkChanged", {});
        }
    },
};

export default Vault;

﻿/* Utils */
import staticKeys from './Static';
import Dispatch from './Dispatch';
import Vault from './Vault';


let CareflowApi = CareflowApi || (function() {
    let makeRequest = function(verb, methodUrl, parameters, isRetry, apiType, responseType) {
        let deferred = $.Deferred(); //App api layer / api requests (MakeRequest) still consumes 3 params on success, hangover from jQuery, so cant use native Promise just yet


        _makeApiCall(verb, methodUrl, parameters, isRetry, apiType, responseType).then((data, textStatus, jqXHR, responseHeaders) => {
            deferred.resolve(data, textStatus, jqXHR, responseHeaders);
        }).fail((jqXHR, textStatus, errorThrown) => {
            if (jqXHR && textStatus && errorThrown) deferred.reject(jqXHR, textStatus, errorThrown);
            deferred.reject('api request failed');
        });

        // The deferred object has a promise in it, which is what we want to return.  We can then chain other methods off this one that won't get called until this one completes.  We want to return the promise because promises are immutable.
        return deferred.promise();

    };

    var _makeApiCall = function(verb, methodUrl, parameters, isRetry, apiType, responseType) {

        let deferred = $.Deferred(); //App api layer / api requests (MakeRequest) still consumes 3 params on success, hangover from jQuery, so cant use native Promise just yet

        let baseUrl;
        if (apiType) {
            baseUrl = staticKeys.getStaticKey(apiType);
        } else {
            baseUrl = staticKeys.getStaticKey("ApiRoute");
        }

        if (!baseUrl) deferred.reject("No valid API base URL found");
        //var baseUrl = Modernizr.cors ? staticKeys.getStaticKey("ApiRoute") : staticKeys.getStaticKey("ProxyApiRoute"); Support IE11+ from 12/2017, no proxy requirement from this point

        let url = baseUrl + methodUrl;
        //var token = Vault.getItem(Static.accessTokenKey);
        let token = Vault.getSessionItem(staticKeys.getStaticKey("AccessTokenKey"));

        Dispatch.Call(verb, url, parameters, null, token, null, responseType) //NB, TPI will call Dispatch directly
            .then((response) => { //Returns native promise
                deferred.resolve(response.data, response.statusText, response.request, response.headers);
            })
            .catch(error => {
                console.log(error)
                if (error && error.hasOwnProperty('response') && error.response) deferred.reject(error.response, error.response.statusText, error);
                deferred.reject('api request failed');
            });

        return deferred.promise();

    };
    return {
        MakeRequest: makeRequest
    };
})();

export default CareflowApi;

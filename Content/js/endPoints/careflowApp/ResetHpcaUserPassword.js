﻿/*https://doccom.atlassian.net/browse/DOC-3082*/

/*User is not logged in at this point, so call directly to API. Using CareFlowApp to make API call will reject 'No Active Session and Refresh token expired'. Call via Axios direct*/

/* Utils */
import StaticKeys from 'Endpoints/careflowApp/Static';

var ResetHpcaUserPassword = ResetHpcaUserPassword || {
    callApi: function (emailAddress) {
        var requestObject = {}
        var url = StaticKeys.getStaticKey("ApiAuthRoute") + "account/password/reset";
        var parameters = {
            EmailAddress: emailAddress
        };
        requestObject.method = "POST",
        requestObject.url = url,
        requestObject["data"] = parameters

		return axios(requestObject).then(function (response) {
			return Promise.resolve(response);
		}).catch(function (error) {
			return Promise.reject(error);
        });
    }
};

export default ResetHpcaUserPassword;

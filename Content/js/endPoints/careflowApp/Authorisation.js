﻿/* Utils */
import staticKeys from './Static';
import Dispatch from './Dispatch';
import Vault from './Vault';

var Authorisation = Authorisation || {
    RefreshToken: function() {
        var url = staticKeys.getStaticKey("AuthRoute") + "RefreshToken";
        var refreshToken = Vault.getSessionItem(staticKeys.getStaticKey("RefreshTokenKey"));
        var csrfToken = $("#csrfToken").val();
        var headers = {
            refresh_token: refreshToken,
            requestVerificationToken: csrfToken
        };

        return Dispatch.Call("POST", url, null, headers, null, true, null)
            .then((response) => { //Reset auth credentials

                Vault.setItem("AccessTokenKey", response.headers[staticKeys.getStaticKey("AccessTokenKey")]); //https://doccom.atlassian.net/browse/WEB-713
                if (response.headers[staticKeys.getStaticKey("RefreshTokenKey")]) Vault.setItem("RefreshTokenKey", response.headers[staticKeys.getStaticKey("RefreshTokenKey")]);
                if (response.headers[staticKeys.getStaticKey("ExpiresInKey")]) Vault.setItem("ExpiresInKey", response.headers[staticKeys.getStaticKey("ExpiresInKey")]);
                Vault.setItem("AccessTokenRetreivedOn", new Date().getTime()); //unix

                //new token retrieved, pass it to any other open tabs as theirs will now be invalid
                if (crosstab.supported) {
                    let tabId = crosstab.id;
                    crosstab.broadcast("CareflowAppAuth.AccessTokenKeyRenewed", {
                        AccessTokenKeyRenewedInTabId: tabId,
                        ExpiresInKey: response.headers[staticKeys.getStaticKey("ExpiresInKey")],
                        AccessTokenRetreivedOn: new Date().getTime(),
                        AccessTokenKey: response.headers[staticKeys.getStaticKey("AccessTokenKey")]
                    });
                }
            }).catch(error => { });

    }
};

export default Authorisation;

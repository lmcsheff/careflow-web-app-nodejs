﻿/* Utils */
import CareflowApp from '../../careflow-app';

var GetHandlebarsTemplate = GetHandlebarsTemplate || {
    callApi: function(templatePath, domSelectorToInsertTemplateTo, templateData, domInsertMethod) {
        return $.Deferred(function() {
            var self = this;

            var handlebarsTemplate = CareflowApp.Templates[templatePath];
            if (typeof handlebarsTemplate !== "function") console.log('@@@@@', templatePath);
            var html = (templateData) ? handlebarsTemplate(templateData) : handlebarsTemplate();

            if (domSelectorToInsertTemplateTo) { //Insert to element
                switch (domInsertMethod) {
                    case "html":
                        //Enable any template contained inputs by default
                        domSelectorToInsertTemplateTo.html(html).find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input', '.tt-hint').prop("disabled", false).removeClass("disabled");
                        return self.resolve(html);

                    case "append":
                        domSelectorToInsertTemplateTo.append(html).find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input', '.tt-hint').prop("disabled", false).removeClass("disabled");
                        return self.resolve(html);

                    case "prepend":
                        domSelectorToInsertTemplateTo.prepend(html).find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input', '.tt-hint').prop("disabled", false).removeClass("disabled");
                        return self.resolve(html);

                    default:
                        domSelectorToInsertTemplateTo.html(html).find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input', '.tt-hint').prop("disabled", false).removeClass("disabled");
                        return self.resolve(html);

                }
            } else { //modals dont insert to DOM element, so just return html to be handled by the modal
                return self.resolve(html);
            }
        });
    },

    onTemplateInserted: function(html) { //Common global methods called upon handlebars template insertion
        return $.Deferred(function() {
            var self = this;
            //Fire generic UI actions common on dynamic template elements
            appMain.formatTemplateDates();
            //autosize($("[data-autosize='true']")); //Grow textareas. Max height set via CSS class
            appMain.setElementsAsFullHeight(); //Resize the app with any new template data
            self.resolve(html);
        });
    }
};

export default GetHandlebarsTemplate;

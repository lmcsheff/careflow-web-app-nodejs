/*Confirm users has registered with HPCA account - https://doccom.atlassian.net/browse/WEB-796*/

/*Components*/
import StaticKeys from 'Endpoints/careflowApp/Static';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function confirmHpcaRegistration(guid) {
    var url = StaticKeys.getStaticKey("ApiAuthRoute") + "account/hpcaconfirm";
    let token = $('#csrfToken').val();
    let parameters = {
        "HpcaConfirmationGuid": guid
    };

    return Dispatch.Call("POST", url, parameters, null, token, true);
}

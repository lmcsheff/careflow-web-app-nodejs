/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
/*APIs*/
import Authorisation from 'Endpoints/careflowApp/Authorisation';

export function checkTokenValidity() {

    return new Promise((resolve, reject) => {


        let isRefreshingToken = appMain.getIsRefreshingAccessToken();
        let unixTimestamp = moment.utc().valueOf();
        let retreivedOnTime = moment.utc(Vault.getItem("AccessTokenRetreivedOn")).valueOf(); //localDate as UTC unix
        let expiresIn = parseInt(Vault.getItem("ExpiresInKey")); //Set as seconds value
        let expiresInTime = moment(retreivedOnTime).add(expiresIn, "seconds"); //Get retrieved on + expires in as a unit of time
        let refreshRequiredTimestamp = moment(expiresInTime).subtract("120", 'seconds'); //2 min expiry check for a valid refresh
		let refreshAccessTokenFlag = moment(unixTimestamp).isAfter(refreshRequiredTimestamp); // bool - has the refreshThreshold time been and gone/passed BEFORE the time now
		let timeoutExceeded = moment(unixTimestamp).isAfter(expiresInTime); //Is NOW after the actual expiry time?

		if (timeoutExceeded && !appMain.isTpiIntegrationSession()) return appMain.userLogOut();

		if (!refreshAccessTokenFlag) {

            return resolve('token refresh not applicable');

        } else if (refreshAccessTokenFlag && !isRefreshingToken) {

            appMain.setIsRefreshingAccessToken(true);

            Authorisation.RefreshToken().then(function(response) {
                appMain.setIsRefreshingAccessToken(false);
                VueEventBus.$emit('CareflowApp.RefreshTokenHeartbeatRefreshed', true);
                return resolve();
            }).catch(function(error) {
                appMain.setIsRefreshingAccessToken(false);
                return reject(error)
            });


        } else if (refreshAccessTokenFlag && isRefreshingToken) { //Another request is updating the token - await for it to complete
            console.time("awaitIsRefreshingAccessToken");
            async function msg() {
                const response = await appMain.awaitIsRefreshingAccessToken().catch(error => reject(error));
                //Start timing now
                console.timeEnd("awaitIsRefreshingAccessToken")
                return resolve(response); //Waits for awaitIsRefreshingAccessToken() to resolve
            }
            msg(); //Init wait
        }
    });
}

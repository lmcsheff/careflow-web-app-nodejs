﻿/* Utils */
import PubSub from './PubSub';
import staticKeys from './Static';
import * as RefreshTokenHeartbeat from 'Endpoints/careflowApp/RefreshTokenHeartbeat';

//Browsers will first check to see if CORS is enabled on the server with an OPTION request before moving forward with the actual request.
//https://kapeli.com/cheat_sheets/Axios.docset/Contents/Resources/Documents/index

let Dispatch = Dispatch || {
    Call: function(verb, url, data, headers, token, isAuthLogin, responseType) {
        return new Promise((resolve, reject) => {
            let cancelToken = axios.CancelToken;
            let source = cancelToken.source();
            let requestConfig;
            let getGuid = function() {
                let crypto = window.crypto || window.msCrypto;
                return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
                )
            }
            let requestGuid = getGuid();

            VueEventBus.$on('Dispatch.CancelApiRequest', (obj) => {
                //cancel the request (the message parameter is optional)
                //cancelSubscription.remove();
                maxRetry = -1;
                if (requestConfig) return source.cancel(`API operation canceled by the application: ${requestConfig.url}`);
                return source.cancel(`API operation canceled by the application: no requestConfig defined`);
            });

            //const cancelSubscription = PubSub.subscribe('

            //axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
            let thisHeaders = headers || {};

            thisHeaders.Accept = (token) ? `application/json;version=${staticKeys.getStaticKey('ApiVersion')}` : `application/json, text/html`

            if (token && !isAuthLogin) { /*Integrations pass token in header, not as a param - e.g. medwayIntegration APIs*/
                thisHeaders.Authorization = `Bearer  ${token}`; //Not requred for Auth login, will abort Ajax request if provided at login on old IE
                thisHeaders['API-Key'] = 'A7A7A7A7-A7A7-A7A7-A7A7-A7A7A7A7A7A7'; //https://doccom.atlassian.net/browse/WEB-819
            }

            let requestObject = {};
            (requestObject.headers = JSON.parse(JSON.stringify(thisHeaders))),
            (requestObject.timeout = 30000), //milliseconds

            (requestObject.url = url);
            requestObject.cancelToken = source.token;
            if (responseType) requestObject.responseType = responseType;
            if (verb.toUpperCase() === 'GET') requestObject.params = data || null;

            let maxRetry = 3; //Retry attempts to define

            const requestInstance = axios.create(requestObject);

            requestInstance.interceptors.request.use((config) => {
                // Do something before request is sent
                requestConfig = config;
                if (isAuthLogin) return config;

                return RefreshTokenHeartbeat.checkTokenValidity().then(response => {
                    return config;
                }).catch(error => {
                    return reject(error);
                });
            }, (error) => {

                // Do something with request error
                console.error(`Request error - no response for ${url}`)
                return Promise.reject(error); //The request was made but no response was received
            });

            requestInstance.interceptors.response.use(response => {
                    // Do something with request response

                    return response;
                },
                (error) => {

                    //if (isAuthLogin) return appMain.userLogOut(); //Auth error (refresh / login) - kill session

                    //if (error.request) alert('error.request')
                    // The request was made but no response was received. `error.request` is an instance of XMLHttpRequest in the browser and an instance of  http.ClientRequest in node.js
                    //if (error.request) return Promise(error);
                    --maxRetry; //reduce

                    if (axios.isCancel(error)) {
                        //Programatically cancelled, don't bounce to handlers for API errors'
                        console.error('Request canceled:', error.message);
                        return Promise.reject(error);
                    }

                    if (Math.sign(maxRetry) < 0) {
                        //  requestInstance.interceptors.response.eject(timeoutInterceptor);
                        return Promise.reject(error);
                    }

                    if (error.code === 'ECONNABORTED' && !axios.isCancel(error)) {
                        //Retry only for timeouts


                        console.log(`attempt index: ${maxRetry} for ${url}`);
                        //console.log(error.config);
                        //  if (maxRetry === 1) requestInstance.interceptors.response.eject(timeoutInterceptor); //Last retry - remove interceptor
                        if (maxRetry > 0) {
                            $.toast().reset('all');
                            appMain.triggerAppNotification({
                                Text: 'Your request is taking longer than expected, please wait while we try again',
                                Icon: 'error'
                            });


                            if (verb.toUpperCase() === 'POST') return call = requestInstance.post(url, data);
                            if (verb.toUpperCase() === 'GET') return call = requestInstance();
                            if (verb.toUpperCase() === 'DELETE') return call = requestInstance.delete(url, data);
                            if (verb.toUpperCase() === 'PUT') return call = requestInstance.put(url, data);

                        } else {
                            console.log(error)
                            //Error: timeout of 6000ms exceeded
                            //at module.exports (main.bundle.e552b3217f669cfb02a3.js:32)
                            //at XMLHttpRequest.request.ontimeout (main.bundle.e552b3217f669cfb02a3.js:32)

                            $.toast().reset('all');
                            console.error(`API operation timed out: ${url}`)
                            appMain.triggerAppNotification({
                                Text: 'Your request has timed out, please try again',
                                Icon: 'error'
                            });
                            Raygun.send(error);
                            return Promise.reject(`Request timeout - timeout of ${requestObject.timeout}ms exceeded`);
                        }
                    }

                    if ((token && error.code !== 'ECONNABORTED') && (error.hasOwnProperty('response') && !/^3/.test(error.response.status))) {
                        /*Only for Careflow APIs and non 300 errors (not modified etc), integrations pass token in header, not as a param - e.g. Alerts / Allergies*/
                        PubSub.publish('Dispatch.Api.Error', {
                            Data: error || null,
                            Xhr: error.request.status || null,
                            TextStatus: error.message,
                            Code: error.code || null
                        });
                        console.error(`API error: ${url}`)
                    }

                    //requestInstance.interceptors.response.eject(timeoutInterceptor);

                    return Promise.reject(error); //https://github.com/axios/axios/issues/1226
                }
            );

            let call; //Add after inteceptors declared;
            if (verb.toUpperCase() === 'POST') call = requestInstance.post(url, data);
            if (verb.toUpperCase() === 'GET') call = requestInstance();
            if (verb.toUpperCase() === 'DELETE') call = requestInstance.delete(url, data);
            if (verb.toUpperCase() === 'PUT') call = requestInstance.put(url, data);

            call.then(response => {
                if (appMain.getAppTimeoutInstance()) appMain.getAppTimeoutInstance().restartTimeout(); //Trigger reset of app timeout on successful API call
                //cancelSubscription.remove();
                return resolve(response);
            }).catch(error => {
                //if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                //API error
                //if (error && error.hasOwnProperty("response")) console.error(error.response);
                // handle non manually cancelled error. This will also include timeouts
                if (error && error.hasOwnProperty("response")) Raygun.send(error);
                //cancelSubscription.remove();
                return reject(error);
                // } else if (error.request) {
                //     // The request was made but no response was received
                //     // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                //     // http.ClientRequest in node.js
                //     return reject(error);
                // }
            });
        });
    }
};

export default Dispatch;

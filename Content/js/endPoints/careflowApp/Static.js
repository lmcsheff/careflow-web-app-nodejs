﻿//App Enum helper

/*Environments
 * http://careflowwebapppat.azurewebsites.net - Test
 * http://careflowwebapp.azurewebsites.net/ - Demo
 */

var staticKeys = staticKeys || (function () {

    var _apiKeys = {//Enum for static app / api references and valid local storage items.
        AccessTokenKey: "access_token",
        //AccessTokenKeyTimeToLive: 3599000, //milliseconds
        //accessTokenKeyTimeToLive: 600, //seconds - production
        RefreshTokenKey: "refresh_token",
        ExpiresInKey: "expires_in", //Seconds (set from header)
        AccessTokenRetreivedOn: "accessTokenRetreivedOn",
        JsonContentType: "application/json; charset=utf-8",
        FormEncodedContentType: "application/x-www-form-urlencoded; charset=UTF-8",
        ApiRoute: "https://testapi.doccom.me/",//Overwritten by appConfig.json at app bootup
        ApiAuthRoute: "https://testauth.doccom.me/",//Overwritten by appConfig.json at app bootup
        //ApiRoute: "http://devmobileapi.careflowapp.com/",
        //ApiRoute: "https://api.doccom.me/",
        //ApiRoute: "https://demoappapi.careflowapp.com/",
        ProxyApiRoute: "../../api/",
        AuthRoute: "../../auth/",
        UnauthorizedResponseText: "Unauthorized",
        ApiVersion: "10",//Overwritten by appConfig.json at app bootup
        AppVersion: '0.1',//Overwritten by appConfig.json at app bootup
        AppEnvironment: "AppEnvironment",//Overwritten by appConfig.json at app bootup
        GetRequestingUserSummary: "GetRequestingUserSummary",
        GetMemberAreasAUserCanStartAConversationIn: "GetMemberAreasAUserCanStartAConversationIn",
        GetAllPopulationsForUser: "GetAllPopulationsForUser",
        LoggedInUserName: "LoggedInUserName",
        LoggedInUserAvatar: "LoggedInUserAvatar",
        LoggedInUserExternalId: "LoggedInUserExternalId",
        ActiveNetworkId: "ActiveNetworkId",
        ActiveNetworkMemberAreaId: "ActiveNetworkMemberAreaId",
        ActiveNetworkName: "ActiveNetworkName",
        ActiveGroupMemberAreaId: "ActiveGroupMemberAreaId",
        ActiveGroupName: "ActiveGroupName",
        ActiveNetworkGroupMemberships: "ActiveNetworkGroupMemberships", //Teams within the users active networks
        NoActiveUserNetworks:"NoActiveUserNetworks",
        OnDutyStatus: "OnDuty",
        AccessTokenRetrievedOn: "AccessTokenRetrievedOn",
        AccessTokenExpiresIn: "AccessTokenExpiresIn",
        IdleTimerLastActivity: "IdleTimerLastActivity",
        ActiveTabCounter: "ActiveTabCounter",
        ActiveUserSessionSet :"ActiveUserSessionSet",
        SessionRequest: "SessionRequest",
        SessionData: "SessionData",
        OpenedBrowserTabCounter: "OpenedBrowserTabCounter",
        ApplicationUserID: "ApplicationUserID",
        LoggedInUserFirstName: "LoggedInUserFirstName",
        LoggedInUserLastName: "LoggedInUserLastName",
        LoggedInUserEmail: "LoggedInUserEmail",
        ItSupportPermissions: "ItSupportPermissions",
        ItSupportNetworks: "ItSupportNetworks",
        ItSupportActiveNetworkTeams: "ItSupportActiveNetworkTeams",
        ItSupportActiveNetworkId: "ItSupportActiveNetworkId",
        NetworkTeamsAllowingTasks: "NetworkTeamsAllowingTasks",
        UnreadCountsForSingleNetwork: "UnreadCountsForSingleNetwork", //Badge counts
		UnreadCountsForAllNetworks: "UnreadCountsForAllNetworks", //Badge counts
		UserPermissionsForAllUsersMemberAreas: "UserPermissionsForAllUsersMemberAreas",
		IsAppIntegrationSession : "IsAppIntegrationSession" //TPI / Medway flags

    };

    var getStaticKey = function (key) {
      if (_.has(_apiKeys, key)) {
            return _apiKeys[key]; //[] Accepts all string as keys (not just valid identifiers)
        } else {
            return false;
        }

    };

    var setStaticKey = function(key, value) {
        if (_.has(_apiKeys, key)) {
           return _apiKeys[key] = value; //[] Accepts all string as keys (not just valid identifiers)
        } else {
            return false;
        }

    };

    return {
        getStaticKey: getStaticKey,
        setStaticKey: setStaticKey
    }


}());
//apiRoute: "https://testapi.careflowapp.com/",
//apiRoute: "https://devapi.careflowapp.com/",
//apiRoute: "https://api.doccom.me/",

export default staticKeys;

﻿export default {
    NetworkPreferenceView: true,
    NetworkInbox: true,
    PatientSearch: true,
    TeamDirectory: true,
    MemberDirectory: true,
    Alerts: true,
    Tasks: {
        MyTasks: true,
    },
    Messages: true,
    Dashboard: false,
    TeamView: true,
    Teams: {
        TeamUpdates: true,
        PatientLists: true,
        PatientHandovers: true,
        TeamTasks: true,
        ReferralsSent: true,
        ReferralsReceived: true,
        Files: true,
        ClinicalTags: true,
        TeamMembers: true

    },
    PatientView: {
        PatientFeed: true,
        PatientTasks: true,
        PatientHandovers: true,
        PatientTags: true,
        PatientBanner: true,
        GetPatientActions: {
            LaunchAction: true, //Event to launch GetPatientActions ddl
            ReferPatient: true,
            PostUpdate: true,
            HandoverPatient: true,
            PatientLists: true,
            PatientTags: true,
            PatientTasks: {
                ViewTasks: true,
                RaiseTasks: true
            }

        } 

    },
    UserAdmin: {
        ToggleDutyStatus: true,
        SignOut:true
    }

  
};

﻿/* Utils */
import staticKeys   from './Static';
import Dispatch     from './Dispatch';
import Vault        from './Vault';

var AuthenticationSignOut = AuthenticationSignOut || {
    callApi: function() {
        var verb = "POST";
        var methodUrl = staticKeys.getStaticKey("AuthRoute") + "Logout";
        var isRetry = null;
        var token = $('#csrfToken').val();
        var refreshToken = Vault.getSessionItem(staticKeys.getStaticKey("RefreshTokenKey"));
        var headers = {
            refresh_token: refreshToken,
            RequestVerificationToken: token
        };
        var parameters = null;

        return Dispatch.Call(verb, methodUrl, parameters, headers, token, true);
    },

    responseHandler: function(data, textStatus, jqXHR) {
    }
};

export default AuthenticationSignOut;
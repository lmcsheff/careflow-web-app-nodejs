/* Utils */
import StaticKeys from 'Endpoints/careflowApp/Static';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function registerNewUserAccount(
    firstName,
    lastName,
    emailAddress,
    password
) {
    let verb = 'POST';
    let methodUrl = `${StaticKeys.getStaticKey("ApiAuthRoute")}account/register`;
    let token = $('#csrfToken').val();

    let parameters = {
        "FirstName": firstName,
        "LastName": lastName,
        "Email": emailAddress,
        "Password": password
    };
    return Dispatch.Call(verb, methodUrl, parameters, null, token, true);
}

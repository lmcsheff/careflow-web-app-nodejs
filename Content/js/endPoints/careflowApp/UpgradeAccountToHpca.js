﻿/*https://doccom.atlassian.net/browse/DOC-3087*/

/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var UpgradeToHpca = UpgradeToHpca || {
    callApi: function (string) {
        var verb = "POST";
        var methodUrl = "Account/UpgradeToHpca";
        var isRetry = false;
        var parameters = {
            "Password": string
        };

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        UpgradeToHpca.data = data;
        UpgradeToHpca.textStatus = textStatus;
        UpgradeToHpca.jqXHR = jqXHR;
    }
};

export default UpgradeToHpca;

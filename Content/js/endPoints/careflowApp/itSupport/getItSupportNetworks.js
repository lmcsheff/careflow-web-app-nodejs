﻿/* Utils */

import CareflowApi from '../CareflowApi';

var GetItSupportNetworks = GetItSupportNetworks || {

    callApi: function (skip, take) {

        var verb = "GET";
        var methodUrl = "ItSupport/Networks?skip=" + skip + "&take=" + take + "";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetMemberAreasUserCanStartAConversationIn.data = data;
        GetMemberAreasUserCanStartAConversationIn.textStatus = textStatus;

    }

};

export default GetItSupportNetworks;

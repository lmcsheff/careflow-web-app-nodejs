/* Utils */
/*This endpoint is used to pre-populate the user’s email address on the registration page of the Clinical Workspace.*/

import StaticKeys from 'Endpoints/careflowApp/Static';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function getUserData(guid) {
    let verb = 'GET';
    let methodUrl = `${StaticKeys.getStaticKey("ApiAuthRoute")}account/emailAddress/${guid}`;
    let token = $('#csrfToken').val();
    let parameters = null;

    return Dispatch.Call(verb, methodUrl, parameters, null, token, true);
}

﻿var PubSub = (function () {
    var topic = {};
    var singleTimeSubscriptions = {};

    return {
        subscribe: function (name, listener) {
            if (_.isUndefined(topic[name])) {
                topic[name] = { queue: [] };
                topic[name]["type"] = "subscribeAll";
            }
            var index = topic[name].queue.push(listener) - 1;

            return { //Public
                remove: function () {
                    //delete topic[name].queue[index];
                    topic[name].queue.splice(index, 1);
                }
            };
        },

        subscribeOnce: function (name, listener) {
            if (_.isUndefined(singleTimeSubscriptions[name])) {
                singleTimeSubscriptions[name] = { queue: [] };
                singleTimeSubscriptions[name]["type"] = "subscribeOnce";
            }
            var index = singleTimeSubscriptions[name].queue.push(listener) - 1;

            return { //Public
                remove: function () {
                    //delete singleTimeSubscriptions[name].queue[index];
                    singleTimeSubscriptions[name].queue.splice(index, 1);
                }
            };
        },

        publish: function (name, data) {

            //Check both subscription types
            if (!_.isUndefined(singleTimeSubscriptions[name]) && !singleTimeSubscriptions[name].queue.length == 0) {
                //console.log(singleTimeSubscriptions);
                singleTimeSubscriptions[name].queue.forEach(function (callback) {
                    callback(data || null);
                    delete singleTimeSubscriptions[name];//Remove if subscribeOnce subscription - subscribeOnce()
                });
            }

            if (_.isUndefined(topic[name]) || topic[name].queue.length == 0) {
                return;
            }

            topic[name].queue.forEach(function (callback) {
                callback(data || null);
            });
        },

        checkSubscription: function (name) {
            if (_.has(topic, name)) {
                return true;
            } else {
                return false;
            }
        }
    };
})();

// Example usage
//var subscription = event.subscribe('test', function (string) { alert(string); });
//event.publish('test', 'hi!');
//event.publish('test');
//subscription.remove();

export default PubSub;
﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetHomeFeedItem = GetHomeFeedItem || {

    callApi: function (conversationTrackableItemId) {
        var verb = "GET";
        var methodUrl = "HomeFeed/GetHomeFeedItem?conversationTrackableItemId=" + conversationTrackableItemId + "";
        var parameters = null;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetHomeFeedItem.data = data;
        GetHomeFeedItem.textStatus = textStatus;

    }

};

export default GetHomeFeedItem;
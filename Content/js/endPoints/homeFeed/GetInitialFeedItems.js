﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetInitialFeedItems = GetInitialFeedItems || {

    callApi: function (memberAreaId) {

        var verb = "GET";
        var methodUrl = "users/GetInitialFeedItems";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetInitialFeedItems.data = data;
        GetInitialFeedItems.textStatus = textStatus;
        //console.log(JSON.stringify(data));
    }

};

export default GetInitialFeedItems;

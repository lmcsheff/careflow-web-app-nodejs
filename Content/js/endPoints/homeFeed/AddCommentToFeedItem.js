﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var AddCommentToFeedItem = AddCommentToFeedItem || {

    callApi: function (conversationTrackableId, commentText) {

        var verb = "POST";
        var methodUrl = "/homefeed/AddCommentToFeedItem";
        var parameters = { ConversationTrackableID: conversationTrackableId, CommentText: commentText };
        var isRetry = null;
        var data = null;
        
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler : function (data, textStatus, jqXHR) {        
        AddCommentToFeedItem.data = data;
        AddCommentToFeedItem.textStatus = textStatus;
        //return AddCommentToFeedItem.data;
    }
           
};

export default AddCommentToFeedItem;

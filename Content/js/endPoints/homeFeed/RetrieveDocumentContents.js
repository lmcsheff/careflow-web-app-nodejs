﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var RetrieveDocumentContents = RetrieveDocumentContents || {

    callApi: function (contentItemId) {

        var verb = "GET";
        var methodUrl = "homefeed/RetrieveDocumentContentsHtml4?conversationTrackableItemID=" + contentItemId + "&useBase64=true";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry, null, "arraybuffer");
    },

    responseHandler: function (data, textStatus, jqXHR) {
        RetrieveDocumentContents.data = data;
        RetrieveDocumentContents.textStatus = textStatus;

    }

};

export default RetrieveDocumentContents;

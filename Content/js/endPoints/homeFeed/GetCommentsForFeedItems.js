﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetCommentsForFeedItems = GetCommentsForFeedItems || {
    
    callApi: function (contentItem, skip, take) {
       var verb = "GET";
       var methodUrl = "homefeed/GetCommentsForFeedItems/" + contentItem + "?skip=" + skip + "&take=" + take + "";
       var parameters = null;
       var isRetry = null;
       var data = null;
       return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetCommentsForFeedItems.data = data;        
    }
           
};

export default GetCommentsForFeedItems;

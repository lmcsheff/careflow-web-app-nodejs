﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var MarkConversationAsRead = MarkConversationAsRead || {

    callApi: function (itemId) {
        var verb = "POST";
        var methodUrl = "homefeed/MarkConversationAsRead";
        var parameters = {
            ConversationTrackableItemId: itemId,
            StartItemTrackableItemId: itemId
        };
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        MarkConversationAsRead.data = data;
        MarkConversationAsRead.textStatus = textStatus;

    }

};

export default MarkConversationAsRead;

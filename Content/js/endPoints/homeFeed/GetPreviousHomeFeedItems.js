﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetPreviousHomeFeedItems = GetPreviousHomeFeedItems || {

    callApi: function (updatedBefore, memberAreaId, networkId) {
        var setMethodUrl;
        if (memberAreaId != null) {
            setMethodUrl = "homefeed/GetPreviousHomeFeedItems?updatedBefore=" + updatedBefore + "&memberAreaId=" + memberAreaId + ""; // group feed
        } else {
            setMethodUrl = "homefeed/GetPreviousHomeFeedItems?updatedBefore=" + updatedBefore + "&networkId="+networkId+""; // network feed (presently returns all networks, not filtered https://doccom.atlassian.net/browse/WEB-409)
        }

        var verb = "GET";
        var methodUrl = setMethodUrl;
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetPreviousHomeFeedItems.data = data;
    }

};

export default GetPreviousHomeFeedItems;

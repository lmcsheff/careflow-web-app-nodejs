﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetAllActionsTheUserIsPermittedToPerform = GetAllActionsTheUserIsPermittedToPerform || {

    callApi: function () {

        var verb = "GET";
        var methodUrl = "permissions/GetAllActionsTheUserIsPermittedToPerform";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetAllActionsTheUserIsPermittedToPerform.data = data;
        GetAllActionsTheUserIsPermittedToPerform.textStatus = textStatus;

    }

};

export default GetAllActionsTheUserIsPermittedToPerform;
﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var RemoveGroupClinicalTags = RemoveGroupClinicalTags || {
	callApi: function(clinicalTagId, memberAreaId) {
		var verb = 'DELETE';
		var methodUrl =
			'Groups/' + memberAreaId + '/ClinicalTags/' + clinicalTagId + '';
		var parameters = null;
		var isRetry = null;

		return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
	},

	responseHandler: function(data, textStatus, jqXHR) {
		RemoveGroupClinicalTags.data = data;
		RemoveGroupClinicalTags.textStatus = textStatus;
		RemoveGroupClinicalTags.jqXHR = jqXHR;
	}
};

export default RemoveGroupClinicalTags;

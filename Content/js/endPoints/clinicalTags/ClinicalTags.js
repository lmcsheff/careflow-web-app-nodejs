﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var ClinicalTags = ClinicalTags || { //Updates (add or remove) the clinical tags of a patient

    callApi: function (memberAreaId, patientId, parameters) {
        
        var verb = "POST";
        var methodUrl = "Groups/" + memberAreaId + "/Patients/" + patientId + "/ClinicalTags";
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters || null, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        ClinicalTags.data = data;
        ClinicalTags.textStatus = textStatus;
        ClinicalTags.jqXHR = jqXHR;
    }

};

export default ClinicalTags;

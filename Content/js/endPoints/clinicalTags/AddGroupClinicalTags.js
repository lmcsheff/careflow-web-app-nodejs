﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var AddGroupClinicalTags = AddGroupClinicalTags || {
	callApi: function(clinicalTagId, memberAreaId) {
		var verb = 'POST';
		var methodUrl =
			'Groups/' + memberAreaId + '/ClinicalTags/' + clinicalTagId + '';
		var parameters = null;
		var isRetry = null;

		return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
	},

	responseHandler: function(data, textStatus, jqXHR) {
		AddGroupClinicalTags.data = data;
		AddGroupClinicalTags.textStatus = textStatus;
		AddGroupClinicalTags.jqXHR = jqXHR;
	}
};

export default AddGroupClinicalTags;

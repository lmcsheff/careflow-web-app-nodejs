﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var ReasonsForRemovingTag = ReasonsForRemovingTag || {

    callApi: function (memberAreaId) {
        
        var verb = "GET";
        var methodUrl = "Groups/"+memberAreaId+"/ReasonsForRemovingTag";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        ReasonsForRemovingTag.data = data;
        ReasonsForRemovingTag.textStatus = textStatus;
    }

};

export default ReasonsForRemovingTag;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';
//Gets the clinical tags selected in a group.
let GroupClinicalTags = {

    callApi: function(memberAreaId) {
        let verb = "GET";
        let methodUrl = "Groups/" + memberAreaId + "/ClinicalTags";
        let parameters = null;
        let isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },
};

export default GroupClinicalTags;

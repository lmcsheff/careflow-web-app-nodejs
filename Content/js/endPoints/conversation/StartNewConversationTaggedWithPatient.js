﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var StartNewConversationTaggedWithPatient = StartNewConversationTaggedWithPatient || {
    callApi: function (memberAreaId, title, body, patientId) {
        var verb = "POST";
        var methodUrl = "conversation/StartNewConversationTaggedWithPatient";
        var parameters = {
            MemberAreaId: memberAreaId,
            Title: title,
            Body: body,
            PatientExternalIdentifier: patientId,
            StartedFromContentItemId: null
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        StartNewConversationTaggedWithPatient.data = data;
        StartNewConversationTaggedWithPatient.textStatus = textStatus;
        StartNewConversationTaggedWithPatient.jqXHR = jqXHR;
    }
};

export default StartNewConversationTaggedWithPatient;

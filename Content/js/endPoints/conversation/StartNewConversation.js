﻿/* Utils */
import CareflowApi from 'Endpoints/careflowApp/CareflowApi';

var StartNewConversation = StartNewConversation || {
    callApi: function(memberAreaId, title, body) {
        var verb = "POST";
        var methodUrl = "conversation/StartNewConversation";
        var parameters = {
            "MemberAreaId": memberAreaId,
            "Title": title,
            "Body": body,
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    }
};

export default StartNewConversation;

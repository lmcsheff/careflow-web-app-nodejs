﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetMemberAreasUserCanStartAConversationIn = GetMemberAreasUserCanStartAConversationIn || {

    callApi: function () {

        var verb = "GET";
        var methodUrl = "conversation/GetMemberAreasUserCanStartAConversationIn";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetMemberAreasUserCanStartAConversationIn.data = data;
        GetMemberAreasUserCanStartAConversationIn.textStatus = textStatus;

    }

};

export default GetMemberAreasUserCanStartAConversationIn;
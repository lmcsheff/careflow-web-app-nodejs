﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetVideoSASUri = GetVideoSASUri || {

    callApi: function (contentItemId) {
        var verb = "GET";
        var methodUrl = "File/GetVideoSASUri/" + contentItemId + "";
        var parameters = null;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetVideoSASUri.data = data;
    }

};

export default GetVideoSASUri;
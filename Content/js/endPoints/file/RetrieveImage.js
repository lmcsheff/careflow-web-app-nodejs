﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';
/*Get a feed item image */
var RetrieveImage = RetrieveImage || {

    callApi: function(contentItemId, width, height, useBase64, resizeType) {
        var verb = "GET";
        resizeType = (resizeType) ? resizeType : "Crop"; // Crop / MaxSize
        useBase64 = (useBase64) ? useBase64 : true;
        var methodUrl = (width && height) ? "file/RetrieveImage/" + contentItemId + "?useBase64=" + useBase64 + "&width=" + width + "&height=" + height + "&resizeType=" + resizeType + "" : "file/RetrieveImage/" + contentItemId + "?userBase64=true";
        var parameters = null;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    }
};

export default RetrieveImage;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var StartConversation = StartConversation || {

    callApi: function (messageText, recipientApplicationUserIds) {

        var verb = "POST";
        var methodUrl = "messages/StartConversation";
        var parameters = {
            "MessageText": messageText,
            "RecipientApplicationUserIds": recipientApplicationUserIds
            };
        var isRetry = null;
               
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler : function (data, textStatus, jqXHR) {        
        StartConversation.data = data;
        StartConversation.textStatus = textStatus;
        StartConversation.jqXHR = jqXHR;
        
    }
           
};

export default StartConversation;
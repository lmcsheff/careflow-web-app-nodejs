﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetConversationSummaryList = GetConversationSummaryList || {

    callApi: function (skip, take) {

        var verb = "GET";
        var methodUrl = 'messages/GetConversationSummaryList/' + skip + '/' + take;
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetConversationSummaryList.data = data;
        GetConversationSummaryList.textStatus = textStatus;

    }

};

export default GetConversationSummaryList;
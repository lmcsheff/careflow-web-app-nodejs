﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var MarkMessageAsRead = MarkMessageAsRead || {

    callApi: function (conversationTrackableItemId, messageTrackableItemId) {

        var verb = "POST";
        var methodUrl = "messages/MarkMessageAsRead";
        var parameters = {
            "ConversationTrackableItemId": conversationTrackableItemId,
            "MessageTrackableItemId": messageTrackableItemId
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        MarkMessageAsRead.data = data;
        MarkMessageAsRead.textStatus = textStatus;
    }

};

export default MarkMessageAsRead;
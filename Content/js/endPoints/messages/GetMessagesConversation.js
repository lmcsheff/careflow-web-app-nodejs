﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetMessagesConversation = GetMessagesConversation || {

    callApi: function (conversationId) {
      
        var verb = "GET";
        var methodUrl = "messages/GetMessagesConversation?conversationId=" + conversationId + "&skip=0&take=50&includeParticipants=TRUE";
        var isRetry = null;
        var data = null;
        var result = null;
        var parameters = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function(data, textStatus, jqXHR) {
        GetMessagesConversation.data = data;
    }

};

export default GetMessagesConversation;
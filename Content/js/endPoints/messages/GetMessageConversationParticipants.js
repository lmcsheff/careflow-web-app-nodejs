﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetMessageConversationParticipants = GetMessageConversationParticipants || {

    callApi: function (conversationId, skip, take) {

        var verb = "GET";
        var methodUrl = "messages/GetMessageConversationParticipants?conversationId="+ conversationId +"&skip=" + skip + "&take=" + take;
        var parameters = null;
        var isRetry = null;
        var data = null;
        
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler : function (data, textStatus, jqXHR) {        
        GetMessageConversationParticipants.data = data;
        GetMessageConversationParticipants.textStatus = textStatus;        
    }
           
};

export default GetMessageConversationParticipants;
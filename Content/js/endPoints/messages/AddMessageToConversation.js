﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var AddMessageToConversation = AddMessageToConversation || {

    callApi: function (conversationTrackableItemId, messageText) {
        
        var verb = "POST";
        var methodUrl = "messages/AddMessageToConversation";
        var parameters = {
            "ConversationTrackableItemId": conversationTrackableItemId,
            "MessageText": messageText
        };
        var isRetry = null;
        var data = null;
        
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler : function (data, textStatus, jqXHR) {        
        AddMessageToConversation.data = data;
        AddMessageToConversation.textStatus = textStatus;
    }
           
};

export default AddMessageToConversation;
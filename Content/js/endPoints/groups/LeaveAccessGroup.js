﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var LeaveAccessGroup = LeaveAccessGroup || {
    callApi: function (accessGroupId) {
        var verb = "POST";
        var methodUrl = "Users/LeaveAccessGroup";
        var parameters = {
            "AccessGroupID": accessGroupId
        };
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        LeaveAccessGroup.data = data;
        LeaveAccessGroup.jqXHR = jqXHR;
        LeaveAccessGroup.textStatus = textStatus;
    }
};

export default LeaveAccessGroup;
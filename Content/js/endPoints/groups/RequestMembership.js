﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var RequestMembership = RequestMembership || {
    callApi: function (memberAreaId) {
        var verb = "POST";
        var methodUrl = "Groups/" + memberAreaId + "/RequestMembership";
        var parameters = null;
        var isRetry = null;
        var data = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        RequestMembership.data = data;
        RequestMembership.jqXHR = jqXHR;
        RequestMembership.textStatus = textStatus;
    }
};

export default RequestMembership;
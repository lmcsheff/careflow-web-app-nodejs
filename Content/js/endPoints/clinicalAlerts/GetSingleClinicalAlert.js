﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetSingleClinicalAlert = GetSingleClinicalAlert || {

    callApi: function (clinicalAlertExternalIdentifier) {
        var verb = "GET";
        var methodUrl = "ClinicalAlerts/GetSingleClinicalAlert?arg1=" + clinicalAlertExternalIdentifier + "";
        var parameters = null;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetSingleClinicalAlert.data = data;
        GetSingleClinicalAlert.textStatus = textStatus;
       
    }
           
};

export default GetSingleClinicalAlert;

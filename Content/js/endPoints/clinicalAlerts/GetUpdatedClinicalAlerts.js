﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetUpdatedClinicalAlerts = GetUpdatedClinicalAlerts || {
    callApi: function (updatedSince, updatedBefore, onlyUnreadAlerts, onlyAlertsWithUnreadComments, alertTopicToFilterBy, subscriptionIdToFilterBy, networkIdToFilterBy) {
        var verb = "GET";
        var methodUrl = "ClinicalAlerts/GetUpdatedClinicalAlerts?updatedBefore=" + updatedBefore + "&onlyUnreadAlerts=" + onlyUnreadAlerts + "&onlyAlertsWithUnreadComments=" + onlyAlertsWithUnreadComments + "&subscriptionIdToFilterBy=" + subscriptionIdToFilterBy + "&alertTopicToFilterBy=" + alertTopicToFilterBy + "&networkIdToFilterBy=" + networkIdToFilterBy+"";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function(data, textStatus, jqXHR) {
        GetUpdatedClinicalAlerts.data = data;
        GetUpdatedClinicalAlerts.textStatus = textStatus;
    }
};

export default GetUpdatedClinicalAlerts;
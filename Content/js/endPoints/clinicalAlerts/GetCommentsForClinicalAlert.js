﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetCommentsForClinicalAlert = GetCommentsForClinicalAlert || {
    
    callApi: function (clinicalAlertExternalIdentifier, skip, take) {
       var verb = "GET";
       var methodUrl = "clinicalalerts/GetCommentsForClinicalAlert/" + clinicalAlertExternalIdentifier + "?skip=" + skip + "&take=" + take + "";
       var parameters = null;
       var isRetry = null;
       var data = null;
       return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetCommentsForClinicalAlert.data = data;
    }
           
};

export default GetCommentsForClinicalAlert;

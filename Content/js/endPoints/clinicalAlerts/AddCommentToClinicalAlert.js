﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

let AddCommentToClinicalAlert = AddCommentToClinicalAlert || {

    callApi: function (clinicalAlertExternalIdentifier, commentText) {
        let verb = "POST";
        let methodUrl = "clinicalalerts/AddCommentToClinicalAlert";
        let parameters = {
            "ClinicalAlertExternalIdentifier": clinicalAlertExternalIdentifier,
            "CommentText": commentText
        };
        let isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        AddCommentToClinicalAlert.data = data;
    }

};

export default AddCommentToClinicalAlert;
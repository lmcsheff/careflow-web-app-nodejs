﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var MarkClinicalAlertAsRead = MarkClinicalAlertAsRead || {

    callApi: function (clinicalAlertExternalIdentifier) {
        var verb = "POST";
        var methodUrl = "clinicalalerts/MarkClinicalAlertAsRead";
        var parameters = {
            ClinicalAlertExternalIdentifier: clinicalAlertExternalIdentifier
        
        };
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        MarkClinicalAlertAsRead.data = data;
    }

};

export default MarkClinicalAlertAsRead;
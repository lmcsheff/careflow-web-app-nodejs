﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var MarkSingleFeedAsViewed = MarkSingleFeedAsViewed || {
    callApi: function (feedType, accessGroupId) {
        /*
            feedType values:
                1 - Wall
                2 - MyMessages
                3 - ClinicalAlerts
        */

        var verb = "POST";
        var methodUrl = "users/MarkSingleFeedAsViewed";
        var parameters = {
            "FeedType": feedType,
            "AccessGroupId": accessGroupId
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        MarkSingleFeedAsViewed.data = data;
        MarkSingleFeedAsViewed.textStatus = textStatus;
        MarkSingleFeedAsViewed.jqXHR = jqXHR;

    }
};

export default MarkSingleFeedAsViewed;
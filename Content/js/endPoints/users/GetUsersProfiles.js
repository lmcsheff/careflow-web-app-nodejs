﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetUsersProfiles = GetUsersProfiles || {

    callApi: function (accessGroupId,applicationUserIds) {

        if (!accessGroupId && !applicationUserIds) return Promise.reject("No params for GetUsersProfiles");

        var verb = "POST";
        var methodUrl = "users/GetUsersProfiles";
        var isRetry = null;

        var parameters = (!applicationUserIds) ? {
            "AccessGroupID": accessGroupId
        } :
         {
             "AccessGroupID": accessGroupId,
             "ApplicationUserIDs": applicationUserIds
         };


        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler : function (data, textStatus, jqXHR) {
        GetUsersProfiles.data = data;
        GetUsersProfiles.textStatus = textStatus;
    }

};

export default GetUsersProfiles;

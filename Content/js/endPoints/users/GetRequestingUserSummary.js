﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetRequestingUserSummary = GetRequestingUserSummary || {
    callApi: function (includeInferredPermissions) {
        includeInferredPermissions = includeInferredPermissions || false;
        var verb = "GET";
        var methodUrl = "users/GetRequestingUserSummary?includeInferredPermissions=" + includeInferredPermissions + "";
        var parameters = null;
        var isRetry = null;
        var data = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetRequestingUserSummary.data = data;
        GetRequestingUserSummary.textStatus = textStatus;
        GetRequestingUserSummary.jqXHR = jqXHR;
    }
};

export default GetRequestingUserSummary;
﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetAllUpdatedItems = GetAllUpdatedItems || {
    callApi: function (memberAreaId) {

        var verb = "GET";
        var methodUrl = "users/GetAllUpdatedItems";
        var parameters = null;
        var isRetry = null;
        
        if (memberAreaId) {
            methodUrl += '?memberAreaId=' + memberAreaId;
        }

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetUserDirectory.data = data;
        GetUserDirectory.textStatus = textStatus;

    }

};

export default GetAllUpdatedItems;

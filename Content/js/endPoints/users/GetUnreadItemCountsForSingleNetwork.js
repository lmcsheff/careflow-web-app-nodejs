﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetUnreadItemCountsForSingleNetwork = GetUnreadItemCountsForSingleNetwork || {
    
    callApi: function (includeSubCounts, networkId, types) {

        var verb = "GET";
        var methodUrl = (types) ? "users/GetUnreadItemCountsForSingleNetwork?includeSubCounts=" + includeSubCounts + "&networkId=" + networkId + "&types=" + types + "" : "users/GetUnreadItemCountsForSingleNetwork?includeSubCounts=" + includeSubCounts + "&networkId=" + networkId + "";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetUnreadItemCountsForSingleNetwork.data = data;
        GetUnreadItemCountsForSingleNetwork.textStatus = textStatus;
        GetUnreadItemCountsForSingleNetwork.jqXHR = jqXHR;

    }

};

export default GetUnreadItemCountsForSingleNetwork;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var MarkFeedAsRead = MarkFeedAsRead || {
    callApi: function (feedType) {
        /*
            feedType values:
                1 - Wall
                2 - MyMessages
                3 - ClinicalAlerts
        */

        var verb = "POST";
        var methodUrl = "users/MarkFeedAsRead";
        var parameters = {
            "FeedType": feedType
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        MarkFeedAsRead.data = data;
        MarkFeedAsRead.textStatus = textStatus;
        MarkFeedAsRead.jqXHR = jqXHR;

    }
};

export default MarkFeedAsRead;
﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetUnreadItemCountsForAllNetworks = GetUnreadItemCountsForAllNetworks || {

    callApi: function (includeSubCounts, types) {
        let methodUrl = (types) ? "Users/GetUnreadItemCountsForAllNetworks?includeSubCounts=" + includeSubCounts + "&types=" + types + "" : "Users/GetUnreadItemCountsForAllNetworks?includeSubCounts=" + includeSubCounts + "";

        var verb = "GET";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetUnreadItemCountsForAllNetworks.data = data;
        GetUnreadItemCountsForAllNetworks.textStatus = textStatus;
        GetUnreadItemCountsForAllNetworks.jqXHR = jqXHR;
        
    }

};

export default GetUnreadItemCountsForAllNetworks;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetUserDirectory = GetUserDirectory || {

    callApi: function () {

        var verb = "GET";
        var methodUrl = "users/GetUserDirectory";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetUserDirectory.data = data;
        GetUserDirectory.textStatus = textStatus;

    }

};

export default GetUserDirectory;

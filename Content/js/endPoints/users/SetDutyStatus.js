﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var SetDutyStatus = SetDutyStatus || {

    callApi: function (onDuty) {

        var verb = "POST";
        var methodUrl = "users/SetDutyStatus";
        var parameters = {
            "OnDuty": onDuty
            };
        var isRetry = null;
               
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler : function (data, textStatus, jqXHR) {        
        SetDutyStatus.data = data;
        SetDutyStatus.textStatus = textStatus;
        SetDutyStatus.jqXHR = jqXHR;
        
    }
           
};

export default SetDutyStatus;

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetUsersByName = GetUsersByName || {

    callApi: function (name, skip, take) {

        var verb = "GET";
        var methodUrl = "users/GetUsersByName?name="+ name +"&skip=0&take=10";
        var isRetry = null;
        var parameters = null;
               
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler : function (data, textStatus, jqXHR) {        
        GetUsersByName.data = data;
        GetUsersByName.textStatus = textStatus;       
    }
           
};

export default GetUsersByName;

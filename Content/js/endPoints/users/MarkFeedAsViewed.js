/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var MarkFeedAsViewed = MarkFeedAsViewed || {
    callApi: function (feedType, accessGroupId) {
        /*
            feedType values:
                1 - Wall
                2 - MyMessages
                3 - ClinicalAlerts
        */

        var verb = "POST";
        var methodUrl = "users/MarkFeedAsViewed";
        var parameters = {
            "FeedType": feedType,
            "AccessGroupId": accessGroupId
        };
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        MarkFeedAsViewed.data = data;
        MarkFeedAsViewed.textStatus = textStatus;
        MarkFeedAsViewed.jqXHR = jqXHR;

    }
};

export default MarkFeedAsViewed;
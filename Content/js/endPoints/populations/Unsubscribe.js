﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var Unsubscribe = Unsubscribe || {

    callApi: function (obj) {
        var verb = "POST";
        var methodUrl = "populations/Unsubscribe";
        var parameters = obj;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry); //returns data, textStatus, jqXHR
    },

    responseHandler: function (data, textStatus, jqXHR) {
        Unsubscribe.data = data;
        Unsubscribe.textStatus = textStatus;

    }

};

export default Unsubscribe;
﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var Subscribe = Subscribe || {

    callApi: function (obj) {
        var verb = "POST";
        var methodUrl = "populations/Subscribe";
        var parameters = obj;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry); //returns data, textStatus, jqXHR
    },

    responseHandler: function (data, textStatus, jqXHR) {
        Subscribe.data = data;
        Subscribe.textStatus = textStatus;

    }

};

export default Subscribe;
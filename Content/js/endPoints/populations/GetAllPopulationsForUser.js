﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetAllPopulationsForUser = GetAllPopulationsForUser || {

    callApi: function(networkId) {
        let verb = "GET";
        let methodUrl = (networkId) ? `populations/GetAllPopulationsForUser?includePatientSearchPopulations=true&networkIdToFilterBy=${networkId}` : `populations/GetAllPopulationsForUser?includePatientSearchPopulations=true`;
        let parameters = null;
        let isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function(data, textStatus, jqXHR) {
        GetAllPopulationsForUser.data = data;
        GetAllPopulationsForUser.textStatus = textStatus;

    }

};

export default GetAllPopulationsForUser;

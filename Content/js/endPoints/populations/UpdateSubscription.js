﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var UpdateSubscription = UpdateSubscription || {

    callApi: function (obj) {
        var verb = "POST";
        var methodUrl = "populations/UpdateSubscription";
        var parameters = obj;
        var isRetry = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry); //returns data, textStatus, jqXHR
    },

    responseHandler: function (data, textStatus, jqXHR) {
        UpdateSubscription.data = data;
        UpdateSubscription.textStatus = textStatus;

    }

};

export default UpdateSubscription;
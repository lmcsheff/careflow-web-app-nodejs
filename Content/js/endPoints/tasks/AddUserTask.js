﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

/*
    Operation to create a user task
*/
var AddUserTask = AddUserTask || {

    callApi: function (team, member, notes, patient, typeCode, typeDescription, urgent) {
        var verb = "POST";
        var methodUrl = "/Tasks";
        var parameters = {
            AllocatedToAccessGroupID: team,
            AssignedToActorID: member,
            Notes: notes,
            PatientExternalIdentifier: patient,
            Type: {
                Code: typeCode,
                Description: typeDescription
            },
            Urgent: urgent
        };
        var isRetry = null;
        var data = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        AddUserTask.data = data;
        AddUserTask.textStatus = textStatus;
    }

};

export default AddUserTask;
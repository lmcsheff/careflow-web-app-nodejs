﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

/*
    Operation to retrieve a list of defined user tasks
*/
var GetUserTasks = GetUserTasks || {

    callApi: function (params) {
       
        var verb = "POST";
        var methodUrl = "/Tasks/List";
        var parameters = params;
        var isRetry = null;
        var data = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler: function (data, textStatus, jqXHR) {       
        GetUserTasks.data = data;
        GetUserTasks.textStatus = textStatus;       
    }
           
};

export default GetUserTasks;
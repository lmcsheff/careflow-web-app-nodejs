﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

/*
    Operation to retrieve available tasks and statuses for a network
*/
var TasksMetaData = TasksMetaData || {

    callApi: function (networkId) {
       
        var verb = "GET";
        var methodUrl = "/Networks/"+ networkId +"/TasksMetaData";
        var parameters = null;
        var isRetry = null;
        var data = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler: function (data, textStatus, jqXHR) {       
        TasksMetaData.data = data;
        TasksMetaData.textStatus = textStatus;
    }
           
};

export default TasksMetaData;
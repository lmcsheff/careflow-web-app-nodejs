﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

/*
    Operation to retrieve a single user tasks
*/
var GetUserTask = GetUserTask || {

    callApi: function (userTaskId, tokeniseHistory) {
       
        var verb = "GET";
        var methodUrl = "Tasks/" + userTaskId;// + "?TokeniseHistory=" + tokeniseHistory + "";
        var parameters = null;
        var isRetry = null;
        var data = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler: function (data, textStatus, jqXHR) {       
        GetUserTask.data = data;
        GetUserTask.textStatus = textStatus;
        GetUserTask.jqXHR = jqXHR;
    }
           
};

export default GetUserTask;
﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

/*
    Operation to create a user tasks
*/
var UserTasks = UserTasks || {

    callApi: function (createUserTask) {
       
        var verb = "POST";
        var methodUrl = "/Tasks";
        var parameters = createUserTask;
        var isRetry = null;
        var data = null;
        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);        
    },

    responseHandler: function (data, textStatus, jqXHR) {       
        UserTasks.data = data;
        UserTasks.textStatus = textStatus;
        UserTasks.jqXHR = jqXHR;
    }
           
};

export default UserTasks;
﻿/*Operation to update a user task*/

/* Utils */
import CareflowApi from 'Endpoints/careflowApp/CareflowApi';
/* API Endpoints */
//import GetUsersProfiles from 'Endpoints/users/GetUsersProfiles';

export function updateTask(contentItemId, accepted, allocatedToAccessGroupId, assignedToActorId, comment, statusId, urgent, removalReasonId) {

    var verb = 'POST';
    var methodUrl = `Tasks/${contentItemId}`
    var parameters = {
        Accepted: accepted,
        AllocatedToAccessGroupId: allocatedToAccessGroupId,
        AssignedToActorId: assignedToActorId,
        Comment: comment,
        ModifiedByThirdPartyUserDescription: null,
        ModifiedByThirdPartyUserId: null,
        StatusId: statusId,
        Urgent: urgent
    };
    if (statusId === 2) { //Removing
        parameters.RemovalReasonId = removalReasonId;
    }
    return CareflowApi.MakeRequest(verb, methodUrl, parameters, null);

}
// var UpdateUserTask = UpdateUserTask || {

//     callApi: function (userTask, comment, removalReasonId) {
//         function makeRequest(response) {

//             var assignedActorId = ((response && response.Data.Passed.length) && response.Data.Passed[0].ActorId) || null;

//             var verb = 'POST';
//             var methodUrl = `Tasks/${userTask.ContentItemId}`
//             var parameters = {
//                 Accepted: accepted,
//                 AllocatedToAccessGroupId: allocatedToAccessGroupId,
//                 AssignedToActorId: assignedToActorId,
//                 Comment: comment,
//                 ModifiedByThirdPartyUserDescription: null,
//                 ModifiedByThirdPartyUserId: null,
//                 StatusId: satusId,
//                 Urgent: urgent
//             };

//             if (satusId === 2) {
//                 parameters.RemovalReasonId = removalReasonId;
//             }

//             var isRetry = null;
//             return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
//         }

//         //TODO: find a better way to retrieve the actorID of assigned user
//         if (userTask.AssignedToUser) {
//             return GetUsersProfiles
//                 .callApi(userTask.AllocatedToGroup.AccessGroupID, [userTask.AssignedToUser.ApplicationUserID])
//                 .then(makeRequest);
//         }

//         return makeRequest();
//     },

//     responseHandler: function (data, textStatus, jqXHR) {
//         UpdateUserTask.data = data;
//         UpdateUserTask.textStatus = textStatus;
//         UpdateUserTask.jqXHR = jqXHR;
//     }

// };

// export default UpdateUserTask;

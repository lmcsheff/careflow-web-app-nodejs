//Sets the smartlist settings to be configured for the group patient list for the specified group

/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

export function callApi(obj) {

	if (!obj) return console.error("No smart list obj passed");

    var verb = "POST";
    var methodUrl = "GroupLists/SaveGroupListDefinition";
    var parameters = obj;
    var isRetry = null;

    return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
}

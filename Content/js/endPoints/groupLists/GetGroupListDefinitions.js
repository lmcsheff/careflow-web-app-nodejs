//Gets the smartlist settings to be configured for the group patient list for the specified group

/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

export function callApi(memberAreaId) {

	if (!memberAreaId) return console.error("No smart list memberAreaId passed");

    var verb = "GET";
    var methodUrl = "GroupLists/GetGroupListDefinition?memberareaid="+ memberAreaId +"";
    var parameters = null;
    var isRetry = null;

    return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
}

//Gets a patient by patient id
/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';
import appMain from '../../../app/app-main';
//https://dev-wkagpas-01:4430/PatientServiceAPI/api-docs#!/PatientService/PatientService_GetPatientV2
//Demo
//https://coret1.syhapp.com/vlive/ROWTA/DEMO/PatientServiceAPI/v1/Patients?localIdentifier
//QATESTWK2
//https://coret1.syhapp.com/vlive/ROWTA/QATESTWK2/PatientServiceAPI/v1/Patients?localIdentifier

export function getPatientMedwayGuid(careflowPatientId, careflowPatient, patientHospitalNumber) {
    //Required ID and patient Obj
    return new Promise((resolve, reject) => {
        if (!careflowPatientId || !careflowPatient || !patientHospitalNumber) return reject('No Patient params');

        //Check cache
        const cachedMedwayPatient = appMain.getLastCachedValidPatientMedwayLookup(careflowPatientId);
        if (cachedMedwayPatient) return resolve(cachedMedwayPatient)

        //Is there an API request in progress for requested patient ID? If so, lets not bombard the webservice, instead wait for it to resolve
        const activePatientRequestInProgress = appMain.checkOpenPatientMedwayApiRequest(careflowPatientId);

        if (activePatientRequestInProgress) {

            //Open requests for this patient...

            appMain.awaitOpenPatientMedwayApiRequest(careflowPatientId).then(response => {
                //Check cache
                const cachedMedwayPatient = appMain.getLastCachedValidPatientMedwayLookup(careflowPatientId);
                if (cachedMedwayPatient) return resolve(cachedMedwayPatient)
                return reject();
            }).catch(error => {
                reject(error)
            })
        } else {

            //No open requests for this patient...

            //Tell the app we are calling medway to resolve this patient
            appMain.setOpenPatientMedwayApiRequest(careflowPatientId);


            let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
            if (!hpcaToken) return reject('No valid token');

            let url = appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl') + `PatientServiceAPI/v1/Patients?localIdentifier=${patientHospitalNumber}`;

            let headers = {
                Authorization: 'Bearer ' + hpcaToken.Token,
                'Content-Type': 'application/json;charset=utf-8'
            };


            Dispatch.Call('GET', url, null, headers, null, false, null)
                .then(response => {

                    if (!response.data.results.length) {
                        //Non matched
                        appMain.clearOpenPatientMedwayApiRequest(careflowPatientId, true);
                        return reject();
                    }

                    //Check patient data is a match
                    let patient = response.data.results[0];

                    //Criteria
                    const careflowPatientPrimaryIdentifier = patientHospitalNumber;
                    const careflowPatientDob = moment(
                        careflowPatient.PatientDateOfBirth.replace(/-/g, ' ')
                    ).format('DD-MM-YYYY');
                    const careflowPatientFullName =
                        careflowPatient.PatientGivenName +
                        careflowPatient.PatientFamilyName;

                    const medwayPatientPrimaryIdentifier = patient.hospitalNumber;
                    const medwayPatientDob = moment(patient.doB).format(
                        'DD-MM-YYYY'
                    );
                    const medwayPatientFullName =
                        patient.forename + patient.surname;

                    let identifiersMatch =
                        (careflowPatientPrimaryIdentifier ===
                            medwayPatientPrimaryIdentifier);
                    let nameMatch =
                        (careflowPatientFullName === medwayPatientFullName);
                    let dobMatch = (careflowPatientDob === medwayPatientDob);

                    //Add to cache with careflow ID
                    if (identifiersMatch && nameMatch && dobMatch) appMain.setLastCachedValidPatientMedwayLookup(patient, careflowPatientId);
                    if (identifiersMatch && nameMatch && dobMatch) appMain.clearOpenPatientMedwayApiRequest(careflowPatientId, false);

                    if (identifiersMatch && nameMatch && dobMatch) return resolve(patient);

                    //Non matched
                    appMain.clearOpenPatientMedwayApiRequest(careflowPatientId, true);
                    return reject();
                })
                .catch(error => {
                    reject(error);
                    appMain.clearOpenPatientMedwayApiRequest(careflowPatientId, true);
                });


        }


    });
}

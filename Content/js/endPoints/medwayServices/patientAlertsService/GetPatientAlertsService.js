﻿/*Medway*/
/*https://qa-wkmapp-01.corp.mgt:4431/PatientAlertsService/api-docs#!/PatientAlertsService/PatientAlertsService_GetPatientAllergiesByPatientId*/
//https://coret1.syhapp.com/vlive/ROWTA/QATESTWK2/PatientAlertsService/v1/Patients/H00000395/Alerts
//2E285865-4288-DF11-B3E0-00155D02B403 - BillyGoat

//Demo
//https://coret1.syhapp.com/vlive/ROWTA/DEMO/PatientAlertsService/v1/Patients/
//QATESTWK2
//https://coret1.syhapp.com/vlive/ROWTA/QATESTWK2/PatientAlertsService/v1/Patients


/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';



export function callApi(serviceType, medwayPatientId, pageSize = 100, pageIndex = 0) {

    let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    let url = appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl') + `PatientAlertsService/v1/Patients/${medwayPatientId}/${serviceType}?pageSize=${pageSize}&pageIndex=${pageIndex}`;
    let headers = {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Make API request

}

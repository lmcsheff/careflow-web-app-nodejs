/*Medway*/
/*GET episodes by SpellId*/
/*https://medwayliveqawk5.corp.mgt:4431//Inpatient/api-docs#!/InpatientService/InpatientService_GetEpisodesBySpellId*/

/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function callApi(spellId) {
    let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    let url = `${appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl')}Inpatient/v1/Spells/${spellId}/Episodes`;
    let headers = {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Make API request
}

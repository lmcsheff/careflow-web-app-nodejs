/*Medway*/
/*Update a patient epsidode*/
/*https://medwayliveqawk5.corp.mgt:4431//Inpatient/api-docs#!/InpatientService/InpatientService_CreateEpisode*/

/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function callApi(episodeId, specialtyId, hpcId) {
    let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    let url = `${appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl')}Inpatient/v1/Episodes/${episodeId}`;
    let headers = {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    let parameters = {
        SpecialtyId: specialtyId,
        ConsultantId: hpcId
    }

    return Dispatch.Call('PUT', url, parameters, headers, null, false, null); //Make API request
}

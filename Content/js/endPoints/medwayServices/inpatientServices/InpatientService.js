/*https://medwayliveqawk5.corp.mgt:4431//Inpatient/api-docs#!/InpatientService*/


/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

/*Globals*/

function getAuthToken() {
    const hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    return {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
}


export function getDischargePlans(spellId) {
    let url = `${appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl')}Inpatient/v4/DischargePlans?spellId=${spellId}`;
    return Dispatch.Call('GET', url, null, getAuthToken(), null, false, null); //Make API request
}

export function getPatientSpells(patientId) {
    let url = `${appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl')}Inpatient/v4/Patients/${patientId}/Spells`;
    return Dispatch.Call('GET', url, null, getAuthToken(), null, false, null); //Make API request
}
//Inpatient/v1/Patients/${patientId}/Spells`;

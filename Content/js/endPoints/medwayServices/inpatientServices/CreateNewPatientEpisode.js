/*Medway*/
/*Create a new patient epsidode*/
/*https://medwayliveqawk5.corp.mgt:4431//Inpatient/api-docs#!/InpatientService/InpatientService_CreateEpisode*/

/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function callApi(spellId, hpcId, adminCategory, specialtyId, startDate) {
    let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    let url = `${appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl')}Inpatient/v1/Episode`;
    let headers = {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    let parameters = {
        HcpId: hpcId, //Hcp Id to transfer Episode to
        SpecialtyId: specialtyId, //Specialty Id to transfer Episode to
        SpellId: spellId,
        startDate: startDate,
        AdminCategory: adminCategory
    }

    return Dispatch.Call('POST', url, parameters, headers, null, false, null); //Make API request
}

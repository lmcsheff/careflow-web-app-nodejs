/*Medway*/
/*https://coret1.syhapp.com/vlive/rowta/qatestwk5/PatientEpisodeService/api-docs#!/PatientEpisodeService/PatientEpisodeService_GetEpisodeTypes*/

/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function callApi() {
    let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    let url = appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl') + `PatientEpisodeService/v1/EpisodeTypes`;
    let headers = {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Make API request
}

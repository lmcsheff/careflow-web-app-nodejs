/*Medway*/
/*https://coret1.syhapp.com/vlive/rowta/qatestwk2/PatientEpisodeService/api-docs#!/PatientEpisodeService/PatientEpisodeService_GetEpisode*/

/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function callApi(medwayPatientId, pageSize = 100, pageIndex = 0) {
    let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    let url = appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl') + `PatientEpisodeService/v1/Patients/${medwayPatientId}?pageSize=${pageSize}&pageIndex=${pageIndex}`;
    let headers = {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Make API request
}

/*https://confluence.systemc.com/display/VDS/COBS+Clinical+observations+microservice*/
/*http://dev2.vitalpac.com/SwaggerUI/dist/index.html?url=http://dev2.vitalpac.com/CObsMicroservice/api-docs/v1/swagger.json#/FhirObservations/FhirObservations_GetObservationsLastn*/
/*https://confluence.systemc.com/display/TLC/Data+Types*/

/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';


export function getVitals(patientHospitalNumber) {
    if (!patientHospitalNumber || !appMain.checkIfFeatureIsEnabledInAppConfig('enablePatientObsAndVitals')) return Promise.reject();

    const hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    const url = appMain.checkIfFeatureIsEnabledInAppConfig('vitalsApiBaseUrl') + `CobsMicroservice/v1/observation$lastn?patient=${patientHospitalNumber}&category=vital-signs`;
    //let url = `https://pit-vpac-01.corp.mgt/CobsMicroservice/v1/observation$lastn?patient=H777780012&category=vital-signs`;
    //let url = `https://pit-vpac-01.corp.mgt/CobsMicroservice/v1/observation$lastn?patient=H777780012&category=vital-signs&max=3&start=2017-01-20T16:00:00Z&end=2018-10-22T23:59:59Z`;
    //Get the most recent 3 vital signs observations of each type for patient CT001 which were taken between 20th Jan 2017 at 4pm UTC and end of Jan 2017

    let headers = {
        'Authorization': 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Direct call

}

export function getEws(patientHospitalNumber) {
    if (!patientHospitalNumber || !appMain.checkIfFeatureIsEnabledInAppConfig('enablePatientObsAndVitals')) return Promise.reject();
    const hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    const url = appMain.checkIfFeatureIsEnabledInAppConfig('vitalsApiBaseUrl') + `CobsMicroservice/v1/observation$lastn?patient=${patientHospitalNumber}&code=445551004&max=1`;

    let headers = {
        'Authorization': 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Direct call

}


export function getBodyHeightAndWeight(patientHospitalNumber) {
    if (!patientHospitalNumber || !appMain.checkIfFeatureIsEnabledInAppConfig('enablePatientObsAndVitals')) return Promise.reject();
    const hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    const url = appMain.checkIfFeatureIsEnabledInAppConfig('vitalsApiBaseUrl') + `CobsMicroservice/v1/observation$lastn?patient=${patientHospitalNumber}&category=body-height-weight&max=1`;

    let headers = {
        'Authorization': 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Direct call

}

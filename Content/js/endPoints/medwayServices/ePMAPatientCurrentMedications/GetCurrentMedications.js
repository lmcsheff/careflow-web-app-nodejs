/*https://52.166.19.180/CurrentMedications/api-docs#!/CurrentMedications/CurrentMedications_GetCurrentMedsLine*/

/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function callApi(medwayGuid, episodeId) {

    if (!medwayGuid || !episodeId) return Promise.reject();

    let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    let url = appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl') + `ePMAPatientCurrentMedications/v1/patient/${medwayGuid}/InpatientMedications?episodeid=${episodeId}`;
    let headers = {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Direct call

}

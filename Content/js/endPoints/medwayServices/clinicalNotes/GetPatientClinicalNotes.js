/*https://coret1.syhapp.com/vlive/rowta/qatestwk5/ClinicalNotes/api-docs#*/

/*Components*/
import Vault from 'Endpoints/careflowApp/Vault';
import Dispatch from 'Endpoints/careflowApp/Dispatch';

export function callApi(medwayGuid, docTypes = 'ClinicalNote,DischargeSummary,Proforma,SDI', page = 1, pageSize = 100, startDate, endDate) {

    if (!medwayGuid) return Promise.reject();
    let hpcaToken = JSON.parse(Vault.getSessionItem('access_token'));
    const baseUrl = appMain.checkIfFeatureIsEnabledInAppConfig('medwayApiBaseUrl');
    let url = (startDate) ?
        `${baseUrl}ClinicalNotes/v1/Patient/${medwayGuid}/ClinicalNotes?docTypes=${docTypes}&page=${page}&pageSize=${pageSize}
		&startDate=${startDate}` : `${baseUrl}ClinicalNotes/v1/Patient/${medwayGuid}/ClinicalNotes?docTypes=${docTypes}&page=${page}&pageSize=${pageSize}`;
    let headers = {
        Authorization: 'Bearer ' + hpcaToken.Token,
        'Content-Type': 'application/json;charset=utf-8'
    };
    return Dispatch.Call('GET', url, null, headers, null, false, null); //Direct call

}

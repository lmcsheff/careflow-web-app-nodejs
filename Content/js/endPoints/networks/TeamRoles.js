/*Allows a user with the ViewProfilesForNetwork permission to view the Team Handover Roles available for their network, and the teams with which roles they have selected.*/

import CareflowApi from 'Endpoints/careflowApp/CareflowApi';

export function callApi(networkExternalIdentifier, skip, take) {

    let verb = "GET";
    let methodUrl = '/Networks/' + networkExternalIdentifier + '/Profiles?skip=' + skip + '&take=' + take + '&include=teams';
    let parameters = null;
    let isRetry = null;

    return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
}

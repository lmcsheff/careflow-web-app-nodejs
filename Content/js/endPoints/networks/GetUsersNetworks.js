﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetUsersNetworks = GetUsersNetworks || {

    //Get all the groups in a network.

    callApi: function (networkId, membershipPolicy, membershipStatus) {

        var url;
        var skip = 0;
        var take = 9999;

        if (membershipPolicy && !membershipStatus) {
            url = "Networks/" + networkId + "/Groups?Skip=" + skip + "&Take=" + take + "&MembershipPolicy=" + membershipPolicy;
        }
        else if (!membershipPolicy && membershipStatus) {
            url = "Networks/" + networkId + "/Groups?Skip=" + skip + "&Take=" + take + "&MembershipStatus=" + membershipStatus + "";
        }
        else if (!membershipPolicy && !membershipStatus) {
            url = "Networks/" + networkId + "/Groups?Skip=" + skip + "&Take=" + take;
        } else {
            url = "Networks/" + networkId + "/Groups?Skip=" + skip + "&Take=" + take + "&MembershipPolicy=" + membershipPolicy + "&MembershipStatus=" + membershipStatus + "";
        }
        
        var verb = "GET";
        var methodUrl = url;
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetUsersNetworks.data = data;
        GetUsersNetworks.textStatus = textStatus;

    }

};

export default GetUsersNetworks;

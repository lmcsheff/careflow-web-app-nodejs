﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var GetNetworkInbox = GetNetworkInbox || {
    callApi: function (networkId) {
        var verb = "GET";
        var methodUrl = "Networks/" + networkId + "/Inbox?skip=0&take=50";
        var parameters = null;
        var isRetry = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHR) {
        GetNetworkInbox.data = data;
        GetNetworkInbox.jqXHR = jqXHR;
        GetNetworkInbox.textStatus = textStatus;
    }
};

export default GetNetworkInbox;

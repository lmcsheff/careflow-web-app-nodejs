/*Allows a Network administrator to update the display colour associated with a Profile. - Requires UpdateNetworkProfile permission*/

import CareflowApi from 'Endpoints/careflowApp/CareflowApi';

export function callApi(networkExternalIdentifier, profileIdentifier, colour) {

    let verb = "POST";
    let methodUrl = '/Networks/' + networkExternalIdentifier + '/Profiles/' + profileIdentifier + '/Update';
    let parameters = {
        "Colour": colour
    };
    let isRetry = null;

    return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
}

﻿/* Utils */
import CareflowApi from '../careflowApp/CareflowApi';

var SetAsPreferredNetwork = SetAsPreferredNetwork || {

    callApi: function (networkId) {

        var verb = "POST";
        var methodUrl = "Networks/"+networkId+"/SetAsPreferred";
        var parameters = null;
        var isRetry = null;
        var data = null;

        return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
    },

    responseHandler: function (data, textStatus, jqXHr) {
        SetAsPreferredNetwork.data = data;
        SetAsPreferredNetwork.textStatus = textStatus;
    }

};

export default SetAsPreferredNetwork;

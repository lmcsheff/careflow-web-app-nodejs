/*Allows a user with the ViewReports permission to view the requested Report.*/

import CareflowApi from 'Endpoints/careflowApp/CareflowApi';

export function getReport(networkId, reportId) {

    let verb = "GET";
    let methodUrl = `/Networks/${networkId}/Reports/${reportId}`;
    let parameters = null;
    let isRetry = null;

    return CareflowApi.MakeRequest(verb, methodUrl, parameters, isRetry);
}

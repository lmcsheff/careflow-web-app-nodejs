﻿/*This module initialises the Vue instances for Team Referrals and Patient Summary Referrals (network and team context) views*/

/* Utils */
import Vault from 'Endpoints/careflowApp/Vault';

/*APIs*/

/*Components*/
import TeamReferralList from 'VueComponents/referrals/TeamPatientReferrals';
import TeamReferralsPatient from 'VueComponents/referrals/TeamPatientReferralActivePanel';

/*Module globals*/
let vm; //Vue instance

function destroy() {
    if (vm) return vm.$destroy();
}

export function init(networkId, memberAreaId, patientId, contentIdToActivate) {
    //Referrals Root view

    vm = new Vue({
        el: '#vue-team-referrals',
        data: function() {
            return {
                MemberAreaId: memberAreaId || null,
                NetworkId: networkId || Vault.getSessionItem('ActiveNetworkId'),
                PatientId: patientId || null,
                ContentIdToActivate: contentIdToActivate || null
            };
        },

        components: { TeamReferralList, TeamReferralsPatient },

        filters: {},

        computed: {},

        watch: {},

        methods: {},

        events: {},

        beforeCreate: function() {},

        created: function() {
            VueEventBus.$on("Router.NewRouteSet", destroy)
        },

        beforeMount: function() {},

        mounted: function() {},

        beforeUpdate: function() {},

        updated: function() {},

        beforeDestroy: function() {
            VueEventBus.$off("Router.NewRouteSet", destroy);
        },

        destroyed: function() {}
    });
}

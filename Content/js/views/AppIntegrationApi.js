//Third party integrtion API module

/*APIs*/
import Vault from 'Endpoints/careflowApp/Vault';
import * as ValidateHandoverToken from 'Endpoints/careflowApp/AuthenticateHandoverToken'
/*Components*/
import IntegrationSplashPage from 'VueComponents/integrations/IntegrationSplashPage';
import appRouter from 'AppJs/app-router';
import appMain from 'AppJs/app-main';

//Globals
let tpiNetworkId;
let vm;

export function init(obj) { //Can accept an obj from router (url param?) or null. Init can be invoked also via method
    vm = new Vue({
        data: function() {
            return {
                InitObj: obj || null
            };
        },
        components: { IntegrationSplashPage },
        methods: {},
        created() {
            Vault.flush();
        },
        beforeDestroy() {},
        mounted() {}
    });
    //Mount
    return vm.$mount('#vue-integration-splash'); //From hbars htm wrapper markup
}

export function getAccessToken(...params) {
	alert('getAccessToken called')

    const token = (params.length) ? params[0] : null;
    const networkId = (params.length > 0) ? parseInt(params[1]) : null;

    tpiNetworkId = networkId;

    //TODO: Only allow method to be called on #/Integration URL

    if (!token) return console.error("No token supplied");
	alert('token ' + token)	
    //Call the API's
    ValidateHandoverToken.authenticateHandoverToken(token)
        .then((response, status, xhr) => {
			alert('authenticateHandoverToken done')
			alert(JSON.stringify(response.data))
            Vault.flush();
            Vault.setItem('AccessTokenKey', response.headers['access_token']);
            Vault.setItem('RefreshTokenKey', response.headers['refresh_token']);
            Vault.setItem('ExpiresInKey', '3600'); //Secs
            Vault.setItem('AccessTokenRetreivedOn', moment().valueOf()); //timestamp in milliseconds
            //Add user email
            Vault.setItem('LoggedInUserEmail', 'SYS_USER'); //TODO - SETUP USER META DATA WITH INTEGRATIONS
            Vault.setItem('GetRequestingUserSummary', []); //For app API isLoggedIn validation - must be set to something

            //Set a flag to identify an Integration Session
            Vault.setItem('IsAppIntegrationSession', {
                SessinStarted: moment().valueOf(),
                SessionNetworkId: networkId
			});

			alert('access_token: ' + response.headers['access_token'])
			alert('refresh_token: ' + response.headers['refresh_token'])

        })
		.then(response => {
			alert('calling getRequestingUserSummary')
			return appMain.getRequestingUserSummary(networkId).then(response => {
				alert(JSON.stringify(Vault.getItem('GetRequestingUserSummary')))
				
                if (networkId) {
                    return appRouter.careflowAppRouter.setLocation(`/#/${networkId}/Home`);
                 } else {
                    return appRouter.careflowAppRouter.setLocation(`/#/1106/Home`);
                 }
            })
        })
		.catch(error => {
			alert('error')
			alert(JSON.stringify(error))
            rg4js('send', {
                error: 'ValidateHandoverToken failed for user',
                customData: [{ error: error }]
            });

            Vault.flush();
            window.location.href = "/";
        });
};

export function callCommand(commandName, obj) {

    if (!commandName || typeof window.external !== "object" || typeof window.external.Push !=="function" || !Vault.getItem('IsAppIntegrationSession')) return false; //https://stackoverflow.com/questions/10694341/what-is-the-use-of-window-external
    /* the second parameter is always a json encoded string. For example in this case you may put: window.external.Push("openEPMA",'{"patientId":"P9f097b7b-65fd-e511-afc9-005056bcebc4"}'); */
    //Call Medway
    return window.external.Push(commandName, JSON.stringify(obj)); //the second parameter being a json string containing the required parameters.
};

export function getTpiNetworkId() {
    if (tpiNetworkId) return tpiNetworkId;
    return null;
}

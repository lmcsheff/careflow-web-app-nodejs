﻿/* API Endpoints */
import GetUsersNetworks from '../endPoints/networks/GetUsersNetworks';

/* Utils */
import Vault from '../endPoints/careflowApp/Vault';

var itSupport = itSupport || (function() {
    var init = function() {
        _getVue();
    };

    var _getVue = function() {
        //Register locally

        var select2Component = {
            props: ['options', 'value'],
            template: '<select><slot></slot></select>',
            mounted: function() {
                var vm = this;
                $(this.$el)
                    .val(this.value)
                    // init select2
                    .select2({
                        data: this.options,
                        placeholder: 'Choose a team...',
                        containerCssClass: "controls-select2-wrap",
                        width: "98%"
                    })
                    // emit event on change.
                    .on('change', function() {
                        vm.$emit('input', parseInt(this.value));
                    });
            },
            watch: {
                value: function(value) {
                    // update value
                    $(this.$el).val(parseInt(value)).trigger('change');
                },
                options: function(options) {
                    // update options
                    $(this.$el).select2({ data: options });
                }
            },
            destroyed: function() {
                $(this.$el).off().select2('destroy');
            }
        };
        var loaderBlockHelper = {
            template: '<div style="padding:1em" class="u-ajax-loader u-clearfix"><i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw u-ajax-loader__icon"></i> <span style="display: inline-block;" class="u-ajax-loader__label">\{{computedMessage}}</span></div>',
            props: ["message"],
            data: function() {
                return {
                    Message: null
                };
            },
            computed: {
                computedMessage: function() {
                    return this.Message;
                }
            },
            created: function() {
                this.Message = this.message;
            }
        };
        var vm = new Vue({
            el: '#itSupportDirectory__view',
            data: function() {
                return {
                    MemberAreaIdToFilterBy: 0,
                    Search: '',
                    IsLoading: true,
                    IsLoadingTeams: false,
                    ItSupportUserNetworks: [],
                    ActiveNetworkId: null,
                    ActiveNetwork: {}
                };
            },
            components: {
                'select2': select2Component, //will only be available in parent's template
                'loading-block': loaderBlockHelper
            },

            computed: {
                fileredNetworks: function() {
                    var that = this;

                    var networks = _.chain(that.ItSupportUserNetworks)
                        .filter(function(network) {
                            return s.include(network.Name.toLowerCase(), that.Search.toLowerCase());
                        })
                        .sortBy(function(network) { return network.Name.toLowerCase(); })
                        .value();
                    return networks;
                }
            },

            watch: {
            
            },

            methods: {
                setActiveNetwork: function(network) {
                    var that = this;
                    that.ActiveNetwork = {};
                    that.ActiveNetworkId = null;
                    that.IsLoadingTeams = true;
                    return GetUsersNetworks.callApi(network.NetworkId).then(function(response, textStatus, jqXHr) {
                        that.ActiveNetwork = response.Data.Groups;
                        that.ActiveNetworkId = network.NetworkId;
                        that.IsLoadingTeams = false;
                        appMain.setElementsAsFullHeight();
                    }).fail(function(error) {
                        that.IsLoadingTeams = false;
                        appMain.setElementsAsFullHeight();
                    });
                },

                getItSupportNetworks: function() {
                    if (!Vault.getItem("ItSupportNetworks")) return console.error("no networks for it admin");
                    this.ItSupportUserNetworks = _.sortBy(Vault.getItem("ItSupportNetworks"), function(obj) { return obj.Name.toLowerCase(); }); //Asc order
                },
            },

            events: {},

            beforeCreate: function() {},

            created: function() {
                var that = this;
                this.getItSupportNetworks();
                appMain.setElementsAsFullHeight();
                this.IsLoading = false;
                //_getItSupportTeams().then(function (response) {
                //    that.ItSupportUserNetworksAndTeamData = response;
                //    appMain.setElementsAsFullHeight();
                //});
            },

            beforeMount: function() {},
            mounted: function() {},
            beforeUpdate: function() {},
            updated: function() {},
            beforeDestroy: function() {},
            destroyed: function() {}
        });
    };

    return {
        init: init
    };
}());

export default itSupport;
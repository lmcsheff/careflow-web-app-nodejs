﻿////EHI demo - nov 2017
////Get result details from Medway - Full View

//var patientResults = patientResults || (function () {
//    var _init = function (patientHospitalNumber, resultId) {
//        if (!patientHospitalNumber) return console.error("No ID  passed for results");
//        _getVueControl(patientHospitalNumber, resultId);
//    };

//    var _getVueControl = function (patientHospitalNumber, resultIdToTrigger) {
//        var loaderBlockHelper = {
//            template: '<div style="padding:1em" class="u-ajax-loader u-clearfix"><i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw u-ajax-loader__icon"></i> <span style="display: inline-block;" class="u-ajax-loader__label">\{{computedMessage}}</span></div>',
//            props: ["message"],
//            data: function () {
//                return {
//                    Message: null
//                };
//            },
//            computed: {
//                computedMessage: function () {
//                    return this.Message;
//                }
//            },
//            created: function () {
//                this.Message = this.message;
//            }
//        };
//        var vm = new Vue({
//            el: "#patient-results-view",
//            data: function () {
//                return {
//                    PatientHospitalNumber: null,
//                    ResultId: null,
//                    PatientResults: null,
//                    PatientResultsDataLoading: false,
//                    PatientResultComponents: null,
//                    NetworkID: null,
//                    Search: '',
//                    IsLoading: true
//                };
//            },
//            components: {
//                'loading-block': loaderBlockHelper
//            },

//            computed: {
//                patientResults: function () {
//                    if (!this.PatientResults) return false;
//                    return this.PatientResults;
//                }
//            },

//            watch: {
//                patientResults: function () {
//                    this.IsLoading = false;
//                }
//            },

//            filters: {
//                formatDate: function (date) {
//                    if (!date) return '';
//                    return appMain.formatDate(date);
//                }
//            },

//            methods: {
//                getAllPatientResults: function () {
//                    var hospitalNumber = this.PatientHospitalNumber;
//                    var that = this;

//                    return axios({
//                        method: 'get',
//                        url: "/medway?path=Results/v3/patient/demo/hospitalno/" + hospitalNumber + "/allresults",
//                        headers: {
//                            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlbW8tbWFwcC0wMS5kZW1vLm1ndC9ERU1PTElWRSIsImF1ZCI6Imh0dHA6Ly9tZWR3YXljbGllbnQuc3lzdGVtYy5jb20vYXBpLyIsIm5iZiI6MTUwODQxNTE2NywiZXhwIjoxNjM0NjQ2NDA3LCJodHRwOi8vaWRlbnRpdHlzZXJ2ZXIudGhpbmt0ZWN0dXJlLmNvbS9jbGFpbXMvY2xpZW50IjoiTWVkd2F5V2ViQXBpLURlbW9MSVZFIiwiaHR0cDovL2lkZW50aXR5c2VydmVyLnRoaW5rdGVjdHVyZS5jb20vY2xhaW1zL3Njb3BlIjoiaHR0cDovL21lZHdheWNsaWVudC5zeXN0ZW1jLmNvbS9hcGkvIiwibmFtZWlkIjoiREVNT1xcRGVtb0FkbWluIiwidW5pcXVlX25hbWUiOiJERU1PXFxEZW1vQWRtaW4iLCJhdXRobWV0aG9kIjoiT0F1dGgyIiwiYXV0aF90aW1lIjoiMjAxNy0xMC0xOVQxMjoxMjo0Ny42NThaIn0.mjfqLKJR8G3bF4BRjjXPniWNvg_IaaekQABXpXiNqzA',
//                            'Accept': 'application/json'
//                        }
//                    }).then(function (response) {
//                        that.PatientResults = response.data;
//                    }).catch(function (error) {
//                        that.PatientResults = [];
//                        console.log(error);
//                    });
//                },

//                getSingleResultComponents: function (resultId) {
//                    if (this.PatientResultsDataLoading) return;
//                    this.PatientResultsDataLoading = true;

//                    this.ResultId = resultId;
//                    var that = this;

//                    return axios({
//                        method: 'get',
//                        url: "/medway?path=Results/v3/result/" + resultId + "",
//                        headers: {
//                            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlbW8tbWFwcC0wMS5kZW1vLm1ndC9ERU1PTElWRSIsImF1ZCI6Imh0dHA6Ly9tZWR3YXljbGllbnQuc3lzdGVtYy5jb20vYXBpLyIsIm5iZiI6MTUwODQxNTE2NywiZXhwIjoxNjM0NjQ2NDA3LCJodHRwOi8vaWRlbnRpdHlzZXJ2ZXIudGhpbmt0ZWN0dXJlLmNvbS9jbGFpbXMvY2xpZW50IjoiTWVkd2F5V2ViQXBpLURlbW9MSVZFIiwiaHR0cDovL2lkZW50aXR5c2VydmVyLnRoaW5rdGVjdHVyZS5jb20vY2xhaW1zL3Njb3BlIjoiaHR0cDovL21lZHdheWNsaWVudC5zeXN0ZW1jLmNvbS9hcGkvIiwibmFtZWlkIjoiREVNT1xcRGVtb0FkbWluIiwidW5pcXVlX25hbWUiOiJERU1PXFxEZW1vQWRtaW4iLCJhdXRobWV0aG9kIjoiT0F1dGgyIiwiYXV0aF90aW1lIjoiMjAxNy0xMC0xOVQxMjoxMjo0Ny42NThaIn0.mjfqLKJR8G3bF4BRjjXPniWNvg_IaaekQABXpXiNqzA',
//                            'Accept': 'application/json'
//                        }
//                    }).then(function (response) {
//                        that.PatientResultsDataLoading = false;
//                        that.PatientResultComponents = response.data;
//                    }).catch(function (error) { that.PatientResultComponents = null; });
//                }
//            },

//            events: {},

//            beforeCreate: function () {
//            },

//            created: function() {
//                appMain.setModuleRootVueInstance(this);
//                if (!patientHospitalNumber) return console.error("No ID  passed for results");

//                this.PatientHospitalNumber = patientHospitalNumber;
//                if (resultIdToTrigger) {
//                    //Get all results
//                    this.getAllPatientResults(this.PatientHospitalNumber).then(this.getSingleResultComponents(resultIdToTrigger));
//                } else {
//                    //Get all results
//                    this.getAllPatientResults(this.PatientHospitalNumber);
//                }
//            },

//            beforeMount: function () { },

//            mounted: function () { },

//            beforeUpdate: function () { },

//            updated: function () {
//                appMain.setElementsAsFullHeight();
//            },

//            beforeDestroy: function () { },

//            destroyed: function () { }
//        });
//    };

//    return {
//        init: _init
//    }
//})();

//export default patientResults;
﻿/*This module initialises and controls the Vue instances for Alerts network view*/

/* Utils */
import Vault from 'Endpoints/careflowApp/Vault';


/*APIs*/

/*Components*/
import AlertsFeed from 'VueComponents/alerts/AlertsInbox';

/*Module globals*/
let vm; //Vue instance

function destroy() {
    if (vm) return vm.$destroy();
}

export function init(networkId) { //MyTasks Root view

    if (!networkId) return console.error("no network id for alerts feed");

    vm = new Vue({
        el: '#vue-alerts',
        data: function() {
            return {
                NetworkId: networkId || Vault.getSessionItem("ActiveNetworkId")
            };
        },

        components: { AlertsFeed },

        filters: {},

        computed: {},

        watch: {},

        methods: {},

        events: {},

        beforeCreate: function() {},

        created: function() {
            VueEventBus.$on("Router.NewRouteSet", destroy)
        },

        beforeMount: function() {},

        mounted: function() {},

        beforeUpdate: function() {},

        updated: function() {},

        beforeDestroy: function() {
            VueEventBus.$off("Router.NewRouteSet", destroy);
        },

        destroyed: function() {}
    });

};

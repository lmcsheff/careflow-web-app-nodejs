﻿import Vault from '../endPoints/careflowApp/Vault';
/* Others */
import appRouter from '../app/app-router';

var biReports = biReports || (function() {
    var init = function(reportId, networkId) {
        function powerBiIsEnabledInNetwork() { //prechecks already done prior to this at router, but another layer...
            return new Promise((resolve, reject) => {
                if (!appMain.getCachedAppConfig() || !appMain.isHpcaAuthTokenType(Vault.getSessionItem("access_token"))) return reject(false);

                var appConfigData = appMain.getCachedAppConfig();
                var environment = appConfigData.environment;

                if (!_.has(appConfigData.networkConfig, environment)) return reject(false);

                var item = _.filter(appConfigData.networkConfig[environment].networks, function(item) {
                    return item.networkId === Vault.getSessionItem("ActiveNetworkId");
                })[0];

                if (!item || !item.enableBiReports || !appMain.getUserPermissionsForNetwork(networkId).includes('InviteMemberFromCareflowDirectory')) return reject(false);
                return resolve(item);
            });
        };

        powerBiIsEnabledInNetwork().then(() => {
            _getVueControl(reportId);
        }).catch((error) => {
            appRouter.careflowAppRouter.setLocation('/#/Home');
        });
    };
    var _getVueControl = function(reportId) {
        var loaderBlockHelper = {
            template: '<div style="padding:1em" class="u-ajax-loader u-clearfix"><i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw u-ajax-loader__icon"></i> <span style="display: inline-block;" class="u-ajax-loader__label">\{{computedMessage}}</span></div>',
            props: ["message"],
            data: function() {
                return {
                    Message: null
                };
            },
            computed: {
                computedMessage: function() {
                    return this.Message;
                }
            },
            created: function() {
                this.Message = this.message;
            }
        };

        let env = appMain.getCurrentEnvironment().toLowerCase() || "";
        let reports; //required until API returns report IDs etc

        if (env === "demo") {

            reports = [{
                    Name: "My day",
                    Key: "75aa4e59-cbea-441c-b0a6-d29d5ab91e29"
                },
                {
                    Name: "Ward planning",
                    Key: "181f5118-f165-4e2f-9a6b-77bd81a082b7"
                },
                {
                    Name: "Community bed state",
                    Key: "786de580-c2b8-411a-a25d-e27130ef88ac"
                },
                {
                    Name: "Users",
                    Key: "87ee4f40-b624-4fd2-a37a-413f137017a6"
                },
                {
                    Name: "Alerts",
                    Key: "f394f281-21e5-4fe2-8a9c-780a59616f24"
                },
                {
                    Name: "Social Care",
                    Key: "8a0b6564-21fb-4b2c-bdaa-68d17f330fe2"
                },
                {
                    Name: "GP Current Activity",
                    Key: "44a505de-0d7e-4701-b4b7-331dac678f23"
                },
                {
                    Name: "GP Acute Summary",
                    Key: "d1e6a68f-65e4-4c2d-af99-18a21e39c208"
                },
                {
                    Name: "Frailty",
                    Key: "b14e8816-64fc-49f1-ace7-72f77d064dc9"
				},
				{
                    Name: "Patient Timeline",
                    Key: "d243291b-10f5-49ed-a5b0-ebe17a10db96"
                }
            ];

        } else { /*Live*/
            reports = [{
                    Name: "Clinical Alerts",
                    Key: "6112c35b-b2c6-4d61-bf55-b02c483abda0"
                },
                {
                    Name: "Referrals",
                    Key: "9b3c2e6e-2113-4c53-ac12-29cfa5428a95"
                },
                {
                    Name: "Tags",
                    Key: "331b1837-32fa-450a-9870-264cf28dcb45"
                },
                {
                    Name: "Team Messages",
                    Key: "849436ef-a286-4def-8ed3-e8ac63f30845"
                },
                {
                    Name: "Team Summary",
                    Key: "3ae3f9bf-f895-4f73-b9a4-ba5324a072a7"
                },
                {
                    Name: "User Messages",
                    Key: "ce399b40-cb74-496d-98e1-d4208966ad37"
                },
                {
                    Name: "Users",
                    Key: "223d406f-97f5-42c8-b03d-2b739b29c3ec"
                }
            ];
        }

        var vm = new Vue({
            el: '#biReports__view',
            data: function() {
                return {
                    ActiveReportTab: null, //default on load
                    ActiveReportName: null, //default on load
                    Environment: env,
                    IsLoading: true,
                    ReportIdPassedToView: reportId,
                    Reports: reports || []
                };
            },
            components: {
                'loading-block': loaderBlockHelper
            },

            computed: {},

            watch: {
                setActiveTab: function(reportId) {
                    this.getBiReport(reportId);
                    appMain.setElementsAsFullHeight();
                }
            },

            methods: {
                setActiveTab: function(reportId, reportName) {
                    appMain.stopApiRequests();
                    //this.IsLoading = true; TODO: Empty iframe first
                    this.ActiveReportTab = reportId;
                    this.ActiveReportName = reportName;
                    this.getBiReport(reportId);
                },
                isActiveTab: function(reportId, reportName) {
                    return this.ActiveReportTab === reportName;
                },
                getBiReport: function(reportId) {

                    var that = this;
                    var url = "/report/embed?reportId=" + reportId;

                    axios.get(url)
                        .then(function(response) {

                            // Read embed application token from Model
                            var accessToken = response.data.EmbedToken.Token;
                            // Read embed URL from Model
                            var embedUrl = response.data.EmbedUrl;

                            // Read report Id from Model
                            var embedReportId = response.data.Id;

                            // Get models. models contains enums that can be used.
                            // var models = window['powerbi-client'].models;
                            var models = window.powerBiclient.models;
                            // Embed configuration used to describe the what and how to embed.
                            // This object is used when calling powerbi.embed.
                            // This also includes settings and options such as filters.
                            // You can find more information at https://github.com/Microsoft/PowerBI-JavaScript/wiki/Embed-Configuration-Details.

                            var config = {
                                type: 'report',
                                tokenType: models.TokenType.Embed,
                                accessToken: accessToken,
                                embedUrl: embedUrl,
                                id: embedReportId,
                                permissions: models.Permissions.All,
                                settings: {
                                    filterPaneEnabled: true,
                                    navContentPaneEnabled: true
                                }
                            };

                            // Get a reference to the embedded report HTML element
                            var reportContainer = document.getElementsByClassName("js-bi-report-container")[0];

                            // Embed the report and display it within the div container.
                            var report = powerbi.embed(reportContainer, config);
                            that.IsLoading = false;
                            appMain.setElementsAsFullHeight();
                        })
                        .catch(function(error) {});
                }
            },

            events: {},

            beforeCreate: function() {},

            created: function() {
                let that = this;
                let env = appMain.getCurrentEnvironment().toLowerCase();
                //Trigger default, or load passed report
                if (reportId) {
                    var reportName = _.filter(that.Reports, function(report) {
                        if (report.Key === reportId) return report;
                        return false;
                    })[0].Name || "";
                    this.ReportIdPassedToView = reportId;
                    this.setActiveTab(this.ReportIdPassedToView, reportName);
                    appMain.setElementsAsFullHeight();
                } else {
                    this.setActiveTab(this.Reports[0].Key, this.Reports[0].Name); //Default to first
                    appMain.setElementsAsFullHeight();
                }
            },

            beforeMount: function() {},
            mounted: function() {},
            beforeUpdate: function() {},
            updated: function() {},
            beforeDestroy: function() {},
            destroyed: function() {}
        });
    };

    return {
        init: init
    };
})();

export default biReports;

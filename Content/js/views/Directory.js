﻿/* Utils */


/* API Endpoints */

/*Components*/
import NetworkDirectory from 'VueComponents/directory/TeamDirectory';
/*Module globals*/
let vm; //Vue instance

function destroy() {
    if (vm) return vm.$destroy();
}

export function initDirectory(obj) {
    if (!obj) return;
    vm = new Vue({
        data: function() {
            return {
                NetworkId: obj.NetworkId
            };
        },
        components: { NetworkDirectory },
        methods: {},
        created: function() {
            VueEventBus.$on("Router.NewRouteSet", destroy)
        },
        beforeDestroy: function() {
            VueEventBus.$off("Router.NewRouteSet", destroy);
        },
        mounted() {}
    });

    //Mount
    return vm.$mount('#vue-network-directory');
}

/*This module initialises amd controls the Vue instances for Manage NetworkProfiles*/

/* Utils */
/*APIs*/
/*Components*/
import ManageNetworkProfiles from 'VueComponents/networkProfiles/ManageNetworkProfiles';

/*Module globals*/
let vm; //Vue instance

function destroy() {
    if (vm) return vm.$destroy();
}

export function init(obj) {

    vm = new Vue({
        el: '#vue-network-profiles',
        data: function() {
            return {
                NetworkId: obj.NetworkId,
                ViewTitle: obj.ViewTitle
            };
        },
        components: { ManageNetworkProfiles },
        methods: {},
        created: function() {
            VueEventBus.$on("Router.NewRouteSet", destroy)
        },
        beforeDestroy() {
            VueEventBus.$off("Router.NewRouteSet", destroy);
        },
        mounted() {}
    });
}

/*Instance to manage all Team specific views*/
import Vault from 'Endpoints/careflowApp/Vault';
import appMain from 'AppJs/app-main';
import appRouter from 'AppJs/app-router';
/*Components*/
import FeedComponent from 'VueComponents/feeds/FeedComponent';
import TeamDirectoryWidget from 'VueComponents/directory/TeamDirectoryListWidget';
import PatientSummary from 'VueComponents/patientDashboard/PatientDashboardWrapper';
import PatientSidePanelList from 'VueComponents/patientList/PatientListSlim';
import TeamNavigation from 'VueComponents/navigation/TeamNavigation';
import TeamTasks from 'VueComponents/tasks/TasksList';
import TeamReferrals from 'VueComponents/referrals/TeamPatientReferrals';
import TeamPatientList from 'VueComponents/patientList/TeamPatientList';
import PatientListFilters from 'VueComponents/patientList/components/PatientListFilters';
import TeamDirectory from 'VueComponents/directory/TeamDirectory';
import ManageTeamTags from 'VueComponents/manageTeamClinicalTags/TeamTags';
import ManageTeamSmartLists from 'VueComponents/teamSmartLists/ManageTeamSmartLists'
import ManageTeamHandoverProfiles from 'VueComponents/handovers/ManageTeamHandoverProfiles';

/*Module globals*/
let vm;
export function getViewStatus() {
    if (vm && document.getElementById("vue-network-team")) return vm;
    return false
};

export function init(networkId, memberAreaId, patientId, viewTitle, route) {

    /*Mount*/
    vm = new Vue({
        el: '#vue-network-team',
        data: function() {
            return {
                MemberAreaId: memberAreaId,
                NetworkId: networkId,
                ViewTitle: viewTitle,
                ActiveView: route,
                ActivePatientId: patientId,
                Patient: null,
                PatientListIsCollapsed: false,
                IsFullPatientListView: (patientId) ? false : true, //Default to full list view if no patient to load passed in
                FiltersAreVisible: false,
                ActiveFilters: {
                    ClinicalTags: [],
                    Clinicians: [],
                    Locations: [],
                }, //default
            };
        },
        components: { FeedComponent, TeamNavigation, TeamDirectoryWidget, TeamDirectory, TeamTasks, TeamReferrals, ManageTeamHandoverProfiles, ManageTeamSmartLists, ManageTeamTags, TeamPatientList, PatientListFilters, PatientSummary, PatientSidePanelList },
        watch: {
            FiltersAreVisible(newVal, oldVal) {
                this.$nextTick(() => {
                    appMain.setElementsAsFullHeight();
                });
            },
            ActivePatientId(newVal, oldVal) {
                if (!newVal) this.Patient = null;
            }
        },
        methods: {

            setActiveView(networkId, memberAreaId, patientId, viewTitle, route) {
                this.NetworkId = networkId;
                this.MemberAreaId = memberAreaId;
                this.ActivePatientId = patientId;
                this.ViewTitle = viewTitle;
                this.ActiveView = route;
                this.IsFullPatientListView = (patientId) ? false : true;

            },
            setActivePatient(obj) {
                //if (!obj || obj.PatientId === this.ActivePatientId) return;  TODO: FIX BUG WHERE ACTIVE PATIENT RETURNS FALSE - NIGELS BUG
                /*Set via obj item on loaded patient list*/
                appMain.closeOpenedPatientContextWindow(); //Close any patient context windows  TODO: FIX BUG WHERE ACTIVE PATIENT RETURNS FALSE - NIGELS BUG
                this.ActivePatientId = obj.PatientId;
                this.Patient = obj.Patient;
                if (this.IsFullPatientListView) this.IsFullPatientListView = false;
                appRouter.careflowAppRouter.setLocation(`/#/${this.NetworkId}/Teams/${this.MemberAreaId}/Patients/${this.ActivePatientId}`)

            },

            getFilterVisibilityButtonText() {
                if (this.FiltersAreVisible) return `Hide filters`;
                if (!this.FiltersAreVisible && this.ActiveFilters && (this.ActiveFilters.ClinicalTags.length || this.ActiveFilters.Clinicians.length || this.ActiveFilters.Locations.length)) return `Show filters (filters applied)`;
                return `Show filters`;
            },
            toggleCollapsed(obj) {
                this.PatientListIsCollapsed = obj.IsCollapsed;
            },
            expandList() {
                this.PatientListIsCollapsed = false;
            },
            toggleIsFullPatientListView(val) {
                this.IsFullPatientListView = val;
            },
            destroy() {
                const route = appRouter.careflowAppRouter.getLocation().split('/');
                const networkId = (route.length > 1) ? route[2] : null;
                const memberAreaId = (route.length > 2) ? route[4] : null;

                if (!memberAreaId) return this.$destroy();//If its NOT a team page, destroy the instance and trigger cleanups
                return false;
            }

        },
        created() {
            VueEventBus.$on("Router.NewRouteSet", this.destroy)
        },
        beforeDestroy() {
            VueEventBus.$off("Router.NewRouteSet", this.destroy)
        },
        mounted() {}
    });

}

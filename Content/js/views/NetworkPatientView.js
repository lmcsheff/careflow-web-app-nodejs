/*This module initialises and controls the Vue instances for /#/:networkId/:patientId*/

/* Utils */
/*APIs*/
import GetPatient from 'Endpoints/patients/GetPatient';
/*Components*/
import PatientDashboard from 'VueComponents/patientDashboard/PatientDashboardWrapper';

/*Module globals*/
let vm; //Vue instance

function destroy() {
    if (vm) return vm.$destroy();
}

export function init(networkId, patientId) {

    if (!networkId || !patientId) return;

    vm = new Vue({
        el: '#vue-network-patient-view',
        data: function() {
            return {
                NetworkId: networkId,
                PatientId: patientId,
                Patient: null,
                ViewTitle: ''
            };
        },
        components: { PatientDashboard },
        methods: {},
		created: function () {
			VueEventBus.$on("Router.NewRouteSet", destroy)
		},
		beforeDestroy() {
			VueEventBus.$off("Router.NewRouteSet", destroy);
		},
        mounted() {
            //Get the patient
            return GetPatient.callApi(patientId, networkId, ['ClinicalTags', 'HandoverNotes'])
                .then((response, data, xhr, fullResponse) => {
                    this.Patient = response.Data;
                }).fail(error => {
                    //TODO - handle error
                });
        }
    });
}

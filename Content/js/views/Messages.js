﻿/* Utils */
import CareflowApp from '../careflow-app';
import Vault from '../endPoints/careflowApp/Vault';
import GetHandlebarsTemplate from '../endPoints/careflowApp/GetHandlebarsTemplate';
import PubSub from '../endPoints/careflowApp/PubSub';

/* API Endpoints */
import GetUserDirectory from '../endPoints/users/GetUserDirectory';
import GetConversationSummaryList from '../endPoints/messages/GetConversationSummaryList';
import GetMessageConversationParticipants from '../endPoints/messages/GetMessageConversationParticipants';
import GetMessagesConversation from '../endPoints/messages/GetMessagesConversation';
import AddMessageToConversation from '../endPoints/messages/AddMessageToConversation';
import MarkMessageAsRead from '../endPoints/messages/MarkMessageAsRead';
import StartConversation from '../endPoints/messages/StartConversation';
import GetUnreadItemCountsForAllNetworks from '../endPoints/users/GetUnreadItemCountsForAllNetworks';

var Messages = Messages || (function () {
    var _cacheTrackableItems = {},
        _cacheUsers = {};

    function _getAllTextNodes(root, result) {
        if (!result) result = [];

        [].forEach.call(root.childNodes, function (child) {
            if (child.nodeType === child.TEXT_NODE && child.textContent.trim() !== '') {
                result.push(child);
            }
            else {
                _getAllTextNodes(child, result);
            }
        });

        return result;
    }

    function _highlightMatches(element, searchValue) {
        var previousMatch = element.querySelector('.match__highlight');

        if (previousMatch) {
            var previousMatchParent = previousMatch.parentNode;

            previousMatchParent.replaceChild(document.createTextNode(previousMatch.textContent), previousMatch);
            previousMatchParent.normalize();
        }

        if (element.textContent.toLowerCase().indexOf(searchValue) === -1) {
            $(element).hide();
            return;
        }

        $(element).show();

        _getAllTextNodes(element).some(function (child) {
            var text = child.textContent,
                indexOfSearchValue = text.toLowerCase().indexOf(searchValue);

            if (indexOfSearchValue !== -1) {
                var childParent = child.parentNode,
                    leftText = document.createTextNode(text.slice(0, indexOfSearchValue)),
                    match = document.createElement('span'),
                    rightText = document.createTextNode(text.slice(indexOfSearchValue + searchValue.length));

                match.className = 'match__highlight';
                match.textContent = text.slice(indexOfSearchValue, indexOfSearchValue + searchValue.length);

                childParent.insertBefore(leftText, child);
                childParent.insertBefore(match, child);

                childParent.replaceChild(rightText, child);

                return true;
            }
        });
    }

    function _init() {
        _getConversationsSummary();

        $(window).on('resize', _updateConversationsParticipantsCounter);
    }

    function _loadUserDirectory() {
        return GetUserDirectory.callApi()
            .then(function (response) {
                var data = response.Data;

                data.NewOrUpdatedUsers.forEach(function (user) {
                    _cacheUsers[user.ApplicationUserID] = user;
                });

                return response;
            });
    }

    function _getCacheUserDirectory() {
        return Object.keys(_cacheUsers).length > 0 ? $.when({
            Data: {
                NewOrUpdatedUsers: Object.keys(_cacheUsers).map(function (applicationUserID) {
                    return _cacheUsers[applicationUserID];
                })
            }
        }) : _loadUserDirectory();
    }

    function _updateConversationsParticipantsCounter() {
        $('.js-calculate-more-participants').each(function () {
            var self = this,
                $parent = $(self).parent('.conversation-item-latest-message'),
                unseenNr = 0;

            if (self.scrollWidth > self.offsetWidth) {
                var visibleParticipants = $(self).find('span').filter(function () {
                    return self.offsetWidth > (this.offsetLeft + this.offsetWidth);
                }),
                    participantsNr = +$parent.attr('data-participants-count');

                unseenNr = participantsNr - visibleParticipants.length;
            }

            $parent.attr('data-participants-unseen', unseenNr ? ('+' + unseenNr) : '');
        });
    }

    function _getAllConversationsSummaries(skip, take, _allConversationsSummaries, _initialPromiseResult) {
        //default references for further "recursive calls"
        if (!_allConversationsSummaries) _allConversationsSummaries = [];
        if (!_initialPromiseResult) _initialPromiseResult = $.Deferred();

        GetConversationSummaryList.callApi(skip, take).then(function (response) {
            //append the new fetched conversations to the final result
            _allConversationsSummaries.push.apply(_allConversationsSummaries, response.Data.ConversationSummaries);

            if (response.Data.ThereAreMore) {
                //get more conversations
                return _getAllConversationsSummaries(skip + take, take, _allConversationsSummaries, _initialPromiseResult);
            }

            //resolve the initial promise when no more conversations
            _initialPromiseResult.resolve(_allConversationsSummaries);
        });

        return _initialPromiseResult.promise();
    }

    function _getAllConversationParticipants(trackableItemId, skip, take, _allConversationParticipants, _initialPromiseResult) {
        //default references for further "recursive calls"
        if (!_allConversationParticipants) _allConversationParticipants = [];
        if (!_initialPromiseResult) _initialPromiseResult = $.Deferred();

        GetMessageConversationParticipants.callApi(trackableItemId, skip, take).then(function (response) {
            //append the new fetched conversations to the final result
            _allConversationParticipants.push.apply(_allConversationParticipants, response.Data.Users);

            if (response.Data.ThereAreMore) {
                //get more participants
                return _getAllConversationParticipants(trackableItemId, skip + take, take, _allConversationParticipants, _initialPromiseResult);
            }

            //resolve the initial promise when no more participants
            _initialPromiseResult.resolve(_allConversationParticipants);
        });

        return _initialPromiseResult.promise();
    }

    function _getConversationsSummary() {
        var $conversations = $('.js-converations-template');

        $conversations.ajaxLoadingHelper({ LoadingMessage: 'Loading conversations...' });

        _getAllConversationsSummaries(0, 50)
            .then(function (conversationSummaries) {
                //cache trackable items for further fast access
                conversationSummaries.forEach(function (item) {
                    _cacheTrackableItems[item.TrackableItemId] = item;
                });

                //convert from unix time?!
                conversationSummaries.forEach(function (item) {
                    item.LastReplyDate = item.LastReplyDate * 1000;
                });

                ////add meta data for view
                //conversationSummaries.forEach(function (item) {
                //    item._particiapntsCount = item.RecipientList.Names.length;
                //});

                //group by date
                var groupOfConversationSummaries = _.groupBy(conversationSummaries, function (item) {
                    return moment(item.OrderingDate).format('DD-MMM-YYYY');
                });

                return GetHandlebarsTemplate.callApi(
                    'Content/js/handlebarsTemplates/Messages__conversations.htm',
                    $conversations,
                    { GroupOfConversationSummaries: groupOfConversationSummaries }
                );
            })
            .then(function () {
                _updateConversationsParticipantsCounter();

                appMain.setElementsAsFullHeight();
              
                $('.js-add-conversation').on('click', function (e) {
                    _startNewConversation();
                }).prop("disabled", false);

                $('.js-conversation-item').on('click', function (e) {
                    var $self = $(this),
                        trackableItemId = $self.attr('data-trackable-item-id'),
                        $newConversation = $('.js-newConversation-template');

                    $('.conversation-item[data-active]').attr('data-active', false);
                    $self.attr('data-active', true);

                    _getConversationMessagesAndParticipants(trackableItemId);

                    $newConversation.hide();
                });

                $('.js-conversation-item:first').triggerHandler("click");
            });
    }

    function _reloadConversationSummary(trackableItemId, reloadAll) {
        return _getAllConversationsSummaries(0, 50)
            .then(function (conversationSummaries) {
                //cache trackable items for further fast access
                conversationSummaries.forEach(function (item) {
                    _cacheTrackableItems[item.TrackableItemId] = item;
                });

                //convert from unix time?!
                conversationSummaries.forEach(function (item) {
                    item.LastReplyDate = item.LastReplyDate * 1000;
                });

                //group by date
                var groupOfConversationSummaries = _.groupBy(conversationSummaries, function (item) {
                    return moment(item.OrderingDate).format('DD-MMM-YYYY');
                });

                return GetHandlebarsTemplate.callApi(
                    'Content/js/handlebarsTemplates/Messages__conversations.htm',
                    null,
                    { GroupOfConversationSummaries: groupOfConversationSummaries }
                );
            })
            .then(function (htmlContent) {
                if (reloadAll) {
                    var $newConversationsWrapper = $(htmlContent),
                        $oldConversationsWrapper = $('.conversations__wrapper');

                    $oldConversationsWrapper.html($newConversationsWrapper.html());

                    $('.conversation-item[data-trackable-item-id="' + trackableItemId + '"]').attr('data-active', true);

                    appMain.setElementsAsFullHeight();

                    $('.js-conversation-item').on('click', function (e) {
                        var $self = $(this),
                            trackableItemId = $self.attr('data-trackable-item-id');

                        $('.conversation-item[data-active]').attr('data-active', false);
                        $self.attr('data-active', true);

                        _getConversationMessagesAndParticipants(trackableItemId);
                    });
                }
                else {
                    var $newConversationItem = $(htmlContent).find('.conversation-item[data-trackable-item-id="' + trackableItemId + '"]'),
                        $oldConverationItem = $('.conversation-item[data-trackable-item-id="' + trackableItemId + '"]');

                    $oldConverationItem.html($newConversationItem.html());
                }

                _updateConversationsParticipantsCounter();
            });
    }

    function _getConversationMessages(trackableItemId) {
        var $messages = $('.js-messages-template');

        $messages.show();
        $messages.ajaxLoadingHelper({ LoadingMessage: 'Loading messages...' });

        return GetMessagesConversation.callApi(trackableItemId)
            .then(function (response) {
                var data = response.Data;

                data.MessagesList.Messages.reverse();

                //cache trackable items for further fast access
                data.MessagesList.Messages.forEach(function (item) {
                    _cacheTrackableItems[item.TrackableItemId] = item;
                });

                //convert from unix time?!
                data.MessagesList.Messages.forEach(function (item) {
                    item.DateMessageSent = item.DateMessageSent * 1000;
                });

                //add viewers profile picture?!
                data.MessagesList.Messages.forEach(function (item) {
                    item.ViewedBy.forEach(function (item) {
                        item.ProfileImageUrl = (data.ParticipantsList.Users.filter(function (user) {
                            return user.ApplicationUserId === item.FullName.ApplicationUserID;
                        })[0] || {}).ProfileImageUrl;
                    });
                });

                //add participants last online?!
                data.ParticipantsList.Users.forEach(function (item) {
                    if (item.LastOnline) return;

                    data.MessagesList.Messages.forEach(function (message) {
                        if (item.LastOnline) return;

                        message.ViewedBy.forEach(function (viewedBy) {
                            if (item.LastOnline) return;

                            if (viewedBy.FullName.ApplicationUserID === item.FullName.ApplicationUserID) {
                                item.LastOnline = viewedBy.LastOnline;
                            }
                        });
                    });
                });

                //group by date
                var groupOfMessages = _.groupBy(data.MessagesList.Messages, function (item) {
                    return moment(item.DateMessageSent).format('DD-MMM-YYYY');
                });

                //group by hour
                Object.keys(groupOfMessages).forEach(function (groupKey) {
                    groupOfMessages[groupKey] = _.groupBy(groupOfMessages[groupKey], function (item) {
                        return moment(item.DateMessageSent).format('HH:mm');
                    });
                });

                GetHandlebarsTemplate.callApi(
                    'Content/js/handlebarsTemplates/Messages__messages.htm',
                    $messages,
                    { GroupOfMessages: groupOfMessages }
                ).then(function () {
                    appMain.setElementsAsFullHeight();

                    var $messagesContainer = $('.js-messages-container');

                    $messagesContainer.linkify();

                    $messagesContainer.scrollTop($messagesContainer.prop('scrollHeight'));

                    _markUnreadConversationMessagesAsRead(data.MessagesList.Messages, trackableItemId);

                    $('.js-show-readBy').each(function () {
                        var $self = $(this);

                        GetHandlebarsTemplate.callApi(
                            'Content/js/handlebarsTemplates/Messages__readBy.htm',
                            null,
                            _cacheTrackableItems[$self.parent('.message-text').attr('data-trackable-item-id')]
                        ).then(function (htmlContent) {
                            $self.on('mouseenter', function (e) {
                                setTimeout(function () {
                                    $self.qtip({
                                        content: $(htmlContent),
                                        position: {
                                            my: 'top left',
                                            at: 'bottom right',
                                            viewport: $(window)
                                        },
                                        show: {
                                            ready: true,
                                            solo: true
                                        },
                                        style: {
                                            tip: false
                                        }
                                    });
                                }, 1);
                            });
                        });
                    });

                    $('.js-send-message').on('click', function (e) {
                        var $self = $(this),
                            $addMessage = $('.js-add-message'),
                            message = $addMessage.val();

                        if (!message) return;

                        $self.ajaxActiveButtonUi({ LockedText: 'Sending...' });

                        NProgress.start();

                        AddMessageToConversation.callApi(trackableItemId, message)
                            .then(function () {
                                return _reloadConversationMessages(trackableItemId);
                            })
                            .then(function () {
                                $self.ajaxActiveButtonUi({ ActionState: 'stop' });
                                $addMessage.val('');

                                //endpoint not synchronized?!
                                setTimeout(function () {
                                    _reloadConversationSummary(trackableItemId, true).then(function () {
                                        NProgress.done();
                                    });
                                }, 3000);
                            });
                    });
                });
            });
    }

    function _getConversationParticipants(trackableItemId) {
        var $participants = $('.js-participants-template');

        $participants.show();
        $participants.ajaxLoadingHelper({ LoadingMessage: 'Loading participants...' });

        return _getAllConversationParticipants(trackableItemId, 0, 50)
            .then(function (participants) {
                participants = _(participants).chain()
                    .sortBy(function (item) {
                        return item.FullName.FirstName.toLowerCase();
                    })
                    .sortBy(function (item) {
                        return item.FullName.LastName.toLowerCase();
                    })
                    .value();

                _getCacheUserDirectory().then(function () {
                    participants.forEach(function (participant) {
                        console.log(participant.ApplicationUserId)
                        participant.LastOnline = (_cacheUsers[participant.FullName.ApplicationUserID] || {}).LastOnline;
                    });

                    GetHandlebarsTemplate.callApi(
                        'Content/js/handlebarsTemplates/Messages__participants.htm',
                        $participants,
                        { Users: participants }
                    ).then(function () {
                        appMain.ajaxLoadingHelperComplete();
                        appMain.setElementsAsFullHeight();
                    });
                });
            });
    }

    function _getConversationMessagesAndParticipants(trackableItemId) {
        return $.when(_getConversationMessages(trackableItemId), _getConversationParticipants(trackableItemId));
    }

    function _reloadConversationMessages(trackableItemId) {
        return GetMessagesConversation.callApi(trackableItemId)
            .then(function (response) {
                var data = response.Data;

                data.MessagesList.Messages.reverse();

                //cache trackable items for further fast access
                data.MessagesList.Messages.forEach(function (item) {
                    _cacheTrackableItems[item.TrackableItemId] = item;
                });

                //convert from unix time?!
                data.MessagesList.Messages.forEach(function (item) {
                    item.DateMessageSent = item.DateMessageSent * 1000;
                });

                //add viewers profile picture?!
                data.MessagesList.Messages.forEach(function (item) {
                    item.ViewedBy.forEach(function (item) {
                        item.ProfileImageUrl = (data.ParticipantsList.Users.filter(function (user) {
                            return user.ApplicationUserId === item.FullName.ApplicationUserID;
                        })[0] || {}).ProfileImageUrl;
                    });
                });

                //group by date
                var groupOfMessages = _.groupBy(data.MessagesList.Messages, function (item) {
                    return moment(item.DateMessageSent).format('DD-MMM-YYYY');
                });

                //group by hour
                Object.keys(groupOfMessages).forEach(function (groupKey) {
                    groupOfMessages[groupKey] = _.groupBy(groupOfMessages[groupKey], function (item) {
                        return moment(item.DateMessageSent).format('HH:mm');
                    });
                });

                GetHandlebarsTemplate.callApi(
                    'Content/js/handlebarsTemplates/Messages__messages.htm',
                    null,
                    { GroupOfMessages: groupOfMessages }
                ).then(function (htmlContent) {
                    var $newMessagesContainer = $(htmlContent).find('.messages-container'),
                        $oldMessagesContainer = $('.messages-container');

                    $oldMessagesContainer.html($newMessagesContainer.html());

                    $oldMessagesContainer.linkify();

                    $oldMessagesContainer.scrollTop($oldMessagesContainer.prop('scrollHeight'));

                    $('.js-show-readBy').each(function () {
                        var $self = $(this);

                        GetHandlebarsTemplate.callApi(
                            'Content/js/handlebarsTemplates/Messages__readBy.htm',
                            null,
                            _cacheTrackableItems[$self.parent('.message-text').attr('data-trackable-item-id')]
                        ).then(function (htmlContent) {
                            $self.on('mouseenter', function (e) {
                                setTimeout(function () {
                                    $self.qtip({
                                        content: $(htmlContent),
                                        position: {
                                            my: 'top left',
                                            at: 'bottom right',
                                            viewport: $(window)
                                        },
                                        show: {
                                            ready: true,
                                            solo: true
                                        },
                                        style: {
                                            tip: false
                                        }
                                    });
                                }, 1);
                            });
                        });
                    });
                });
            });
    }

    function _markUnreadConversationMessagesAsRead(messages, trackableItemId) {
        var currentApplicationUserId = Vault.getItem('ApplicationUserID'),
            unreadMessagesCalls = messages.map(function (item) {

                //let isUnread = !item.ViewedBy.filter(function (item) {
                //    return item.FullName.IsRequestingUser;
                //});

                var isUnread = !item.ViewedBy.filter(function (item) {
                   return item.FullName.ApplicationUserID === currentApplicationUserId;
                }).length;

              
                if (isUnread) {
                    return MarkMessageAsRead.callApi(trackableItemId, item.TrackableItemId).then(function (response, textStatus, jqXHr) {
                        if (/^2/.test(jqXHr.status)) {
                            //Messages are cross-network, so call GetUnreadItemCountsForAllNetworks and refresh local cache
                            appMain.getUnreadCountsForAllNetworks(true, null).then(function (response, textStatus, jqXHr) {}).fail(function(){});
                        }
                    });
                }
            }).filter(Boolean);

        if (unreadMessagesCalls.length) {
            NProgress.start();

            $.when.apply($, unreadMessagesCalls)
                .then(function () {
                    return _reloadConversationSummary(trackableItemId);
                })
                .then(function () {
                    NProgress.done();
                });
        }
    }

    function _startNewConversation() {
        var $messages = $('.js-messages-template'),
            $participants = $('.js-participants-template'),
            $newConversation = $('.js-newConversation-template');

        $messages.hide();
        $participants.hide();
        $newConversation.show();

        $('.conversation-item[data-active]').attr('data-active', false);

        $newConversation.ajaxLoadingHelper({ LoadingMessage: 'Loading people...' });

        var newOrUpdatedUsers = [],
            selectedUsers = [];

        //GetUserDirectory.callApi()
        _getCacheUserDirectory()
            .then(function (response) {
                var data = response.Data;

                data.NewOrUpdatedUsers = newOrUpdatedUsers = _.uniq(data.NewOrUpdatedUsers.filter(function (item) {
                    return item.FirstName || item.LastName;
                }), false, function (item) {
                    return item.ApplicationUserID;
                });

                data.NewOrUpdatedUsers = newOrUpdatedUsers = _(data.NewOrUpdatedUsers).chain()
                    .sortBy(function (item) {
                        return item.FirstName.toLowerCase();
                    })
                    .sortBy(function (item) {
                        return item.LastName.toLowerCase();
                    })
                    .value();

                return GetHandlebarsTemplate.callApi(
                    'Content/js/handlebarsTemplates/Messages__newConversation.htm',
                    $newConversation,
                    { Users: data.NewOrUpdatedUsers }
                );
            })
            .then(function () {
                $('.newConversation-search').hide();

                var suggestiosData = {
                    count: 0,
                    term: '',
                    max: 10
                };

                $('.js-newConversation-people')
                    .select2({
                        dropdownCssClass: 'newConversation-suggestions',
                        width: 'calc(100% - 6em)',
                        data: newOrUpdatedUsers.map(function (item) {
                            item.text = item.FirstName + ' ' + item.LastName;
                            item.id = item.ApplicationUserID;

                            return item;
                        }),
                        templateResult: function (data) {
                            var hbTemplate = CareflowApp.Templates['Content/js/handlebarsTemplates/Messages__newConversation_member.htm'],
                                htmlContent = hbTemplate(data),
                                $suggestions = $('.newConversation-suggestions'),
                                $suggestionsParent = $suggestions.parent(),
                                newPosition = $('.newConversation-form .select2-search--inline').offset();

                            $suggestionsParent.hide();

                            setTimeout(function () {
                                var newPosition = $('.newConversation-form .select2-search--inline').offset(),
                                    suggestionsWidth = $suggestions.outerWidth(true),
                                    maxWidth = $(window).outerWidth(true);

                                if ((newPosition.left + suggestionsWidth) > maxWidth) {
                                    newPosition.left = maxWidth - suggestionsWidth;
                                }

                                $suggestionsParent.css({ left: newPosition.left + 'px' }).show();
                            }, 1);

                            return $(htmlContent);
                        },
                        matcher: function (m, data) {
                            var selectedValues = $('.js-newConversation-people').val() || [];

                            if (suggestiosData.term !== m.term) {
                                suggestiosData.count = 0;
                                suggestiosData.term = m.term;
                            }

                            //check for maximum suggestions / already selected
                            if (suggestiosData.count < suggestiosData.max &&
                                selectedValues.indexOf(data.id) === -1 &&
                                data.text.toLowerCase().indexOf((m.term || '').toLowerCase()) !== -1) {
                                suggestiosData.count++;
                                return data;
                            }
                            return null;
                        }
                    })
                    .on('select2:unselect', function (e) { //prevent reopen when clear button clicked
                        var self = $(this);

                        self.data('select2').options.set('disabled', true);

                        setTimeout(function () {
                            self.data('select2').options.set('disabled', false);
                        }, 0);
                    })
                    .on('select2:open', function (e) {
                        var $self = $(this);

                        suggestiosData.count = 0;
                        suggestiosData.term = '';

                        $('.newConversation-suggestions').parent().hide();

                        setTimeout(function () {
                            var $suggestions = $('.newConversation-suggestions'),
                                $suggestionsParent = $suggestions.parent(),
                                newPosition = $('.newConversation-form .select2-search--inline').offset(),
                                suggestionsWidth = $suggestions.outerWidth(true),
                                maxWidth = $(window).outerWidth(true);

                            if ((newPosition.left + suggestionsWidth) > maxWidth) {
                                newPosition.left = maxWidth - suggestionsWidth;
                            }

                            $suggestionsParent.css({ left: newPosition.left + 'px' }).show();
                        }, 1);

                        $('.select2-search__field').off('keydown').on('keydown', function (e) {
                            //check if tab was pressed
                            if (e.which === '\t'.charCodeAt(0)) {
                                var item = $self.data('select2').$dropdown.find('.select2-results__option--highlighted');

                                if (item) {
                                    //append selected value
                                    $self
                                        .val(($self.val() || []).concat(item.data('data').id))
                                        .trigger('change');
                                }
                            }
                        });
                    })
                    //.on('select2:select', function (e) { //prevent auto sort of displayed tags
                    //    $(this)
                    //        .append($(e.params.data.element).detach())
                    //        .trigger('change');
                    //})
                    .on('change', function (e) {
                        var $people = $('.js-newConversation-people'),
                            selectedApplicationUsersIds = $people.val() || [];

                        //remove data-active for no longer selected users
                        $('.newConversation-search-item[data-active="true"]').each(function () {
                            var $self = $(this),
                                applicationUserId = $self.attr('data-application-user-id');

                            if (selectedApplicationUsersIds.indexOf(applicationUserId) === -1) {
                                $self.attr('data-active', false);
                            }
                        });

                        //set data-active for selected users
                        selectedApplicationUsersIds.forEach(function (applicationUserId) {
                            $('.newConversation-search-item[data-application-user-id="' + applicationUserId + '"]').attr('data-active', true);
                        });

                        //resize elements as they might be affected by the new height size of selected users
                        appMain.setElementsAsFullHeight();
                    })
                    .on('select2:opening', function (e) {
                        if ($('.newConversation-form').find('.select2-search__field').val().length === 0) {
                            $('.newConversation-suggestions').parent().hide(); //don't show suggestions before typing
                            return false;
                        }
                    });

                appMain.setElementsAsFullHeight();
                appMain.ajaxLoadingHelperComplete();

                $('.js-search-people').on('click', function (e) {
                    var $newConversationSearch = $('.newConversation-search');

                    $newConversationSearch.toggle();

                    if ($newConversationSearch.is(':visible')) {
                        $('.js-search-input').focus();
                    }

                    appMain.setElementsAsFullHeight();
                });

                $('.js-search-input').on('input', function (e) {
                    var searchValue = $(this).val().toLowerCase().trim();

                    $('.js-search-bold')[searchValue === '' ? 'addClass' : 'removeClass']('u-bold');

                    $('.newConversation-search-item').each(function () {
                        _highlightMatches(this, searchValue);
                    });
                });

                $('.newConversation-search-item').on('click', function (e) {
                    var $self = $(this),
                        $people = $('.js-newConversation-people'),
                        selectedApplicationUsersIds = $people.val() || [],
                        applicationUserId = $self.attr('data-application-user-id'),
                        indexOfApplicationUserId = selectedApplicationUsersIds.indexOf(applicationUserId);

                    if (indexOfApplicationUserId === -1) {
                        selectedApplicationUsersIds.push(applicationUserId);
                    }
                    else {
                        selectedApplicationUsersIds.splice(indexOfApplicationUserId, 1)
                    }

                    $people.val(selectedApplicationUsersIds);

                    $people.trigger('change');

                    $('.js-search-input').val('').trigger('input');
                });

                $('.js-send-newMessage').on('click', function (e) {
                    var $self = $(this),
                        $addMessage = $('.js-add-newMessage'),
                        message = ($addMessage.val() || '').trim(),
                        selectedApplicationUsersIds = $('.js-newConversation-people').val() || [];

                    if (!message) return;

                    if (selectedApplicationUsersIds.length === 0) {
                        appMain.triggerAppNotification({
                            Text: "Please select at least one recipient.",
                            Heading: "",
                            Icon: "warning"
                        });

                        return;
                    }

                    $self.ajaxActiveButtonUi({ LockedText: 'Sending...' });

                    NProgress.start();

                    StartConversation.callApi(message, selectedApplicationUsersIds.map(Number)).then(function (response) {
                        var data = response.Data;

                        //endpoint not synchronized?!
                        setTimeout(function () {
                            _getConversationMessagesAndParticipants(data.ContentItemID).then(function () {
                                //$addMessage.val('');
                                _reloadConversationSummary(data.ContentItemID, true).then(function () {
                                    NProgress.done();
                                });
                            });
                        }, 3000);
                    });
                });
            });
    }

    return {
        init: _init
    };
})();

export default Messages;
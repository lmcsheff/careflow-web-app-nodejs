import Vault from 'Endpoints/careflowApp/Vault';
import AppMainNavigation from 'VueComponents/navigation/MainNavigation'
import appMain from 'AppJs/app-main';
import * as AppIntegrationApi from 'Views/AppIntegrationApi'


let vm;
export function initAppMainNavigation(networkId = Vault.getSessionItem('ActiveNetworkId')) {

    // Check if the instance exists or is null
    if (vm) return vm;

    let userSummaryData = Vault.getItem("GetRequestingUserSummary");
	let isTpiSession = appMain.isTpiIntegrationSession();
	if (isTpiSession) networkId = AppIntegrationApi.getTpiNetworkId();

    const preferredNetwork = (!isTpiSession && userSummaryData && userSummaryData.Data.PreferredNetwork && userSummaryData.Data.PreferredNetwork.hasOwnProperty("NetworkId")) ? userSummaryData.Data.PreferredNetwork.NetworkId : null;

    /*Mount*/
    vm = new Vue({
        el: '#vue-app-nav',
        data: function() {
            return {
                NetworkId: networkId || preferredNetwork,
                PreferredNetwork: preferredNetwork
            };
        },
        components: { AppMainNavigation },
        methods: {},
        created() {},
        beforeDestroy() {},
		mounted() {
			//Let's get the badge counts
			this.$nextTick(() => {
				appMain.getUnreadCountsForAllNetworks(true, null).fail(error => { }); //Get the badge counts for all user networks
				if (networkId || preferredNetwork) appMain.getUnreadCountsForSingleNetwork(true, networkId || preferredNetwork, null);
			});
		}
    });
}

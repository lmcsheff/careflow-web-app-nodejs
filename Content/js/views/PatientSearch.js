/*This module initialises and controls the Vue instances for patient search*/

/* Utils */
import Vault from 'Endpoints/careflowApp/Vault';
/*APIs*/
/*Components*/
import NetworkPatientSearch from 'VueComponents/patientSearch/NetworkPatientSearch';
import PatientDashboard from 'VueComponents/patientDashboard/PatientDashboardWrapper';

/*Module globals*/
let vm; //Vue instance

function destroy() {
    if (vm) return vm.$destroy();
}

export function init(networkId, searchTerm, patientId, configObj) {

    //MyTasks Root view
    vm = new Vue({
        el: '#vue-network-patient-search',
        data: function() {
            return {
                NetworkId: networkId || Vault.getSessionItem('ActiveNetworkId'),
                SearchTerm: searchTerm || null, //Obj
                PatientId: patientId || null,
                ActivePatient: null,
                RouteConfig: (configObj) ? JSON.parse(atob(configObj)) : null //Base64 config obj
            };
        },

        components: { NetworkPatientSearch, PatientDashboard },

        filters: {},

        computed: {},

        watch: {},

        methods: {
            setSearchPatient(patientId, patient) {
                this.PatientId = patientId;
                this.ActivePatient = patient
            }
        },

        events: {},

        beforeCreate: function() {},

        created: function() {
            VueEventBus.$on("Router.NewRouteSet", destroy)
        },

        beforeMount: function() {},

        mounted: function() {},

        beforeUpdate: function() {},

        updated: function() {},

        beforeDestroy: function() {
            VueEventBus.$off("Router.NewRouteSet", destroy);
        },

        destroyed: function() {}
    });
}

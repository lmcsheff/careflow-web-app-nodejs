﻿/*This module initialises amd controls the Vue instances for My Tasks, Team Tasks and Patient Tasks views*/

/* Utils */
import Vault from '../endPoints/careflowApp/Vault';

/*APIs*/

/*Components*/
import Messages from '../vue/components/messages/Messages';

/*Module globals*/
let _VueRoot; //Root Vue instance

export function init(networkId) {//MyTasks Root view
   
    _VueRoot = new Vue({
        el: '#vue-messages',
        data: function () {
            return {
                NetworkId: networkId || Vault.getSessionItem("ActiveNetworkId")
            };
        },

        components: { Messages },

        filters: {},

        computed: {},

        watch: {},

        methods: {},

        events: {},

        beforeCreate: function () { },

        created: function () {
            appMain.setModuleRootVueInstance(this);
        },

        beforeMount: function () { },

        mounted: function () { },

        beforeUpdate: function () { },

        updated: function () { },

        beforeDestroy: function () { },

        destroyed: function () { }
    });
};


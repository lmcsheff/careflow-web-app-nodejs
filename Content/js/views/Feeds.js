﻿/* Utils */
//import PubSub from '../endPoints/careflowApp/PubSub';
//import Vault from '../endPoints/careflowApp/Vault';
//import GetHandlebarsTemplate from '../endPoints/careflowApp/GetHandlebarsTemplate';

/* API Endpoints */
//import StartNewConversation from '../endPoints/conversation/StartNewConversation';


/* Views */
//import groupHome from './GroupHome';
//import patientSearch from './PatientSearch';
//import patientView from './PatientView';
//import networkHome from './NetworkHome';

var feeds = feeds || (function() {
    /*Module to setup Group / Team Feeds*/
    //Functions
    return {}
}());

export default feeds;

//Third party integrtion API module

/*APIs*/
/*Components*/
import NetworkAdminNavigation from 'VueComponents/networkAdmin/NetworkAdminNavigation';
import NetworkAdminBody from 'VueComponents/networkAdmin/NetworkAdminBody';
import appMain from '../app/app-main';

export function init(networkId) { //Can accept an obj from router (url param?) or null. Init can be invoked also via method
    const vm = new Vue({
        data: function() {
            return {
                NetworkId: networkId || null,
                LatestEpisodeId: null,
                ActiveItem: null,
                ActiveItemTitle: null,
                ActiveItemConfig: null
            };
        },
        components: { NetworkAdminNavigation, NetworkAdminBody },
        methods: {
            setActiveItem(item) {
                if (!item) return;
                this.ActiveItem = item.ActiveItem;
                this.ActiveItemTitle = item.ActiveItemTitle;
                this.ActiveItemConfig = item.ActiveItemConfig;
            }
        },
        created() {},
        beforeDestroy() {},
        mounted() {

        }
    });
    //Mount
    vm.$mount('#vue-network-admin-wrapper'); //From hbars htm wrapper
};

export function userCanAccessNetworkAdministration() {
    //expand as features added - can user access the Network Administration view - /#/Administration
    const permissions = appMain.getUserPermissionsForAllUsersMemberAreas();
    const networkPermissions = ['UpdateNetworkProfile', 'ViewReports'];

    return permissions.some(x => networkPermissions.includes(x));
}

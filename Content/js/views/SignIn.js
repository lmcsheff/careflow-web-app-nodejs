﻿/*This module initialises amd controls the Vue instances for Sign In*/

/* Utils */
/*APIs*/

/*Components*/
import SignIn from 'VueComponents/signIn/SignInController';

/*Module globals*/
let vm; //Vue instance

function destroy() {
    if (vm) return vm.$destroy();
}

export function initSignIn(guid = null, isUserRegistration = false) {
    NProgress.start();
    vm = new Vue({
        data: function() {
            return {
                Guid: guid,
                IsUserRegistration: isUserRegistration
            };
        },
        components: { SignIn },
        methods: {},
        created: function() {
            VueEventBus.$on("Router.NewRouteSet", destroy)
        },
        beforeDestroy: function() {
            VueEventBus.$off("Router.NewRouteSet", destroy);
        },
        mounted() {
            NProgress.done();
        }
    });
    //Mount
    return vm.$mount('#vue-signIn');
};

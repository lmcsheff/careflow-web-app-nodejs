﻿/* Utils */
import Vault from 'Endpoints/careflowApp/Vault';
/* API Endpoints */
/* Views */
/* Others */
import appRouter from 'AppJs/app-router';
import appMain from 'AppJs/app-main';

/*Components*/
import FeedComponent from 'VueComponents/feeds/FeedComponent';
import TeamLists from 'VueComponents/networkHome/TeamList';
import SetPreferredNetworkModal from 'VueComponents/networkHome/SetPreferredNetworkModal';
import NetworkDirectoryList from 'VueComponents/directory/TeamDirectoryListWidget';
import NetworkUpdatesWidget from 'VueComponents/networkHome/NetworkUpdatesWidget';
import { VueContentLoading } from 'vue-content-loading';

let vm; //Vue instance

export function initUserNetworkHome(networkId = Vault.getSessionItem('ActiveNetworkId'), refreshRequestingSummary = true) {

    // Check if the instance exists or is null
    //if (vm) return vm;

    let userSummaryData = Vault.getItem("GetRequestingUserSummary");
    let usersNetworks = appMain.getNetworksUserIsMemberOf();
    let isShellTpiSession = appMain.isTpiIntegrationSession();

    const preferredNetwork = (!isShellTpiSession && userSummaryData && userSummaryData.Data.PreferredNetwork && userSummaryData.Data.PreferredNetwork.hasOwnProperty("NetworkId")) ? userSummaryData.Data.PreferredNetwork.NetworkId : null;

    //Is there a valid network ID for the user and its NOT a TpiSession
    if (!networkId && !preferredNetwork && !isShellTpiSession) {

        if (Vault.getItem("NoActiveUserNetworks")) { //user has been flagged as having NO network memberships
            return appMain.getFatalErrorUserNotificationModal("You are not a member of any networks or teams, please contact your system administrator.");
        }

        if (usersNetworks.Networks.length === 1) {
            networkId = usersNetworks.Networks[0].NetworkId;
            return appMain.isNetworkRouteValidForUser(networkId, null).then((response) => {
                createVueInstance(networkId, preferredNetwork, refreshRequestingSummary);
            }).catch(error => {
                //TODO: fatal message about not having ANY valid networks?
                appMain.userLogOut()
            });
        } else if (usersNetworks.Networks.length > 1) {
            //Ask user to select a network - will send back round with networkId as preferred network
            return createVueInstance(networkId, preferredNetwork, refreshRequestingSummary, true);
        }

    } else {
        //Lets check the validity and perform session setups (ActiveNetworkId etc.) - pass null permissions, sign out if fails

        if (isShellTpiSession && !networkId) return appMain.userLogOut();

        return appMain.isNetworkRouteValidForUser(networkId || preferredNetwork, null).then((response) => {
            createVueInstance(networkId, preferredNetwork, refreshRequestingSummary);
        }).catch(error => {
            //TODO: Bounce user home? Check the flows here
            appMain.userLogOut()
        });
    }
}

function destroy() {
    if (vm) return vm.$destroy();
}

function createVueInstance(networkId, preferredNetwork, refreshRequestingSummary, promptUserToSelectNetwork = false) {
    /*Mount*/

    vm = new Vue({
        el: '#vue-network-home',
        data: function() {
            return {
                NetworkId: networkId || preferredNetwork,
                PreferredNetwork: preferredNetwork,
                RefreshRequestingSummary: refreshRequestingSummary,
                PromptUserToSelectNetwork: promptUserToSelectNetwork,
                MemberAreaId: null,
                PatientId: null
            };
        },
        components: { TeamLists, FeedComponent, NetworkDirectoryList, SetPreferredNetworkModal, VueContentLoading, NetworkUpdatesWidget },
        methods: {},
        created() {
            VueEventBus.$on("Router.NewRouteSet", destroy)
        },
        beforeDestroy() {
            VueEventBus.$off("Router.NewRouteSet", destroy);
        },
        mounted() {
            //Let's get the badge counts
            appMain.getUnreadCountsForAllNetworks(true, null).fail(error => {}); //Get the badge counts for all user networks
            if (networkId || preferredNetwork) appMain.getUnreadCountsForSingleNetwork(true, networkId || preferredNetwork, null)

        }
    });
}

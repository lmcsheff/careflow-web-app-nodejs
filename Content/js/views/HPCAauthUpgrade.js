﻿// /*Endpoints*/
// import GetRequestingUserSummary from '../endPoints/users/GetRequestingUserSummary';
// /* Utils */
// import Vault from '../endPoints/careflowApp/Vault';
// import GetHandlebarsTemplate from '../endPoints/careflowApp/GetHandlebarsTemplate';
// import Upgradetohpca from '../endPoints/careflowApp/UpgradeAccountToHpca';
// import HpcaConfirmationEmailResend from '../endPoints/careflowApp/HpcaConfirmationEmailResend';

// export function init(routerContext) {
//     if (!$(".js-hpca-upgrade-account-btn").length) return console.error("No UI el to bind");
//     //View bindings
//     $(".js-hpca-upgrade-account-btn").on("click",
//         function() {

//             //Lock btn
//             var $self = $(this);
//             $self.ajaxActiveButtonUi({
//                 ActionState: "start",
//                 LockedText:"Please wait..."
//             });


//             Upgradetohpca.callApi().then(function (response, textStatus, JQXHr) {
//                 if (JQXHr.status === 200) {
//                     return routerContext
//                         .partial('Content/js/handlebarsTemplates/hpcaAuthUpgrade/hpcaUpgradeViewEmailSent.htm').then(
//                             function() {
//                                 //Add UI bindings
//                                 $(".js-hpca-resend-conf-email").on("click",
//                                     function() {
//                                         var $self = $(this);
//                                         //Lock btn
//                                         $self.ajaxActiveButtonUi({
//                                             ActionState: "start"
//                                         });

//                                         HpcaConfirmationEmailResend.callApi(Vault.getItem("LoggedInUserEmail")).then(
//                                             function(data, textStatus, jqXHr) {
//                                                 if (jqXHr.status === 200) {
//                                                     appMain.triggerAppNotification({
//                                                         Text: "We've sent you an email containing a verification link",
//                                                         Icon: "success"
//                                                     });
//                                                     //Update button, remove binding
//                                                     $self.html("Email sent, please check your inbox").unbind();
//                                                     return;
//                                                 } else {
//                                                     return appMain.triggerAppNotification({
//                                                         Text:
//                                                             "There was a problem sending you the email, please try again",
//                                                         Icon: "error"
//                                                     });
//                                                 }
//                                             }).fail(function() {
//                                             return appMain.triggerAppNotification({
//                                                 Text: "There was a problem sending you the email, please try again",
//                                                 Icon: "error"
//                                             });
//                                         }).always(function(data, textStatus, jqXHr) {
//                                             if (jqXHr.status !== 200) {
//                                                 $self.ajaxActiveButtonUi({
//                                                     ActionState: "stop"
//                                                 });
//                                             }
//                                         });
//                                     });
//                                 appMain.setElementsAsFullHeight();
//                             });
//                 } else {
//                     return appMain.triggerAppNotification({
//                         Text: "There was a problem upgrading your account, please try again",
//                         Icon: "error"
//                     });
//                 }
//             }).then(function() {
//                 $self.ajaxActiveButtonUi({
//                     ActionState: "stop"
//                 });
//                 }).fail(function(error) {
//                 $self.ajaxActiveButtonUi({
//                     ActionState: "stop"
//                 });
//                 return appMain.triggerAppNotification({
//                     Text: "There was a problem upgrading your account, please try again",
//                     Icon: "error"
//                 });
//             });
//         });
// }

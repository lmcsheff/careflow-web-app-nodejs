﻿/* Utils */
//import CareflowApp from '../careflow-app';
import Vault from '../endPoints/careflowApp/Vault';
import PubSub from '../endPoints/careflowApp/PubSub';

import appRouter from '../app/app-router';

/*Components*/
import FeedComponent from 'VueComponents/feeds/FeedComponent'; //https://medium.com/@timoxley/named-exports-as-the-default-export-api-670b1b554f65
import TeamDirectoryWidget from 'VueComponents/directory/TeamDirectoryListWidget'; /*Side panel*/
import PatientSummary from 'VueComponents/patientDashboard/PatientDashboardWrapper';
import PatientSidePanelList from 'VueComponents/patientList/PatientListSlim';
import TeamNavigation from 'VueComponents/navigation/TeamNavigation'
import TeamTasks from 'VueComponents/tasks/TasksList'
import TeamReferrals from 'VueComponents/referrals/TeamPatientReferrals'
import TeamPatientList from 'VueComponents/patientList/TeamPatientList';
import PatientListFilters from 'VueComponents/patientList/components/PatientListFilters';
import TeamDirectory from 'VueComponents/directory/TeamDirectory';
import ManageTeamTags from 'VueComponents/manageTeamClinicalTags/TeamTags'
import ManageTeamSmartLists from 'VueComponents/teamSmartLists/ManageTeamSmartLists'
import ManageTeamHandoverProfiles from 'VueComponents/handovers/ManageTeamHandoverProfiles'
import appMain from '../app/app-main';



var groupHome = groupHome || (function() {


    //Shared components across class
    var _selectedPatients = []; //Patients selected in patient list
    var _memberAreaId;
    //var _isSameTeamContext; //Bool to determine if team context being loaded is same as actively loaded (so is user navigating in same group). Set in Init().
    var _listType;
    //var _patientListLayoutViewType = "list"; //Render full-fat patient list in 'grid' of 'list' view. Default to 'list'.
    // var _patientSidePanelListIsCollapsed; //Flag for status of patient list side panel collapsed status
    var _patientsData = { //The loaded team patient list data
        ActiveNetwork: Vault.getSessionItem("ActiveNetworkId"), //Default as active / context Network
        ActiveTeam: null,
        Data: []
    };
    // var _feedTypeLoaded; //Flag to identify feed type loaded in view. 'TeamFeed' or 'PatientFeed'

    // var _teamPatientViewLastUrl = '';

    var _isGroupViewTemplateLoaded; //Check for base template presence in DOM
    // var _$dataTablePatientList; //DOM Element
    //var _$dataTablePatientListTableObj; //Datatable Object
    // var _$dataTablePatientHandoverList; //DOM Element
    // var _$dataTableSinglePatientHandoverListHistory; //DOM Element
    // var _$dataTablePatientHandoverListTableObj; //Datatable Object

    var _activeGroupListFilters = []; //Hold selected / active patient list filters
    // var _patientsEditModeActive; //Flag for UI setup to remove patients. Indicates state
    // var _viewSubscriptionReferences = []; //Container to hold subscription references, only named references not functions. Useful for simple checks across class
    var _handoverApiVersion = 2; //Temporary flag until new Handover APIs are released (version 2) July 11 2017. Can remove then or change reference to '2' to make team handovers work

    var _init = function(memberAreaId) { //View bootstraps and cleanups
        if (!memberAreaId) { //Must be an active group
            console.error("No member area set");
            return appRouter.careflowAppRouter.setLocation("/#/Home");
        } else {
            //Waypoint.destroyAll(); //Clear any existing waypoints, view changed
            //  $("[data-view-panel='group-home']").empty(); //Clear group view panels on init() (mainly patient list related)

            if (_memberAreaId) { //Check against cached memberareaID before reset
                // _isSameTeamContext = (_memberAreaId === memberAreaId) ? true : false;
            }
            _memberAreaId = memberAreaId; //Set / reset memberAreaId

            this.memberAreaId = _memberAreaId; //Set the public api

            // _teamPatientViewLastUrl = ''; //reset the last sub-view url

            //Ensure selected (checkboxes in lists) and patient data (cached) is reset on route load / method call
            _selectedPatients.length = 0;

            if (_.has(_patientsData, "ActiveTeam") && _patientsData.ActiveTeam !== memberAreaId) { //Clear patient cache if new memberAreaId is different
                clearCachedPatientListData();
            } else if (!_.has(_patientsData, "ActiveTeam")) { //No team set, clear
                clearCachedPatientListData();
            }

            _activeGroupListFilters.length = 0; //Hold selected / active filter options

            PubSub.publish("GroupHome.SelectedPatientsUpdated", {
                //Reset list selections across UI on init of new group view to all listeners
                CountOfSelectedPatients: _selectedPatients.length
            });
            _patientsData.ActiveNetwork = Vault.getSessionItem("ActiveNetworkId"); //Default as active / context Network

            //Set group title and Build group navigation
            //getGroupNavigation(_memberAreaId, false);

            //Init Vue.js
            //appMain.destroyModuleRootVueInstance(); //Destroy sub instances on team views navigation
        }
    };

    /*Functions*/

    var getHandoverApiVersion = function() {
        // if (typeof _handoverApiVersion === "number" && _handoverApiVersion) {
        //     return _handoverApiVersion;
        // }
        // return false;
    };
    var getSelectedPatients = function() { //Public API to return selected patients (patients ticked in a patient list. Context dependant upon loaded list type)
        //return _selectedPatients;
    };

    var getLoadedPatientListAndType = function() {
        // return {
        //     PatientListIsLoaded: ($(".js-patientList__table[data-context='PatientList']").length) ? true : false,
        //     PatientHandoverListIsLoaded: ($(".js-patientList__table[data-context='PatientHandoverList']").length) ? true : false,
        //     SinglePatientTeamHandoversViewIsLoaded: ($(".js-patientList__table[data-view-type='SinglePatientTeamHandovers']").length) ? true : false, //Single patient current team handovers
        //     AnyPatientListIsLoaded: ($(".js-patientList__table[data-context='PatientList']").length || $(".js-patientList__table[data-context='PatientHandoverList']").length) ? true : false,
        //     SlimPatientListIsLoaded: ($(".js-teamPatients-sidePanel-wrapper").length) ? true : false, //Patients
        //     DataContext: (_listType) ? _listType : false,
        //     CountOfPatients: _patientsData.Data.length
        // };
    };

    var clearCachedPatientListData = function() {
        if (_.has(_patientsData, "Data") && _patientsData.Data.length) {
            return _patientsData.Data.length = 0;
        }
        return false;
    };






    /*Team Views*/
    let initTeamUpdates = function(obj) {
        if (!obj) return;
        NProgress.start();
        const vm = new Vue({
            data: function() {
                return {
                    MemberAreaId: obj.MemberAreaId,
                    NetworkId: obj.NetworkId,
                    ViewTitle: obj.ViewTitle
                };
            },
            components: { FeedComponent, TeamDirectoryWidget, TeamNavigation },
            methods: {},
            created() {},
            beforeDestroy() {},
            mounted() {
                NProgress.done();
            }
        });

        //Mount
        return vm.$mount('#vue-team-updates');

    };


    // let initPatientDashboard = function(obj) {

    //     if (!obj) return;
    //     NProgress.start();
    //     const vm = new Vue({
    //         //el: '#vue-patient-dashboard',
    //         data: function() {
    //             return {
    //                 ActivePatientId: obj.PatientId || null,
    //                 Patient: obj.Patient || null,
    //                 MemberAreaId: obj.MemberAreaId,
    //                 NetworkId: obj.NetworkId,
    //                 ViewTitle: obj.ViewTitle,
    //                 PatientListIsCollapsed: false,
    //                 IsFullPatientListView: true, //Default to full list view
    //                 FiltersAreVisible: false,
    //                 ActiveFilters:  {
	// 						ClinicalTags: [],
	// 						Clinicians: [],
	// 						Locations: [],
	// 				}, //default
    //             };
    //         },
    //         components: { PatientSummary, PatientSidePanelList, TeamPatientList, PatientListFilters, TeamNavigation },
    //         watch: {
    //             FiltersAreVisible(newVal, oldVal) {
    //                 this.$nextTick(() => {
    //                     appMain.setElementsAsFullHeight();
    //                 });
    //             }
    //         },
    //         methods: {
    //             setActivePatient(obj) {
    //                 if (!obj || obj.PatientId === this.ActivePatientId) return;
    //                 /*Set via obj item on loaded patient list*/
    //                 appMain.closeOpenedPatientContextWindow(); //Close any patient context windows
    //                 //appMain.stopApiRequests();

    //                 this.ActivePatientId = obj.PatientId;
    //                 this.Patient = obj.Patient;
    //                 if (this.IsFullPatientListView) this.IsFullPatientListView = false;
    //             },

    //             getFilterVisibilityButtonText() {
    //                 if (this.FiltersAreVisible) return `Hide filters`;
    //                 if (!this.FiltersAreVisible && this.ActiveFilters && (this.ActiveFilters.ClinicalTags.length || this.ActiveFilters.Clinicians.length || this.ActiveFilters.Locations.length)) return `Show filters (filters applied)`;
    //                 return `Show filters`;
    //             },
    //             toggleCollapsed(obj) {
    //                 this.PatientListIsCollapsed = obj.IsCollapsed;
    //             },
    //             expandList() {
    //                 this.PatientListIsCollapsed = false;
    //             },
    //             toggleIsFullPatientListView(val) {
    //                 this.IsFullPatientListView = val;
    //             },
    //         },
    //         created() {},
    //         beforeDestroy() {},
    //         mounted() {
    //             NProgress.done();
    //         }
    //     });

    //     //Mount
    //     return vm.$mount('#vue-patient-dashboard');

    // };


    let initTeamTasks = function(obj) {
        if (!obj) return;
        NProgress.start();
        const vm = new Vue({
            data: function() {
                return {
                    MemberAreaId: obj.MemberAreaId,
                    NetworkId: obj.NetworkId,
                    ViewTitle: obj.ViewTitle
                };
            },
            components: { TeamTasks, TeamNavigation },
            methods: {},
            created() {},
            beforeDestroy() {},
            mounted() {
                NProgress.done();
            }
        });

        //Mount
        return vm.$mount('#vue-team-tasks');

    };

    let initTeamReferrals = function(obj) {
        if (!obj) return;
        NProgress.start();
        const vm = new Vue({
            data: function() {
                return {
                    MemberAreaId: obj.MemberAreaId,
                    NetworkId: obj.NetworkId,
                    ViewTitle: obj.ViewTitle
                };
            },
            components: { TeamReferrals, TeamNavigation },
            methods: {},
            created() {},
            beforeDestroy() {},
            mounted() {
                NProgress.done();
            }
        });

        //Mount
        return vm.$mount('#vue-team-referrals');

    };

    let initTeamPatientList = function(obj) {
        if (!obj) return;
        NProgress.start();
        const vm = new Vue({
            data: function() {
                return {
                    MemberAreaId: obj.MemberAreaId,
                    NetworkId: obj.NetworkId,
                    ViewTitle: obj.ViewTitle,
                    ListType: obj.ListType
                };
            },
            components: { TeamPatientList, TeamNavigation },
            methods: {},
            created() {},
            beforeDestroy() {},
            mounted() {
                NProgress.done();
            }
        });

        //Mount
        return vm.$mount('#vue-team-patient-list');

    };

    let initTeamDirectory = function(obj) {
        if (!obj) return;
        NProgress.start();
        const vm = new Vue({
            data: function() {
                return {
                    MemberAreaId: obj.MemberAreaId,
                    NetworkId: obj.NetworkId
                };
            },
            components: { TeamDirectory, TeamNavigation },
            methods: {},
            created() {},
            beforeDestroy() {},
            mounted() {
                NProgress.done();
            }
        });

        //Mount
        return vm.$mount('#vue-team-directory');

    };


    let initManageTeamTags = function(obj) {

        if (!obj) return;
        NProgress.start();
        const vm = new Vue({
            data: function() {
                return {
                    ViewTitle: obj.ViewTitle,
                    MemberAreaId: obj.MemberAreaId,
                    NetworkId: obj.NetworkId
                };
            },
            components: { ManageTeamTags, TeamNavigation },
            methods: {},
            created() {},
            beforeDestroy() {},
            mounted() {
                NProgress.done();
            }
        });

        //Mount
        return vm.$mount('#vue-team-tags');

    };


    let initManageTeamSmartLists = function(obj) {

        if (!obj) return;
        NProgress.start();
        const vm = new Vue({
            data: function() {
                return {
                    ViewTitle: obj.ViewTitle,
                    MemberAreaId: obj.MemberAreaId,
                    NetworkId: obj.NetworkId
                };
            },
            components: { ManageTeamSmartLists, TeamNavigation },
            methods: {},
            created() {},
            beforeDestroy() {},
            mounted() {
                NProgress.done();
            }
        });

        //Mount
        return vm.$mount('#vue-team-smart-lists');

    };


    let initManageTeamProfiles = function(obj) {

        if (!obj) return;
        NProgress.start();
        const vm = new Vue({
            data: function() {
                return {
                    ViewTitle: obj.ViewTitle,
                    MemberAreaId: obj.MemberAreaId,
                    NetworkId: obj.NetworkId
                };
            },
            components: { ManageTeamHandoverProfiles, TeamNavigation },
            methods: {},
            created() {},
            beforeDestroy() {},
            mounted() {
                NProgress.done();
            }
        });

        //Mount
        return vm.$mount('#vue-manage-team-profiles');

    };


    /*Team patient dashboard page summary tab widgets*/

    function getPatientObs(patientId, memberAreaId, isNetworkContext) { //DEMO env
        // PubSub.publish("PatientView.GetPatientObsSummary", {
        //     PatientId: patientId,
        //     Patient: patientView.getCachedPatient(),
        //     IsModal: false,
        //     MemberAreaId: memberAreaId || null,
        //     IsNetworkContext: isNetworkContext,
        //     DomContainer: $(".js-teamPatientView-obs-template")
        // });
    };

    function getPatientResults(patientId, memberAreaId, isNetworkContext) { //Medway Results - DEMO env
        // PubSub.publish("PatientView.GetPatientResultsSummary", {
        //     PatientId: patientId,
        //     Patient: patientView.getCachedPatient(),
        //     IsModal: false,
        //     MemberAreaId: memberAreaId || null,
        //     IsNetworkContext: isNetworkContext,
        //     DomContainer: $(".js-teamPatientView-results-template")
        // });
    };


    return {
        init: _init,
        isGroupViewTemplateLoaded: _isGroupViewTemplateLoaded,
        memberAreaId: _memberAreaId,
        //getGroupPatientList: _getGroupPatientList,
        //initPatientDashboard: initPatientDashboard,
        initTeamUpdates: initTeamUpdates,
        initTeamTasks: initTeamTasks,
        initTeamReferrals: initTeamReferrals,
        initTeamPatientList: initTeamPatientList,
        initTeamDirectory: initTeamDirectory,
        initManageTeamTags: initManageTeamTags,
        initManageTeamSmartLists: initManageTeamSmartLists,
        initManageTeamProfiles: initManageTeamProfiles,
        getSelectedPatients: getSelectedPatients,
        getLoadedPatientListAndType: getLoadedPatientListAndType,
        clearCachedPatientListData: clearCachedPatientListData,
        getHandoverApiVersion: getHandoverApiVersion

    };
})();

export default groupHome;

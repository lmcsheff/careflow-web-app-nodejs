﻿/*This module initialises amd controls the Vue instances for My Tasks, Team Tasks and Patient Tasks views*/

/* Utils */
import Vault from 'Endpoints/careflowApp/Vault';
/*APIs*/
/*Components*/
import TaskList from 'VueComponents/tasks/TasksList';

/*Module globals*/
let vm; //Vue instance

function destroy() {
    if (vm) return vm.$destroy();
}

export function init(networkId, memberAreaId, patientId, contentIdToActivate) {
    //MyTasks Root view
    vm = new Vue({
        el: '#vue-tasks',
        data: function() {
            return {
                MemberAreaId: memberAreaId || null,
                NetworkId: networkId || Vault.getSessionItem('ActiveNetworkId'),
                PatientId: patientId || null,
                SuppliedActiveTaskContentId: contentIdToActivate || null
            };
        },

        components: { TaskList },

        filters: {},

        computed: {},

        watch: {},

        methods: {},

        events: {},

        beforeCreate: function() {},

        created: function() {
            VueEventBus.$on("Router.NewRouteSet", destroy)
        },

        beforeMount: function() {},

        mounted: function() {},

        beforeUpdate: function() {},

        updated: function() {},

        beforeDestroy: function() {
            VueEventBus.$off("Router.NewRouteSet", destroy);
        },

        destroyed: function() {}
    });
}

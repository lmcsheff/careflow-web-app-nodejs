﻿/* Utils */
import Vault                    from '../endPoints/careflowApp/Vault';
import GetHandlebarsTemplate    from '../endPoints/careflowApp/GetHandlebarsTemplate';

/* API Endpoints */
import GetUsersProfiles from '../endPoints/users/GetUsersProfiles';

var Profiles = Profiles || (function () {
    function _init(userId) {
		var userSummary = Vault.getItem('GetRequestingUserSummary').Data,
			isOwnProfile = !userId || userId === userSummary.FullName.ApplicationUserID,
			activeNetworkId = Vault.getSessionItem('ActiveNetworkId'),
			accessGroupId = appMain.getAccessGroupId(appMain.getNetworkMemberAreaId(activeNetworkId)),
			groupsByNetworkId = Vault.getItem('ActiveNetworkGroupMemberships').Groups || {};

        if (isOwnProfile) {
            userId = userSummary.FullName.ApplicationUserID;
        }

        appMain.ajaxLoadingHelper({
            DomElement: $('.js-profile-template'),
            LoaderMessage: "Loading profile..."
        });

        NProgress.start();

        GetUsersProfiles
            .callApi(accessGroupId, [userId])
            .then(function (response) {
                if (response.Data.Failed.length !== 0) {
                    $.toast({
                        text: 'Something went wrong!',
                        heading: 'Error',
                        icon: 'error',
                        showHideTransition: 'fade',
                        hideAfter: 10000
                    });

                    return;
                }

                var profileDetails = response.Data.Passed[0];

                return GetHandlebarsTemplate.callApi(
                    'Content/js/handlebarsTemplates/Profiles__Profile.htm',
                    $('.js-profile-template'),
                    {
                        FirstName: profileDetails.FirstName,
                        LastName: profileDetails.LastName,
                        ProfileImageUrl: profileDetails.ProfileImageLink,
                        EmailAddress: profileDetails.EmailAddress,
                        MobileNumberCountryCode: profileDetails.MobileNumberCountryCode,
                        MobileNumber: profileDetails.MobileNumber,
                        Profession: profileDetails.Profession,
                        Grade: profileDetails.Grade,
                        Specialty: profileDetails.Specialty,
                        Organisation: isOwnProfile ? userSummary.PlaceOfWork || profileDetails.Organisation : profileDetails.Organisation,
                        GroupsInCurrentNetwork: (
                            profileDetails.MemberAreaIDs
                                .map(function (memberAreaId) {
                                    return groupsByNetworkId.filter(function (item) {
                                        return item.MemberAreaID === memberAreaId;
                                    })[0];
                                })
                                .filter(Boolean)
                                .sort(function (a, b) {
                                    return a.Name.toLowerCase().localeCompare(b.Name.toLowerCase());
                                })
                         ),
                        _ownProfile: !!isOwnProfile,
                        _activeNetworkId: activeNetworkId
                    }
                );
            })
            .then(function (htmlContent) {
                NProgress.done();

                appMain.ajaxLoadingHelperComplete();
                appMain.setElementsAsFullHeight();
            })
            .fail(function () {
                $.toast({
                    text: 'Error retrieving profile',
                    heading: 'Error',
                    icon: 'error',
                    showHideTransition: 'fade',
                    hideAfter: 10000
                });
            });
    }

    return {
        init: _init
    };
})();

export default Profiles;

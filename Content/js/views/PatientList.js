﻿// /*This module initialises and controls the Vue instances for PatientLists views*/

// /* Utils */
// import Vault from '../endPoints/careflowApp/Vault';

// /*APIs*/

// /*Components*/
// import TeamPatientList from '../vue/components/patientList/TeamPatientList';

// /*Module globals*/
// let _VueRoot; //Root Vue instance


// export function init(networkId, memberAreaId, patientId, listType) {//PatientList Root view

//     appMain.destroyModuleRootVueInstance();//clear down any root vue instances

//     _VueRoot = new Vue({
//         el: '#vue-teamPatientList',
//         data: function () {
//             return {
//                 MemberAreaId: memberAreaId || null,
//                 NetworkId: networkId || Vault.getSessionItem("ActiveNetworkId"),
//                 PatientId: patientId || null,
//                 ListType: listType || null
//             };
//         },

//         components: { TeamPatientList },

//         filters: {},

//         computed: {},

//         watch: {},

//         methods: {},

//         events: {},

//         beforeCreate: function () { },

//         created: function () {

//             appMain.setModuleRootVueInstance(this);
//         },

//         beforeMount: function () { },

//         mounted: function () { },

//         beforeUpdate: function () { },

//         updated: function () { },

//         beforeDestroy: function () {

//         },

//         destroyed: function() {

//         }
//     });
// };

/*APIs*/
/*Components*/
import NetworkUpdatesWrapper from 'VueComponents/networkUpdates/NetworkUpdatesWrapper';
import appMain from '../app/app-main';

export function init(networkId, viewTitle, route = 'updates', contentItemId) { //Can accept an obj from router (url param?) or null. Init can be invoked also via method
    if (!networkId) return;
    const vm = new Vue({
        data: function() {
            return {
                NetworkId: networkId,
                MemberAreaId: appMain.getNetworkMemberAreaId(networkId),
                ContentIdToActivate: contentItemId || null,
                ViewTitle: viewTitle || '',
                Route: route
            };
        },
        components: { NetworkUpdatesWrapper },
        methods: {
            setRoute(networkId, viewTitle, route) {
                if (!route) return;
                this.NetworkId = networkId;
                this.MemberAreaId = appMain.getNetworkMemberAreaId(networkId);
                this.ViewTitle = viewTitle;
                this.Route = route;
            }
        },
        created() {
            appMain.setModuleRootVueInstance(this);
        },
        beforeDestroy() {},
        mounted() {}
    });
    //Mount
    vm.$mount('#vue-network-updates'); //From hbars htm wrapper
};

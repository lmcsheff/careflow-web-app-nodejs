﻿/* Utils */
//import CareflowApp from '../careflow-app';
import Vault from '../endPoints/careflowApp/Vault';
import PubSub from '../endPoints/careflowApp/PubSub';
import GetHandlebarsTemplate from '../endPoints/careflowApp/GetHandlebarsTemplate';

/* API Endpoints */
//import GetUsersProfiles from '../endPoints/users/GetUsersProfiles';
//import TasksMetaData from '../endPoints/tasks/TasksMetaData';
//import AddUserTask from '../endPoints/tasks/AddUserTask';
import GetPatient from '../endPoints/patients/GetPatient';
//import ClinicalTags from '../endPoints/clinicalTags/ClinicalTags';
//import GroupClinicalTags from '../endPoints/clinicalTags/GroupClinicalTags';
//import GetPatientFeedItems from '../endPoints/patients/GetPatientFeedItems';
import AddPatientToGroupList from '../endPoints/patients/AddPatientToGroupList';
import RemovePatientFromGroupList from '../endPoints/patients/RemovePatientFromGroupList';
//import UpsertHandoverNote from '../endPoints/patients/UpsertHandoverNote';
//import GetReferralGroups from '../endPoints/referral/GetReferralGroups';
//import SendReferral from '../endPoints/referral/SendReferral';
import StartNewConversationTaggedWithPatient from '../endPoints/conversation/StartNewConversationTaggedWithPatient';
//import ReasonsForRemovingTag from '../endPoints/clinicalTags/ReasonsForRemovingTag';

/* Views */
//import groupHome from './GroupHome';
import feeds from './Feeds';

/* Others */
//import appRouter from '../app/app-router';

var patientView =
	patientView ||
	(function() {
		//DataContexts: GetPatientActions, PatientView

		var _subscriptions = (function() {
			PubSub.subscribe('CareflowApp.Router.RouteCleanup', function(obj) {
				viewCleanup();
			});

			PubSub.subscribe('PatientView.GetPatientActions', function(obj) {
				_patientContextActionsShowOptions(obj);
			});

			PubSub.subscribe('PatientView.GetPatientActions.Referral', function(
				obj
			) {
				_patientContextActionsGetPatientReferralControl(obj);
			});

			PubSub.subscribe('PatientView.GetPatientActions.Update', function(
				obj
			) {
				_patientContextActionsPostUpdate(obj);
			});

			PubSub.subscribe('PatientView.GetPatientActions.Handover', function(
				obj
			) {
				_patientContextActionsGetHandoverControl(obj);
			});

			PubSub.subscribe(
				'PatientView.GetPatientActions.PatientLists',
				function(obj) {
					_patientContextActionsGetPatientTeamListsControl(obj);
				}
			);

			PubSub.subscribe(
				'PatientView.GetPatientActions.AddPatientToList',
				function(obj) {
					_addPatientToGroupList(obj);
				}
			);

			PubSub.subscribe(
				'PatientView.GetPatientActions.PatientAddedToList',
				function(obj) {
					//Individual patient list membership management
					// if (!_patientData) return;

					// if (
					// 	_patientData &&
					// 	typeof _patientData.Data.GroupLists !== 'undefined'
					// ) {
					// 	_patientData.Data.GroupLists.push({
					// 		Update cached Patient Data model
					// 		MemberAreaId: parseInt(obj.MemberAreaId, 10),
					// 		MemberAreaName: appMain.getMemberAreaName(
					// 			parseInt(obj.MemberAreaId, 10)
					// 		),
					// 		ListInclusionTypes: ['Pin']
					// 	});
					// }

					// _patientData.Data.GroupListExternalIdentifiers.push(
					// 	appMain.getAccessGroupExternalIdentifier(
					// 		parseInt(obj.MemberAreaId, 10),
					// 		false
					// 	)
					// );

					// If in a team list view, update UI
					// if (
					// 	groupHome.getLoadedPatientListAndType()[
					// 		'SlimPatientListIsLoaded'
					// 	] === true &&
					// 	obj.MemberAreaId ===
					// 		Vault.getSessionItem('ActiveGroupMemberAreaId')
					// ) {
					// 	Find patient and flag as no longer pending delete, if it was
					// 	var $patientLi = $(
					// 		'.js-teamPatients-sidePanel-wrapper'
					// 	).find(
					// 		'.js-teamPatientList-patient[data-patientid=' +
					// 			obj.PatientId +
					// 			']'
					// 	);
					// 	if (
					// 		$patientLi.hasClass(
					// 			'teamPatientList__patient-pending--removal'
					// 		)
					// 	) {
					// 		$patientLi
					// 			.addClass('teamPatientList__patient')
					// 			.removeClass(
					// 				'teamPatientList__patient-pending--removal'
					// 			);
					// 	}
					// }
				}
			);

			PubSub.subscribe(
				'PatientView.GetPatientActions.PatientRemovedFromList',
				function(obj) {
					// if (!_patientData) return;

					// if (
					// 	_patientData &&
					// 	typeof _patientData.Data.GroupLists !== 'undefined'
					// ) {
					// 	_patientData.Data.GroupLists = _.without(
					// 		_patientData.Data.GroupLists,
					// 		_.findWhere(_patientData.Data.GroupLists, {
					// 			MemberAreaId: parseInt(obj.MemberAreaId, 10)
					// 		})
					// 	); //Update Patient Data model, remove the pinned group list external ID
					// }

					// _patientData.Data.GroupListExternalIdentifiers = _.without(
					// 	_patientData.Data.GroupListExternalIdentifiers,
					// 	appMain.getAccessGroupExternalIdentifier(
					// 		parseInt(obj.MemberAreaId, 10),
					// 		false
					// 	)
					// );

					// //If in a team list view, update UI
					// if (
					// 	groupHome.getLoadedPatientListAndType()[
					// 		'SlimPatientListIsLoaded'
					// 	] === true &&
					// 	obj.MemberAreaId ===
					// 		Vault.getSessionItem('ActiveGroupMemberAreaId')
					// ) {
					// 	//Find patient and flag as pending delete
					// 	var $patientLi = $(
					// 		'.js-teamPatients-sidePanel-wrapper'
					// 	).find(
					// 		'.js-teamPatientList-patient[data-patientid=' +
					// 			obj.PatientId +
					// 			']'
					// 	);
					// 	$patientLi
					// 		.removeClass('teamPatientList__patient')
					// 		.addClass(
					// 			'teamPatientList__patient-pending--removal'
					// 		);
					// }
				}
			);

			// PubSub.subscribe(
			// 	'PatientView.GetPatientActions.PatientTags',
			// 	function(obj) {
			// 		_patientContextActionsGetPatientClinicalTagsControl(obj);
			// 	}
			// );

			// PubSub.subscribe(
			// 	'PatientView.GetPatientActions.PatientTags.GetRemoveTagUI',
			// 	function(obj) {
			// 		_patientContextActions__removePatientTag(obj);
			// 	}
			// );

			// PubSub.subscribe(
			// 	'PatientView.GetPatientActions.PatientTags.RemoveTag',
			// 	function(obj) {
			// 		_removePatientClinicalTag(obj);
			// 	}
			//);

			//When tag has been removed via webservice, update the UI - clinicalTagRemoved callback event fired
			// PubSub.subscribe(
			// 	'PatientView.GetPatientActions.PatientTags.TagRemoved',
			// 	function(obj) {
			// 		$(
			// 			'.js-clinicaltag[data-clinicaltagid=' +
			// 				obj.ClinicalTagId +
			// 				'][data-patientid=' +
			// 				obj.PatientId +
			// 				'][data-addedbygroupid=' +
			// 				obj.MemberAreaId +
			// 				']'
			// 		).fadeOut(); //Remove the tag in the UI

			// 		//Update the local patient model and remove the clinical tag against the set memberAreaID. Model loaded on patient view context, not in feeds etc as patient id taken from event and GetPatient API called.
			// 		_updatePatientClinicalTagsViewModelWithRemovedTag(
			// 			obj.ClinicalTagId,
			// 			obj.MemberAreaId
			// 		);

			// 		appMain.closeAllqTips();
			// 	}
			// );

			//When tag has been added via webservice (TagAdded emitted by webservice)
			// PubSub.subscribe(
			// 	'PatientView.GetPatientActions.PatientTags.TagAdded',
			// 	function(obj) {}
			// );

			PubSub.subscribe(
				'PatientView.GetPatientActions.GetAdditionalIDsUi',
				function(obj) {
					_getPatientAdditionalIdsUI(obj);
				}
			);

			// PubSub.subscribe(
			// 	'PatientView.GetPatientActions.GetClinicalTagsMetaUi',
			// 	function(obj) {
			// 		_getPatientClinicalTagsUI(obj);
			// 	}
			// );

			// PubSub.subscribe(
			// 	'PatientView.GetPatientActions.PatientTasks.Raise',
			// 	function(obj) {
			// 		var handle_memberSelect_template = function(
			// 				includeLastOnline,
			// 				data
			// 			) {
			// 				data._includeLastOnline = includeLastOnline;

			// 				if (data.disabled) return data;
			// 				if (!data.name) return null;

			// 				var hbTemplate =
			// 						CareflowApp.Templates[
			// 							'Content/js/handlebarsTemplates/PatientView__raiseTaskModal_member.htm'
			// 						],
			// 					htmlContent = hbTemplate(data);

			// 				return $(htmlContent);
			// 			},
			// 			prepare_memberSelect_data = function(response) {
			// 				var passedData = response.Data.Passed,
			// 					currentUserActorId = Vault.getItem(
			// 						'GetRequestingUserSummary'
			// 					).Data.ActorID,
			// 					processedData = passedData.map(function(item) {
			// 						var templateData = {
			// 							id: item.ActorId,
			// 							name:
			// 								item.FirstName +
			// 								' ' +
			// 								item.LastName,
			// 							lastOnline: item.LastOnline, //new Date(item.LastOnline).toLocaleString(),
			// 							profession: item.Profession,
			// 							image: item.ProfileImageLink,
			// 							text:
			// 								item.FirstName +
			// 								' ' +
			// 								item.LastName, //used for search
			// 							duty: item.OnDuty
			// 						};

			// 						if (item.ActorId === currentUserActorId) {
			// 							templateData.text = templateData.name =
			// 								'Assign to me';
			// 						}

			// 						return templateData;
			// 					}),
			// 					sorter = function(data) {
			// 						return _(data)
			// 							.chain()
			// 							.sortBy(function(item) {
			// 								//sort by name
			// 								return item.text;
			// 							})
			// 							.sortBy(function(item) {
			// 								//sort by last online time
			// 								return (
			// 									new Date(
			// 										item.lastOnline
			// 									).getTime() * -1
			// 								); //reverse order
			// 							})
			// 							.sortBy(function(item) {
			// 								//sort by duty
			// 								return !item.duty;
			// 							})
			// 							.sortBy(function(item) {
			// 								//list option 'Assign to me' to the top
			// 								return item.text !== 'Assign to me';
			// 							})
			// 							.value();
			// 					};

			// 				$('.js-memberSelect')
			// 					.select2({
			// 						width: '75%',
			// 						data: processedData,
			// 						templateSelection: handle_memberSelect_template.bind(
			// 							handle_memberSelect_template,
			// 							false
			// 						),
			// 						templateResult: handle_memberSelect_template.bind(
			// 							handle_memberSelect_template,
			// 							true
			// 						),
			// 						allowClear: true,
			// 						sorter: sorter
			// 					})
			// 					.val('')
			// 					.trigger('change');

			// 				appMain.formatTemplateDates();

			// 				if (passedData.length === 0) {
			// 					appMain.triggerAppNotification({
			// 						Text: 'No members for selected team',
			// 						Heading: '',
			// 						Icon: 'warning'
			// 					});
			// 				}
			// 			},
			// 			template = {
			// 				path:
			// 					'Content/js/handlebarsTemplates/PatientView__raiseTaskModal.htm',
			// 				data: {
			// 					PatientDetails: {},
			// 					Tasks: [],
			// 					Teams: []
			// 				}
			// 			},
			// 			handle_modal_afterContent = function(e) {
			// 				var domContent = this.$instance;

			// 				domContent
			// 					.find('select')
			// 					.select2({
			// 						width: '75%'
			// 					}) //initialize select2 instance for every select in form
			// 					.on('select2:unselect', function(e) {
			// 						//prevent reopen when clear button clicked
			// 						var self = $(this);

			// 						self.data('select2').options.set(
			// 							'disabled',
			// 							true
			// 						);

			// 						setTimeout(function() {
			// 							self.data('select2').options.set(
			// 								'disabled',
			// 								false
			// 							);
			// 						}, 0);
			// 					})
			// 					.on('select2:select', function(e) {
			// 						$(this).trigger('input'); //for parsley trigger
			// 					});

			// 				var activeGroupMemberAreaId = Vault.getSessionItem(
			// 						'ActiveGroupMemberAreaId'
			// 					),
			// 					activeGroup = template.data.Teams.filter(
			// 						function(team) {
			// 							return (
			// 								team.MemberAreaId ===
			// 								activeGroupMemberAreaId
			// 							);
			// 						}
			// 					)[0];

			// 				domContent
			// 					.find('.js-teamSelect')
			// 					.on('change', function(e) {
			// 						var container_memberSelect = domContent
			// 								.find('.js-memberSelect')
			// 								.parent()
			// 								.find('.select2'),
			// 							accessGroupId = $(this).val();

			// 						if (!accessGroupId) return;

			// 						//show loading
			// 						appMain.ajaxLoadMask({
			// 							ContainerDomObject: container_memberSelect
			// 						});

			// 						GetUsersProfiles.callApi(accessGroupId)
			// 							.then(prepare_memberSelect_data)
			// 							.then(function() {
			// 								//hide loading
			// 								appMain.ajaxLoadUnMask({
			// 									ContainerDomObject: container_memberSelect
			// 								});
			// 							});
			// 					})
			// 					.val(
			// 						activeGroup ? activeGroup.AccessGroupId : ''
			// 					)
			// 					.trigger('change'); //update members if active group is preselected

			// 				domContent
			// 					.find('[data-component-tickbox]')
			// 					.cfTickbox();

			// 				domContent
			// 					.find('.js-raiseTaskbutton')
			// 					.on('click', function(e) {
			// 						if (
			// 							!domContent
			// 								.find('.js-createTaskForm')
			// 								.parsley()
			// 								.validate()
			// 						)
			// 							return;

			// 						var team = domContent
			// 								.find('.js-teamSelect')
			// 								.val(),
			// 							member = domContent
			// 								.find('.js-memberSelect')
			// 								.val(),
			// 							notes = domContent
			// 								.find('.js-notesTextarea')
			// 								.val(),
			// 							typeCode = domContent
			// 								.find('.js-typeSelect')
			// 								.val(),
			// 							typeDescription = domContent
			// 								.find('.js-typeSelect')
			// 								.find('option:selected')
			// 								.text(),
			// 							urgent = domContent
			// 								.find('[data-component-tickbox]')
			// 								.data('isTicked');

			// 						$(this).ajaxActiveButtonUi({
			// 							DisableContainerInputs: $(
			// 								'.js-createTaskForm'
			// 							)
			// 						});

			// 						AddUserTask.callApi(
			// 							team,
			// 							member,
			// 							notes,
			// 							obj.PatientId,
			// 							typeCode,
			// 							typeDescription,
			// 							urgent
			// 						)
			// 							.then(function(response) {
			// 								appMain.triggerAppNotification({
			// 									Text:
			// 										'Task raised successfully',
			// 									Heading: 'Task raised',
			// 									Icon: 'success'
			// 								});

			// 								PubSub.publish(
			// 									'Tasks.NewPatientTaskRaised',
			// 									{
			// 										PatientId: obj.PatientId,
			// 										ContentItemId:
			// 											response.Data
			// 												.ContentItemID
			// 									}
			// 								);

			// 								/*Broadcast to Vue.js components*/
			// 								return setTimeout(() => {
			// 									VueEventBus.$emit(
			// 										'Tasks.NewPatientTaskRaised',
			// 										{
			// 											ApiResponse: response,
			// 											Team: team,
			// 											Member: member,
			// 											Notes: notes,
			// 											PatientId:
			// 												obj.PatientId,
			// 											TypeCode: typeCode,
			// 											TypeDescription: typeDescription,
			// 											Urgent: urgent
			// 										}
			// 									);
			// 								}, 2000); //Allow raven to Sync so delay
			// 							})
			// 							.fail(function(response) {
			// 								appMain.triggerAppNotification({
			// 									Text:
			// 										'The task could not be raised, please try again',
			// 									Heading: 'There was a issue',
			// 									Icon: 'error'
			// 								});
			// 							})
			// 							.always(function() {
			// 								domContent.data(
			// 									'warningClose',
			// 									false
			// 								); //cancel next prompt for save

			// 								$.featherlight.close();
			// 							});
			// 					});

			// 				domContent.data('warningClose', true); //prompt a confirmation when closing

			// 				$.proxy(
			// 					$.featherlight.defaults.afterContent,
			// 					this,
			// 					e
			// 				)();
			// 			};

			// 		var patientDataSet = $.Deferred();

			// 		if (obj.PatientData) {
			// 			patientDataSet.resolve(obj.PatientData);
			// 		} else if (getCachedPatient()) {
			// 			patientDataSet.resolve(getCachedPatient());
			// 		} else {
			// 			patientView
			// 				.getPatient(
			// 					obj.PatientId,
			// 					false,
			// 					obj.NoCachePatient
			// 				)
			// 				.then(function(response) {
			// 					patientDataSet.resolve(response.Data);
			// 				});
			// 		}

			// 		patientDataSet
			// 			.then(function(response) {
			// 				var data = response,
			// 					activeNetworkId = Vault.getSessionItem(
			// 						'ActiveNetworkId'
			// 					);

			// 				template.data.PatientDetails = data;

			// 				return TasksMetaData.callApi(activeNetworkId);
			// 			})
			// 			.then(function(response) {
			// 				var data = response.Data;

			// 				template.data.Tasks = data.Types;
			// 				template.data.Teams = data.GroupsAllowingTasks;

			// 				if (!data.GroupsAllowingTasks.length) {
			// 					appMain.triggerAppNotification({
			// 						Text:
			// 							'There are no available teams for your network',
			// 						Heading: '',
			// 						Icon: 'warning'
			// 					});
			// 				}

			// 				return GetHandlebarsTemplate.callApi(
			// 					template.path,
			// 					null,
			// 					template.data
			// 				);
			// 			})
			// 			.then(function(htmlContent) {
			// 				if ($.featherlight.current()) {
			// 					$.featherlight.close(); //Close any active modals
			// 				}

			// 				$.featherlight(htmlContent, {
			// 					afterContent: handle_modal_afterContent,
			// 					beforeClose: function(e) {
			// 						if (
			// 							$.featherlight
			// 								.current()
			// 								.$instance.data('warningClose') ===
			// 							false
			// 						)
			// 							return; //no need to prompt for save

			// 						$.proxy(
			// 							$.featherlight.defaults.beforeClose,
			// 							this,
			// 							e
			// 						)(e);

			// 						var message = $('<p />', {
			// 								text:
			// 									'Are you sure you want to leave without saving your changes?',
			// 								class: ''
			// 							}),
			// 							ok = $('<button />', {
			// 								text: 'Leave, discard changes',
			// 								class:
			// 									'btn--plain--block-pad u-pull-right js-prompt-confirm'
			// 							}),
			// 							cancel = $('<button/>', {
			// 								text: 'Keep editing',
			// 								class:
			// 									'btn u-pull-left js-prompt-cancel'
			// 							});

			// 						appMain.drawPromptConfirmDialog(
			// 							message.add(ok).add(cancel),
			// 							'Changes to raise task will be lost',
			// 							function(event, api) {
			// 								$('.js-prompt-confirm').on(
			// 									'click',
			// 									function(e) {
			// 										$.featherlight
			// 											.current()
			// 											.$instance.data(
			// 												'warningClose',
			// 												false
			// 											); //cancel next prompt for save
			// 										api.destroy(e);
			// 										$.featherlight.close();
			// 									}
			// 								);

			// 								$('.js-prompt-cancel').on(
			// 									'click',
			// 									function(e) {
			// 										api.destroy(e);
			// 									}
			// 								);
			// 							}
			// 						);

			// 						return false; //prevent close
			// 					}
			// 				});
			// 			})
			// 			.done(GetHandlebarsTemplate.onTemplateInserted);
			// 	}
			// );

			PubSub.subscribe('PatientView.GetPatientObsSummary', function(obj) {
				/*Vital pac - DEMO*/
				_getPatientObs(obj);
			});

			PubSub.subscribe('PatientView.GetPatientResultsSummary', function(
				obj
			) {
				_getPatientResults(obj);
			});

			PubSub.subscribe(
				// 'PatientView.GetPatientSummaryFeedItemsMinimal',
				// function(obj) {
				// 	//Get patient feed widget for the summary page widget
				// 	_getpatientFeedItemsMinimal(obj);
				// }
			);
		})();

		//Shared
		var _isPatientView; //Patient is loaded on a PatientView route view
		var _patientId;
		var _patientData;

		var viewCleanup = function() {
			_isPatientView = false;
			_patientId = null;
			_patientData = null;
		};

		var getCachedPatient = function() {
			//Public API to return cached patient
			if (_patientData) {
				return _patientData;
			}
			return false;
		};

		var setCachedPatient = function(patientData) {
			//Public API to set cached patient
			if (patientData) return (_patientData = patientData);
			return console.error('Patient data not set');
		};

		var deleteCachedPatient = function() {
			//Public API to delete cached patient data
			if (_patientData) return (_patientData = null);
			return false;
		};

		var getPatient = function(patientId, isPatientView, doNotCachePatient) {
			//Load the patient and check whether the patient is loaded in feed or in patient view context (PatientRecord - /#/Patient/)
			return $.Deferred(function() {
				var self = this;

				if (!patientId) {
					self.reject();
				}

				if (_patientData && typeof _patientData === 'object') {
					if (
						patientId ===
						_patientData.Data.PatientExternalIdentifier
					) {
						return self.resolve(_patientData); //Patient requested is cached already, so return it
					}
				}

				_patientId = patientId;
				_isPatientView = isPatientView ? true : false; //Is a dedicated patient record view (TODO: deprecate - old code logic)

				var includeParameters =
					'ClinicalTags, HandoverNotes, GroupLists';

				GetPatient.callApi(
					patientId,
					Vault.getSessionItem('ActiveNetworkId'),
					includeParameters
				)
					.then(function(response) {
						if (
							typeof doNotCachePatient === 'boolean' &&
							doNotCachePatient === true
						) {
							//TODO: Option to load patient adHoc and just return data, do not cache it
							return self.resolve(response);
						}
						_patientData = response;
						self.resolve(response);
					})
					.fail(function(error) {
						self.reject(error);
					});
			});
		};

		// var _updateHandoverNotesViewModel = function(
		// 	situationBody,
		// 	backgroundBody,
		// 	assessmentBody,
		// 	recommendationBody,
		// 	teamHandoverSentFrom,
		// 	teamHandoverSentFromId
		// ) {
		// 	//Where there is a single cached patient, not a cahed patient list
		// 	if (!getCachedPatient())
		// 		return console.error('No patient to update handover for'); //No cached patient

		// 	var patientData = getCachedPatient();

		// 	return $.Deferred(function() {
		// 		var self = this;
		// 		var dateLastUpdated = moment.utc().format();
		// 		//Update the handover notes View model
		// 		var handoverNotes = {
		// 			GroupName: teamHandoverSentFrom,
		// 			MemberAreaId: parseInt(teamHandoverSentFromId),
		// 			DateLastUpdated: dateLastUpdated,
		// 			DaysSinceLastUpdate: 0,
		// 			Notes: [
		// 				{
		// 					Key: 'Situation',
		// 					Position: 0,
		// 					Value: situationBody
		// 				},
		// 				{
		// 					Key: 'Background',
		// 					Position: 1,
		// 					Value: backgroundBody
		// 				},
		// 				{
		// 					Key: 'Assessment',
		// 					Position: 2,
		// 					Value: assessmentBody
		// 				},
		// 				{
		// 					Key: 'Recommendation',
		// 					Position: 3,
		// 					Value: recommendationBody
		// 				}
		// 			],
		// 			UpdatedByName: Vault.getItem('LoggedInUserName'),
		// 			IsCurrent: true,
		// 			Scope: 'Team'
		// 		};

		// 		//Update the local models Handover array
		// 		if (
		// 			patientData.Data.HandoverNotes !== typeof 'undefined' &&
		// 			_.isArray(patientData.Data.HandoverNotes)
		// 		) {
		// 			//If there is a matching team handover, set current to false
		// 			patientData.Data.HandoverNotes.forEach(function(item) {
		// 				if (
		// 					item.MemberAreaId ===
		// 					parseInt(teamHandoverSentFromId)
		// 				) {
		// 					item.IsCurrent = false;
		// 				}
		// 			});

		// 			patientData.Data.HandoverNotes.unshift(handoverNotes); //add new note to beginning of array
		// 			//patientData.Data.HandoverNotes = [handoverNotes];
		// 		} else {
		// 			patientData.Data.HandoverNotes = [handoverNotes];
		// 		}

		// 		setCachedPatient(patientData); //Passes by reference anyhow

		// 		console.log('****^^^', getCachedPatient());
		// 		self.resolve();
		// 	});
		// };

		/*Feeds*/

		//var _parseFeedItemData = function (loadEarlierItems, skip, take, patientId, context) { //Parse Patient view feed data. Seperate from content feeds (Feeds module) due to API and payload structure. Patient feed is skip / take, team feeds use updatedBefore property, so VERY dofferent
		//    return new $.Deferred(function () {
		//        var result = this;

		//        GetPatientFeedItems.callApi(patientId, Vault.getSessionItem("ActiveNetworkId"), skip, take).then(function (data, textStatus, jqXHr) {
		//            //Add day / month to object for grouping
		//            data.Data.FeedItems.forEach(function (item) {
		//                var sortByDate = moment(item.LastCommentDate, 'YYYY-MM-DD');
		//                var month = sortByDate.format('MMM');
		//                var day = sortByDate.format('D');
		//                var year = sortByDate.format('YYYY');

		//                item.DayOfLastComment = day + "-" + month + "-" + year;
		//                item.ConversationTrackableItemId = item.ContentItemID; //Add property to match Group feeds data structure in template (FeedBody__list.htm)

		//                //Get the latest 3 comments
		//                item.LatestThreeComments = null;

		//                if (item.Comments.length > 2) { //Only populate if the item has more than 3 comments
		//                    var latestThreeComments = _.clone(item.Comments); //Arrays passed by value, not reference
		//                    var len = latestThreeComments.length;
		//                    var start = len - 3;
		//                    var commentData = latestThreeComments.splice(start, len);
		//                    item.LatestThreeComments = commentData;
		//                    item.Comments.length = 0; //clear the existing comments array as no longer used in this object
		//                }

		//                //Add memberAreaID for template, API does not yet supply
		//                item.MemberAreaID = appMain.getMemberAreaName(null, null, true, item.ContentItemAccessGroupID)["MemberAreaID"];
		//            });

		//            var patientFeedItemsGroupedbyLastCommentDate = _.groupBy(data.Data.FeedItems, function (dateSorted) { //Group by day
		//                return dateSorted.DayOfLastComment;
		//            });

		//            var templateData = {
		//                Data: [] //the data to be pushed to template
		//            };

		//            if (!_.isEmpty(patientFeedItemsGroupedbyLastCommentDate)) {
		//                $.each(patientFeedItemsGroupedbyLastCommentDate, function (key, val) { //TODO add forEach refactor
		//                    var dayOfLastComment;
		//                    $.each(val, function () {
		//                        dayOfLastComment = this.DayOfLastComment;
		//                    });
		//                    templateData.Data.push({
		//                        DayOfLastComment: dayOfLastComment,
		//                        FeedItems: val, //The data
		//                        DataContext: context,
		//                        FeedItemPartialData: {
		//                            //Object of useful data to pass to partial
		//                            DataContext: context,
		//                            ActiveNetworkId: Vault.getSessionItem("ActiveNetworkId"),
		//                            RenderPatientBannerInFeed: false
		//                        },
		//                    });
		//                });
		//            } else {
		//                templateData.Data.push({
		//                    DayOfLastComment: null,
		//                    FeedItems: [], //The data
		//                    DataContext: context,
		//                    FeedItemPartialData: {
		//                        //Object of useful data to pass to partial
		//                        DataContext: context,
		//                        ActiveNetworkId: Vault.getSessionItem("ActiveNetworkId"),
		//                        RenderPatientBannerInFeed: false
		//                    },
		//                });
		//            }
		//            PubSub.publish("PatientView.ParseFeedItemData.Done", { Data: data });
		//            result.resolve(templateData, data.Data.ThereAreMore);
		//        }).fail();
		//    });
		//};

		//var _getPatientFeedItems = function (patientId, skip, take, $domSelectorToInsertTemplateTo, contentIdToactivate) { //Get data and build UI
		//    if (!patientId || !$domSelectorToInsertTemplateTo) { //no patientId, bail out
		//        window.location.replace("/NetworkHome");
		//    }

		//    appMain.ajaxLoadingHelper({
		//        DomElement: $domSelectorToInsertTemplateTo,
		//        LoaderMessage: "Loading all patient updates",
		//        DisableNoInputs: true
		//    });

		//    return $.Deferred(function () {
		//        var result = this;
		//        var patientFeedSkip = (skip) ? skip : 0;
		//        var patientFeedTake = (take) ? take : 25;
		//        var templatePath = "Content/js/handlebarsTemplates/FeedBody__list.htm";

		//        $.when(_parseFeedItemData(false, patientFeedSkip, patientFeedTake, patientId, "PatientFeed")).then(function (data, thereAreMore) {
		//            GetHandlebarsTemplate.callApi(templatePath, $domSelectorToInsertTemplateTo, data).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
		//                //feeds.getCreateFeedContentControls(null, 4, "PatientFeed", _patientData.Data, false);

		//                if (thereAreMore) patientFeedSkip += patientFeedTake; //Update skip and take to load earlier items
		//                feeds.getFeedEventBindings({
		//                    IsMockItem: false,
		//                    IsPatientFeed: true,
		//                    RenderPatientBannerInFeed: true, //data.Data[0].FeedItemPartialData.RenderPatientBannerInFeed,
		//                    PatientId: patientId,
		//                    MemberAreaId: null,
		//                    MemberAreaName: null,
		//                    LatestFeedItemOrderByDate: null, //Patient feed is skip and take
		//                    Skip: patientFeedSkip,
		//                    Take: patientFeedTake,
		//                    DomSelectorToInsertFeedTemplateTo: $domSelectorToInsertTemplateTo,
		//                    QtipContainer: $(".js-feedBody-wrapper"),
		//                    QtipViewport: $(".js-feedBody-wrapper"),
		//                    ThereAreMoreFeedItems: thereAreMore
		//                });

		//                feeds.setLastLoadedFeedType("PatientFeed");

		//                //If a content ID passed
		//                if (contentIdToactivate) {
		//                    var $feedItem = $domSelectorToInsertTemplateTo.find($(".js-feedBody--item[data-contentid=" + contentIdToactivate + "]"));

		//                    $feedItem.scrollintoview({ bottomPushPixels: 200 });
		//                    $feedItem.triggerHandler("click");
		//                }

		//                data = undefined;//delete data;
		//                result.resolve(thereAreMore);
		//            }).catch(function () { });
		//        });
		//    });
		//};

		//var _getEarlierPatientFeedItems = function (obj) { //Load earlier feed items into UI, triggered on scroll to base via a Waypoint
		//    return new $.Deferred(function () {
		//        var self = this;

		//        $.when(_parseFeedItemData(true, obj.Skip, obj.Take, obj.PatientId, obj.DataContext || "PatientFeed")).then(function (data, thereAreMore) { //Set context for patien
		//            if (!!data.Data.FeedItems) { //No feed items returned
		//                obj.DomElement.attr("data-feed-end", true);
		//                self.resolve();
		//                return;
		//            }

		//            data.IsFirstLoadOfFeed = obj.IsFirstLoadOfFeed;
		//            GetHandlebarsTemplate.callApi(obj.TemplatePath, obj.DomElementToInsertTemplateTo, data, "append").done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
		//                if (!thereAreMore) {
		//                    obj.DomElement.attr("data-feed-end", true);
		//                } else {
		//                    obj.Skip += obj.Take; //Update skip and take to load earlier items
		//                }

		//                feeds.getFeedEventBindings({
		//                    DomWrapper: obj.DomWrapper,
		//                    IsMockItem: false,
		//                    IsPatientFeed: true, //No patient banners in Patient Feed, so dont bind patient 'banner' events
		//                    IsFirstLoadOfFeed: false,
		//                    DataContext: obj.DataContext || "PatientFeed",
		//                    MemberAreaId: null,
		//                    MemberAreaName: null, //All team updates
		//                    LatestFeedItemOrderByDate: null, //Patient feed is skip and take
		//                    Skip: obj.Skip,
		//                    Take: obj.Take,
		//                    FeedItemsLoadMore: {
		//                        WaypointTrigger: obj.WaypointTrigger,
		//                        WaypointContext: obj.WaypointContext,
		//                        PatientSpecificFeed: {
		//                            DomElementToInsertEarlierItemsTo: obj.DomElementToInsertTemplateTo,
		//                            TemplatePath: obj.TemplatePath
		//                        },
		//                    },
		//                    PatientId: obj.PatientId,
		//                    ThereAreMoreFeedItems: thereAreMore,
		//                    RenderPatientBannerInFeed: obj.RenderPatientBannerInFeed,
		//                    QtipContainer: obj.QtipContainer,
		//                    QtipViewport: obj.QtipViewport,
		//                });

		//                NProgress.done();
		//                self.resolve(thereAreMore);
		//            });
		//        });
		//    });
		//};

		// var _getpatientFeedItemsMinimal = function(obj) {
		// 	//Widget on Patient Dashboard to get stripped down, latest 5 only
		// 	GetPatientFeedItems.callApi(
		// 		obj.PatientId,
		// 		Vault.getSessionItem('ActiveNetworkId'),
		// 		obj.Skip,
		// 		obj.Take
		// 	).then(function(response, textStatus, jqXHr) {
		// 		//Build widget UI
		// 		GetHandlebarsTemplate.callApi(
		// 			'Content/js/handlebarsTemplates/GroupHome__PatientView__patient-feed-widget.htm',
		// 			obj.DomContainer,
		// 			{
		// 				Data: response.Data.FeedItems,
		// 				ThereAreMore: response.Data.ThereAreMore
		// 			}
		// 		)
		// 			.then(GetHandlebarsTemplate.onTemplateInserted)
		// 			.then(function() {
		// 				//Bind to main Updates view on patient feed
		// 				$('.js-patient-updates-title').on('click', function(e) {
		// 					var $this = $(this);
		// 					var contentId = $this.data('content-id');
		// 					if (contentId) {
		// 						//Append the data ID so the link can consume it in the view
		// 						$(
		// 							".js-teamPatient-href[data-viewurl='patient-updates']"
		// 						)
		// 							.data('trigger-content-id', contentId)
		// 							.triggerHandler('click');
		// 					}
		// 					return;
		// 				});
		// 				//Bind View All link
		// 				$('.js-patient-updates-view-all').on('click', function(
		// 					e
		// 				) {
		// 					$(
		// 						".js-teamPatient-href[data-viewurl='patient-updates']"
		// 					).triggerHandler('click');
		// 				});
		// 			})
		// 			.fail(function(error) {});
		// 	});
		// };

		// var _addPatientClinicalTag = function(options) {
		// 	/*Structure of tags passed
        // var ClinicalTagIds = {
        //     TagIds: [],
        //     TagMeta: [{
        //         TagId: tagId,
        //         Name: tagName
        //      }]
        // };*/

		// 	var obj = {
		// 		MemberAreaId: null,
		// 		PatientId: null,
		// 		ClinicalTagIds: []
		// 	};

		// 	$.extend(obj, options);

		// 	var parameters = {
		// 		Added: obj.ClinicalTagIds.TagIds,
		// 		Removed: []
		// 	};
		// 	return $.Deferred(function() {
		// 		var self = this;
		// 		ClinicalTags.callApi(
		// 			obj.MemberAreaId,
		// 			obj.PatientId,
		// 			parameters
		// 		).then(function(data, textStatus, jQXHr) {
		// 			//API call
		// 			if (jQXHr.status === 200) {
		// 				//All added
		// 				var countOfTagsAdded = 0;

		// 				//For every tag added
		// 				obj.ClinicalTagIds.TagIds.forEach(function(item) {
		// 					var addedTagId = item;
		// 					var tagName;

		// 					obj.ClinicalTagIds.TagMeta.forEach(function(
		// 						metaItem
		// 					) {
		// 						if (metaItem.TagId === addedTagId) {
		// 							tagName = metaItem.Name;
		// 						}
		// 					});

		// 					data.Messages.forEach(function(message) {
		// 						if (
		// 							message.Entity.ID === addedTagId &&
		// 							message.Code === 0
		// 						) {
		// 							//Tag added with Code 0 - success
		// 							PubSub.publish(
		// 								'PatientView.GetPatientActions.PatientTags.TagAdded',
		// 								{
		// 									//Publish for each tag added
		// 									Message: data,
		// 									MessageString: message.Message,
		// 									MemberAreaId: obj.MemberAreaId,
		// 									ClinicalTagId: addedTagId,
		// 									ClinicalTagName: tagName,
		// 									PatientId: obj.PatientId,
		// 									DataContext: obj.DataContext
		// 								}
		// 							);

		// 							/*Broadcast to Vue.js components*/
		// 							VueEventBus.$emit(
		// 								'PatientView.GetPatientActions.PatientTags.TagAdded',
		// 								{
		// 									//Publish for each tag added
		// 									Message: data,
		// 									MessageString: message.Message,
		// 									MemberAreaId: obj.MemberAreaId,
		// 									ClinicalTagId: addedTagId,
		// 									ClinicalTagName: tagName,
		// 									PatientId: obj.PatientId,
		// 									DataContext: obj.DataContext
		// 								}
		// 							);
		// 						} else if (
		// 							message.Entity.ID === addedTagId &&
		// 							data.Messages.Code === 1
		// 						) {
		// 							//1 = Clinical tag was previously added to the patient, warn user

		// 							appMain.triggerAppNotification({
		// 								Text:
		// 									tagName +
		// 									'  was previously added to the patient in ' +
		// 									appMain.getMemberAreaName(
		// 										obj.MemberAreaId
		// 									),
		// 								Heading: 'Clinical Tag not added',
		// 								Icon: 'warning'
		// 							});
		// 						}
		// 					});

		// 					countOfTagsAdded++;

		// 					if (
		// 						countOfTagsAdded ===
		// 						obj.ClinicalTagIds.TagIds.length
		// 					) {
		// 						PubSub.publish(
		// 							'PatientView.GetPatientActions.PatientTags.AllQueuedTagsAdded',
		// 							{
		// 								//Publish for last tag in array
		// 								Message: data,
		// 								MemberAreaId: obj.MemberAreaId,
		// 								ClinicalTagId: addedTagId,
		// 								ClinicalTagName: tagName,
		// 								PatientId: obj.PatientId,
		// 								DataContext: obj.DataContext
		// 							}
		// 						);
		// 						//Tell Vue components
		// 						VueEventBus.$emit(
		// 							'PatientView.GetPatientActions.PatientTags.AllQueuedTagsAdded'
		// 						);
		// 					}
		// 				});

		// 				self.resolve(data, textStatus, jQXHr);
		// 			} else {
		// 				self.reject(data, textStatus, jQXHr);
		// 			}
		// 		});
		// 	});
		// };

		// var _removePatientClinicalTag = function(obj) {
		// 	//Tags are removed inline
		// 	var patientId = obj.PatientId;
		// 	var clinicalTagId = parseInt(obj.ClinicalTagId, 10);
		// 	var memberAreaId = parseInt(obj.MemberAreaId);

		// 	var reasonForRemovingTagId =
		// 		obj.ReasonForRemovingTagID == '-1'
		// 			? null
		// 			: parseInt(obj.ReasonForRemovingTagID, 10);
		// 	var customReasonForRemovingTag = !reasonForRemovingTagId
		// 		? $(
		// 				'.js-removeClinicalTag-custom-reason-txt[data-patientid=' +
		// 					patientId +
		// 					'][data-clinicaltagid=' +
		// 					clinicalTagId +
		// 					'][data-memberareaid=' +
		// 					memberAreaId +
		// 					']'
		// 		  ).val()
		// 		: '';

		// 	var parameters = {
		// 		Added: [],
		// 		Removed: [
		// 			{
		// 				ClinicalTagID: clinicalTagId,
		// 				CustomReasonForRemovingTag: customReasonForRemovingTag,
		// 				ReasonForRemovingTagID: reasonForRemovingTagId
		// 			}
		// 		]
		// 	};

		// 	return $.Deferred(function() {
		// 		var self = this;

		// 		ClinicalTags.callApi(memberAreaId, patientId, parameters).then(
		// 			function(data, textStatus, jqXHr) {
		// 				if (jqXHr.status === 200) {
		// 					PubSub.publish(
		// 						'PatientView.GetPatientActions.PatientTags.TagRemoved',
		// 						{
		// 							Message: data,
		// 							MemberAreaId: memberAreaId,
		// 							ClinicalTagId: clinicalTagId,
		// 							PatientId: patientId,
		// 							Parameters: parameters,
		// 							Event: null,
		// 							DomObject: null,
		// 							DataContext: obj.DataContext,
		// 							QtipViewport: obj.QtipViewport,
		// 							QtipContainer: obj.QtipContainer
		// 						}
		// 					);

		// 					/*Broadcast to Vue.js components*/
		// 					VueEventBus.$emit(
		// 						'PatientView.GetPatientActions.PatientTags.TagRemoved',
		// 						{
		// 							Message: data,
		// 							MemberAreaId: memberAreaId,
		// 							ClinicalTagId: clinicalTagId,
		// 							PatientId: patientId,
		// 							Parameters: parameters,
		// 							Event: null,
		// 							DomObject: null,
		// 							DataContext: obj.DataContext,
		// 							QtipViewport: obj.QtipViewport,
		// 							QtipContainer: obj.QtipContainer
		// 						}
		// 					);

		// 					self.resolve();
		// 				} else {
		// 					self.reject();
		// 				}
		// 				//appMain.ajaxLoadingHelperComplete();
		// 				// appMain.closeAllqTips();
		// 			}
		// 		);
		// 	});
		// };

		// var _updatePatientClinicalTagsViewModelWithAddedTag = function(
		// 	clinicalTagId,
		// 	memberAreaId,
		// 	tagValue
		// ) {
		// 	return $.Deferred(function() {
		// 		var result = this;
		// 		var patientData = patientView.getCachedPatient();

		// 		if (!patientData) return false;

		// 		var dataCheck = _.where(patientData.Data.ClinicalTags, {
		// 			ID: parseInt(clinicalTagId, 10),
		// 			AddedByGroupID: parseInt(memberAreaId, 10)
		// 		});

		// 		if (!dataCheck.length) {
		// 			//Update the local patient model, add the tag, not already present
		// 			patientData.Data.ClinicalTags.push({
		// 				//array always returned in object, empty if no tags
		// 				AddedByActorName: Vault.getItem('LoggedInUserName'),
		// 				AddedByGroupID: parseInt(memberAreaId, 10),
		// 				AddedByGroupName: appMain.getMemberAreaName(
		// 					parseInt(memberAreaId, 10)
		// 				),
		// 				AddedDate: moment(moment())
		// 					.local()
		// 					.format(),
		// 				DaysSinceAdded: 0,
		// 				ID: parseInt(clinicalTagId, 10),
		// 				Value: tagValue
		// 			});

		// 			patientView.setCachedPatient(patientData); //Passes by reference anyhow
		// 			result.resolve();
		// 		}
		// 	});
		// };

		// var _updatePatientClinicalTagsViewModelWithRemovedTag = function(
		// 	clinicalTagId,
		// 	memberAreaWhereTagWasApplied,
		// 	tagName
		// ) {
		// 	//Update the local patient model and remove the clinical tag against the set memberAreaID.
		// 	return $.Deferred(function() {
		// 		var result = this;
		// 		var patientData = patientView.getCachedPatient();

		// 		if (!patientData) return false;

		// 		var dataCheck = _.where(patientData.Data.ClinicalTags, {
		// 			ID: parseInt(clinicalTagId, 10),
		// 			AddedByGroupID: parseInt(memberAreaWhereTagWasApplied, 10)
		// 		});

		// 		if (dataCheck.length) {
		// 			//Update the local patient model, remove the tag, is present
		// 			patientData.Data.ClinicalTags = _.without(
		// 				patientData.Data.ClinicalTags,
		// 				_.findWhere(patientData.Data.ClinicalTags, {
		// 					ID: parseInt(clinicalTagId, 10),
		// 					AddedByGroupID: parseInt(
		// 						memberAreaWhereTagWasApplied,
		// 						10
		// 					)
		// 				})
		// 			);

		// 			patientView.setCachedPatient(patientData); //Passes by reference anyhow
		// 			result.resolve();
		// 		}
		// 	});
		// };

		// var getClinicalTagsThatCanBeAppliedToAPatient = function(memberAreaId) {
		// 	if (!memberAreaId) return false;

		// 	return $.Deferred(function() {
		// 		var self = this;
		// 		$.when(
		// 			GroupClinicalTags.callApi(memberAreaId).then(function(
		// 				data,
		// 				textStatus,
		// 				jqXHr
		// 			) {
		// 				var clinicalTagsAvailable = {};
		// 				clinicalTagsAvailable.Data = [];

		// 				$.each(data.Data, function(key, val) {
		// 					if (this.IsSelected === true) {
		// 						clinicalTagsAvailable.Data.push(this);
		// 					}
		// 				});

		// 				self.resolve(clinicalTagsAvailable.Data); //Return built array
		// 			})
		// 		);
		// 	});
		// };

		// var getMemberAreasThatAUserCanHandoverFrom = function() {
		// 	//Get the logged in users member areas to check permissions for handover
		// 	if (!Vault.getItem('GetRequestingUserSummary')) return;

		// 	var loggedInUsersMemberAreasAndPermissions = Vault.getItem(
		// 		'GetRequestingUserSummary'
		// 	);

		// 	var memberAreasUserCanSendHandoverFrom = {
		// 		Data: []
		// 	};

		// 	loggedInUsersMemberAreasAndPermissions.Data.MemberAreasAndPermissions.forEach(
		// 		function(item) {
		// 			//Filter by teams context  only, exclude networks. Check to ensure that the user has permissions in this area to send a handover from. If not, don't populate area in ddl.
		// 			if (
		// 				item.IsNetwork === false &&
		// 				appMain.checkUserPermissionForMemberArea(
		// 					item.MemberAreaId,
		// 					'UpsertHandoverNote'
		// 				)
		// 			) {
		// 				memberAreasUserCanSendHandoverFrom.Data.push({
		// 					MemberAreaName: item.MemberAreaName,
		// 					MemberAreaId: item.MemberAreaId
		// 				});
		// 			}
		// 		}
		// 	);
		// 	return memberAreasUserCanSendHandoverFrom;
		// };

		// var getMemberAreasThatAUserCanManagePatientClinicalTagsFrom = function() {
		// 	/*Get groups that user can add tags to patient from*/
		// 	var returnData = [];

		// 	if (!Vault.getItem('GetRequestingUserSummary')) return returnData;

		// 	var loggedInUsersMemberAreasAndPermissions = Vault.getItem(
		// 		'GetRequestingUserSummary'
		// 	);

		// 	loggedInUsersMemberAreasAndPermissions.Data.MemberAreasAndPermissions.forEach(
		// 		function(item) {
		// 			//Filter by teams context only, exclude networks. Check to ensure that the user has permissions in this area. If not, don't return. Tags can only be removed by the user if they are a member of the group where the tag was applied to the patient.

		// 			if (
		// 				item.IsNetwork === false &&
		// 				appMain.checkUserPermissionForMemberArea(
		// 					item.MemberAreaId,
		// 					'ManagePatientClinicalTags'
		// 				)
		// 			) {
		// 				returnData.push({
		// 					MemberAreaName: item.MemberAreaName,
		// 					MemberAreaId: item.MemberAreaId,
		// 					CanManageTags: true
		// 				});
		// 			}
		// 		}
		// 	);

		// 	return returnData;
		// };

		var _getPatientAdditionalIdsUI = function(obj) {
			//Patient banner and lists additiional ID's UI

			var $self = obj.DomObject;
			if (typeof $self.timers === 'undefined' || !$self.timers)
				$self.timers = {};

			var text;

			if (_.has(obj, 'ContentId') && obj.ContentId) {
				//Set the text and determine if its in a content feed.
				text = $(
					'.js-patientbanner-additionalidentifiers[data-patientid=' +
						obj.PatientId +
						'][data-contentid=' +
						obj.ContentId +
						']'
				).clone();
			} else {
				text = $(
					'.js-patientbanner-additionalidentifiers[data-patientid=' +
						obj.PatientId +
						']'
				).clone();
			}

			appMain.drawClickToOpenPopoutBubble({
				DomObject: $self,
				Content: text,
				QtipViewport: obj.QtipViewport,
				QtipContainer: obj.QtipContainer,
				Classes: '.js-patient-additional-ids-tip',
				PositionMy: obj.QtipContainerMy
					? obj.QtipContainerMy
					: 'top right', // Position my top left...
				PositionAt: obj.QtipContainerAt
					? obj.QtipContainerAt
					: 'bottom right' // at the bottom right of...
			});
		};

		// var _getPatientClinicalTagsUI = function(obj) {
		// 	//Meta data popover
		// 	//Event:e,
		// 	//DataContext: obj.DataContext,
		// 	//PatientExternalIdentifier: patientId,
		// 	//ID: clinicalTagId,
		// 	//AddedByGroupID: clinicalTagAddedByGroupId,
		// 	//AddedByActorName: addedByActor,
		// 	//AddedDate: dateAdded

		// 	GetHandlebarsTemplate.callApi(
		// 		'Content/js/handlebarsTemplates/patientTags/PatientTag__metaData.htm',
		// 		null,
		// 		obj
		// 	).then(function(html) {
		// 		appMain.drawMouseEnteredPopoutBubble({
		// 			DomObject: $(obj.Event.currentTarget),
		// 			Content: $(html),
		// 			Classes: 'js-clinicaltag-meta-tip',
		// 			ShowCallBack: function() {
		// 				appMain.formatTemplateDates();
		// 			}
		// 		});
		// 	});
		// };

		var _addPatientToGroupList = function(obj) {
			return AddPatientToGroupList.callApi(
				obj.MemberAreaId,
				obj.PatientId
			).then(function (data, textStatus, jqXHr) {
				if (data.Code === 1) {
					// PubSub.publish(
					// 	'PatientView.GetPatientActions.PatientAddedToList',
					// 	{
					// 		Message: data,
					// 		MemberAreaId: obj.MemberAreaId,
					// 		PatientId: obj.PatientId
					// 	}
					// );
				}
			});
		};

		var _removePatientFromGroupList = function(obj) {
			return RemovePatientFromGroupList.callApi(
				obj.MemberAreaId,
				obj.PatientId
			).then(function(data, textStatus, jqXHr) {
				if (data.Code === 1) {
					// PubSub.publish(
					// 	'PatientView.GetPatientActions.PatientRemovedFromList',
					// 	{
					// 		Message: data,
					// 		MemberAreaId: obj.MemberAreaId,
					// 		PatientId: obj.PatientId
					// 	}
					// );
				}
			});
		};

		/*Global patient actions*/

		// var _patientContextActionsShowOptions = function(obj) {
		// 	var options = {
		// 		QtipContainerMy: 'top right',
		// 		QtipContainerAt: 'bottom right',
		// 		Tip: false,
		// 		ControlId: 3, //Default
		// 		IsGlobalActionsEvent: false,
		// 		Classes:
		// 			'patientContextActions__qTip js-patientContextActions-qTip',
		// 		DoNotCachePatient: false,
		// 		EdmsLinkIsEnabled: appMain.checkIfFeatureIsEnabledInAppConfig(
		// 			'enableEdmsLink'
		// 		),
		// 		MediViewerLinkIsEnabled: appMain.checkIfFeatureIsEnabledInAppConfig(
		// 			'enableMediViewerLink'
		// 		),
		// 		AspryaLinkIsEnabled: appMain.checkIfFeatureIsEnabledInAppConfig(
		// 			'enableAspyraLink'
		// 		)
		// 	};
		// 	$.extend(options, obj);

		// 	//Lock the element prior to API call
		// 	options.DomObject.prop('disabled', true);

		// 	//Get modal content
		// 	var templatePath =
		// 		'Content/js/handlebarsTemplates/PatientView__contextActionsModal.htm';
		// 	$.when(
		// 		GetHandlebarsTemplate.callApi(templatePath, false, options)
		// 	).done(function(html) {
		// 		appMain.drawClickToOpenPopoutBubble({
		// 			DomObject: options.DomObject,
		// 			Content: html,
		// 			Classes: options.Classes,
		// 			Tip: options.Tip,
		// 			PositionMy: options.QtipContainerMy,
		// 			PositionAt: options.QtipContainerAt,
		// 			QtipViewport: obj.QtipViewport,
		// 			QtipContainer: obj.QtipContainer,
		// 			ShowCallBack: function(api) {
		// 				//Show loading state prior to loading patient
		// 				appMain.ajaxLoadMask({
		// 					ContainerDomObject: $(
		// 						'.js-patientContextActions-qTip'
		// 					)
		// 				});

		// 				//Publish to listeners that the Patient Actions list has rendered in Ui
		// 				PubSub.publish(
		// 					'PatientView.GetPatientActions.ActionsUiRendered',
		// 					{
		// 						PatientId: obj.PatientId,
		// 						DataContext: obj.DataContext
		// 					}
		// 				);

		// 				//Add active state to trigger and unLock the element
		// 				options.DomObject.addClass(
		// 					'patientContextActions__launchModal-btn--active'
		// 				).prop('disabled', false);

		// 				//Load the patient
		// 				$.when(
		// 					patientView
		// 						.getPatient(
		// 							obj.PatientId,
		// 							false,
		// 							obj.DoNotCachePatient
		// 						)
		// 						.then(function(data) {
		// 							//Handle non cached / cached patient data.  _patientData set by getPatient() if cached
		// 							var nonCachedPatientData = null;
		// 							if (obj.DoNotCachePatient) {
		// 								nonCachedPatientData = data;
		// 							}

		// 							appMain.ajaxLoadUnMask({
		// 								ContainerDomObject: $(
		// 									'.js-patientContextActions-qTip'
		// 								)
		// 							});

		// 							//Bindings
		// 							$('.js-patientContextActions-refer-patient')
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									PubSub.publish(
		// 										'PatientView.GetPatientActions.Referral',
		// 										{
		// 											PatientData: obj.DoNotCachePatient
		// 												? nonCachedPatientData.Data
		// 												: _patientData.Data,
		// 											PatientId: obj.PatientId,
		// 											ContentId: obj.ContentId,
		// 											Event: e,
		// 											DomObject: $(this),
		// 											DataContext:
		// 												obj.DataContext,
		// 											RenderPatientBannerInFeed:
		// 												feeds.getLastLoadedFeedType() ===
		// 												'PatientFeed'
		// 													? false
		// 													: true,
		// 											IsMemberAreaContext:
		// 												obj.IsMemberAreaContext,
		// 											MemberAreaId:
		// 												obj.MemberAreaId,
		// 											Permissions: obj.Permissions
		// 										}
		// 									);
		// 									api.hide();
		// 									return false;
		// 								});

		// 							$('.js-patientContextActions-post-update')
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									PubSub.publish(
		// 										'PatientView.GetPatientActions.Update',
		// 										{
		// 											PatientData: obj.DoNotCachePatient
		// 												? nonCachedPatientData.Data
		// 												: _patientData.Data,
		// 											PatientId: obj.PatientId,
		// 											ContentId: obj.ContentId,
		// 											Event: e,
		// 											ControlId:
		// 												options.ControlId,
		// 											DomObject: $(this),
		// 											DataContext:
		// 												obj.DataContext,
		// 											RenderPatientBannerInFeed:
		// 												feeds.getLastLoadedFeedType() ===
		// 												'PatientFeed'
		// 													? false
		// 													: true,
		// 											IsMemberAreaContext:
		// 												obj.IsMemberAreaContext,
		// 											MemberAreaId:
		// 												obj.MemberAreaId,
		// 											Permissions: obj.Permissions
		// 										}
		// 									);
		// 									api.hide();
		// 									return false;
		// 								});
		// 							$(
		// 								'.js-patientContextActions-handover-patient'
		// 							)
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									PubSub.publish(
		// 										'PatientView.GetPatientActions.Handover',
		// 										{
		// 											PatientData: obj.DoNotCachePatient
		// 												? nonCachedPatientData.Data
		// 												: _patientData.Data,
		// 											PatientId: obj.PatientId,
		// 											ContentId: obj.ContentId,
		// 											IsModal: true,
		// 											Event: e,
		// 											DomObject: $(this),
		// 											DataContext:
		// 												obj.DataContext,
		// 											RenderPatientBannerInFeed:
		// 												feeds.getLastLoadedFeedType() ===
		// 												'PatientFeed'
		// 													? false
		// 													: true,
		// 											IsMemberAreaContext:
		// 												obj.IsMemberAreaContext,
		// 											MemberAreaId:
		// 												obj.MemberAreaId,
		// 											Permissions:
		// 												obj.Permissions,
		// 											EnableEditMode: true
		// 										}
		// 									);
		// 									api.hide();
		// 									return false;
		// 								});

		// 							$(
		// 								'.js-patientContextActions-handover-history-patient'
		// 							)
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									//Can be a lengthy load time...
		// 									appMain.ajaxLoadMask({
		// 										ContainerDomObject: $(
		// 											'.js-patientContextActions-qTip'
		// 										)
		// 									});

		// 									var $self = $(this);
		// 									PubSub.publish(
		// 										'GroupHome.GetPatientActions.PatientHandoverHistory',
		// 										{
		// 											PatientData: obj.DoNotCachePatient
		// 												? nonCachedPatientData.Data
		// 												: _patientData.Data,
		// 											PatientId: obj.PatientId,
		// 											MemberAreaId:
		// 												obj.MemberAreaId,
		// 											NetworkId: Vault.getSessionItem(
		// 												'ActiveNetworkId'
		// 											),
		// 											IsModal: true,
		// 											DomElement: $self
		// 										}
		// 									);

		// 									return false;
		// 								});

		// 							$('.js-patientContextActions-patient-lists')
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									PubSub.publish(
		// 										'PatientView.GetPatientActions.PatientLists',
		// 										{
		// 											PatientId: obj.PatientId,
		// 											PatientData: obj.DoNotCachePatient
		// 												? nonCachedPatientData.Data
		// 												: _patientData.Data,
		// 											ContentId: null,
		// 											Event: e,
		// 											DomObject: $(this),
		// 											DataContext:
		// 												obj.DataContext,
		// 											RenderPatientBannerInFeed:
		// 												feeds.getLastLoadedFeedType() ===
		// 												'PatientFeed'
		// 													? false
		// 													: true,
		// 											IsMemberAreaContext:
		// 												obj.IsMemberAreaContext,
		// 											MemberAreaId:
		// 												obj.MemberAreaId,
		// 											Permissions:
		// 												obj.Permissions,
		// 											IsModal: true
		// 										}
		// 									);
		// 									api.hide();
		// 									return false;
		// 								});
		// 							$('.js-patientContextActions-patient-tags')
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									PubSub.publish(
		// 										'PatientView.GetPatientActions.PatientTags',
		// 										{
		// 											PatientId: obj.PatientId,
		// 											PatientData: obj.DoNotCachePatient
		// 												? nonCachedPatientData.Data
		// 												: _patientData.Data,
		// 											ContentId: null,
		// 											Event: e,
		// 											DomObject: $(
		// 												e.currentTarget
		// 											),
		// 											IsModal: true,
		// 											DataContext:
		// 												obj.DataContext,
		// 											RenderPatientBannerInFeed:
		// 												feeds.getLastLoadedFeedType() ===
		// 												'PatientFeed'
		// 													? false
		// 													: true,
		// 											IsMemberAreaContext:
		// 												obj.IsMemberAreaContext,
		// 											MemberAreaId:
		// 												obj.MemberAreaId,
		// 											Permissions: obj.Permissions
		// 										}
		// 									);
		// 									api.hide();
		// 									return false;
		// 								});
		// 							$('.js-patientContextActions-raise-task')
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									PubSub.publish(
		// 										'PatientView.GetPatientActions.PatientTasks.Raise',
		// 										{
		// 											PatientData: obj.DoNotCachePatient
		// 												? nonCachedPatientData.Data
		// 												: _patientData.Data,
		// 											PatientId: obj.PatientId,
		// 											ContentId: obj.ContentId,
		// 											Event: e,
		// 											DomObject: $(this)
		// 										}
		// 									);
		// 									api.hide();
		// 									return false;
		// 								});
		// 							$('.js-patientContextActions-view-tasks')
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									PubSub.publish(
		// 										'PatientView.GetPatientActions.PatientTasks.ViewTasks',
		// 										{
		// 											PatientData: obj.DoNotCachePatient
		// 												? nonCachedPatientData.Data
		// 												: _patientData.Data,
		// 											PatientId: obj.PatientId,
		// 											ContentId: obj.ContentId,
		// 											Event: e,
		// 											DomObject: $(this),
		// 											DataContext:
		// 												'GetPatientActions'
		// 										}
		// 									);
		// 									api.hide();
		// 									return false;
		// 								});

		// 							$(
		// 								'.js-patientContextActions-view-patient-record'
		// 							)
		// 								.unbind()
		// 								.on('click', function(e) {
		// 									//https://doccom.atlassian.net/browse/WEB-637
		// 									var patientId;
		// 									var patientGroupListCheck;
		// 									if (
		// 										!obj.IsMemberAreaContext &&
		// 										_.has(
		// 											obj,
		// 											'IsFeedLoadedStatus'
		// 										) &&
		// 										!obj.IsFeedLoadedStatus
		// 											.FeedIsLoadedInDom
		// 									) {
		// 										//network level, no feed loaded

		// 										let x = {
		// 											SearchData: obj.DoNotCachePatient
		// 												? nonCachedPatientData
		// 														.Data
		// 														.PatientIdentifiers
		// 														.PrimaryIdentifier
		// 														.Value
		// 												: _patientData.Data
		// 														.PatientIdentifiers
		// 														.PrimaryIdentifier
		// 														.Value,
		// 											SearchType: 'Name'
		// 										};

		// 										//Base64 encode
		// 										appRouter.careflowAppRouter.setLocation(
		// 											'/#/' +
		// 												Vault.getSessionItem(
		// 													'ActiveNetworkId'
		// 												) +
		// 												'/PatientSearch/' +
		// 												btoa(JSON.stringify(x))
		// 										);

		// 										// PubSub.publish("PatientSearch.SendSearchQuery", {
		// 										//     DataContext: obj.DataContext,
		// 										//     IsModal: false,
		// 										//     SearchType: "PatientIdentifier", //called on _executeSearchParameters()
		// 										//     ResultsListControlType: 1,
		// 										//     InfiniteScroll: true,
		// 										//     SearchData: {
		// 										//         SiteName: "",
		// 										//         AreaName: "",
		// 										//         ClinicianName: "",
		// 										//         SearchTerm: (obj.DoNotCachePatient) ? nonCachedPatientData.Data.PatientIdentifiers.PrimaryIdentifier.Value : _patientData.Data.PatientIdentifiers.PrimaryIdentifier.Value
		// 										//     },
		// 										//     NetworkId: Vault.getSessionItem("ActiveNetworkId") || null
		// 										// });
		// 									} else if (
		// 										!obj.IsMemberAreaContext &&
		// 										_.has(
		// 											obj,
		// 											'IsFeedLoadedStatus'
		// 										) &&
		// 										obj.IsFeedLoadedStatus
		// 											.FeedIsLoadedInDom &&
		// 										obj.IsFeedLoadedStatus
		// 											.LastLoadedFeedType ===
		// 											'NetworkFeed' &&
		// 										obj.ContentSourceMemberAreaId
		// 									) {
		// 										//Is a click from a 'NetworkFeed', attempt to send to the relevant team
		// 										patientId = obj.DoNotCachePatient
		// 											? nonCachedPatientData.Data
		// 													.PatientExternalIdentifier
		// 											: _patientData.Data
		// 													.PatientExternalIdentifier;
		// 										patientGroupListCheck =
		// 											groupHome.isPatientIsOnGroupList(
		// 												patientId
		// 											) || false;

		// 										return appRouter.careflowAppRouter.setLocation(
		// 											'#/' +
		// 												Vault.getSessionItem(
		// 													'ActiveNetworkId'
		// 												) +
		// 												'/Teams/' +
		// 												obj.ContentSourceMemberAreaId +
		// 												'/Patients/' +
		// 												patientId +
		// 												''
		// 										);
		// 									} else if (
		// 										obj.IsMemberAreaContext
		// 									) {
		// 										//Team context, load the patientquicklist view
		// 										patientId = obj.DoNotCachePatient
		// 											? nonCachedPatientData.Data
		// 													.PatientExternalIdentifier
		// 											: _patientData.Data
		// 													.PatientExternalIdentifier;
		// 										patientGroupListCheck =
		// 											groupHome.isPatientIsOnGroupList(
		// 												patientId
		// 											) || false;

		// 										return appRouter.careflowAppRouter.setLocation(
		// 											'#/' +
		// 												Vault.getSessionItem(
		// 													'ActiveNetworkId'
		// 												) +
		// 												'/Teams/' +
		// 												obj.MemberAreaId +
		// 												'/Patients/' +
		// 												patientId +
		// 												''
		// 										);
		// 									} else {
		// 										//Default to network level

		// 										let x = {
		// 											SearchData: obj.DoNotCachePatient
		// 												? nonCachedPatientData
		// 														.Data
		// 														.PatientIdentifiers
		// 														.PrimaryIdentifier
		// 														.Value
		// 												: _patientData.Data
		// 														.PatientIdentifiers
		// 														.PrimaryIdentifier
		// 														.Value,
		// 											SearchType: 'Name'
		// 										};

		// 										//Base64 encode
		// 										appRouter.careflowAppRouter.setLocation(
		// 											'/#/' +
		// 												Vault.getSessionItem(
		// 													'ActiveNetworkId'
		// 												) +
		// 												'/PatientSearch/' +
		// 												btoa(JSON.stringify(x))
		// 										);

		// 										// return PubSub.publish("PatientSearch.SendSearchQuery", {
		// 										//     DataContext: obj.DataContext,
		// 										//     IsModal: false,
		// 										//     SearchType: "PatientIdentifier", //called on _executeSearchParameters()
		// 										//     ResultsListControlType: 1,
		// 										//     InfiniteScroll: true,
		// 										//     SearchData: {
		// 										//         SiteName: "",
		// 										//         AreaName: "",
		// 										//         ClinicianName: "",
		// 										//         SearchTerm: (obj.DoNotCachePatient) ? nonCachedPatientData.Data.PatientIdentifiers.PrimaryIdentifier.Value : _patientData.Data.PatientIdentifiers.PrimaryIdentifier.Value
		// 										//     },
		// 										//     NetworkId: Vault.getSessionItem("ActiveNetworkId") || null
		// 										// });;
		// 									}
		// 								});

		// 							/*EDMS*/
		// 							if (
		// 								appMain.checkIfFeatureIsEnabledInAppConfig(
		// 									'enableEdmsLink'
		// 								)
		// 							) {
		// 								let edmsWindow;
		// 								$('.js-patientContextActions-edms')
		// 									.unbind()
		// 									.on('click', function() {
		// 										//https://doccom.atlassian.net/browse/WEB-825
		// 										var $self = $(this);
		// 										var patientId = obj.DoNotCachePatient
		// 											? nonCachedPatientData.Data
		// 													.PatientIdentifiers
		// 													.PrimaryIdentifier
		// 													.Value
		// 											: _patientData.Data
		// 													.PatientIdentifiers
		// 													.PrimaryIdentifier
		// 													.Value;
		// 										if (!patientId) return false;
		// 										let url =
		// 											'http://evolve-test/evolve-emb/Portal/ExternalLink.aspx?logonmethod=automatic&hospitalNumber=' +
		// 											patientId +
		// 											'';
		// 										//live: http://evolve-live/evolve-emb/Portal/ExternalLink.aspx?logonmethod=automatic&hospitalNumber={0}
		// 										edmsWindow = window.open(
		// 											url,
		// 											'edmsWindow'
		// 										); //https://stackoverflow.com/questions/15818892/chrome-javascript-window-open-in-new-tab
		// 										return appMain.setOpenedPatientContextWindow(
		// 											edmsWindow
		// 										);
		// 									});
		// 							}

		// 							/*Medi Viewer*/
		// 							if (
		// 								appMain.checkIfFeatureIsEnabledInAppConfig(
		// 									'enableMediViewerLink'
		// 								)
		// 							) {
		// 								let mediViewerWindow;
		// 								$(
		// 									'.js-patientContextActions-medi-viewer'
		// 								)
		// 									.unbind()
		// 									.on('click', function() {
		// 										//https://doccom.atlassian.net/browse/WEB-844*/

		// 										let patientId = obj.DoNotCachePatient
		// 											? nonCachedPatientData.Data
		// 													.PatientIdentifiers
		// 													.PrimaryIdentifier
		// 													.Value
		// 											: _patientData.Data
		// 													.PatientIdentifiers
		// 													.PrimaryIdentifier
		// 													.Value;

		// 										//Capture the event tracking GA goal
		// 										if (
		// 											window.ga &&
		// 											appMain.checkIfFeatureIsEnabledInAppConfig(
		// 												'googleAnalyticsId'
		// 											)
		// 										) {
		// 											ga('send', {
		// 												hitType: 'event', //https://developers.google.com/analytics/devguides/collection/analyticsjs/events
		// 												eventCategory:
		// 													'MediViewerLinkClicked',
		// 												eventAction: 'Request',
		// 												eventLabel:
		// 													Vault.getItem(
		// 														'LoggedInUserExternalId'
		// 													) +
		// 													' clicked mediViewer link for ' +
		// 													'Identifier: ' +
		// 													patientId
		// 											});
		// 										}

		// 										var $self = $(this);
		// 										let url =
		// 											'https://mvdemo1.immj.net/medway?username=sysc_demo&patientKey=AW123456';
		// 										mediViewerWindow = window.open(
		// 											url,
		// 											'mediViewerWindow'
		// 										); //https://stackoverflow.com/questions/15818892/chrome-javascript-window-open-in-new-tab
		// 										return appMain.setOpenedPatientContextWindow(
		// 											mediViewerWindow
		// 										);
		// 									});
		// 							}

		// 							/*ASPRYA Link*/
		// 							if (
		// 								appMain.checkIfFeatureIsEnabledInAppConfig(
		// 									'enableAspyraLink'
		// 								)
		// 							) {
		// 								let aspryaWindow;
		// 								$(
		// 									'.js-patientContextActions-asprya-viewer'
		// 								)
		// 									.unbind()
		// 									.on('click', function() {
		// 										//https://doccom.atlassian.net/browse/WEB-844*/

		// 										let patientId = obj.DoNotCachePatient
		// 											? nonCachedPatientData.Data
		// 													.PatientIdentifiers
		// 													.PrimaryIdentifier
		// 													.Value
		// 											: _patientData.Data
		// 													.PatientIdentifiers
		// 													.PrimaryIdentifier
		// 													.Value;

		// 										//Capture the event tracking GA goal
		// 										if (
		// 											window.ga &&
		// 											appMain.checkIfFeatureIsEnabledInAppConfig(
		// 												'googleAnalyticsId'
		// 											)
		// 										) {
		// 											ga('send', {
		// 												hitType: 'event', //https://developers.google.com/analytics/devguides/collection/analyticsjs/events
		// 												eventCategory:
		// 													'AspryaLinkClicked',
		// 												eventAction: 'Request',
		// 												eventLabel:
		// 													Vault.getItem(
		// 														'LoggedInUserExternalId'
		// 													) +
		// 													' clicked Asprya link for ' +
		// 													'Identifier: ' +
		// 													patientId
		// 											});
		// 										}

		// 										var $self = $(this);
		// 										let url =
		// 											'http://52.151.77.118/AccessNET/ImageWEB/ExamLoad.aspx?PatientID=' +
		// 											patientId +
		// 											'&Username=systemc&password=Password';
		// 										aspryaWindow = window.open(
		// 											url,
		// 											'aspryaWindow'
		// 										); //https://stackoverflow.com/questions/15818892/chrome-javascript-window-open-in-new-tab
		// 										return appMain.setOpenedPatientContextWindow(
		// 											aspryaWindow
		// 										);
		// 									});
		// 							}
		// 						})
		// 						.fail(function(error) {
		// 							return false;
		// 						})
		// 				);
		// 			},
		// 			HideCallBack: function(api) {
		// 				//Publish to listeners that the Patient Actions list has closed in Ui
		// 				PubSub.publish(
		// 					'PatientView.GetPatientActions.ActionsUiClosed',
		// 					{
		// 						PatientId: obj.PatientId,
		// 						DataContext: obj.DataContext
		// 					}
		// 				);

		// 				if (options.IsGlobalActionsEvent) {
		// 					//Outside single patient context
		// 					PubSub.publish(
		// 						'GroupHome.GetContextActions.ActionsUiClosed',
		// 						{
		// 							PatientId: obj.PatientId,
		// 							DataContext: obj.DataContext
		// 						}
		// 					);
		// 				}
		// 				options.DomObject.removeClass(
		// 					'patientContextActions__launchModal-btn--active'
		// 				).prop('disabled', false);
		// 				if (obj.DataContext === 'GetPatientActions')
		// 					patientView.deleteCachedPatient(); //Clear the cached patient if GetPatientActions. Patient data is passed to actions
		// 			}
		// 		});
		// 	});
		// };

		// var _patientContextActionsGetHandoverControl = function(obj) {
		// 	//View and edit patient team handovers

		// 	if (!_patientData && !_.has(obj, 'PatientData')) {
		// 		return console.error('No patient set');
		// 	}

		// 	var templateData = {};
		// 	var templatePath =
		// 		'Content/js/handlebarsTemplates/PatientView__handoverControl.htm';
		// 	var _closePromptBool = false;
		// 	var _handoverFormIsInEditMode = false;

		// 	//Check if the user has permission from the originating member area currently active
		// 	templateData.IsNetworkContext = obj.IsNetworkContext;
		// 	templateData.MemberAreasUserCanSendHandoverFrom = getMemberAreasThatAUserCanHandoverFrom();
		// 	templateData.DataContext = obj.DataContext;
		// 	templateData.IsModal = obj.IsModal;
		// 	(templateData.EnableEditMode =
		// 		typeof obj.EnableEditMode !== 'undefined'
		// 			? obj.EnableEditMode
		// 			: false), //Allows you to open a modal with editable fields etc, not labels
		// 		(templateData.PatientDetails = obj.PatientData
		// 			? JSON.parse(JSON.stringify(obj.PatientData))
		// 			: JSON.parse(JSON.stringify(_patientData.Data))); //Can be passed or use preset. Use passed data as default, but rarely is it needed to be passed. Clone for mutation below...
		// 	var isNewHandover = templateData.PatientDetails.HandoverNotes.length
		// 		? false
		// 		: true;

		// 	//Create a useful cache of patient handover data, before parsing
		// 	var patientHandoverDataCache;
		// 	if (!isNewHandover) {
		// 		patientHandoverDataCache = [].concat(
		// 			templateData.PatientDetails.HandoverNotes
		// 		);
		// 	} else {
		// 		patientHandoverDataCache = [];
		// 	}

		// 	//Useful cache of memberareas that patient has a current handover from
		// 	var teamsPatientHasHandoversFrom = _.map(
		// 		templateData.PatientDetails.HandoverNotes,
		// 		function(note) {
		// 			if (note.IsCurrent) {
		// 				return note.MemberAreaId;
		// 			}
		// 		}
		// 	);

		// 	templateData.TeamsPatientHasHandoversFrom = teamsPatientHasHandoversFrom; //Add to template data object

		// 	if (!isNewHandover && obj.MemberAreaId) {
		// 		//There are handover notes - ensure we filter for active team, else treat as 'new handover'
		// 		//Filter data (for template to use) to check if the handover is from active team. If team and network, take the latest one
		// 		templateData.PatientDetails.HandoverNotes = [
		// 			_
		// 				.chain(templateData.PatientDetails.HandoverNotes)
		// 				.filter(function(note) {
		// 					if (
		// 						groupHome.getHandoverApiVersion() !== 1 &&
		// 						note.IsCurrent
		// 					) {
		// 						//V2 feature, disable for V1
		// 						//return note.MemberAreaId === obj.MemberAreaId;  //Handover Roll Back - pending business decisions - https://doccom.atlassian.net/browse/WEB-567
		// 						return note;
		// 					} else if (note.IsCurrent) {
		// 						//V1 (can return multiple notes)
		// 						return note;
		// 					}
		// 					return false;
		// 				})
		// 				.sortBy(function(note) {
		// 					return -note.DateLastUpdated;
		// 				}) //Sort by earliest date '-' This ensures if dupes for a team, we get the latest
		// 				.uniq(function(note) {
		// 					return note.MemberAreaId;
		// 				}, true)
		// 				.value()[0]
		// 		]; //Handover Roll Back - pending business decisions, so take latest now from this collection - https://doccom.atlassian.net/browse/WEB-567;
		// 	} else if (obj.IsNetworkContext) {
		// 		//Network context
		// 		if (!isNewHandover) {
		// 			templateData.PatientDetails.HandoverNotes = [
		// 				_
		// 					.chain(templateData.PatientDetails.HandoverNotes)
		// 					.filter(function(note) {
		// 						if (
		// 							groupHome.getHandoverApiVersion() !== 1 &&
		// 							note.IsCurrent
		// 						) {
		// 							//V2 feature, disable for V1
		// 							return note;
		// 						} else if (note.IsCurrent) {
		// 							//V1 (can return multiple notes)
		// 							return note;
		// 						}
		// 						return false;
		// 					})
		// 					.sortBy(function(note) {
		// 						return -note.DateLastUpdated;
		// 					}) //Sort by earliest date '-' This ensures if dupes for a team, we get the latest
		// 					.uniq(function(note) {
		// 						return note.MemberAreaId;
		// 					}, true)
		// 					.value()[0]
		// 			]; //Handover Roll Back - pending business decisions, so take latest now from this collection - https://doccom.atlassian.net/browse/WEB-567
		// 		}
		// 	}

		// 	console.log('*%%%%%%%***', obj);
		// 	console.log('****', templateData);

		// 	if (_.has(obj, 'IsModal') && obj.IsModal) {
		// 		//Open in GetPatientActions modal context
		// 		var openModal = $.featherlight.current(); //Close any active modals, can be Global search modal
		// 		if (openModal) {
		// 			$.featherlight.close();
		// 		}

		// 		function confirmCloseModal() {
		// 			//pass attributes to a self-closed element
		// 			var message = $('<p />', {
		// 					text:
		// 						'Are you sure you want to leave without saving your changes?',
		// 					class: ''
		// 				}),
		// 				ok = $('<button />', {
		// 					text: 'Leave, discard changes',
		// 					class:
		// 						'btn--plain--block-pad u-pull-right js-prompt-confirm'
		// 				}),
		// 				cancel = $('<button/>', {
		// 					text: 'Keep editing',
		// 					class: 'btn u-pull-left js-prompt-cancel'
		// 				});

		// 			var callBack = function(event, api) {
		// 				$('.js-prompt-confirm').on('click', function(e) {
		// 					_closePromptBool = true;
		// 					api.destroy(e);
		// 					$.featherlight.close();
		// 				});

		// 				$('.js-prompt-cancel').on('click', function(e) {
		// 					api.destroy(e);
		// 				});
		// 			};
		// 			appMain.drawPromptConfirmDialog(
		// 				message.add(ok).add(cancel),
		// 				'Changes to handover will be lost',
		// 				callBack
		// 			);
		// 		}

		// 		if (!_.has(obj, 'DataContext'))
		// 			obj.DataContext = 'GetPatientActions'; //Default to GetPatientActions if not passed

		// 		var subscription;

		// 		GetHandlebarsTemplate.callApi(templatePath, false, templateData)
		// 			.done(GetHandlebarsTemplate.onTemplateInserted)
		// 			.then(function(html) {
		// 				var handoverModal = $.featherlight(html, {
		// 					closeOnEsc: false,
		// 					closeOnClick: true,
		// 					type: 'html',
		// 					beforeOpen: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.beforeOpen,
		// 							this,
		// 							e
		// 						)(e);
		// 						appMain.closeAllqTips();
		// 						subscription = PubSub.subscribe(
		// 							'PatientView.GetPatientActions.Handover.Saved',
		// 							function(obj) {
		// 								_closePromptBool = true; //Handover saved, so close without prompt
		// 								handoverModal.close(); //Close the modal, fire notification

		// 								appMain.triggerAppNotification({
		// 									Text:
		// 										'Patient handover saved for ' +
		// 										templateData.PatientDetails
		// 											.PatientFamilyName +
		// 										', ' +
		// 										templateData.PatientDetails
		// 											.PatientGivenName +
		// 										' (' +
		// 										templateData.PatientDetails
		// 											.PatientIdentifiers
		// 											.PrimaryIdentifier.Label +
		// 										': ' +
		// 										templateData.PatientDetails
		// 											.PatientIdentifiers
		// 											.PrimaryIdentifier.Value +
		// 										')',
		// 									Heading: '',
		// 									Icon: 'success'
		// 								});
		// 							}
		// 						);
		// 					},
		// 					beforeClose: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.beforeClose,
		// 							this,
		// 							e
		// 						)(e);

		// 						if (
		// 							!_closePromptBool &&
		// 							_handoverFormIsInEditMode
		// 						) {
		// 							confirmCloseModal();
		// 							return false; //Prevent close
		// 						} else {
		// 							subscription.remove();
		// 						}
		// 					},
		// 					afterContent: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.afterContent,
		// 							this,
		// 							e
		// 						)(e);
		// 						$('.js-appModal-wrapper')
		// 							.find('[data-context]')
		// 							.attr('data-context', obj.DataContext); //Set Context
		// 						bindPatientHandoverUiControls(true);
		// 						appMain.formatTemplateDates();
		// 						if (templateData.EnableEditMode)
		// 							toggleEditableHandoverNotesUi(); //Flip into edit mode if initial context set
		// 					}
		// 				});
		// 			});
		// 	} else {
		// 		//Standard view, pass in containers
		// 		if (!obj.DomSelectorToInsertTemplateTo)
		// 			obj.DomSelectorToInsertTemplateTo = $(
		// 				'.js-teamPatientView-handover-template'
		// 			);

		// 		console.log('/////////////////////', templateData);

		// 		GetHandlebarsTemplate.callApi(
		// 			templatePath,
		// 			obj.DomSelectorToInsertTemplateTo,
		// 			templateData
		// 		)
		// 			.done(GetHandlebarsTemplate.onTemplateInserted)
		// 			.then(function() {
		// 				obj.DomSelectorToInsertTemplateTo.find(
		// 					'[data-context]'
		// 				).attr('data-context', obj.DataContext); //Set Context
		// 				bindPatientHandoverUiControls(
		// 					false,
		// 					templateData.PatientDetails.HandoverNotes.length
		// 				);
		// 				appMain.formatTemplateDates();
		// 			});
		// 	}

		// 	//Privates
		// 	function bindPatientHandoverUiControls(isModal, countOfHandovers) {
		// 		if (isModal) {
		// 			if (isNewHandover) {
		// 				//Set up to edit new handover
		// 				$(
		// 					'.js-block--handover-sendingTeam-ddl[data-context=' +
		// 						obj.DataContext +
		// 						'] option[data-memberareaid=' +
		// 						Vault.getSessionItem(
		// 							'ActiveGroupMemberAreaId'
		// 						) +
		// 						']'
		// 				).prop('selected', true); //Pre set team
		// 				$('.js-block--handover-sendingTeam-ddl').select2({
		// 					width: '93%'
		// 				});

		// 				$(
		// 					'.js-patientHandoverNote-editBtn[data-context=' +
		// 						obj.DataContext +
		// 						']'
		// 				)
		// 					.unbind()
		// 					.on('click', function() {
		// 						var $self = $(this);
		// 						$self.ajaxActiveButtonUi({
		// 							ActionState: 'start',
		// 							DisableContainerInputs: $(
		// 								'.js-patientHandover-form'
		// 							),
		// 							LockedText: 'Saving handover'
		// 						});
		// 						saveHandoverNotes();
		// 					});

		// 				//Set max char counter on inputs
		// 				$(
		// 					'.js-patientHandoverNote-txt[data-context=' +
		// 						obj.DataContext +
		// 						']'
		// 				).each(function() {
		// 					var $self = $(this);
		// 					var handoverField = $self.data('handover');

		// 					var $charCounter = $(
		// 						'.js-patientHandover-note-counter[ data-handover=' +
		// 							handoverField +
		// 							'][data-context=' +
		// 							obj.DataContext +
		// 							']'
		// 					);

		// 					$self.on('keyup paste', function(e) {
		// 						if (!e.keyCode) {
		// 							//since no key was down at the time of the event we can assume it was from the toolbar or right click menu, and not a ctrl+v
		// 							setTimeout(function(e) {
		// 								changeElementTypeContentListnerBindings(
		// 									e,
		// 									$self,
		// 									$charCounter
		// 								);
		// 							}, 200);
		// 							return;
		// 						} else {
		// 							changeElementTypeContentListnerBindings(
		// 								e,
		// 								$self,
		// 								$charCounter
		// 							);
		// 						}
		// 					});
		// 				});

		// 				//render sending team DDL
		// 				$(
		// 					'.js-block--handover-sendingTeam-ddl-wrapper'
		// 				).removeClass('u-no-display');
		// 				//$(".js-block--handover-sendingTeam-ddl[data-context=" + obj.DataContext + "] option[data-memberareaid=" + Vault.getSessionItem("ActiveGroupMemberAreaId") + "]").prop("selected", true); //Pre set team
		// 				$('.js-block--handover-sendingTeam-ddl').select2({
		// 					width: '93%'
		// 				});

		// 				if (_.has(obj, 'MemberAreaId')) {
		// 					$(
		// 						'.js-block--handover-sendingTeam-ddl[data-context=' +
		// 							obj.DataContext +
		// 							']'
		// 					)
		// 						.val(obj.MemberAreaId)
		// 						.trigger('change.select2');
		// 				}

		// 				_handoverFormIsInEditMode = true; //Edit mode by default for new
		// 			} else {
		// 				//Not a new handover
		// 				$(
		// 					'.js-patientHandoverNote-editBtn[data-context=' +
		// 						obj.DataContext +
		// 						']'
		// 				)
		// 					.unbind()
		// 					.on('click', toggleEditableHandoverNotesUi);
		// 			}
		// 		} else {
		// 			//Not a Modal, regular view component
		// 			if (obj.IsNetworkContext) {
		// 				//Removed for handover roll back
		// 				//$(".js-patient-view-handover-trigger").unbind().on("click", function () {
		// 				//    var $self = $(this);
		// 				//    var teamId = $self.data("team");
		// 				//    var $icon = $self.find(".js-trigger-icon");
		// 				//    var teamHandover = $(".js-patient-view-handover-item[data-team=" + teamId + "]");
		// 				//    if (teamHandover.is(":visible")) {
		// 				//        teamHandover.addClass("u-no-display");
		// 				//        $icon.addClass("fa-plus").removeClass("fa-minus");
		// 				//        return;
		// 				//    } else {
		// 				//        teamHandover.removeClass("u-no-display");
		// 				//        $icon.addClass("fa-minus").removeClass("fa-plus");
		// 				//        return;
		// 				//    }
		// 				//});
		// 				////If only 1 handover, open it
		// 				//if (countOfHandovers === 1) $(".js-patient-view-handover-trigger").triggerHandler("click");
		// 			}
		// 			//Edit - launch modal - and allow click on handover text to launch edit modal in team context
		// 			$(
		// 				'.js-patientView-patient-handover-edit-btn, .js-patientHandoverNote-txt'
		// 			)
		// 				.unbind()
		// 				.on('click', function(e) {
		// 					PubSub.publish(
		// 						'PatientView.GetPatientActions.Handover',
		// 						{
		// 							PatientId:
		// 								templateData.PatientDetails
		// 									.PatientExternalIdentifier,
		// 							MemberAreaId:
		// 								Vault.getSessionItem(
		// 									'ActiveGroupMemberAreaId'
		// 								) || null,
		// 							IsNetworkContext: obj.IsNetworkContext,
		// 							ContentId: null,
		// 							Event: e,
		// 							DomObject: $(this),
		// 							IsModal: true,
		// 							DomSelectorToInsertTemplateTo:
		// 								obj.DomSelectorToInsertTemplateTo,
		// 							DataContext: 'GetPatientActions',
		// 							EnableEditMode: true
		// 						}
		// 					);
		// 				});
		// 		}
		// 	}

		// 	function toggleEditableHandoverNotesUi() {
		// 		//Set toggle to edit mode in Modal, from search results
		// 		_handoverFormIsInEditMode = true;

		// 		$('.js-patientHandover-form')
		// 			.removeClass('patientHandover__form--existing')
		// 			.addClass('patientHandover__form'); //Toggle UI class for edit mode

		// 		function changeElementType($element, newType) {
		// 			//Convert the elements to textareas / spans etc. Set the max chars count on textareas
		// 			$element.each(function() {
		// 				var attrs = {};

		// 				$.each(this.attributes, function(idx, attr) {
		// 					attrs[attr.nodeName] = attr.nodeValue;
		// 				});

		// 				$(this).replaceWith(function() {
		// 					var newEl = $('<' + newType + '/>', attrs).append(
		// 						$(this).contents()
		// 					);

		// 					if (newType == 'textarea') {
		// 						var handoverField = newEl.data('handover');

		// 						autosize(newEl); //Grow textareas

		// 						//Set max char counter on inputs
		// 						var maxLength = 1000; //API restriction
		// 						var $charCounter = $(
		// 							'.js-patientHandover-note-counter[ data-handover=' +
		// 								handoverField +
		// 								'][data-context=' +
		// 								obj.DataContext +
		// 								']'
		// 						);

		// 						var eventBinding = function(e) {
		// 							if (!e.keyCode) {
		// 								//since no key was down at the time of the event we can assume it was from the toolbar or right click menu, and not a ctrl+v
		// 								_.delay(function(e) {
		// 									changeElementTypeContentListnerBindings(
		// 										e,
		// 										newEl,
		// 										$charCounter
		// 									);
		// 								}, 200); //Allow time to paste
		// 								return;
		// 							} else {
		// 								changeElementTypeContentListnerBindings(
		// 									e,
		// 									newEl,
		// 									$charCounter
		// 								);
		// 							}
		// 						};

		// 						var throttled = _.throttle(eventBinding, 200); //Invoke at most every .2 seconds
		// 						newEl.unbind().on('keyup paste', throttled);
		// 						$charCounter.removeClass('u-no-display');
		// 					}

		// 					return newEl;
		// 				});
		// 			});
		// 		}

		// 		$('.js-appModal-wrapper')
		// 			.find(
		// 				'.js-patientHandoverNote-txt[data-context=' +
		// 					obj.DataContext +
		// 					']'
		// 			)
		// 			.each(function() {
		// 				//Only in modal control. Patient list handled in GroupHome module
		// 				if ($(this).context.nodeName != 'TEXTAREA') {
		// 					//Not already a textarea
		// 					changeElementType($(this), 'textarea');
		// 				}
		// 			});

		// 		$('.js-block--handover-sendingTeam-ddl-wrapper').removeClass(
		// 			'u-no-display'
		// 		);

		// 		//Pre set team (team or latest, if no team)
		// 		var memberAreaIdToUse;
		// 		if (!isNewHandover) {
		// 			memberAreaIdToUse =
		// 				patientHandoverDataCache[0].MemberAreaId ||
		// 				Vault.getSessionItem('ActiveGroupMemberAreaId');
		// 		} else {
		// 			memberAreaIdToUse =
		// 				Vault.getSessionItem('ActiveGroupMemberAreaId') || null;
		// 		}

		// 		$('.js-block--handover-sendingTeam-ddl')
		// 			.select2({
		// 				width: '93%'
		// 			})
		// 			.on('change', function(e) {
		// 				//Get the active handover (if any) for the selected team. This is modal specific.

		// 				//Handover Roll Back - pending business decisions - https://doccom.atlassian.net/browse/WEB-567 - just allow latest to now be edited
		// 				return;

		// 				//var memberAreaId = $("option:selected", this).data("memberareaid");

		// 				//var handoverNotesToRender = _.chain(patientHandoverDataCache) //Use the cache, do not mutate master on each selection. TODO: USE IMMUTABLE.JS
		// 				//    .filter(function(note) {
		// 				//        if (groupHome.getHandoverApiVersion() !== 1 && note.IsCurrent) { //V2 feature, disable for V1
		// 				//            return note.MemberAreaId === memberAreaId;
		// 				//        } else if (note.IsCurrent) { //V1
		// 				//            return note;
		// 				//        }
		// 				//        return false;
		// 				//    })
		// 				//    .sortBy(function(note) { return -note.DateLastUpdated; }) //Sort by earliest date '-' This ensures if dupes for a team, we get the latest
		// 				//    .uniq(function(note) { return note.MemberAreaId; }, true)
		// 				//    .value()[0]; //Array returned, just return the 0 index

		// 				//var domContainer = $.featherlight.current().$content;
		// 				//var $updatedBy = domContainer.find(".js-patientHandoverNote-updatedByName");
		// 				//var $updatedByTeam = domContainer.find(".js-patientHandoverNote-updatedByGroup");
		// 				//var $updatedByDate = domContainer.find(".js-patientHandoverNote-updatedByTime");
		// 				//var $metaWrapper = $(".js-patientHandover-meta-row");

		// 				//domContainer.find(".js-patientHandoverNote-txt").each(function() {
		// 				//    var $self = $(this);
		// 				//    var itemType = $self.data("handover");

		// 				//    if (typeof handoverNotesToRender !== "undefined" && handoverNotesToRender) {
		// 				//        switch (itemType) {
		// 				//        case "situation":
		// 				//            $self.text(handoverNotesToRender.Notes[0].Value);
		// 				//            break;
		// 				//        case "background":
		// 				//            $self.text(handoverNotesToRender.Notes[1].Value);
		// 				//            break;
		// 				//        case "assessment":
		// 				//            $self.text(handoverNotesToRender.Notes[2].Value);
		// 				//            break;
		// 				//        case "recommendation":
		// 				//            $self.text(handoverNotesToRender.Notes[3].Value);
		// 				//            break;
		// 				//        default:
		// 				//        }
		// 				//        //Set meta
		// 				//        $metaWrapper.removeClass("u-no-display");
		// 				//        $updatedBy.text(handoverNotesToRender.UpdatedByName);
		// 				//        $updatedByTeam.text(handoverNotesToRender.GroupName);
		// 				//        $updatedByDate.text(moment(handoverNotesToRender.DateLastUpdated).format("DD-MMM-YYYY, HH:mm"));
		// 				//    } else {
		// 				//        $self.text("");
		// 				//        $metaWrapper.addClass("u-no-display");
		// 				//        $updatedBy.text("");
		// 				//        $updatedByTeam.text("");
		// 				//        $updatedByDate.text("");
		// 				//    }
		// 				//});
		// 			});

		// 		if (_.has(obj, 'MemberAreaId') && obj.MemberAreaId) {
		// 			$(
		// 				'.js-block--handover-sendingTeam-ddl[data-context=' +
		// 					obj.DataContext +
		// 					']'
		// 			)
		// 				.val(obj.MemberAreaId)
		// 				.trigger('change.select2');
		// 		} else {
		// 			$(
		// 				'.js-block--handover-sendingTeam-ddl[data-context=' +
		// 					obj.DataContext +
		// 					']'
		// 			)
		// 				.val(memberAreaIdToUse)
		// 				.trigger('change.select2');
		// 		}

		// 		//Set save button context on edit button
		// 		$(
		// 			'.js-patientHandoverNote-editBtn[data-context=' +
		// 				obj.DataContext +
		// 				']'
		// 		)
		// 			.unbind()
		// 			.on('click', function() {
		// 				var $self = $(this);
		// 				$self.ajaxActiveButtonUi({
		// 					ActionState: 'start',
		// 					DisableContainerInputs: $(
		// 						'.js-patientHandover-form'
		// 					),
		// 					LockedText: 'Saving handover'
		// 				});
		// 				saveHandoverNotes();
		// 			})
		// 			.text('Save handover');
		// 	}

		// 	function changeElementTypeContentListnerBindings(
		// 		e,
		// 		$el,
		// 		$charCounter
		// 	) {
		// 		var maxLength = 1000; //API restriction
		// 		var length = $el.val().length;
		// 		length = maxLength - length;
		// 		$charCounter.text(length);

		// 		if ($el[0].value.length == maxLength) {
		// 			e.preventDefault();
		// 		} else if ($el[0].value.length > maxLength) {
		// 			// Maximum exceeded, trim it
		// 			$el[0].value = $el[0].value.substring(0, maxLength).trim();
		// 			$charCounter.text(0);
		// 		}
		// 	}

		// 	function saveHandoverNotes() {
		// 		//Save handovers via modal context
		// 		var teamToSendHandoverFrom;
		// 		var teamToSendHandoverFromLabel;
		// 		var situationBody;
		// 		var backgroundBody;
		// 		var assessmentBody;
		// 		var recommendationBody;

		// 		if (obj.IsModal) {
		// 			teamToSendHandoverFrom = parseInt(
		// 				$('.js-appModal-wrapper')
		// 					.find(
		// 						'.js-block--handover-sendingTeam-ddl[data-context=' +
		// 							obj.DataContext +
		// 							']'
		// 					)
		// 					.val()
		// 			);
		// 			teamToSendHandoverFromLabel = $('.js-appModal-wrapper')
		// 				.find(
		// 					'.js-block--handover-sendingTeam-ddl[data-context=' +
		// 						obj.DataContext +
		// 						'] option:selected'
		// 				)
		// 				.text();
		// 			situationBody = $('.js-appModal-wrapper')
		// 				.find(
		// 					".js-patientHandoverNote-txt[data-handover='situation'][data-context=" +
		// 						obj.DataContext +
		// 						']'
		// 				)
		// 				.val();
		// 			backgroundBody = $('.js-appModal-wrapper')
		// 				.find(
		// 					".js-patientHandoverNote-txt[data-handover='background'][data-context=" +
		// 						obj.DataContext +
		// 						']'
		// 				)
		// 				.val();
		// 			assessmentBody = $('.js-appModal-wrapper')
		// 				.find(
		// 					".js-patientHandoverNote-txt[data-handover='assessment'][data-context=" +
		// 						obj.DataContext +
		// 						']'
		// 				)
		// 				.val();
		// 			recommendationBody = $('.js-appModal-wrapper')
		// 				.find(
		// 					".js-patientHandoverNote-txt[data-handover='recommendation'][data-context=" +
		// 						obj.DataContext +
		// 						']'
		// 				)
		// 				.val();
		// 		} else {
		// 			teamToSendHandoverFrom = parseInt(
		// 				$(
		// 					'.js-block--handover-sendingTeam-ddl[data-context=' +
		// 						obj.DataContext +
		// 						']'
		// 				).val()
		// 			);
		// 			teamToSendHandoverFromLabel = $(
		// 				'.js-block--handover-sendingTeam-ddl[data-context=' +
		// 					obj.DataContext +
		// 					'] option:selected'
		// 			).text();
		// 			situationBody = $(
		// 				".js-patientHandoverNote-txt[data-handover='situation'][data-context=" +
		// 					obj.DataContext +
		// 					']'
		// 			).val();
		// 			backgroundBody = $(
		// 				".js-patientHandoverNote-txt[data-handover='background'][data-context=" +
		// 					obj.DataContext +
		// 					']'
		// 			).val();
		// 			assessmentBody = $(
		// 				".js-patientHandoverNote-txt[data-handover='assessment'][data-context=" +
		// 					obj.DataContext +
		// 					']'
		// 			).val();
		// 			recommendationBody = $(
		// 				".js-patientHandoverNote-txt[data-handover='recommendation'][data-context=" +
		// 					obj.DataContext +
		// 					']'
		// 			).val();
		// 		}

		// 		//Check at least 1 SBAR field is entered
		// 		if (
		// 			(!situationBody &&
		// 				!backgroundBody &&
		// 				!assessmentBody &&
		// 				!recommendationBody) ||
		// 			!teamToSendHandoverFrom
		// 		) {
		// 			$(
		// 				'.js-patientHandoverNote-editBtn[data-context=' +
		// 					obj.DataContext +
		// 					']'
		// 			).ajaxActiveButtonUi({
		// 				ActionState: 'stop',
		// 				DisableContainerInputs: $('.js-patientHandover-form')
		// 			});
		// 			$(
		// 				'.js-sbar-error[data-context=' + obj.DataContext + ']'
		// 			).text(
		// 				'Please select a team and enter details in at least one field'
		// 			);
		// 			if (obj.IsModal) appMain.setModalElementsAsFullHeight();
		// 			return false;
		// 		} else {
		// 			$(
		// 				'.js-sbar-error[data-context=' + obj.DataContext + ']'
		// 			).text('');
		// 		}

		// 		if (
		// 			situationBody.length > 1000 ||
		// 			backgroundBody.length > 1000 ||
		// 			assessmentBody.length > 1000 ||
		// 			recommendationBody.length > 1000
		// 		)
		// 			return false;

		// 		UpsertHandoverNote.callApi(
		// 			teamToSendHandoverFrom,
		// 			situationBody,
		// 			backgroundBody,
		// 			assessmentBody,
		// 			recommendationBody,
		// 			obj.PatientId
		// 		)
		// 			.then(function(data, textStatus, jqXHr) {
		// 				if (jqXHr.status === 204) {
		// 					//Update the handover notes View model
		// 					 VueEventBus.$emit('PatientView.GetPatientActions.Handover.Saved', obj.PatientData); //Tell Vue - TODO: REMOVE ON REFACTOR

		// 					if (_patientData) {
		// 						//If a cached patient, update the model
		// 						$.when(
		// 							_updateHandoverNotesViewModel(
		// 								situationBody,
		// 								backgroundBody,
		// 								assessmentBody,
		// 								recommendationBody,
		// 								teamToSendHandoverFromLabel,
		// 								teamToSendHandoverFrom
		// 							).then(function() {
		// 								PubSub.publish(
		// 									'PatientView.GetPatientActions.Handover.Saved',
		// 									{
		// 										IsNewHandover: isNewHandover,
		// 										PatientId: obj.PatientId,
		// 										FromTeam: teamToSendHandoverFromLabel,
		// 										MemberAreaId: teamToSendHandoverFrom,
		// 										Situation: situationBody,
		// 										Background: backgroundBody,
		// 										Assessment: assessmentBody,
		// 										Recommendation: recommendationBody,
		// 										DataContext: obj.DataContext,
		// 										IsModal: obj.IsModal
		// 									}
		// 								);

		// 								//Rebuild in UI controls where required with updated note. Only needed where a patient is cached.
		// 								PubSub.publish(
		// 									'PatientView.GetPatientActions.Handover',
		// 									{
		// 										PatientId: _patientId,
		// 										IsModal: false,
		// 										DataContext:
		// 											'TeamContextPatientView',
		// 										DomSelectorToInsertTemplateTo:
		// 											obj.DomSelectorToInsertTemplateTo,
		// 										MemberAreaId:
		// 											Vault.getSessionItem(
		// 												'ActiveGroupMemberAreaId'
		// 											) || null,
		// 										IsNetworkContext: Vault.getSessionItem(
		// 											'ActiveGroupMemberAreaId'
		// 										)
		// 											? false
		// 											: true

		// 										//PatientId: _patientId,
		// 										//MemberAreaId: Vault.getSessionItem("ActiveGroupMemberAreaId") || teamToSendHandoverFrom,
		// 										//IsNewHandover: isNewHandover,
		// 										//IsModal: false,
		// 										//DataContext: obj.DataContext,
		// 										//DomSelectorToInsertTemplateTo: obj.DomSelectorToInsertTemplateTo
		// 									}
		// 								);
		// 							})
		// 						);
		// 					} else {
		// 						//No cached patient, assume modal
		// 						PubSub.publish(
		// 							'PatientView.GetPatientActions.Handover.Saved',
		// 							{
		// 								IsNewHandover: isNewHandover,
		// 								PatientId: obj.PatientId,
		// 								FromTeam: teamToSendHandoverFromLabel,
		// 								MemberAreaId: teamToSendHandoverFrom,
		// 								DataContext: obj.DataContext,
		// 								IsModal: obj.IsModal,
		// 								Situation: situationBody,
		// 								Background: backgroundBody,
		// 								Assessment: assessmentBody,
		// 								Recommendation: recommendationBody
		// 							}
		// 						);
		// 					}
		// 				}
		// 			})
		// 			.fail();
		// 	}
		// };

		// var _patientContextActionsGetPatientClinicalTagsControl = function(
		// 	obj
		// ) {
		// 	//Modal or view template

		// 	console.log(JSON.stringify(obj))

		// 	var thereAreTeamTags = thereAreTeamTagsAndOtherTeamTags(obj);

		// 	var templatePath =
		// 		'Content/js/handlebarsTemplates/patientTags/PatientView__patientTagsControl.htm';
		// 	var patientClinicalTags = {
		// 		PatientExternalIdentifier: obj.PatientId,
		// 		Data: obj.PatientData.ClinicalTags,
		// 		MemberAreasUserCanManageTagsFrom: getMemberAreasThatAUserCanManagePatientClinicalTagsFrom(),
		// 		PatientDetails: obj.PatientData, //Can be passed or use preset.
		// 		DataContext: obj.DataContext,
		// 		ActiveMemberArea: obj.MemberAreaId ? obj.MemberAreaId : false,
		// 		ThereAreTeamTags: thereAreTeamTags.ThisTeamTags,
		// 		ThereAreOtherTeamTags: thereAreTeamTags.OtherTeamTags,
		// 		IsModal: obj.IsModal,
		// 		IsFirstLoad: true
		// 	};
		// 	var selectedTagsToAdd = {
		// 		TagIds: [],
		// 		TagMeta: []
		// 	};

		// 	if (obj.IsModal) {
		// 		var tagRemovedSubscription;
		// 		var tagAddedSubscription;
		// 		var allTagsAddedSubscription;
		// 		var _closePromptBool = false;

		// 		GetHandlebarsTemplate.callApi(
		// 			templatePath,
		// 			false,
		// 			patientClinicalTags
		// 		).then(function (html) {


		// 				$.featherlight(html, {
		// 					closeOnEsc: true,
		// 					closeOnClick: true,
		// 					type: 'html',
		// 					beforeOpen: function (e) {
		// 						var activeModal = this;
		// 						$.proxy($.featherlight.defaults.beforeOpen,	this,e)();
		// 						//appMain.closeAllqTips();
		// 						tagRemovedSubscription = PubSub.subscribe(
		// 							'PatientView.GetPatientActions.PatientTags.TagRemoved',
		// 							function(obj) {
		// 								//Update tag in modal context - set add UI

		// 								var $modal = $('.js-appModal-wrapper');
		// 								var $liContentWrapper = $modal.find(
		// 									'.js-active-tag-li-content[data-clinicaltagid=' +
		// 										obj.ClinicalTagId +
		// 										'][data-addedbygroupid=' +
		// 										obj.MemberAreaId +
		// 										']'
		// 								);
		// 								var $removeIcon = $liContentWrapper.find(
		// 									'.js-clinicaltag-remove-icon[data-clinicaltagid=' +
		// 										obj.ClinicalTagId +
		// 										']'
		// 								);
		// 								var $metaData = $liContentWrapper.find(
		// 									'.js-clinicaltag-meta[data-clinicaltagid=' +
		// 										obj.ClinicalTagId +
		// 										']'
		// 								);

		// 								$removeIcon.unbind();

		// 								//  var classLists = $modal.find($(".js-clinicaltag-add-icon"))[0].classList;
		// 								//  var htmlText = $modal.find($(".js-clinicaltag-add-icon")).html();

		// 								$removeIcon
		// 									.removeClass()
		// 									.addClass(
		// 										'btn--plain tag__ui-status js-clinicaltag-add-icon'
		// 									)
		// 									.attr({
		// 										'data-is-patient-tag': false
		// 									})
		// 									.html(
		// 										'<i class="fa fa-square-o js-tag-ui-status" aria-hidden="true"></i>'
		// 									);
		// 								$metaData.empty();

		// 								//  var wrapperClass = $modal.find($(".js-clinicaltag-add-tag"))[0].classList;
		// 								$liContentWrapper
		// 									.removeClass()
		// 									.addClass(
		// 										'patientTags-li__content--canAdd js-clinicaltag-add-tag js-tag-name'
		// 									);

		// 								//Bindings - tags are removed inline, one by one. Adding can be batched
		// 								bindClinicalTagUiElements(
		// 									obj.MemberAreaId
		// 								);

		// 								//Update the local patient model and set the added clinical tag as active.
		// 								if (patientView.getCachedPatient()) {
		// 									$.when(
		// 										_updatePatientClinicalTagsViewModelWithRemovedTag(
		// 											obj.ClinicalTagId,
		// 											obj.MemberAreaId,
		// 											obj.ClinicalTagName
		// 										).then(function() {
		// 											//update the tags block in PatientView
		// 											//Now Reloads - https://doccom.atlassian.net/browse/WEB-742
		// 											//PubSub.publish("PatientView.GetPatientActions.PatientTags", {
		// 											//    PatientId: obj.PatientId,
		// 											//    MemberAreaId: obj.MemberAreaId,
		// 											//    IsModal: false,
		// 											//    DataContext: obj.DataContext,
		// 											//    QtipContainer: null,
		// 											//    QtipViewport: $(window),
		// 											//    CloseEvents: 'click unfocus'
		// 											//});
		// 										})
		// 									);
		// 								}
		// 							}
		// 						);

		// 						tagAddedSubscription = PubSub.subscribe(
		// 							'PatientView.GetPatientActions.PatientTags.TagAdded',
		// 							function(obj) {
		// 								//Update tag in modal context - set remove UI
		// 								console.log(obj);

		// 								var $modal = $('.js-appModal-wrapper');
		// 								var $li = $modal.find(
		// 									'.js-clinicaltag-add-tag[data-clinicaltagid=' +
		// 										obj.ClinicalTagId +
		// 										']'
		// 								);
		// 								var $liContentWrapper = $modal.find(
		// 									'.js-clinicaltag-add-tag[data-clinicaltagid=' +
		// 										obj.ClinicalTagId +
		// 										']'
		// 								);
		// 								var $addIcon = $liContentWrapper.find(
		// 									'.js-clinicaltag-add-icon[data-clinicaltagid=' +
		// 										obj.ClinicalTagId +
		// 										']'
		// 								);
		// 								var $metaData = $liContentWrapper.find(
		// 									'.js-clinicaltag-meta[data-clinicaltagid=' +
		// 										obj.ClinicalTagId +
		// 										']'
		// 								);

		// 								$addIcon.unbind();
		// 								$liContentWrapper.unbind();

		// 								$liContentWrapper.attr({
		// 									'data-addedbygroupid':
		// 										obj.MemberAreaId,
		// 									'data-context': obj.DataContext
		// 								});

		// 								// var classLists = $modal.find($(".js-clinicaltag-remove-icon"))[0].classList;
		// 								// var text = $modal.find($(".js-clinicaltag-remove-icon"))[0].innerHTML;

		// 								$addIcon
		// 									.attr({
		// 										'data-is-patient-tag': true,
		// 										'data-addedbygroupid':
		// 											obj.MemberAreaId,
		// 										'data-context': obj.DataContext
		// 									})
		// 									.removeClass()
		// 									.addClass(
		// 										'patientTags-remove-btn u-pull-right js-clinicaltag-remove-icon'
		// 									)
		// 									.html('Remove');

		// 								// var wrapperClass = $modal.find($(".js-active-tag-li-content"))[0].classList;
		// 								$liContentWrapper
		// 									.removeClass()
		// 									.addClass(
		// 										'patientTags-li__content js-active-tag-li-content'
		// 									);
		// 								$metaData.text(
		// 									Vault.getItem('LoggedInUserName') +
		// 										', just now'
		// 								);

		// 								//  $metaData.empty();

		// 								//Update the local patient model and set the added clinical tag as active.
		// 								if (patientView.getCachedPatient()) {
		// 									$.when(
		// 										_updatePatientClinicalTagsViewModelWithAddedTag(
		// 											obj.ClinicalTagId,
		// 											obj.MemberAreaId,
		// 											obj.ClinicalTagName
		// 										).then(function() {
		// 											//Now Reloads - https://doccom.atlassian.net/browse/WEB-742
		// 											//update the tags block in PatientView
		// 											//PubSub.publish("PatientView.GetPatientActions.PatientTags", {
		// 											//    PatientId: obj.PatientId,
		// 											//    MemberAreaId: Vault.getSessionItem("ActiveGroupMemberAreaId") || null,
		// 											//    IsModal: false,
		// 											//    DataContext: obj.DataContext,
		// 											//    QtipContainer: null,
		// 											//    QtipViewport: $(window),
		// 											//    CloseEvents: 'click unfocus',
		// 											//    HideDistance: 30
		// 											//});
		// 										})
		// 									);
		// 								}
		// 							}
		// 						);

		// 						allTagsAddedSubscription = PubSub.subscribe(
		// 							'PatientView.GetPatientActions.PatientTags.AllQueuedTagsAdded',
		// 							function() {
		// 								//Clear selections
		// 								selectedTagsToAdd.TagIds.length = 0;
		// 								selectedTagsToAdd.TagMeta.length = 0;
		// 								//Bindings
		// 								bindClinicalTagUiElements(
		// 									obj.MemberAreaId
		// 								);
		// 								activeModal.close(); //Close modal
		// 							}
		// 						);
		// 					},

		// 					afterContent: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.afterContent,
		// 							this,
		// 							e
		// 						)();
		// 						$('.js-appModal-wrapper')
		// 							.find('[data-context]')
		// 							.attr('data-context', obj.DataContext); //Set context

		// 						if (obj.MemberAreaId) {
		// 							getPatientTagsForSelectedTeam(
		// 								obj.MemberAreaId
		// 							); //Load the team tags in modal body
		// 						} else {
		// 							bindClinicalTagUiElements(false); //No team, must select a team first
		// 						}
		// 					},
		// 					beforeClose: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.beforeClose,
		// 							this,
		// 							e
		// 						)();

		// 						if (
		// 							!_closePromptBool &&
		// 							!!selectedTagsToAdd.TagIds.length
		// 						) {
		// 							confirmCloseModal();
		// 							return false; //Prevent close
		// 						} else {
		// 							tagAddedSubscription.remove();
		// 							tagRemovedSubscription.remove();
		// 							allTagsAddedSubscription.remove();
		// 						}
		// 					},
		// 					afterClose: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.afterClose,
		// 							this,
		// 							e
		// 						)();
		// 					}
		// 				});
		// 			});
		// 	} else {
		// 		//Regular control, view tags with link to edit
		// 		// var domSelectorToInsertTemplateTo = $(".js-teamPatientView-tags-tab-template");
		// 		// GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, patientClinicalTags).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
		// 		//     /*Render tag meta data popovers. A patient can have duplicate tags so need to also check on the member area where tag was applied (this will be a unique combination). Merge the values to create a unique tag ID so patient does not have duplicate meta data.*/
		// 		//     domSelectorToInsertTemplateTo.find("[data-context]").attr("data-context", obj.DataContext);
		// 		//     bindClinicalTagUiElements(obj.MemberAreaId);
		// 		//     $(".js-patientView-manage-tags[data-context=" + obj.DataContext + "]").unbind().on("click", function (e) { //Specific to Non modal control
		// 		//         PubSub.publish("PatientView.GetPatientActions.PatientTags", {
		// 		//             PatientId: obj.PatientId,
		// 		//             PatientData: _patientData.Data,
		// 		//             ContentId: null,
		// 		//             Event: e,
		// 		//             DomObject: $(e.currentTarget),
		// 		//             IsModal: true,
		// 		//             DataContext: obj.DataContext, //Launch modal in GetPatientActions context, so hard code context
		// 		//             RenderPatientBannerInFeed: (feeds.getLastLoadedFeedType() === "PatientFeed") ? false : true,
		// 		//             IsMemberAreaContext: obj.IsMemberAreaContext,
		// 		//             MemberAreaId: Vault.getSessionItem("ActiveGroupMemberAreaId") || null,
		// 		//             Permissions: obj.Permissions
		// 		//         });
		// 		//     });
		// 		//     appMain.formatTemplateDates();
		// 		// });
		// 	}

		// 	//Privates
		// 	function bindClinicalTagUiElements(teamId) {
		// 		if (obj.IsModal) {
		// 			//Modal bindings
		// 			var $addTagEl = $('.js-clinicaltag-add-tag');
		// 			var $existingTag = $('.js-active-tag-li-content');
		// 			var $addTagsSaveBtn = $('.js-patientTags-add-tags-save');
		// 			var $teamSelectDdl = $('.js-clinicaltag-member-groups');

		// 			//Clear any search val
		// 			$('.js-patientTags-filter-txt').val('');
		// 			//Team Select
		// 			if (!teamId) {
		// 				$teamSelectDdl.unbind().on('change', function() {
		// 					var teamSelected = parseInt(
		// 						$('option:selected', this).val(),
		// 						10
		// 					);
		// 					teamId = teamSelected || null;
		// 					getPatientTagsForSelectedTeam(teamSelected);
		// 				});
		// 				$teamSelectDdl.select2({
		// 					placeholder: 'Choose a team...'
		// 				});
		// 			} else {
		// 				//Team present, bind full control
		// 				//Live tag filter
		// 				var searchWrapper = document.getElementsByClassName(
		// 					'js-patientTags-ul-template-search-wrap'
		// 				)[0];
		// 				var options = {
		// 					valueNames: [{ data: ['filter-name'] }],
		// 					searchClass: 'js-patientTags-filter-txt'
		// 				};
		// 				var userList = new List(searchWrapper, options); //Get the element node
		// 				//searchWrapper.setAttribute('data-is-bound', true);

		// 				//Add a tag in modal
		// 				$addTagEl.unbind().on('click', function() {
		// 					var $self = $(this);
		// 					var tagId = $self.data('clinicaltagid');
		// 					var tagName = $self.data('value');
		// 					var tagPlural;

		// 					if (_.contains(selectedTagsToAdd.TagIds, tagId)) {
		// 						//Remove selected tag from array
		// 						selectedTagsToAdd.TagIds = _.without(
		// 							selectedTagsToAdd.TagIds,
		// 							tagId
		// 						);
		// 						selectedTagsToAdd.TagMeta = _.reject(
		// 							selectedTagsToAdd.TagMeta,
		// 							function(tag) {
		// 								return tag.TagId === tagId;
		// 							}
		// 						);

		// 						$self.removeClass('tag--added');
		// 						var $uiIcon = $self.find('.js-tag-ui-status');
		// 						$uiIcon
		// 							.removeClass('fa-check-square-o')
		// 							.addClass('fa-square-o');

		// 						if (selectedTagsToAdd.TagIds.length) {
		// 							tagPlural =
		// 								selectedTagsToAdd.TagIds.length > 1
		// 									? 'tags'
		// 									: 'tag';
		// 							$addTagsSaveBtn
		// 								.text(
		// 									'Add ' +
		// 										selectedTagsToAdd.TagIds
		// 											.length +
		// 										' ' +
		// 										tagPlural +
		// 										' to patient'
		// 								)
		// 								.removeClass('u-no-display');
		// 							appMain.setModalElementsAsFullHeight();
		// 						} else {
		// 							$addTagsSaveBtn
		// 								.text('')
		// 								.addClass('u-no-display');
		// 							appMain.setModalElementsAsFullHeight();
		// 						}
		// 					} else {
		// 						//Add
		// 						selectedTagsToAdd.TagIds.push(tagId);
		// 						selectedTagsToAdd.TagMeta.push({
		// 							TagId: tagId,
		// 							Name: tagName
		// 						});
		// 						$self.addClass('tag--added');
		// 						var $uiIcon = $self.find('.js-tag-ui-status');
		// 						$uiIcon
		// 							.addClass('fa-check-square-o')
		// 							.removeClass('fa-square-o');
		// 						tagPlural =
		// 							selectedTagsToAdd.TagIds.length > 1
		// 								? 'tags'
		// 								: 'tag';
		// 						$addTagsSaveBtn
		// 							.text(
		// 								'Add ' +
		// 									selectedTagsToAdd.TagIds.length +
		// 									' ' +
		// 									tagPlural +
		// 									' to patient'
		// 							)
		// 							.removeClass('u-no-display');
		// 						appMain.setModalElementsAsFullHeight();
		// 					}
		// 				});

		// 				//Save Bind
		// 				//if (!$addTagsSaveBtn.data("is-bound")) {
		// 				$addTagsSaveBtn
		// 					.unbind()
		// 					.on('click', function(e) {
		// 						if (!selectedTagsToAdd.TagIds.length)
		// 							return false;
		// 						var $self = $(this);
		// 						$self.ajaxActiveButtonUi({
		// 							ActionState: 'start',
		// 							LockedText: 'Saving tags',
		// 							DisableContainerInputs: $(
		// 								'.js-patientTags-modalWrapper'
		// 							)
		// 						});
		// 						appMain.ajaxLoadMask({
		// 							ContainerDomObject: $(
		// 								'.js-patientTags-list'
		// 							)
		// 						});

		// 						$.when(
		// 							_addPatientClinicalTag({
		// 								MemberAreaId: teamId,
		// 								PatientId: obj.PatientId,
		// 								ClinicalTagIds: selectedTagsToAdd,
		// 								DataContext: obj.DataContext
		// 							}).then(function(data, textStatus, jqXHr) {
		// 								if (/^2/.test(jqXHr.status)) {
		// 									// Status Codes 2xx
		// 									$self.ajaxActiveButtonUi({
		// 										ActionState: 'stop',
		// 										DisableContainerInputs: $(
		// 											'.js-patientTags-modalWrapper'
		// 										)
		// 									});
		// 									appMain.ajaxLoadUnMask({
		// 										ContainerDomObject: $(
		// 											'.js-patientTags-list'
		// 										)
		// 									});
		// 									if (
		// 										!selectedTagsToAdd.TagIds.length
		// 									) {
		// 										$self
		// 											.text('')
		// 											.addClass('u-no-display');
		// 									}
		// 								}
		// 							})
		// 						);
		// 					})
		// 					.data('is-bound', true);

		// 				if (!selectedTagsToAdd.TagIds.length)
		// 					$addTagsSaveBtn.text('').addClass('u-no-display');

		// 				// } else { //Called again, binding in place
		// 				//if (!selectedTagsToAdd.TagIds.length) {
		// 				//    $addTagsSaveBtn.text("").addClass("u-no-display");
		// 				//}
		// 				// }
		// 			}
		// 		} else {
		// 			//Regular template control
		// 			$('.js-clinicaltag[data-context=' + obj.DataContext + ']')
		// 				.unbind()
		// 				.on('mouseenter', function(e) {
		// 					var $self = $(this);
		// 					var patientId = $self.data('patientid');
		// 					var clinicalTagId = $self.data('clinicaltagid');
		// 					var clinicalTagAddedByGroupId = $self.data(
		// 						'addedbygroupid'
		// 					);
		// 					var addedByActor = $self.data('addedbyactor');
		// 					var dateAdded = $self.data('addeddate');

		// 					PubSub.publish(
		// 						'PatientView.GetPatientActions.GetClinicalTagsMetaUi',
		// 						{
		// 							Event: e,
		// 							DataContext: obj.DataContext,
		// 							PatientExternalIdentifier: patientId,
		// 							ID: clinicalTagId,
		// 							AddedByGroupID: clinicalTagAddedByGroupId,
		// 							AddedByGroupName: appMain.getMemberAreaName(
		// 								parseInt(clinicalTagAddedByGroupId)
		// 							),
		// 							AddedByActorName: addedByActor,
		// 							AddedDate: dateAdded
		// 						}
		// 					);
		// 				});
		// 		}

		// 		//All context bindings

		// 		$(
		// 			'.js-clinicaltag-remove-icon[data-context=' +
		// 				obj.DataContext +
		// 				']'
		// 		)
		// 			.unbind()
		// 			.on('click', function(e) {
		// 				var $self = $(this);

		// 				PubSub.publish(
		// 					'PatientView.GetPatientActions.PatientTags.GetRemoveTagUI',
		// 					{
		// 						PatientId: obj.PatientId,
		// 						ContentId: null,
		// 						QtipContainer: $.featherlight.current()
		// 							? $self.parent()
		// 							: $self.closest(
		// 									'.js-fullHeight-bodyElement[data-scroll="true"]'
		// 							  ),
		// 						ClinicalTagId: $(e.currentTarget).data(
		// 							'clinicaltagid'
		// 						),
		// 						ClinicalTagName: $(e.currentTarget).data(
		// 							'clinicaltagname'
		// 						),
		// 						MemberAreaId: $(e.currentTarget).data(
		// 							'addedbygroupid'
		// 						),
		// 						Event: e,
		// 						DomObject: $(e.currentTarget),
		// 						DataContext: obj.DataContext
		// 					}
		// 				);

		// 				//PubSub.subscribeOnce("PatientView.GetPatientActions.PatientTags.RemoveTag", function () {
		// 				//    $self.ajaxActiveButtonUi({
		// 				//        ActionState: "start",
		// 				//        DisableContainerInputs: $(".js-patientTags-modalWrapper"),
		// 				//        LockedText: "Removing..."
		// 				//    });
		// 				//});

		// 				//PubSub.subscribeOnce("PatientView.GetPatientActions.PatientTags.TagRemoved", function () {
		// 				//    $self.ajaxActiveButtonUi({
		// 				//        ActionState: "stop",
		// 				//        DisableContainerInputs: $(".js-patientTags-modalWrapper"),
		// 				//    });
		// 				//});
		// 			});
		// 	}

		// 	function thereAreTeamTagsAndOtherTeamTags(obj) {
		// 		//Get tags, seperated by this team and other teams
		// 		//var tags = _patientData.Data.ClinicalTags;
		// 		var tags = obj.PatientData.ClinicalTags;

		// 		var thisTeamTags = !!_.where(tags, {
		// 			AddedByGroupID: Vault.getSessionItem(
		// 				'ActiveGroupMemberAreaId'
		// 			)
		// 		}).length;

		// 		var otherTeamTags = !!_.filter(tags, function(tag) {
		// 			return (
		// 				tag.AddedByGroupID !=
		// 				Vault.getSessionItem('ActiveGroupMemberAreaId')
		// 			);
		// 		}).length;

		// 		return {
		// 			ThisTeamTags: thisTeamTags,
		// 			OtherTeamTags: otherTeamTags
		// 		};
		// 	}

		// 	function getPatientTagsForSelectedTeam(teamId) {
		// 		//Load tags in modal via inline partial, by team selected
		// 		var $self = $(this);
		// 		var domSelectorToInsertTemplateTo = $(
		// 			'.js-patientTags-ul-template'
		// 		);
		// 		var templatePath =
		// 			'Content/js/handlebarsTemplates/patientTags/Modal_patientTagsList.htm';
		// 		var teamTags = _.filter(patientClinicalTags.Data, function(
		// 			tag
		// 		) {
		// 			//Get team tags
		// 			return tag.AddedByGroupID === parseInt(teamId, 10);
		// 		});
		// 		var tagsThatCanBeAddedFromTeamSelected;

		// 		domSelectorToInsertTemplateTo.ajaxLoadingHelper({
		// 			LoadingMessage: 'Loading tags...'
		// 		});

		// 		$.when(
		// 			getClinicalTagsThatCanBeAppliedToAPatient(teamId).then(
		// 				function(data) {
		// 					//Filter out tags already active on patient
		// 					var availableTagIds = [];
		// 					var index;
		// 					for (index = 0; index < teamTags.length; ++index) {
		// 						availableTagIds.push(teamTags[index]['ID']);
		// 					}

		// 					tagsThatCanBeAddedFromTeamSelected = _.reject(
		// 						data,
		// 						function(tag) {
		// 							return _.contains(availableTagIds, tag.ID);
		// 						}
		// 					);

		// 					var templateData = {
		// 						IsModal: obj.IsModal,
		// 						Data: teamTags, //Tags already active on patient
		// 						AvailableTeamTagsThatCanBeAdded: tagsThatCanBeAddedFromTeamSelected,
		// 						IsFirstLoad: false,
		// 						DataContext: obj.DataContext
		// 					};
		// 					GetHandlebarsTemplate.callApi(
		// 						templatePath,
		// 						false,
		// 						templateData
		// 					)
		// 						.then(GetHandlebarsTemplate.onTemplateInserted)
		// 						.then(function(html) {
		// 							domSelectorToInsertTemplateTo.html(html);
		// 							bindClinicalTagUiElements(teamId);
		// 							appMain.ajaxLoadingHelperComplete();
		// 							appMain.formatTemplateDates();
		// 						});
		// 				}
		// 			)
		// 		); //Deferred, makes API call)
		// 	}

		// 	function confirmCloseModal() {
		// 		//pass attributes to a self-closed element
		// 		var message = $('<p />', {
		// 				text:
		// 					'Are you sure you want to leave without saving your changes?',
		// 				class: ''
		// 			}),
		// 			ok = $('<button />', {
		// 				text: 'Leave, discard changes',
		// 				class:
		// 					'btn--plain--block-pad u-pull-right js-prompt-confirm'
		// 			}),
		// 			cancel = $('<button/>', {
		// 				text: 'Keep editing',
		// 				class: 'btn u-pull-left js-prompt-cancel'
		// 			});

		// 		var callBack = function(event, api) {
		// 			$('.js-prompt-confirm').on('click', function(e) {
		// 				_closePromptBool = true;
		// 				api.destroy(e);
		// 				$.featherlight.close();
		// 			});

		// 			$('.js-prompt-cancel').on('click', function(e) {
		// 				api.destroy(e);
		// 			});
		// 		};
		// 		appMain.drawPromptConfirmDialog(
		// 			message.add(ok).add(cancel),
		// 			'New tags will not be saved',
		// 			callBack
		// 		);
		// 	}
		// };

		// var _patientContextActionsGetPatientTeamListsControl = function(obj) {//Modal - deprecate with vue
		// 	//Get lists the patient is on and can be added to - /Patients

		// 	if (!_patientData && !obj.PatientData) return;

		// 	let patientData = _patientData
		// 		? _patientData.Data
		// 		: obj.PatientData;

		// 	//var listsPatientIsOn = _patientData.Data.GroupLists;
		// 	let listsPatientCanBeAddedTo = appMain.getGroupListsPatientCanBeAddedTo(
		// 		patientData.GroupListExternalIdentifiers,
		// 		patientData.GroupLists
		// 	); //Pass in existing memberships

		// 	let templateData = {
		// 		PatientExternalIdentifier:
		// 			patientData.PatientExternalIdentifier,
		// 		MergedPatientLists: listsPatientCanBeAddedTo,
		// 		Patient: {
		// 			PatientDetails: patientData //To match formatting of object elsewhere
		// 		},
		// 		DataContext: obj.DataContext,
		// 		IsModal: obj.IsModal
		// 	};

		// 	let templatePath =
		// 		'Content/js/handlebarsTemplates/PatientView__patientListsControl.htm';
		// 	let domSelectorToInsertTemplateTo = obj.IsModal
		// 		? null
		// 		: $('.js-teamPatientView-lists-tab-template');

		// 	GetHandlebarsTemplate.callApi(
		// 		templatePath,
		// 		domSelectorToInsertTemplateTo,
		// 		templateData
		// 	).then(function (html) {
		// 			if (obj.IsModal) {
		// 				let $modal = $.featherlight(html, {
		// 					closeOnEsc: true,
		// 					closeOnClick: true,
		// 					type: 'html',
		// 					beforeClose: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.beforeClose,
		// 							this,
		// 							e
		// 						)();
		// 					},
		// 					beforeOpen: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.beforeOpen,
		// 							this,
		// 							e
		// 						)();
		// 						appMain.closeAllqTips();
		// 					},
		// 					afterContent: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.afterContent,
		// 							this,
		// 							e
		// 						)();
		// 						domSelectorToInsertTemplateTo = $.featherlight.current()
		// 							.$content; //Assign DOM container
		// 					},
		// 					events: {
		// 						hide: function() {
		// 							patientData = null;
		// 						}
		// 					}
		// 				});
		// 			}

		// 			//Bindings

		// 			domSelectorToInsertTemplateTo
		// 				.find('[data-component-tickbox]')
		// 				.cfTickbox()
		// 				.on('tick', function(e) {
		// 					var $self = $(this);
		// 					var status =
		// 						$self.attr('data-is-ticked') === 'true'
		// 							? true
		// 							: false;

		// 					if (status) {
		// 						appMain.ajaxLoadMask();

		// 						_addPatientToGroupList({
		// 							MemberAreaId: $self.data('memberareaid'),
		// 							PatientId: obj.PatientId //Set when patient loaded into cache
		// 						}).then(response => {

		// 							if (obj.IsModal) {
		// 								PubSub.publish(
		// 									'PatientView.GetPatientActions.PatientListStatusUpdated',
		// 									{
		// 										PatientId: obj.PatientId
		// 									}
		// 								);

		// 							}
		// 							//Tell Vue
		// 							VueEventBus.$emit(
		// 								'PatientView.GetPatientActions.PatientListStatusUpdated',
		// 								{
		// 									PatientId: obj.PatientId,
		// 									IsModal:obj.IsModal
		// 								}
		// 							);

		// 							appMain.ajaxLoadUnMask();
		// 						});
		// 					} else {
		// 						appMain.ajaxLoadMask();
		// 						_removePatientFromGroupList({
		// 							MemberAreaId: $self.data('memberareaid'),
		// 							PatientId: obj.PatientId //Set when patient loaded into cache
		// 						}).then(function() {
		// 							if (obj.IsModal) {
		// 								PubSub.publish(
		// 									'PatientView.GetPatientActions.PatientListStatusUpdated',
		// 									{
		// 										PatientId: obj.PatientId
		// 									}
		// 								);
		// 							}
		// 							//Tell Vue
		// 							VueEventBus.$emit(
		// 								'PatientView.GetPatientActions.PatientListStatusUpdated',
		// 								{
		// 									PatientId: obj.PatientId,
		// 									IsModal:obj.IsModal
		// 								}
		// 							);
		// 							appMain.ajaxLoadUnMask();
		// 						});
		// 					}
		// 				});

		// 			//$(".js-patientView-patient-lists-edit-btn").unbind().on("click", function (e) {
		// 			//    PubSub.publish("PatientView.GetPatientActions.PatientLists", {
		// 			//        PatientId: obj.PatientId,
		// 			//        ContentId: null,
		// 			//        Event: e,
		// 			//        DomObject: $(e.currentTarget),
		// 			//        DataContext: "GetPatientActions", //Open modal in context of GetPatientActions
		// 			//        ReloadView: false,
		// 			//        IsModal: true
		// 			//    });
		// 			//});

		// 			$('.js-patientLists-inclusion-type-icon')
		// 				.unbind()
		// 				.on('mouseenter', function() {
		// 					//Inclusion type popover
		// 					var $self = $(this);
		// 					var inclusionType = $self.data('inclusion-type');
		// 					var text =
		// 						inclusionType === 'Pin'
		// 							? 'Patient has been pinned to the team list'
		// 							: 'Patient is on the list due to Smart List criteria';

		// 					appMain.drawMouseEnteredPopoutBubble({
		// 						DomObject: $self,
		// 						Content: text,
		// 						QtipViewport: obj.QtipViewport,
		// 						Classes: '.js-patientList-inclusion-type-tip'
		// 					});
		// 				})
		// 				.on('click', function(e) {
		// 					e.stopPropagation();
		// 					return false;
		// 				});

		// 			//Clean up
		// 			templateData = null;
		// 		}).fail(error => {
		// 		});
		// };

		// var _patientContextActionsGetPatientReferralControl = function(obj) {
		// 	GetReferralGroups.callApi(
		// 		Vault.getSessionItem('ActiveNetworkId')
		// 	).then(function(data) {
		// 		var openModal = $.featherlight.current();
		// 		if (openModal) {
		// 			//Close the patient search modal and clear patientSearch.getSelectedPatients(). Close it here to allow GetReferralGroups API call, so modals close/open in one transition without a delay.
		// 			$.featherlight.close();
		// 		}

		// 		var templatePath =
		// 			'Content/js/handlebarsTemplates/PatientView__referral.htm';
		// 		var subscription;

		// 		data.PatientDetails = _patientData
		// 			? _patientData.Data
		// 			: obj.PatientData; //Take patient from cache if present

		// 		GetHandlebarsTemplate.callApi(templatePath, false, data).then(
		// 			function(html) {
		// 				var referralModal = $.featherlight(html, {
		// 					closeOnEsc: true,
		// 					closeOnClick: true,
		// 					type: 'html',
		// 					beforeClose: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.beforeClose,
		// 							this,
		// 							e
		// 						)();
		// 						subscription.remove();
		// 					},
		// 					beforeOpen: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.beforeOpen,
		// 							this,
		// 							e
		// 						)();
		// 						appMain.closeAllqTips();
		// 					},
		// 					afterContent: function(e) {
		// 						$.proxy(
		// 							$.featherlight.defaults.afterContent,
		// 							this,
		// 							e
		// 						)();

		// 						autosize($("[data-autosize='true']")); //Grow textareas
		// 						var $referralReceiverDdl = $(
		// 							'.js-patientReferral-receivingTeam-ddl'
		// 						);
		// 						var $referralSenderDdl = $(
		// 							'.js-patientReferral-sendingTeam-ddl'
		// 						);

		// 						$referralReceiverDdl.select2({
		// 							placeholder: 'Please select team',
		// 							width: '70%'
		// 						});

		// 						$referralSenderDdl
		// 							.select2({
		// 								placeholder: 'Please select team',
		// 								width: '70%'
		// 							})
		// 							.val(obj.MemberAreaId)
		// 							.trigger('change');

		// 						//Set Data Context - GetPatientActions
		// 						$('.js-appModal-wrapper')
		// 							.find('[data-context]')
		// 							.attr('data-context', obj.DataContext);

		// 						window.Parsley.addValidator('multipleOf', {
		// 							validateNumber: function(
		// 								value,
		// 								requirement
		// 							) {
		// 								return value % requirement === 0;
		// 							},
		// 							requirementType: 'integer',
		// 							messages: {
		// 								en:
		// 									'This value should be a multiple of %s.',
		// 								fr:
		// 									"Ce nombre n'est pas un multiple de %s."
		// 							}
		// 						});

		// 						//Validator to enforce ending and receiving teams cannot be the same
		// 						window.Parsley.addValidator(
		// 							'checkReferralTeamsValid',
		// 							{
		// 								validateNumber: function(
		// 									value,
		// 									requirement
		// 								) {
		// 									return (
		// 										value !=
		// 										$referralSenderDdl.val()
		// 									);
		// 								},
		// 								requirementType: 'integer',
		// 								messages: {
		// 									en:
		// 										'Sending and receiving teams cannot be the same'
		// 								}
		// 							}
		// 						);

		// 						//Submit binding
		// 						$('.js-patientReferal-submitBtn')
		// 							.unbind()
		// 							.on('click', function() {
		// 								var $self = $(this);

		// 								var formElement = $(
		// 									'.js-patientReferal-formWrapper[data-context=' +
		// 										obj.DataContext +
		// 										''
		// 								);

		// 								formElement.parsley().validate();

		// 								if (!formElement.parsley().isValid()) {
		// 									return false;
		// 								}

		// 								$self.ajaxActiveButtonUi({
		// 									ActionState: 'start',
		// 									DisableContainerInputs: formElement,
		// 									LockedText: 'Starting referral...'
		// 								});

		// 								_patientContextActionsSubmitReferral({
		// 									DataContext: obj.DataContext,
		// 									PatientData: data.PatientDetails,
		// 									PatientId:
		// 										data.PatientDetails
		// 											.PatientExternalIdentifier,
		// 									ReceivingTeam: $referralReceiverDdl.val(),
		// 									SendingTeam: $referralSenderDdl.val(),
		// 									SituationTxt: $(
		// 										'.js-patientReferal-situationTxt'
		// 									).val(),
		// 									BackgroundTxt: $(
		// 										'.js-patientReferal-backgroundTxt'
		// 									).val(),
		// 									AssessmentTxt: $(
		// 										'.js-patientReferal-assessmentTxt'
		// 									).val(),
		// 									RecommentdationTxt: $(
		// 										'.js-patientReferal-recommentdationTxt'
		// 									).val()
		// 								})
		// 									.then(function() {})
		// 									.fail(function() {});
		// 							});

		// 						subscription = PubSub.subscribe(
		// 							'GetPatientActions.ReferralSent',
		// 							function(obj) {
		// 								//Push UI alert
		// 								appMain.triggerAppNotification({
		// 									Text:
		// 										'Your patient referral was started',
		// 									Heading: '',
		// 									Icon: 'success'
		// 								});

		// 								referralModal.close();
		// 							}
		// 						);
		// 					},
		// 					events: {
		// 						hide: function() {}
		// 					}
		// 				});
		// 			}
		// 		);
		// 	});
		// };

		// var _patientContextActionsSubmitReferral = function(obj) {
		// 	var referralFields = [
		// 		{
		// 			FieldName: 'Situation',
		// 			FieldValue: obj.SituationTxt
		// 		},
		// 		{
		// 			FieldName: 'Background',
		// 			FieldValue: obj.BackgroundTxt
		// 		},
		// 		{
		// 			FieldName: 'Assessment',
		// 			FieldValue: obj.AssessmentTxt
		// 		},
		// 		{
		// 			FieldName: 'Recommendation',
		// 			FieldValue: obj.RecommentdationTxt
		// 		}
		// 	];

		// 	return SendReferral.callApi(
		// 		obj.SendingTeam,
		// 		obj.ReceivingTeam,
		// 		obj.PatientId,
		// 		null,
		// 		referralFields
		// 	)
		// 		.then(function(data, textStatus, jQXHr) {
		// 			if (/^2/.test(jQXHr.status)) {
		// 				// PubSub.publish('GetPatientActions.ReferralSent', {
		// 				// 	ContentItemId: data.Data.ContentItemId,
		// 				// 	PatientData: obj.PatientData,
		// 				// 	PatientId: obj.PatientId,
		// 				// 	ReceivingTeam: obj.ReceivingTeam,
		// 				// 	SendingTeam: obj.SendingTeam,
		// 				// 	MemberAreaId: obj.SendingTeam,
		// 				// 	SituationTxt: obj.SituationTxt,
		// 				// 	BackgroundTxt: obj.BackgroundTxt,
		// 				// 	AssessmentTxt: obj.AssessmentTxt,
		// 				// 	RecommentdationTxt: obj.RecommentdationTxt
		// 				// });

		// 				//Any Vue.js listeners
		// 				// VueEventBus.$emit('GetPatientActions.ReferralSent', {
		// 				// 	ContentItemId: data.Data.ContentItemId,
		// 				// 	PatientData: obj.PatientData,
		// 				// 	PatientId: obj.PatientId,
		// 				// 	ReceivingTeam: obj.ReceivingTeam,
		// 				// 	SendingTeam: obj.SendingTeam,
		// 				// 	MemberAreaId: obj.SendingTeam,
		// 				// 	SituationTxt: obj.SituationTxt,
		// 				// 	BackgroundTxt: obj.BackgroundTxt,
		// 				// 	AssessmentTxt: obj.AssessmentTxt,
		// 				// 	RecommentdationTxt: obj.RecommentdationTxt
		// 				// });
		// 			}
		// 		})
		// 		.fail(function() {});
		// };

		var _patientContextActionsPostUpdate = function(obj) {
			var options = {
				PatientData: _patientData ? _patientData.Data : null,
				ControlId: 3,
				ParentControlId: null,
				DataContext: '',
				MemberAreaId: ''
			};
			$.extend(options, obj);

			$.when(
				feeds.getCreateFeedContentControls(
					options.MemberAreaId,
					options.ControlId,
					options.DataContext,
					options.PatientData,
					true,
					options.RenderPatientBannerInFeed,
					options.ParentControlId
				)
			).then(function(html, callback) {
				$.featherlight(html, {
					closeOnEsc: true,
					closeOnClick: true,
					type: 'html',
					beforeClose: function() {},
					beforeOpen: function(e) {
						$.proxy($.featherlight.defaults.beforeOpen, this, e)();
						appMain.closeAllqTips();
					},
					afterContent: function(e) {
						$.proxy(
							$.featherlight.defaults.afterContent,
							this,
							e
						)();
						callback(
							options.MemberAreaId,
							options.ControlId,
							options.DataContext,
							options.PatientData,
							true,
							options.RenderPatientBannerInFeed,
							options.ParentControlId
						);
					},
					afterClose: function(e) {
						$.proxy($.featherlight.defaults.afterClose, this, e)();
					}
				});
			});
		};

		//TODO:REWORK / REFACTOR FROM HERE ON

		var patient_submitUpdate = function(obj) {
			//Send update via a content control form. Control can be PatientView, GetPatientActions of ContentFeed contexts
			var submitUpdateResult = $.Deferred(function() {
				var result = this;
				StartNewConversationTaggedWithPatient.callApi(
					obj.MemberAreaId,
					obj.Title,
					obj.Body,
					obj.PatientId,
					obj.StartedFromContentItemId
				).then(function(data, textStatus, jQXHr) {
					if (/^2/.test(jQXHr.status)) {
						//Vue.JS - Emit Event
						VueEventBus.$emit('Feeds.NewPatientFeedItemAdded', {
							MemberAreaId: obj.MemberAreaId,
							Title: obj.Title,
							Body: obj.Body,
							ContentItemID: data.Data.ContentItemID,
							PatientData: obj.PatientData
						});
					}

					result.resolve({
						Data: data,
						TextStatus: textStatus,
						JQXHr: jQXHr
					});
				});

				return result;
			});

			//Any (#) clinical tags added to be parsed and appled
			var tagsAddedInPatientUpdate = $(
				'.js-patientUpdate-updateBody-txt[data-context=' +
					obj.DataContext +
					']'
			).data('tags-added')
				? $(
						'.js-patientUpdate-updateBody-txt[data-context=' +
							obj.DataContext +
							']'
				  ).data('tags-added')
				: false; //Data added dynamically whilst user is typing update and adding tags

			if (tagsAddedInPatientUpdate) {
				var clinicalTagsToApplyToPatient = {};
				clinicalTagsToApplyToPatient.Data = [];
				clinicalTagsToApplyToPatient.TagMetaData = [];

				//If there are tags that are not already applied to the patient in the context of the group ID, apply them. Firstly, check that they were not removed from the update textarea after being added

				$.each(tagsAddedInPatientUpdate.Data, function() {
					var tagString = this.inputTagText;

					if (obj.Body.toString().indexOf(tagString) >= 0) {
						//Check that tag has not been entered twice in body, so already exists here. If so, skip it
						if (
							_.indexOf(
								clinicalTagsToApplyToPatient.Data,
								this.tagID
							) === -1
						) {
							clinicalTagsToApplyToPatient.Data.push(this.tagID);
							clinicalTagsToApplyToPatient.TagMetaData.push(this);
						} else {
							return;
						}
					}
				});

				var addClinicalTagsResult = function(deferred) {
					_addPatientClinicalTag({
						MemberAreaId: obj.MemberAreaId,
						PatientId: obj.PatientId,
						ClinicalTagIds: clinicalTagsToApplyToPatient.Data,
						ClinicalTagMetaData: clinicalTagsToApplyToPatient,
						TagAddedContext: 'PatientUpdate',
						DataContext: obj.DataContext
					})
						.then(function(data, textStatus, jQXHr) {
							deferred.resolve(data, textStatus, jQXHr);
						})
						.fail();
				};
			} else {
				addClinicalTagsResult = function(deferred) {
					deferred.resolve(false);
				};
			}

			$.when($.Deferred(addClinicalTagsResult), submitUpdateResult).then(
				function(addClinicalTagsResultData, submitUpdateResultData) {
					if (submitUpdateResultData.JQXHr.status === 200) {
						//Publish update

						PubSub.publish('PatientView.GlobalUpdateAdded', {
							//Update added in non PatientView context
							PatientData: obj.PatientData,
							Body: obj.Body,
							Title: obj.Title,
							MemberAreaId: obj.MemberAreaId,
							ContentItemId:
								submitUpdateResultData.Data.Data.ContentItemID,
							ResetFeedControls: true,
							IsPatientView: getCachedPatient() ? true : false,
							DataContext: obj.DataContext,
							RenderPatientBannerInFeed:
								obj.RenderPatientBannerInFeed,
							ControlId: obj.ControlId,
							IsModal: obj.IsModal
						});

						//Tell Vue TODO:remove after refactor
						setTimeout(() => {
							VueEventBus.$emit(
								'PatientView.GlobalUpdateAdded',
								obj
							);
						}, 3000); //Allow for Raven DB sync lapse

						if (obj.IsModal) {
							$.featherlight.close(); //close the GetPatientActions modal
						}

						//Push UI alert
						appMain.triggerAppNotification({
							Text: 'Patient update added successfully',
							Heading: '',
							Icon: 'success'
						});
					}
				}
			);
		};

		// var _patientContextActions__removePatientTag = function(obj) {
		// 	//Tags are removed via a tip modal
		// 	if (obj.DomObject.data('rendered')) {
		// 		//Add here due to async call for get removal reasons. Prevent if open already
		// 		obj.DomObject.qtip('destroy');
		// 		obj.DomObject.data('rendered', false);
		// 		return;
		// 	}

		// 	var clinicalTagId = obj.ClinicalTagId;
		// 	var clinicalTagName = obj.ClinicalTagName;
		// 	var memberAreaWhereTagWasApplied = parseInt(obj.MemberAreaId);
		// 	var patientId = obj.PatientId;
		// 	var templatePath =
		// 		'Content/js/handlebarsTemplates/PatientView__removeClinicalTag.htm';
		// 	var templateData = {};
		// 	templateData.MemberAreasThatAUserCanManagePatientClinicalTagsFrom = getMemberAreasThatAUserCanManagePatientClinicalTagsFrom();
		// 	templateData.TagId = clinicalTagId;
		// 	templateData.ReasonForRemovingTagID = null;
		// 	templateData.TagName = clinicalTagName;
		// 	templateData.PatientExternalIdentifier = patientId;
		// 	templateData.DataContext = obj.DataContext;
		// 	templateData.MemberAreaWhereTagWasApplied = memberAreaWhereTagWasApplied;
		// 	templateData.UserHasPermission = appMain.checkUserPermissionForMemberArea(
		// 		memberAreaWhereTagWasApplied,
		// 		'ManagePatientClinicalTags'
		// 	);
		// 	if (templateData.UserHasPermission)
		// 		templateData.MemberAreaNameWhereTagWasApplied = appMain.getMemberAreaName(
		// 			memberAreaWhereTagWasApplied
		// 		); //Only looks in teams user is a member of

		// 	//Get Reasons for removing
		// 	ReasonsForRemovingTag.callApi(memberAreaWhereTagWasApplied)
		// 		.then(function(data, textStatus, jqXHR) {
		// 			// Promise returns data object
		// 			templateData.ReasonsForRemovingTag = data.Data;
		// 		})
		// 		.then(function(data) {
		// 			return GetHandlebarsTemplate.callApi(
		// 				templatePath,
		// 				false,
		// 				templateData
		// 			).then(GetHandlebarsTemplate.onTemplateInserted);
		// 		})
		// 		.then(function(html) {
		// 			appMain.drawClickToOpenPopoutBubble({
		// 				DomObject: obj.DomObject,
		// 				Content: html, //Passed from handlebars
		// 				QtipViewport: obj.QtipViewport,
		// 				QtipContainer: obj.QtipContainer,
		// 				ShowCallBack: function(api) {
		// 					//Cancel
		// 					//$(".js-removeClinicalTag-cancelBtn[data-clinicaltagid=" + clinicalTagId + "][data-patientid=" + patientId + "][data-context=" + obj.DataContext + "]").unbind().on("click", function () {
		// 					//    api.destroy();
		// 					//});

		// 					var $reasonForRemovingTagIDddL = $(
		// 						'.js-removeClinicalTag-reasons-ddl[data-clinicaltagid=' +
		// 							obj.ClinicalTagId +
		// 							'][data-memberareaid=' +
		// 							obj.MemberAreaId +
		// 							'][data-context=' +
		// 							obj.DataContext +
		// 							']'
		// 					);
		// 					var $customTxtWrapper = $(
		// 						'.js-removeClinicalTag--customtxt-wrapper[data-clinicaltagid=' +
		// 							clinicalTagId +
		// 							'][data-memberareaid=' +
		// 							memberAreaWhereTagWasApplied +
		// 							'][data-context=' +
		// 							obj.DataContext +
		// 							']'
		// 					);
		// 					var $customReasonTxt = $(
		// 						'.js-removeClinicalTag-custom-reason-txt[data-clinicaltagid=' +
		// 							clinicalTagId +
		// 							'][data-memberareaid=' +
		// 							memberAreaWhereTagWasApplied +
		// 							'][data-context=' +
		// 							obj.DataContext +
		// 							']'
		// 					);
		// 					var $reasonForRemovingTagSaveBtn = $(
		// 						'.js-removeClinicalTag-saveBtn[data-context=' +
		// 							obj.DataContext +
		// 							'][data-patientid=' +
		// 							obj.PatientId +
		// 							'][data-clinicaltagid=' +
		// 							clinicalTagId +
		// 							']'
		// 					);
		// 					var $formElement = $(
		// 						'.js-clinicaltag__manage-form[data-context=' +
		// 							obj.DataContext +
		// 							'][data-patientid=' +
		// 							patientId +
		// 							'][data-clinicaltagid=' +
		// 							clinicalTagId +
		// 							'][data-memberareaid=' +
		// 							obj.MemberAreaId +
		// 							']'
		// 					);

		// 					//Remove tag
		// 					$reasonForRemovingTagSaveBtn
		// 						.unbind()
		// 						.on('click', function(e) {
		// 							var $self = $(this);
		// 							$formElement.parsley().validate();

		// 							if (!$formElement.parsley().isValid()) {
		// 								api.reposition(); //Manual reposition as content changes
		// 								return false;
		// 							}

		// 							$self.ajaxActiveButtonUi({
		// 								ActionState: 'start',
		// 								LockedText: 'Removing tag...',
		// 								DisableContainerInputs: $formElement
		// 							});

		// 							PubSub.publish(
		// 								'PatientView.GetPatientActions.PatientTags.RemoveTag',
		// 								{
		// 									PatientId: patientId,
		// 									ContentId: null,
		// 									ClinicalTagId: clinicalTagId,
		// 									MemberAreaId: memberAreaWhereTagWasApplied,
		// 									ReasonForRemovingTagID: $(
		// 										'option:selected',
		// 										$reasonForRemovingTagIDddL
		// 									).val(),
		// 									Event: e,
		// 									DomObject: $self,
		// 									QtipViewport: obj.QtipViewport,
		// 									QtipContainer: obj.QtipContainer,
		// 									DataContext: obj.DataContext
		// 								}
		// 							);
		// 						});

		// 					//Reasons for removal DDL bindings
		// 					$reasonForRemovingTagIDddL
		// 						.unbind()
		// 						.on('change', function() {
		// 							if (
		// 								$(this)
		// 									.find('option:selected')
		// 									.val() === '-1'
		// 							) {
		// 								// "Other"
		// 								$customTxtWrapper.show();
		// 								api.reposition(); //Manual reposition as content changes
		// 								$customReasonTxt.attr({
		// 									'data-parsley-required': true,
		// 									'data-parsley-required-message':
		// 										'Please specify a reason'
		// 								});
		// 							} else {
		// 								$customTxtWrapper.hide();
		// 								api.reposition(); //Manual reposition as content changes
		// 								$customReasonTxt.attr({
		// 									'data-parsley-required': false
		// 								});
		// 							}
		// 						});
		// 				},
		// 				Classes: 'js-removeClinicalTag-qTip'
		// 			});
		// 		});
		// };

		var _getPatientObs = function(obj) {
			//VitalPac patient Obs - EHI Demo
			//  var patientId = 'H000000882';
			var packetId;
			var hospitalNumber =
				obj.Patient.PatientIdentifiers.PrimaryIdentifier.Value ||
				null;

			function getInterpretationClass(code) {
				code = parseInt(code);
				switch (true) {
					case code == 0:
						return 'patient-observations__interpretation-0';
					case code == 1:
						return 'patient-observations__interpretation-1';
					case code == 2:
						return 'patient-observations__interpretation-2';
					case code == 3:
						return 'patient-observations__interpretation-3';
					default:
						return '';
				}
			}

			function getEwsClass(code) {
				code = parseInt(code);
				switch (true) {
					case code == 0:
						return 'patient-observations__ews-0';
					case code == 1:
						return 'patient-observations__ews-1';
					case code == 2:
						return 'patient-observations__ews-2';
					case code == 3:
						return 'patient-observations__ews-3';
					case code == 4:
						return 'patient-observations__ews-4';
					case code == 5:
						return 'patient-observations__ews-5';
					case code == 6:
						return 'patient-observations__ews-6';
					case code == 7:
						return 'patient-observations__ews-7';
					case code == 8:
						return 'patient-observations__ews-8';
					case code == 9:
						return 'patient-observations__ews-9';
					default:
						return '';
				}
			}

			var ewsScore = axios
				.get(
					'https://demoazvp-int.syhapp.com/CobsMicroservice/v1/observation$lastn?patient=' +
						hospitalNumber +
						'&code=445551004&max=3'
				)
				.then(function(response) {
					if (!response.data.entry) return false;

					return response.data;
				})
				.catch(function(error) {
					return error;
				});

			var patientObs = axios
				.get(
					'https://demoazvp-int.syhapp.com/CobsMicroservice/v1/observation$lastn?patient=' +
						hospitalNumber +
						'&category=vital-signs&max=1'
				)
				.then(function(response) {
					console.log(response.data);
					//Parse Data

					if (!response.data.entry) return null;
					var templateData = {
						Data: [],
						Recorded: response.data.entry.length
							? moment(
									response.data.entry[0].resource
										.effectiveDateTime
							  ).format('DD-MMM-YYYY, HH:mm')
							: null,
						RecordedBy: response.data.entry.length
							? response.data.entry[0].resource.performer[0]
									.display
							: null
					};
					response.data.entry.forEach(function(item) {
						var shortName;
						switch (item.resource.code.text) {
							case 'Heart Rate':
								shortName = 'HR';
								break;
							case 'Blood Pressure':
								shortName = 'BP';
								break;
							case 'Respiratory Rate':
								shortName = 'RR';
								break;
							case 'Temperature':
								shortName = 'Temp';
								break;
							case 'Heart Rate ':
								shortName = 'HR';
								break;
							default:
								shortName = null;

								break;
						}

						var data = {
							Name: item.resource.code.text,
							ShortName: shortName,
							Value: _.has(item.resource, 'valueQuantity')
								? item.resource.valueQuantity.value
								: item.resource.valueCodeableConcept.coding[0]
										.display,
							Unit: _.has(item.resource, 'valueQuantity')
								? item.resource.valueQuantity.unit
								: null,
							Interpretation:
								item.resource.interpretation.coding[0]
									.display !== 'n/a'
									? item.resource.interpretation.coding[0]
											.display
									: '',
							InterpretationClass: getInterpretationClass(
								item.resource.interpretation.coding[0].display
							)
						};

						templateData.Data.push(data);
					});

					return templateData;
				})
				.catch(function(error) {
					console.log(error);
				});

			Promise.all([patientObs, ewsScore]).then(function(response) {
				//If data is sufficient, build the EWS graph in the summary
				if (response[1] && response[1].entry.length) {
					getSinglePatientEwsDataForLastTwentyFourHours(
						hospitalNumber
					)
						.then(function(response) {
							//VitalPac indicates that scores are taken from even items in response (0,2,4,6,8 etc)
							var chartData = [];
							var labelData = [];
							var labelCounter = 1;

							var x = _.filter(response.entry, function(
								element,
								index
							) {
								return index % 2 === 0;
							});

							x.forEach(function(item) {
								chartData.push({
									meta: moment(
										item.resource.effectiveDateTime
									).format('DD-MMM-YYYY, HH:mm'),
									value: parseInt(
										item.resource.valueCodeableConcept
											.coding[0].display
									)
								});
								labelData.push(labelCounter);
								labelCounter++;
							});

							x.length = 0; //Clear array
							console.log(99999999, chartData);

							//Build the chart in UI
							if (chartData && chartData.length > 2) {
								//Only build chart if more that 2 result sets
								var chart = new Chartist.Line(
									'.ct-chart',
									{
										labels: labelData,
										series: [chartData.reverse()]
									},
									{
										low: 0,
										high: 9,
										fullWidth: true,
										plugins: [Chartist.plugins.tooltip()]
									}
								);
							}
						})
						.catch(function(error) {});
				}

				GetHandlebarsTemplate.callApi(
					'Content/js/handlebarsTemplates/GroupHome__PatientView__vital-pac-test.htm',
					obj.DomContainer,
					response[0]
						? {
								Data: response[0].Data.length
									? response[0]
									: '',
								CountofEWSScores: response[1]
									? response[1].entry.length
									: null,
								EWS: response[1]
									? [
											{
												Score: response[1].entry[0]
													? response[1].entry[0]
															.resource
															.valueCodeableConcept
															.coding[0].display
													: '',
												Date: response[1].entry[0]
													? moment(
															response[1].entry[0]
																.resource
																.effectiveDateTime
													  ).format(
															'DD-MMM-YYYY, HH:mm'
													  )
													: '',
												Class: response[1].entry[0]
													? getEwsClass(
															response[1].entry[0]
																.resource
																.valueCodeableConcept
																.coding[0]
																.display
													  )
													: ''
											},
											{
												Score: response[1].entry[2]
													? response[1].entry[2]
															.resource
															.valueCodeableConcept
															.coding[0].display
													: '',
												Date: response[1].entry[2]
													? moment(
															response[1].entry[2]
																.resource
																.effectiveDateTime
													  ).format(
															'DD-MMM-YYYY, HH:mm'
													  )
													: '',
												Class: response[1].entry[2]
													? getEwsClass(
															response[1].entry[2]
																.resource
																.valueCodeableConcept
																.coding[0]
																.display
													  )
													: ''
											},
											{
												Score: response[1].entry[4]
													? response[1].entry[4]
															.resource
															.valueCodeableConcept
															.coding[0].display
													: '',
												Date: response[1].entry[4]
													? moment(
															response[1].entry[4]
																.resource
																.effectiveDateTime
													  ).format(
															'DD-MMM-YYYY, HH:mm'
													  )
													: '',
												Class: response[1].entry[4]
													? getEwsClass(
															response[1].entry[4]
																.resource
																.valueCodeableConcept
																.coding[0]
																.display
													  )
													: ''
											}
									  ]
									: null
						  }
						: null
				)
					.then(GetHandlebarsTemplate.onTemplateInserted)
					.then(function() {});
			});
		};

		var getSinglePatientObsData = function(patientId) {
			//Call VitalPac API to get patient obs data
		};

		var getSinglePatientEwsData = function(patientId) {
			//Call VitalPac API to get patient EWS data
			if (!patientId)
				return console.error('no patientid param passed for EWS');
			function getEwsClass(code) {
				code = parseInt(code);
				switch (true) {
					case code == 0:
						return 'patient-observations__ews-0';
					case code == 1:
						return 'patient-observations__ews-1';
					case code == 2:
						return 'patient-observations__ews-2';
					case code == 3:
						return 'patient-observations__ews-3';
					case code == 4:
						return 'patient-observations__ews-4';
					case code == 5:
						return 'patient-observations__ews-5';
					case code == 6:
						return 'patient-observations__ews-6';
					case code == 7:
						return 'patient-observations__ews-7';
					case code == 8:
						return 'patient-observations__ews-8';
					case code == 9:
						return 'patient-observations__ews-9';
					default:
						return '';
				}
			}

			var ewsScore = axios
				.get(
					'https://demoazvp-int.syhapp.com/CobsMicroservice/v1/observation$lastn?patient=' +
						patientId +
						'&code=445551004&max=1'
				)
				.then(function(response) {
					return response.data;
				})
				.catch(function(error) {
					return error;
				});

			return ewsScore
				.then(function(response) {
					if (!response.entry || !response.entry.length) return false;

					return {
						CountofEWSScores: response.entry.length,
						EWS: [
							{
								Score: response.entry[0]
									? response.entry[0].resource
											.valueCodeableConcept.coding[0]
											.display
									: '',
								Date: response.entry[0]
									? moment(
											response.entry[0].resource
												.effectiveDateTime
									  ).format('DD-MMM-YYYY, HH:mm')
									: '',
								Class: response.entry[0]
									? getEwsClass(
											response.entry[0].resource
												.valueCodeableConcept.coding[0]
												.display
									  )
									: ''
							}
						]
					};
				})
				.catch(function(error) {
					return error;
				});
		};

		var getSinglePatientEwsDataForLastTwentyFourHours = function(
			hospitalNumber
		) {
			var utcNow = moment.utc(new Date()).format();
			var utcStartDate = moment().subtract(365, 'day');
			utcStartDate = moment.utc(utcStartDate).format();

			return axios
				.get(
					'https://demoazvp-int.syhapp.com/CobsMicroservice/v1/observation$lastn?patient=' +
						hospitalNumber +
						'&code=445551004&start=' +
						utcStartDate +
						'&end=' +
						utcNow +
						'&max=4'
				)
				.then(function(response) {
					return response.data;
				})
				.catch(function(error) {
					return error;
				});
		};

		var _getPatientResults = function(obj) {
			//Patient summary widget - EHI demo 2017
			var hospitalNumber =
				obj.Patient.PatientIdentifiers.PrimaryIdentifier.Value ||
				null;

			//Medway Results API
			var allResultsApi =
				'/medway?path=Results/v3/patient/demo/hospitalno/' +
				hospitalNumber +
				'/allresults';
			var recentResultsApi =
				'/medway?path=Results/v3/patient/demo/hospitalno/' +
				hospitalNumber +
				'/recentresults';

			var allResultsApiCall = axios({
				method: 'get',
				url: allResultsApi,
				headers: {
					Authorization:
						'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlbW8tbWFwcC0wMS5kZW1vLm1ndC9ERU1PTElWRSIsImF1ZCI6Imh0dHA6Ly9tZWR3YXljbGllbnQuc3lzdGVtYy5jb20vYXBpLyIsIm5iZiI6MTUwODQxNTE2NywiZXhwIjoxNjM0NjQ2NDA3LCJodHRwOi8vaWRlbnRpdHlzZXJ2ZXIudGhpbmt0ZWN0dXJlLmNvbS9jbGFpbXMvY2xpZW50IjoiTWVkd2F5V2ViQXBpLURlbW9MSVZFIiwiaHR0cDovL2lkZW50aXR5c2VydmVyLnRoaW5rdGVjdHVyZS5jb20vY2xhaW1zL3Njb3BlIjoiaHR0cDovL21lZHdheWNsaWVudC5zeXN0ZW1jLmNvbS9hcGkvIiwibmFtZWlkIjoiREVNT1xcRGVtb0FkbWluIiwidW5pcXVlX25hbWUiOiJERU1PXFxEZW1vQWRtaW4iLCJhdXRobWV0aG9kIjoiT0F1dGgyIiwiYXV0aF90aW1lIjoiMjAxNy0xMC0xOVQxMjoxMjo0Ny42NThaIn0.mjfqLKJR8G3bF4BRjjXPniWNvg_IaaekQABXpXiNqzA',
					Accept: 'application/json'
				}
			})
				.then(function(response) {
					return response.data;
				})
				.catch(function(error) {
					console.log(error);
				});

			var recentResultsApiCall = axios({
				method: 'get',
				url: recentResultsApi,
				headers: {
					Authorization:
						'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlbW8tbWFwcC0wMS5kZW1vLm1ndC9ERU1PTElWRSIsImF1ZCI6Imh0dHA6Ly9tZWR3YXljbGllbnQuc3lzdGVtYy5jb20vYXBpLyIsIm5iZiI6MTUwODQxNTE2NywiZXhwIjoxNjM0NjQ2NDA3LCJodHRwOi8vaWRlbnRpdHlzZXJ2ZXIudGhpbmt0ZWN0dXJlLmNvbS9jbGFpbXMvY2xpZW50IjoiTWVkd2F5V2ViQXBpLURlbW9MSVZFIiwiaHR0cDovL2lkZW50aXR5c2VydmVyLnRoaW5rdGVjdHVyZS5jb20vY2xhaW1zL3Njb3BlIjoiaHR0cDovL21lZHdheWNsaWVudC5zeXN0ZW1jLmNvbS9hcGkvIiwibmFtZWlkIjoiREVNT1xcRGVtb0FkbWluIiwidW5pcXVlX25hbWUiOiJERU1PXFxEZW1vQWRtaW4iLCJhdXRobWV0aG9kIjoiT0F1dGgyIiwiYXV0aF90aW1lIjoiMjAxNy0xMC0xOVQxMjoxMjo0Ny42NThaIn0.mjfqLKJR8G3bF4BRjjXPniWNvg_IaaekQABXpXiNqzA',
					Accept: 'application/json'
				}
			})
				.then(function(response) {
					//Parse Data
				})
				.catch(function(error) {
					console.log(error);
				});

			Promise.all([allResultsApiCall, recentResultsApiCall])
				.then(function(a) {
					GetHandlebarsTemplate.callApi(
						'Content/js/handlebarsTemplates/GroupHome__PatientView__results-test.htm',
						obj.DomContainer,
						{
							Data: a[0]
						}
					)
						.then(GetHandlebarsTemplate.onTemplateInserted)
						.then(function() {
							//Bind to main Updates view on patient feed
							$('.js-patient-results-title').on('click', function(e) {
								var $this = $(this);
								var contentId = $this.data('result-id');
								if (contentId) {

									VueEventBus.$emit(
										'PatientSummaryWidget.Results.ViewResult',
										{
											ContentId: contentId
										}
									);
									//Append the data ID so the link can consume it in the view
									// $(
									// 	".js-teamPatient-href[data-viewurl='patient-results']"
									// )
									// 	.data('trigger-content-id', contentId)
									// 	.triggerHandler('click');
								}
								return;
							});
							//Bind View All link
							$('.js-patient-results-view-all').on(
								'click',
								function (e) {
									VueEventBus.$emit('PatientSummaryWidget.Results.ViewAll',{});

									// $(
									// 	".js-teamPatient-href[data-viewurl='patient-results']"
									// ).triggerHandler('click');
								}
							);
						});
				})
				.catch(function(error) {
					GetHandlebarsTemplate.callApi(
						'Content/js/handlebarsTemplates/GroupHome__PatientView__results-test.htm',
						obj.DomContainer,
						{ Data: [] }
					);
				});
		};

		return {
			getPatient: getPatient,
			viewCleanup: viewCleanup,
			//getPatientFeed: _getPatientFeedItems,
			//getEarlierPatientFeedItems: _getEarlierPatientFeedItems,
			//getClinicalTagsThatCanBeAppliedToAPatient: getClinicalTagsThatCanBeAppliedToAPatient,
			getCachedPatient: getCachedPatient,
			setCachedPatient: setCachedPatient,
			deleteCachedPatient: deleteCachedPatient,
			//getMemberAreasThatAUserCanHandoverFrom: getMemberAreasThatAUserCanHandoverFrom,
			//updateHandoverNotesViewModel: _updateHandoverNotesViewModel,
			patient_submitUpdate: patient_submitUpdate,
			getSinglePatientEwsData: getSinglePatientEwsData,
			getSinglePatientEwsDataForLastTwentyFourHours: getSinglePatientEwsDataForLastTwentyFourHours
		};
	})();

export default patientView;

﻿/* Utils */
import PubSub                   from '../endPoints/careflowApp/PubSub';
import Vault                    from '../endPoints/careflowApp/Vault';
import GetHandlebarsTemplate    from '../endPoints/careflowApp/GetHandlebarsTemplate';

/* API Endpoints */
import GetAllPopulationsForUser     from '../endPoints/populations/GetAllPopulationsForUser';
import GetUsersNetworks             from '../endPoints/networks/GetUsersNetworks';
import Subscribe                    from '../endPoints/populations/Subscribe';
import UpdateSubscription           from '../endPoints/populations/UpdateSubscription';
import Unsubscribe 					from '../endPoints/populations/Unsubscribe';
import UnsubscribeFromGroupSubscription   from 'Endpoints/subscription/UnsubscribeFromGroupSubscription';
import UpsertGroupSubscription      from '../endPoints/subscription/UpsertGroupSubscription';

var alertSubscriptions = alertSubscriptions || (function () {
    var _subscriptions = function () {
        PubSub.subscribe('AlertSubscriptions.GetSubscriptionItem', function (obj) {
            _getSubscriptionItem(obj);
        });

        PubSub.subscribe('AlertSubscriptions.GetSubscriptionItem.Result', function (obj) {
            _setSubscriptionItemUi(obj);
            _getSubscriptionSummary(obj);
        });

        PubSub.subscribe('AlertSubscriptions.EditSubscription', function (obj) {
            _editSubscriptionItem(obj);
        });

        PubSub.subscribe('AlertSubscriptions.EditSubscription.SetItem', function (obj) {
            _setSubscriptionItem(obj);
        });

        PubSub.subscribe('AlertSubscriptions.SubscriptionSummary', function (obj) {
            _getSubscriptionSummary(obj);
        });

        PubSub.subscribe('AlertSubscriptions.SaveSubscription', function (obj) {
            _saveSubscription(obj);
        });

        PubSub.subscribe('AlertSubscriptions.SaveGroupSubscription', function (obj) {
            _saveGroupSubscription(obj);
        });

        PubSub.subscribe('AlertSubscriptions.DeleteSubscription', function (obj) {
            _deleteSubscription(obj);
        });

        PubSub.subscribe('AlertSubscriptions.SubscriptionSavedSuccess', function (obj) {
            _subscriptionSavedUi(obj);
        });

        PubSub.subscribe('AlertSubscriptions.SubscriptionDeletedSuccess', function (obj) {
            _subscriptionSavedUi(obj);
        });
    }();

    //Shared across class
    var _getAllPopulationsForUserData = null;

    var _getAllPopulationsForUser = function () {
        var self = new $.Deferred();

        if (_getAllPopulationsForUserData) return self.resolve(_getAllPopulationsForUserData).promise();

        $.when(GetAllPopulationsForUser.callApi()).then(function (data, textStatus, jqXHR) {
            _getAllPopulationsForUserData = data;
            self.resolve(data);
        }).fail(function () { self.reject(); });

        return self.promise();
    };

    var _getSubscriptionItem = function (obj) {
        if (!_.has(obj, "SubscriptionId")) return false;

        var self = new $.Deferred();
        var subscriptionData = new $.Deferred();

        if (!_getAllPopulationsForUserData) {
            subscriptionData = _getAllPopulationsForUser();
        } else {
            //Deep copy and clone
            subscriptionData.resolve($.extend(true, {}, _getAllPopulationsForUserData));
        }

        $.when(subscriptionData).then(function (data) {
            var result = _.where(data.Data.CurrentSubscriptions, {
                "SubscriptionId": obj.SubscriptionId
            });

            self.resolve(result);

            //Fire only when loading a new Item in UI, not programatically getting an Item. Below is fired whenever a new subscription is selected in UI//TODO MOVE TO CLICK OF ITEM LOAD
            if (obj.DataContext.toString() === "GetSubscriptionItem") {
                //Build new Request Object for the active subscription Item

                alertSubscriptions.alertSubscribeRequestObj = result;

                //Listners for UI (_setSubscriptionItemUi)
                PubSub.publish("AlertSubscriptions.GetSubscriptionItem.Result", {
                    ViewType: "AlertSubscriptions",
                    IsGroup: obj.IsGroup,
                    CanManageGroupSubscriptions:obj.CanManageGroupSubscriptions,
                    DataContext: obj.DataContext,
                    Data: result,
                    DomObject: obj.DomObject
                });
            }
        }).fail(function () { self.reject(); });

        return self.promise();
    };

    var _setSubscriptionItemUi = function (obj) {
        if (obj.DataContext.toString() !== "GetSubscriptionItem") return false; //Don't run if going into edit mode, as already built/run for the active / selected item. Only on "GetSubscriptionItem"

        //Empty containers
        $(".js-alertSubscription-new-subscription-template").empty();

        //Set / Remove active states
        $(".js-alertFeed-item").attr("data-active", "false");
        obj.DomObject.attr("data-active", "true");

        //Build the UI
        var templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__subscriptionDetails.htm";
        var domSelectorToInsertTemplateTo = $(".js-alertSubscription-detail-template");
        GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, obj.Data).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
            //Bindings
            $(".js-alertSubscription-edit-btn").data("subscription-details", obj.Data);
            $(".js-alertSubscription-edit-btn").on("click", function (e) {
                PubSub.publish("AlertSubscriptions.EditSubscription", {
                    Event: e,
                    DomObject: $(this),
                    ViewType: "AlertSubscriptions",
                    SubscriptionId: $(this).data("subscription-id"),
                    DataContext: $(this).data("context")
                });
            });

            NProgress.done();
        });
    };

    var _editSubscriptionItem = function (obj) {
        //$.when(_getSubscriptionItem(obj).then(function(result) {
        //    alert(JSON.stringify(result));
        //}));

        //Build Request Object based on current subscription
        var alertSubscribeRequestObj = {
            //Get full details to pre-select elements
            Data: obj.DomObject.data("subscription-details")
        };

        var templatePath, domSelectorToInsertTemplateTo;

        if (!_.has(obj, "DataContext")) return false;

        //Topics
        if (obj.DataContext === "topics") {
            $.when(_getAllPopulationsForUser()).then(function (data) {
                //Filter returned data by originating subscription network
                data = _.where(data.Data.AvailablePopulations, {
                    "NetworkExternalIdentifier": alertSubscribeRequestObj.Data[0].NetworkExternalIdentifier
                });

                //Sort Data - locally as required
                if (data.length) data[0].Topics = _.sortBy(data[0].Topics, function (o) { return o.Title.toLowerCase(); });

                templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__editableTopics.htm";
                domSelectorToInsertTemplateTo = $(".js-alertSubscription-edit-topics-template");
                GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, data).done(GetHandlebarsTemplate.onTemplateInserted);

                //Pre select
                $(".js-alertSubscription-topic-chk").each(function () {
                    var $self = $(this);
                    var topic = $self.val();

                    $.each(alertSubscribeRequestObj.Data[0].Topics, function (key, val) {
                        if (this.TopicId === topic) $self.prop("checked", true);
                        return;
                    });
                });

                //Bindings
                $(".js-alertSubscription-topic-chk").unbind().on("click", function (e) {
                    var $self = $(this);

                    //Is it all topics?
                    if (this.checked) {
                        if (!$self.val()) {
                            //Remove any topic selections already made
                            $(".js-alertSubscription-topic-chk").each(function (e) {
                                var $self = $(this);
                                if ($self.prop("checked") && $self.val()) {
                                    $self.prop("checked", false).triggerHandler("click");
                                }
                            });

                            $(".js-alertSubscription-topic-chk").prop("disabled", true);
                            $self.prop("disabled", false);
                        }
                    } else {
                        if (!$self.val()) {
                            $(".js-alertSubscription-topic-chk").prop("disabled", false);
                            $self.prop("disabled", false);
                        }
                    }

                    PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                        Event: e,
                        DomObject: $self,
                        Element: this, //reference to the DOM element of invocation
                        ViewType: "AlertSubscriptions",
                        DataContext: "SetTopic",
                        TopicData: $self.data("topicdata"),
                        TopicId: $self.val()
                    });
                });
            });
        }

        if (obj.DataContext === "patients") {
            $.when(_getAllPopulationsForUser()).then(function (data) {
                //Filter returned data by originating subscription network
                data = _.where(data.Data.AvailablePopulations, {
                    "NetworkExternalIdentifier": alertSubscribeRequestObj.Data[0].NetworkExternalIdentifier
                });

                //Append existing subscription to data passed to template
                data[0].ExistingSubscription = alertSubscribeRequestObj;

                templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__editablePatients.htm";
                domSelectorToInsertTemplateTo = $(".js-alertSubscription-edit-patients-template");
                GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, data).done(GetHandlebarsTemplate.onTemplateInserted);

                //Step 1
                //Patient Type context setter
                var originalContext = alertSubscribeRequestObj.Data[0].Type.PopulationType;
                var originalLocationContextType = (alertSubscribeRequestObj.Data[0].Type.PopulationType === "Clinician") ? "clinician" : "location";
                var locationContext;

                $(".js-alertSubscription-patient-type-select").unbind().on("click", function () {
                    locationContext = $(this).data("context");

                    if (locationContext === "clinician") {
                        $(".js-alertSubscription-location-location").hide();
                        $(".js-alertSubscription-location-clinician").show();
                        $(".js-alertSubscription-patient-type-select[data-context='location']").hide();
                    } else {
                        $(".js-alertSubscription-location-location").show();
                        $(".js-alertSubscription-location-clinician").hide();
                        $(".js-alertSubscription-patient-type-select[data-context='clinician']").hide();
                    }

                    //Button UI
                    var text = $(this).text();
                    $(this).removeClass("alertSubscription-patient-types-btn").addClass("alertSubscription-patient-types-btn--active");

                    this.innerHTML = "<i class='fa fa-chevron-down' aria-hidden='true'></i> " + text;

                    //Pre selections
                    //Mark locations as selected (mark attr checked / selected in raw template html) for multi select
                    var currentSites = [];
                    $.each(alertSubscribeRequestObj.Data[0].Site, function (key, val) {
                        currentSites.push(this.SiteName);
                        return;
                    });

                    $(".js-alertSubscription-patients-location-ddl[data-context=" + originalLocationContextType + "] > option").each(function (id, val) {
                        var value = (this.value).toString();
                        if (_.contains(currentSites, value)) $(this).prop("selected", true);
                        return;
                    });

                    //Bindings Select2
                    $(".js-alertSubscription-patients-clinician-ddl").select2({
                        placeholder: 'Choose a consultant',
                        width: "100%",
                        allowClear: true
                    });
                    $(".js-alertSubscription-patients-location-ddl").select2({
                        placeholder: 'Choose a location',
                        width: "100%",
                        allowClear: true,
                        templateResult: function (data, container) {
                            if (data.element) {
                                $(container).addClass($(data.element).attr("class"));
                            }
                            return data.text;
                        }
                    });

                    //Mark consultant as selected, if contexct matched
                    if (originalLocationContextType === "clinician") $(".js-alertSubscription-patients-clinician-ddl").val(alertSubscribeRequestObj.Data[0].Type.PopulationName).trigger("change");

                    //If Area is set, create this
                    if (originalContext === "Area") {
                        $(".js-alertSubscription-patients-location-area-ddl[data-context='location']").select2({
                            placeholder: 'Choose a location',
                            width: "100%",
                            allowClear: true
                        });

                        //Set pre-selected
                        //Mark area as selected (attr checked in raw html) for multi select
                        $(".js-alertSubscription-patients-location-area-ddl[data-context='location'] > option").each(function (id, val) {
                            var value = (this.value).toString();
                            if (value == alertSubscribeRequestObj.Data[0].Type.PopulationName) $(this).prop("selected", true);
                        });

                        //Area - initial setup for remove only, subsequently bound to selected Site
                        $(".js-alertSubscription-patients-location-area-ddl[data-context='location']").unbind("select2:unselect select2:select").on("select2:unselect select2:select", _locationAreaUiBinding);

                        //Area selected, so no additional sites or areas may be added
                        $(".js-alertSubscription-patients-location-ddl[data-context='location']").select2({ maximumSelectionLength: 1, width: "100%", placeholder: 'Choose a location', });
                        $(".js-alertSubscription-patients-location-area-ddl[data-context='location']").select2({ maximumSelectionLength: 1, width: "100%", placeholder: 'Choose an area', });
                    }
                }); //End Patient Type context setter binding

                //Step 2
                //Patient selections - selection made

                $(".js-alertSubscription-patients-location-ddl, .js-alertSubscription-patients-clinician-ddl").on("select2:select", function (e) {
                    _setPatientSelectionsUi({
                        Event: e,
                        DomObject: $(this),
                        Element: this,
                        ViewType: "AlertSubscriptions",
                        DataContext: "SetPatients",
                        Value: $(this).val(),
                        DataType: $(this).data("type"),
                        originalContext: originalContext,
                        OriginalLocationContextType: originalLocationContextType,
                        LocationContext: locationContext,
                        Data: data
                    });

                    //Publish selection
                    PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                        Event: e,
                        DomObject: $(this),
                        Element: this,
                        ViewType: "AlertSubscriptions",
                        DataContext: "SetPatients",
                        Value: $(this).val(),
                        DataType: $(this).data("type"),
                        IsRemoving: false,
                        IsAddingItem: e.params.data.id,
                        LocationContext: locationContext
                    });
                });

                //Removing a location
                $(".js-alertSubscription-patients-location-ddl").on("select2:unselect", function (e) {
                    //Stop select2 opening onremove
                    if (!e.params.originalEvent) {
                        return;
                    }
                    e.params.originalEvent.stopPropagation();

                    _setPatientSelectionsUi({
                        Event: e,
                        DomObject: $(this),
                        Element: this,
                        ViewType: "AlertSubscriptions",
                        DataContext: "SetPatients",
                        Value: $(this).val(),
                        DataType: $(this).data("type"),
                        IsRemoving: true,
                        IsRemovingItem: e.params.data.id,
                        originalContext: originalContext,
                        OriginalLocationContextType: originalLocationContextType,
                        LocationContext: locationContext,
                        Data: data
                    });

                    //Remove selection

                    PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                        Event: e,
                        DomObject: $(this),
                        Element: this,
                        ViewType: "AlertSubscriptions",
                        DataContext: "SetPatients",
                        Value: $(this).val(),
                        DataType: $(this).data("type"),
                        IsRemoving: true,
                        IsRemovingItem: e.params.data.id,
                        LocationContext: locationContext
                    });
                });
            });
        }

        if (obj.DataContext === "colour") {
            templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__editableColours.htm";
            domSelectorToInsertTemplateTo = $(".js-alertSubscription-edit-colour-template");
            GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, false).done(GetHandlebarsTemplate.onTemplateInserted);

            //Bindings and Pre Select
            $(".js-alertSubscribe-colour-picker-ddl").simplecolorpicker({
                theme: "fontawesome"
            });

            var currentColour = alertSubscribeRequestObj.Data[0].SubscriptionColour;

            if (_.startsWith(currentColour, "0x")) {
                var length = currentColour.length;
                currentColour = currentColour.substring(2, length + 1);
            }

            $(".js-alertSubscribe-colour-picker-ddl").simplecolorpicker('selectColor', '#' + currentColour);

            $(".js-alertSubscribe-colour-picker-ddl").unbind().on("change", function (e) { //C# requires 0x prefix, so remove
                var selectedColour = $(this).val();

                if (_.startsWith(selectedColour, "#")) {
                    var length = selectedColour.length;
                    selectedColour = selectedColour.substring(1, length + 1);
                }

                PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                    DomElement: $(this),
                    Event: e,
                    ViewType: "AlertSubscriptions",
                    DataContext: "SetSubscriptionColour",
                    Colour: selectedColour
                });
            });
        }

        if (obj.DataContext === "type") {

            var data = {
                IsGroup: alertSubscribeRequestObj.Data[0].IsGroup,
                CanManageGroupSubscriptions : appMain.checkUserPermissionForNetwork(Vault.getSessionItem("ActiveNetworkId"), "ManageGroupSubscription")
            }
            var usersGroups;

            var getUsersGroups = function () {
                var self = new $.Deferred();

                if (usersGroups) return self.resolve(usersGroups).promise();

                $.when(GetUsersNetworks.callApi(Vault.getSessionItem("ActiveNetworkId"), null, null).then(function (data, textStatus, jqXHR) {
                    usersGroups = data;
                    self.resolve(data, textStatus, jqXHR);
                }).fail(function (data, textStatus, jqXHR) {
                    usersGroups = null;
                    self.reject(data, textStatus, jqXHR);
                }));

                return self.promise();
            };

            var setGroupSubscriptionUi = function (activeGroupMemberAreaId) {

                $(".js-alertSubscription-type-controls").show();
                appMain.ajaxLoadingHelper({
                    DomElement: $(".js-groupSubscription-groups-list-template")
                });

                $.when(getUsersGroups()).then(function (data, textStatus, jqXHR) {
                    appMain.ajaxLoadingHelperComplete({
                        DomElement: $(".js-groupSubscription-groups-list-template")
                    });
                    var templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__groupSubscriptionList.htm";
                    var domSelectorToInsertTemplateTo = $(".js-groupSubscription-groups-list-template");
                    GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, data).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
                        $(".js-alertSubscription-type-controls").show();

                        $(".js-groupSubscription-groups-ddl").unbind().on("change", function (e) {
                            var selectedTeam = $(this).val();
                            if (selectedTeam) {
                                PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                                    DomElement: $(this),
                                    Event: e,
                                    ViewType: "AlertSubscriptions",
                                    DataContext: "SetSubscriptionType",
                                    SubscriptionType: "group",
                                    SubscriptionTeam: selectedTeam
                                });
                            }
                        }).select2({
                            placeholder: 'Select team',
                            width: "100%",
                            allowClear: true
                        }).val(activeGroupMemberAreaId).trigger("change");

                        appMain.ajaxActiveButtonUi({
                            ActionState: "stop"
                        });
                    });
                });
            };

            templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__editableType.htm";
            domSelectorToInsertTemplateTo = $(".js-alertSubscription-edit-type-template");
            GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, data).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
                //Group Subscriptions
                //Load groups of active network to create Group Subscription in
                if (alertSubscribeRequestObj.Data[0].IsGroup) {
                    setGroupSubscriptionUi(alertSubscribeRequestObj.Data[0].GroupMemberAreaId);
                }

                //Bindings
                $(".js-alertSubscription-type-chk").unbind().on("click", function (e) {
                    var $self = $(this);
                    var type = $self.val();

                    if (type === "group") {
                        setGroupSubscriptionUi(alertSubscribeRequestObj.Data[0].GroupMemberAreaId);
                    } else {
                        $(".js-alertSubscription-type-controls").hide();

                        PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                            DomElement: $(this),
                            Event: e,
                            ViewType: "AlertSubscriptions",
                            DataContext: "SetSubscriptionType",
                            SubscriptionType: type,
                            SubscriptionTeam: null
                        });
                    }
                });
            });
        }
    };

    var _createNewSubscription = function () {
        NProgress.start();

        //If no existing wrapper visible, remove it
        if ($(".js-alertSubscription-no-active-block").is(":visible")) $(".js-alertSubscription-no-active-block").remove();

        //Reset the alert subscribe request object for a new subscription - this matches propertiers in GetAllPopulationsForUser.CurrentSubscriptions
        alertSubscriptions.alertSubscribeRequestObj =
        [
            {
                "NetworkExternalIdentifier": appMain.getNetworkExternalNetworkId(Vault.getSessionItem("ActiveNetworkId")),
                "SubscriptionColour": null,
                "Site": [],
                "Type": {
                    "PopulationName": "",
                    "PopulationType": ""
                },
                "Topics": []
            }
        ];

        var templatePath, domSelectorToInsertTemplateTo;

        //Empty container
        $(".js-alertSubscription-detail-template").empty();
        $(".js-alertSubscription-subscription-summary-template").empty();

        templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__new.htm";
        domSelectorToInsertTemplateTo = $(".js-alertSubscription-new-subscription-template");
        GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, false).done(GetHandlebarsTemplate.onTemplateInserted);

        //Build UI blocks
        $.when(_getAllPopulationsForUser()).then(function (data) {
            //Filter returned population data by network subscription is being created in
            data = _.where(data.Data.AvailablePopulations, {
                "NetworkExternalIdentifier": appMain.getNetworkExternalNetworkId(Vault.getSessionItem("ActiveNetworkId"))
            });

            //Sort Data - locally as required
            if (data.length) data[0].Topics = _.sortBy(data[0].Topics, function (o) { return o.Title.toLowerCase(); });

            //Topics
            templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__editableTopics.htm";
            domSelectorToInsertTemplateTo = $(".js-alertSubscription-edit-topics-template");
            GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, data).done(GetHandlebarsTemplate.onTemplateInserted);

            //Bindings
            $(".js-alertSubscription-topic-chk").unbind().on("click", function (e) {
                var $self = $(this);

                //Is it all topics?
                if (this.checked) {
                    if (!$self.val()) {
                        //Remove any topic selections already made
                        $(".js-alertSubscription-topic-chk").each(function (e) {
                            var $self = $(this);
                            if ($self.prop("checked") && $self.val()) {
                                $self.prop("checked", false).triggerHandler("click");
                            }
                        });

                        $(".js-alertSubscription-topic-chk").prop("disabled", true);
                        $self.prop("disabled", false);
                    }
                } else {
                    if (!$self.val()) {
                        $(".js-alertSubscription-topic-chk").prop("disabled", false);
                        $self.prop("disabled", false);
                    }
                }

                PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                    Event: e,
                    DomObject: $self,
                    Element: this, //reference to the DOM element of invocation
                    ViewType: "AlertSubscriptions",
                    DataContext: "SetTopic",
                    TopicData: $self.data("topicdata"),
                    TopicId: $self.val(),
                    NewSubscription: true
                });
            });

            //Patients

            templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__editablePatients.htm";
            domSelectorToInsertTemplateTo = $(".js-alertSubscription-edit-patients-template");
            GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, data).done(GetHandlebarsTemplate.onTemplateInserted);

            //Step 1
            //Patient Type context setter

            var locationContext;

            $(".js-alertSubscription-patient-type-select").unbind().on("click", function () {
                var $self = $(this);

                locationContext = $self.data("context");

                if (locationContext === "clinician") {
                    $(".js-alertSubscription-location-location").hide();
                    $(".js-alertSubscription-location-clinician").show();
                    $(".js-alertSubscription-patient-type-select[data-context='location']").hide();
                } else {
                    $(".js-alertSubscription-location-location").show();
                    $(".js-alertSubscription-location-clinician").hide();
                    $(".js-alertSubscription-patient-type-select[data-context='clinician']").hide();
                }

                var text = $self.text();
                $self.removeClass("alertSubscription-patient-types-btn").addClass("alertSubscription-patient-types-btn--active");

                this.innerHTML = "<i class='fa fa-chevron-down' aria-hidden='true'></i> " + text;

                //Pre selections

                //Bindings Select2
                $(".js-alertSubscription-patients-clinician-ddl").select2({
                    placeholder: 'Choose a consultant',
                    width: "100%",
                    allowClear: true
                });
                $(".js-alertSubscription-patients-location-ddl").select2({
                    placeholder: 'Choose a location',
                    width: "100%",
                    allowClear: true
                });
            }); //End Patient Type context setter binding

            //Step 2
            //Patient selections - selection made

            $(".js-alertSubscription-patients-location-ddl, .js-alertSubscription-patients-clinician-ddl").on("select2:select", function (e) {
                _setPatientSelectionsUi({
                    Event: e,
                    DomObject: $(this),
                    Element: this,
                    ViewType: "AlertSubscriptions",
                    DataContext: "SetPatients",
                    Value: $(this).val(),
                    DataType: $(this).data("type"),
                    originalContext: null,
                    OriginalLocationContextType: null,
                    LocationContext: locationContext,
                    Data: data,
                    NewSubscription: true
                });

                //Publish selection
                PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                    Event: e,
                    DomObject: $(this),
                    Element: this,
                    ViewType: "AlertSubscriptions",
                    DataContext: "SetPatients",
                    Value: $(this).val(),
                    DataType: $(this).data("type"),
                    IsRemoving: false,
                    IsAddingItem: e.params.data.id,
                    LocationContext: locationContext,
                    NewSubscription: true
                });
            });

            //Removing a location
            $(".js-alertSubscription-patients-location-ddl").on("select2:unselect", function (e) {
                //Stop select2 opening onremove
                if (!e.params.originalEvent) {
                    return;
                }
                e.params.originalEvent.stopPropagation();

                _setPatientSelectionsUi({
                    Event: e,
                    DomObject: $(this),
                    Element: this,
                    ViewType: "AlertSubscriptions",
                    DataContext: "SetPatients",
                    Value: $(this).val(),
                    DataType: $(this).data("type"),
                    IsRemoving: true,
                    IsRemovingItem: e.params.data.id,
                    originalContext: null,
                    OriginalLocationContextType: null,
                    LocationContext: locationContext,
                    Data: data,
                    NewSubscription: true
                });

                //Remove selection

                PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                    Event: e,
                    DomObject: $(this),
                    Element: this,
                    ViewType: "AlertSubscriptions",
                    DataContext: "SetPatients",
                    Value: $(this).val(),
                    DataType: $(this).data("type"),
                    IsRemoving: true,
                    IsRemovingItem: e.params.data.id,
                    LocationContext: locationContext,
                    NewSubscription: true
                });
            });

            //Area - initial setup for remove only, subsequently bound to selected Site
            $(".js-alertSubscription-patients-location-area-ddl[data-context='location']").unbind("select2:unselect select2:select").on("select2:unselect select2:select", _locationAreaUiBinding);

            //Colour

            templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__editableColours.htm";
            domSelectorToInsertTemplateTo = $(".js-alertSubscription-edit-colour-template");
            GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, false).done(GetHandlebarsTemplate.onTemplateInserted);

            //Bindings and Pre Select
            $(".js-alertSubscribe-colour-picker-ddl").simplecolorpicker({
                theme: "fontawesome"
            });

            //remove default colour (data-selected) on init as set by plugin (no option for empty)
            $(".simplecolorpicker").find(".color").removeAttr("data-selected");

            $(".js-alertSubscribe-colour-picker-ddl").on("change", function (e) {
                //Hex: Uses a mix of 6 numbers and characters
                //RGB: Uses 3 sets of 3 numbers which have a range of 0 – 255

                var selectedColour = $(this).val();

                if (_.startsWith(selectedColour, "#")) {
                    var length = selectedColour.length;
                    selectedColour = selectedColour.substring(1, length + 1);
                }

                PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                    DomElement: $(this),
                    Event: e,
                    ViewType: "AlertSubscriptions",
                    DataContext: "SetSubscriptionColour",
                    Colour: selectedColour,
                    NewSubscription: true
                });
            });

            //Type

            templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__editableType.htm";
            domSelectorToInsertTemplateTo = $(".js-alertSubscription-edit-type-template");
            var usersGroups;
            var getUsersGroups = function () {
                var self = new $.Deferred();

                if (usersGroups) return self.resolve(usersGroups).promise();

                $.when(GetUsersNetworks.callApi(Vault.getSessionItem("ActiveNetworkId"), null, null).then(function (data, textStatus, jqXHR) {
                    usersGroups = data;
                    self.resolve(data, textStatus, jqXHR);
                }).fail(function (data, textStatus, jqXHR) {
                    usersGroups = null;
                    self.reject(data, textStatus, jqXHR);
                }));

                return self.promise();
            };
            var setGroupSubscriptionUi = function (activeGroupMemberAreaId) {
                $(".js-alertSubscription-type-controls").show();
                appMain.ajaxLoadingHelper({
                    DomElement: $(".js-groupSubscription-groups-list-template"),
                    DisableNoInputs: true
                });



                $.when(getUsersGroups()).then(function (data, textStatus, jqXHR) {
                    var templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__groupSubscriptionList.htm";
                    var domSelectorToInsertTemplateTo = $(".js-groupSubscription-groups-list-template");
                    GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, data).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
                        //Fire by default, so blank team selection values can be caught
                        PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                            DomElement: null,
                            Event: null,
                            ViewType: "AlertSubscriptions",
                            DataContext: "SetSubscriptionType",
                            SubscriptionType: "group",
                            SubscriptionTeam: null,
                            NewSubscription: true
                        });

                        $(".js-groupSubscription-groups-ddl").unbind().on("change", function (e) {
                            var selectedTeam = $(this).val();
                            PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                                DomElement: $(this),
                                Event: e,
                                ViewType: "AlertSubscriptions",
                                DataContext: "SetSubscriptionType",
                                SubscriptionType: "group",
                                SubscriptionTeam: selectedTeam,
                                NewSubscription: true
                            });
                        }).select2({
                            placeholder: 'Select team',
                            width: "100%",
                            allowClear: true
                        });
                    });
                });
            };

            GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, {
                CanManageGroupSubscriptions: appMain.checkUserPermissionForNetwork(Vault.getSessionItem("ActiveNetworkId"), "ManageGroupSubscription")
            }).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
                //Bindings - Creating new subscription, so remove checked property
                $(".js-alertSubscription-type-chk").prop("checked", false).unbind().on("click", function (e) {
                    var $self = $(this);
                    var type = $self.val();

                    if (type === "group") {
                        setGroupSubscriptionUi();
                    } else {
                        $(".js-alertSubscription-type-controls").hide();
                        PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                            DomElement: $(this),
                            Event: e,
                            ViewType: "AlertSubscriptions",
                            DataContext: "SetSubscriptionType",
                            SubscriptionType: type,
                            SubscriptionTeam: null,
                            NewSubscription: true
                        });
                    }
                });

                //New subscription, default as personal type
                $(".js-alertSubscription-type-chk[value='personal']").prop("checked", true);
            });

            NProgress.done();
        });
    };

    var _setPatientSelectionsUi = function (obj) {
        //Function to manage UI for Patients locations

        //If context differs from original context, clear down selections when a new one is made
        if (obj.OriginalLocationContextType !== obj.LocationContext) {
            var removeIcon;

            if (obj.LocationContext === "clinician") {
                //Clear locations
                removeIcon = $(".js-alertSubscription-location-location").find(".select2-selection__choice");
                $.each(removeIcon, function () {
                    var value = $(this).prop("title");

                    PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                        ViewType: "AlertSubscriptions",
                        DataContext: "SetPatients",
                        Value: value,
                        DataType: "location",
                        IsRemoving: true,
                        IsRemovingItem: value,
                        LocationContext: "clinician"
                    });
                });
            } else if (obj.LocationContext === "location") {
                removeIcon = $(".js-alertSubscription-location-clinician").find(".select2-selection__choice");
                $.each(removeIcon, function () {
                    var value = $(this).prop("title");

                    PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                        ViewType: "AlertSubscriptions",
                        DataContext: "SetPatients",
                        Value: value,
                        DataType: "location",
                        IsRemoving: true,
                        IsRemovingItem: value,
                        LocationContext: "clinician"
                    });
                });
            }
        } //End selection clearing

        //If Single Site, present Area option. If All Sites, stop further selections
        if (obj.LocationContext === "location") {
            var selectedSites = (obj.DomObject.val()) ? obj.DomObject.val().length : false;

            if (_.contains(obj.Value, "NoneProvided")) { //All Sites selected, disable further selections
                //Clear down any already selected sites, as All Sites is selected
                removeIcon = $(".js-alertSubscription-location-location").find(".select2-selection__choice");
                removeIcon.each(function () {
                    var $self = $(this);
                    var value = $self.prop("title");

                    if (value !== "NoneProvided") {
                        PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                            ViewType: "AlertSubscriptions",
                            DataContext: "SetPatients",
                            Value: value,
                            DataType: "location",
                            IsRemoving: true,
                            IsRemovingItem: value,
                            LocationContext: "clinician"
                        });

                        var select2El = $(".js-alertSubscription-location-location").find(".js-alertSubscription-patients-location-ddl");
                        select2El.val(["NoneProvided"]).trigger("change");

                        //Disable other sites
                        $(".js-alertSubscription-location-location").find(".js-alertSubscription-patients-location-ddl option:not(:selected)").prop("disabled", true);
                    }
                });
            } else {//Ensure all options are enabled as All Sites not selected
                $(".js-alertSubscription-location-location").find(".js-alertSubscription-patients-location-ddl option").prop("disabled", false);
            }

            if (selectedSites === 1) {
                //Get Areas of the single, newly selected Site
                var sites = obj.Data[0].Sites;
                var selectedSite = obj.DomObject.val();
                var filteredAreas = [];

                _.filter(sites, function (site, i) {//TODO: REFACTOR
                    if (site.SiteName == selectedSite) {
                        $.each(site.Areas, function (index, val) {
                            filteredAreas.push({
                                id: index,
                                text: val.AreaName
                            });
                            return;
                        });
                    };
                });

                //Show the control
                $(".js-alertSubscription-area-wrapper").show();

                //Build filtered Areas DDL
                var $locationAreaSelect = $(".js-alertSubscription-patients-location-area-ddl[data-context='location']").select2({ width: "100%" }).val('').trigger('change');
                $locationAreaSelect.select2({
                    data: filteredAreas,
                    placeholder: 'Choose an area',
                    width: "100%"
                });

                //If Area selected, no more than 1 site allowed
                $locationAreaSelect.unbind("select2:select select2:unselect").on("select2:select select2:unselect", _locationAreaUiBinding);
            } else if (selectedSites && selectedSites > 1) {
                //More than 1 site, Area no longer valid. Clear down and remove
                $(".js-alertSubscription-patients-location-area-ddl[data-context='location']").val('').trigger('change');
                $(".js-alertSubscription-area-wrapper").hide();
                $(".js-alertSubscription-patients-location-ddl[data-context='location']").select2({ maximumSelectionLength: -1, width: "100%", placeholder: 'Choose a location', });
            } else if (!selectedSites) {
                //No sites, so clear any areas
                removeIcon = $(".js-alertSubscription-area-wrapper").find(".select2-selection__choice");
                $.each(removeIcon, function () {
                    var value = $(this).prop("title");

                    PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                        ViewType: "AlertSubscriptions",
                        DataContext: "SetPatients",
                        Value: value,
                        DataType: "location",
                        IsRemoving: true,
                        IsRemovingItem: value,
                        LocationContext: "clinician"
                    });
                });

                $(".js-alertSubscription-patients-location-area-ddl[data-context='location']").val('').trigger('change');
                $(".js-alertSubscription-area-wrapper").hide();
                $(".js-alertSubscription-patients-location-ddl[data-context='location']").select2({ maximumSelectionLength: -1, width: "100%", placeholder: 'Choose a location', });
                //alert("none!");
            }
        } else if (obj.LocationContext === "clinician") {
        }
    };

    var _locationAreaUiBinding = function (e) {
        //Always passed by reference so context set as $(this)

        var $locationAreaSelect = $(".js-alertSubscription-patients-location-area-ddl[data-context='location']").select2({
            placeholder: 'Choose an area',
            width: "100%",
        });
        var selectedAreas = ($(this).val()) ? $(this).val().length : false;

        if (e.type === "select2:unselect") {
            //Stop select2 opening onremove
            if (!e.params.originalEvent) {
                return;
            }
            e.params.originalEvent.stopPropagation();

            PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                Event: e,
                DomObject: $(this),
                Element: this,
                ViewType: "AlertSubscriptions",
                DataContext: "SetPatients",
                Value: $(this).val(),
                DataType: $(this).data("type"),
                IsRemoving: true,
                IsRemovingItem: e.params.data.text,
                LocationContext: "area"
            });

            if (!selectedAreas) {
                $(".js-alertSubscription-patients-location-ddl[data-context='location']").select2({ maximumSelectionLength: -1, width: "100%", placeholder: 'Choose a location', });
            }
        } else if (e.type === "select2:select") {
            if (selectedAreas && selectedAreas === 1) {
                $(".js-alertSubscription-patients-location-ddl[data-context='location']").select2({ maximumSelectionLength: 1, width: "100%", placeholder: 'Choose a location', });
                $locationAreaSelect.select2({ maximumSelectionLength: 1, width: "100%" });

                //Publish selection
                PubSub.publish("AlertSubscriptions.EditSubscription.SetItem", {
                    Event: e,
                    DomObject: $(this),
                    Element: this,
                    ViewType: "AlertSubscriptions",
                    DataContext: "SetPatients",
                    Value: $(this).val(),
                    DataType: $(this).data("type"),
                    IsRemoving: false,
                    IsAddingItem: e.params.data.text,
                    LocationContext: "area"
                });
            } else if (!selectedAreas) {
                $(".js-alertSubscription-patients-location-ddl[data-context='location']").select2({ maximumSelectionLength: -1, width: "100%", placeholder: 'Choose a location', });
            }
        }
    };

    var _setSubscriptionItem = function (obj) {

        //Get the subscription type
        var subscriptionType;
        if ($(".js-alertSubscription-type-chk[name=subscription-type-chk]").is(":visible")) {
            subscriptionType = $('.js-alertSubscription-type-chk[name=subscription-type-chk]:checked').val();
        } else {//If in non edit mode so checkbox not visible / rendered, and only label rendered in DOM, get the set data attr
            subscriptionType = $(".js-alertSubscription-subscription-type").data("type");
        }

        //Check for All Topics selected
        var userSelectedAllTopics;
        if ($(".js-alertSubscription-topic-chk[value='']").prop("checked")) {
            userSelectedAllTopics = true;
        }

        //Check for new subscription flag
        var isNewSubscription;
        if (_.has(obj, "NewSubscription")) {
            isNewSubscription = (obj.NewSubscription) ? true : false;
        }

        if (obj.DataContext === "SetTopic") {
            if (obj.Element.checked) {
                //Is it 'All Topics' = no topicId value ""
                if (!obj.DomObject.val()) {
                    alertSubscriptions.alertSubscribeRequestObj[0].Topics.length = 0;
                    userSelectedAllTopics = true;
                } else {
                    userSelectedAllTopics = false;
                    if (!_.contains((alertSubscriptions.alertSubscribeRequestObj[0].Topics, obj.TopicId, 0)));

                    alertSubscriptions.alertSubscribeRequestObj[0].Topics.push(obj.TopicData);
                }
            } else {
                //Is it 'All Topics' = no topicId value "". If so, ignore
                userSelectedAllTopics = false;

                if (obj.DomObject.val()) {
                    alertSubscriptions.alertSubscribeRequestObj[0].Topics = _.filter(alertSubscriptions.alertSubscribeRequestObj[0].Topics, function (topic) { return topic.TopicId !== obj.TopicId; });
                }
            }
        }
        if (obj.DataContext === "SetPatients") {
            if (obj.LocationContext === "clinician") {
                if (obj.DataType === "location") {
                    if (obj.IsRemoving) {
                        alertSubscriptions.alertSubscribeRequestObj[0].Site = _.filter(alertSubscriptions.alertSubscribeRequestObj[0].Site, function (site) { return site.SiteName !== obj.IsRemovingItem; });
                    } else { //Add a site
                        alertSubscriptions.alertSubscribeRequestObj[0].Site.push({
                            SiteName: obj.IsAddingItem
                        });
                    }
                } else if (obj.DataType === "clinician") {
                    if (obj.Value !== "") {
                        alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationType = "Clinician";
                        alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationName = obj.Value;
                    } else {
                        alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationType = "NoneProvided";
                        alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationName = null;
                    }
                }
            } else if (obj.LocationContext === "location") {
                if (obj.IsRemoving) {
                    alertSubscriptions.alertSubscribeRequestObj[0].Site = _.filter(alertSubscriptions.alertSubscribeRequestObj[0].Site, function (site) { return site.SiteName !== obj.IsRemovingItem; });

                    alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationType = "NoneProvided";
                    alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationName = null;
                } else { //Add a site
                    if (obj.IsAddingItem === "NoneProvided") { //If All Sites are selected in UI
                        alertSubscriptions.alertSubscribeRequestObj[0].Site.length = 0;
                    } else {
                        alertSubscriptions.alertSubscribeRequestObj[0].Site.push({
                            SiteName: obj.IsAddingItem
                        });
                    }

                    alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationType = "NoneProvided";
                    alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationName = null;
                }
            } else if (obj.LocationContext === "area") {
                if (obj.IsRemoving) {
                    alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationType = "NoneProvided";
                    alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationName = null;
                } else {
                    alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationType = "Area";
                    alertSubscriptions.alertSubscribeRequestObj[0].Type.PopulationName = obj.IsAddingItem;
                }
            }
        }

        if (obj.DataContext === "SetSubscriptionColour") {
            alertSubscriptions.alertSubscribeRequestObj[0].SubscriptionColour = obj.Colour;
        }

        if (obj.DataContext === "SetSubscriptionType") {

            alertSubscriptions.alertSubscribeRequestObj[0].IsGroup = (obj.SubscriptionType === "group") ? true : false;
            if (alertSubscriptions.alertSubscribeRequestObj[0].IsGroup) {
                alertSubscriptions.alertSubscribeRequestObj[0].GroupMemberAreaId =  parseInt(obj.SubscriptionTeam,10);
                alertSubscriptions.alertSubscribeRequestObj[0].GroupMemberAreaName = appMain.getMemberAreaName(obj.SubscriptionTeam);
            }
            subscriptionType = obj.SubscriptionType;
        }

        //Selections UI panel setup - update the request object alertSubscriptions.alertSubscribeRequestObj and pass
        PubSub.publish("AlertSubscriptions.SubscriptionSummary", {
            Data: alertSubscriptions.alertSubscribeRequestObj,
            SubscriptionType: subscriptionType,
            IsGroup: (obj.SubscriptionType === "group") ? true : false,
            NewSubscription: isNewSubscription,
            AllTopicsSubscription: userSelectedAllTopics
        });
    };

    var _getSubscriptionSummary = function (obj) { //alertSubscriptions.alertSubscribeRequestObj passed in as Data

        var userSelectedAllTopics;
        var isNewSubscription;

        if (_.has(obj, "AllTopicsSubscription")) {
            userSelectedAllTopics = (obj.AllTopicsSubscription) ? true : false;
        }

        if (_.has(obj, "NewSubscription")) {
            isNewSubscription = (obj.NewSubscription) ? true : false;
        }

        //Build the UI
        var templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__subscriptionSummary.htm";
        var domSelectorToInsertTemplateTo = $(".js-alertSubscription-subscription-summary-template");
        GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, obj).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
            var subscriptionId = obj.Data[0].SubscriptionId;

            if (isNewSubscription) {//No existing subscription, context is as New Subscription
                if (obj.Data[0].SubscriptionColour) { //Validation, check mandatory selections have been made in UI
                    if (obj.Data[0].Topics.length > 0 || userSelectedAllTopics) {
                        //alert(obj.SubscriptionType);
                        if (obj.SubscriptionType === "personal") {
                            $(".js-alertSubscription-save-btn").show().unbind().on("click", function (e) {
                                $(this).ajaxActiveButtonUi(); //fire plugin
                                PubSub.publish("AlertSubscriptions.SaveSubscription", {
                                    ViewType: "AlertSubscriptions",
                                    Data: obj.Data,
                                    DataContext: "NewSubscription"
                                });
                            });
                        } else if (obj.SubscriptionType === "group") {
                            //alert(obj.SubscriptionType);
                            //Set as group subscription save
                            var selectedTeam = $(".js-groupSubscription-groups-ddl").val();
                            if (selectedTeam) {
                                $(".js-alertSubscription-save-btn").show().unbind().on("click", function (e) {
                                    $(this).ajaxActiveButtonUi(); //fire plugin
                                    PubSub.publish("AlertSubscriptions.SaveGroupSubscription", {
                                        ViewType: "AlertSubscriptions",
                                        Data: obj.Data,
                                        MemberAreaID: selectedTeam
                                    });
                                });
                            } else {
                                $(".js-alertSubscription-save-btn").hide().unbind();
                            }
                        }
                    }
                } else {//Not valid
                    $(".js-alertSubscription-save-btn").hide().unbind();
                    return;
                }
            } else {//Update - are subscription values changed? Get original subscription data (_getSubscriptionItem) and compare against built response object to check
                $.when(_getSubscriptionItem({
                    SubscriptionId: subscriptionId,
                    DataContext: "ItemUpdate"
                }).then(function (result) {
                    if (result.length) { //Existing subscriptionId returned

                      //If original topics array is empty, flag as 'All Topics'
                        if (!result[0].Topics.length && !obj.Data[0].Topics.length) userSelectedAllTopics = true;

                        //Sort both comparing objects by name to account for any ordering discrepencies caused by removing and re-adding items in the UI so object positioning differs for _.isEqual
                        result[0].Site = _.sortBy(result[0].Site, 'SiteName');
                        result[0].Topics = _.sortBy(result[0].Topics, 'Title');

                        obj.Data[0].Site = _.sortBy(obj.Data[0].Site, 'SiteName');
                        obj.Data[0].Topics = _.sortBy(obj.Data[0].Topics, 'Title');

                        //console.log(JSON.stringify(obj.Data));
                        //console.log(JSON.stringify(result));

                        if (!!_.isEqual(result, obj.Data)) {//Ensure cast bool returned
                            $(".js-alertSubscription-save-btn").hide().unbind();
                        } else {//Changes found

                            if (obj.Data[0].SubscriptionColour) { //Validation, check mandatory selections have been made in UI
                                if (obj.Data[0].Topics.length > 0 || userSelectedAllTopics) {
                                    if (!obj.IsGroup) {
                                        $(".js-alertSubscription-save-btn").show().unbind().on("click", function (e) {
                                            $(this).ajaxActiveButtonUi(); //fire plugin
                                            PubSub.publish("AlertSubscriptions.SaveSubscription", {
                                                ViewType: "AlertSubscriptions",
                                                Data: obj.Data,
                                                DataContext: "Update"
                                            });
                                        });
                                    } else if (obj.IsGroup) {
                                        //Set as group subscription save. Check a group is selected
                                        var memberArea = $(".js-groupSubscription-groups-ddl").val();

                                        if (memberArea) {
                                            $(".js-alertSubscription-save-btn").show().unbind().on("click", function (e) {
                                                $(this).ajaxActiveButtonUi(); //fire plugin
                                                PubSub.publish("AlertSubscriptions.SaveGroupSubscription", {
                                                    ViewType: "AlertSubscriptions",
                                                    Data: obj.Data,
                                                    MemberAreaID: memberArea
                                                });
                                            });
                                        } else {
                                            $(".js-alertSubscription-save-btn").hide().unbind();
                                            return;
                                        }
                                    }
                                }
                            } else {
                                $(".js-alertSubscription-save-btn").hide().unbind();
                            }
                        }
                    }
                }));
            }//End update

            //Bindings
            //Delete subscription
            $(".js-alertSubscription-delete-btn").unbind().on("click", appMain.ajaxActiveButtonUi).on("click", function () {
                PubSub.publish("AlertSubscriptions.DeleteSubscription", {
                    ViewType: "AlertSubscriptions",
                    Data: obj.Data
                });
            });
        });
    };

    var _saveSubscription = function (obj) {
        function getSites() {
            var sites = [];
            $.each(obj.Data[0].Site, function () {
                sites.push(this.SiteName);
            });
            return sites;
        }

        function getTopics() {
            var topics = [];
            $.each(obj.Data[0].Topics, function () {
                topics.push(this.TopicId);
            });
            return topics;
        }

        function setColourPrefix() {
            var colour = obj.Data[0].SubscriptionColour;

            if (_.startsWith(colour, "0x")) {
                return colour;
            } else {
                return "0x" + colour;
            }
        }

        var data = {
            NetworkExternalIdentifier: obj.Data[0].NetworkExternalIdentifier,
            SubscriptionColour: setColourPrefix(),
            Sites: getSites(),
            PopulationName: obj.Data[0].Type.PopulationName,
            PopulationType: _getPopulationType(obj.Data[0].Type.PopulationType),
            Topics: getTopics()
        };

        //Call API

        if (obj.DataContext === "NewSubscription") {
            Subscribe.callApi(data).then(function (data, textStatus, jqXHR) {

                //Update population cache (so reflects in alert filters etc)
                GetAllPopulationsForUser.callApi().then((response, textStatus, jqXHR) => {
                    if (/^2/.test(jqXHR.status)) {
                        Vault.setItem("GetAllPopulationsForUser", response);
                        PubSub.publish("AlertSubscriptions.SubscriptionSavedSuccess", {
                            ViewType: "AlertSubscriptions",
                            DataContext: "SubscriptionSaved",
                            SubscriptionId: data.Data.SubscriptionId,
                            ResponseCode: jqXHR.status
                        });
                    } else {
                        throw new Error("No population data set"); // rejects the promise
                    }
                }).fail(() => {
                    PubSub.publish("AlertSubscriptions.SubscriptionSavedSuccess", {
                        ViewType: "AlertSubscriptions",
                        DataContext: "SubscriptionSaved",
                        SubscriptionId: data.Data.SubscriptionId,
                        ResponseCode: jqXHR.status
                    });
                });
            }).fail();
        } else if (obj.DataContext === "Update") {
            //Add in SubscriptionId for update
            var updateData = $.extend({}, data, { "SubscriptionId": obj.Data[0].SubscriptionId });

            UpdateSubscription.callApi(updateData).then(function (data, textStatus, jqXHR) {

                //Update population cache (so reflects in alert filters etc)
                GetAllPopulationsForUser.callApi().then((response, textStatus, jqXHR) => {
                    if (/^2/.test(jqXHR.status)) {
                        Vault.setItem("GetAllPopulationsForUser", response);
                        PubSub.publish("AlertSubscriptions.SubscriptionSavedSuccess", {
                            ViewType: "AlertSubscriptions",
                            DataContext: "SubscriptionUpdated",
                            SubscriptionId: data.Data.SubscriptionId,
                            ResponseCode: jqXHR.status
                        });
                    } else {
                        throw new Error("No population data set"); // rejects the promise
                    }
                }).fail(() => {
                    PubSub.publish("AlertSubscriptions.SubscriptionSavedSuccess", {
                        ViewType: "AlertSubscriptions",
                        DataContext: "SubscriptionUpdated",
                        SubscriptionId: data.Data.SubscriptionId,
                        ResponseCode: jqXHR.status
                    });
                });
            }).fail();
        }
    };

    var _saveGroupSubscription = function (obj) {
        function getSites() {
            var sites = [];
            $.each(obj.Data[0].Site, function () {
                sites.push(this.SiteName);
            });
            return sites;
        }

        function getTopics() {
            var topics = [];
            $.each(obj.Data[0].Topics, function () {
                topics.push(this.TopicId);
            });
            return topics;
        }

        function setColourPrefix() {
            var colour = obj.Data[0].SubscriptionColour;

            if (_.startsWith(colour, "0x")) {
                return colour;
            } else {
                return "0x" + colour;
            }
        }

        var data = {
            "MemberAreaID": obj.MemberAreaID,
            "Subscription": {
                "NetworkExternalIdentifier": obj.Data[0].NetworkExternalIdentifier,
                "SubscriptionColour": setColourPrefix(),
                "Sites": getSites(),
                "PopulationName": obj.Data[0].Type.PopulationName,
                "PopulationType": _getPopulationType(obj.Data[0].Type.PopulationType),
                "Topics": getTopics()
            }
        }

        //Call API
        UpsertGroupSubscription.callApi(data).then(function (data, textStatus, jqXHR) {
            PubSub.publish("AlertSubscriptions.SubscriptionSavedSuccess", {
                ViewType: "AlertSubscriptions",
                DataContext: "GroupSubscriptionSaved",
                SubscriptionId: data.Data.SubscriptionId,
                ResponseCode: jqXHR.status
            });
        }).fail();
    };

    var _deleteSubscription = function (obj) {

		let apiCall;
		if (obj.Data.length && obj.Data[0].IsGroup) {
			apiCall = UnsubscribeFromGroupSubscription.callApi(obj.Data[0].GroupMemberAreaId, obj.Data[0].SubscriptionId);
		} else {
			let data = {
				"SubscriptionId": obj.Data[0].SubscriptionId
			};
			apiCall = Unsubscribe.callApi(data);
		}

		$.when(apiCall.then(function (data, textStatus, jqXHR) {

            //Update population cache (so reflects in alert filters etc)
            GetAllPopulationsForUser.callApi().then((response, textStatus, jqXHR) => {
                if (/^2/.test(jqXHR.status)) {
                    Vault.setItem("GetAllPopulationsForUser", response);
                    PubSub.publish("AlertSubscriptions.SubscriptionDeletedSuccess", {
                        ViewType: "AlertSubscriptions",
                        DataContext: "SubscriptionDeleted",
                        SubscriptionId: data.Data.SubscriptionId,
                        ResponseCode: jqXHR.status
                    });
                } else {
                    throw new Error("No population data set"); // rejects the promise
                }
            }).fail(() => {
                PubSub.publish("AlertSubscriptions.SubscriptionSavedSuccess", {
                    ViewType: "AlertSubscriptions",
                    DataContext: "SubscriptionSaved",
                    SubscriptionId: data.Data.SubscriptionId,
                    ResponseCode: jqXHR.status
                });
            });
        }));
    };

    var _getPopulationType = function (populationType) {
        switch (populationType) {
            case "NoneProvided":
                return 0;
            case "Clinician":
                return 1;
            case "Area":
                return 2;
            default:
                return 0;
        }
    };

    var _subscriptionSavedUi = function (obj) {
        //Clear down GetAllPopulations follwoing the successful save
        if (obj.ResponseCode === 200) _getAllPopulationsForUserData = null;

        //Cleanup
        if (obj.ResponseCode === 200) delete alertSubscriptions.alertSubscribeRequestObj;

        //Refresh Subscriptions list
        getUsersSubscriptions();

        //Push UI alert
        var text = (obj.DataContext === "SubscriptionDeleted") ? "Subscription deleted" : "Subscription successfully saved";

        appMain.triggerAppNotification({
            Text: text,
            Heading: "",
            Icon:"success"
        });


    };

    var getUsersSubscriptions = function () {
        NProgress.start();

        $.when(_getAllPopulationsForUser().then(function (data) {
            //Manipulate the Subscription Colour property
            $.each(data.Data.CurrentSubscriptions, function (index, val) {
                if (val.SubscriptionColour) {
                    var colour = val.SubscriptionColour;
                    if (_.startsWith(colour, "0x")) {
                        var length = val.SubscriptionColour.length;
                        val.SubscriptionColour = colour.substring(2, length + 1);
                    }
                }
            });


            //Build the UI
            var templatePath = "Content/js/handlebarsTemplates/AlertSubscriptions__activeSubscriptions.htm";
            var domSelectorToInsertTemplateTo = $(".js-alertSubscriptions-context-template");
            GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, data.Data.CurrentSubscriptions).done(GetHandlebarsTemplate.onTemplateInserted).then(function () {
                //Bindings
                var $self;

                $(".js-alertSubscriptions-new-btn").unbind("click").on("click", _createNewSubscription);

                $(".js-alertFeed-item").unbind("click").on("click", function (e) {
                    $self = $(e.currentTarget);

                    NProgress.start();
                    PubSub.publish("AlertSubscriptions.GetSubscriptionItem", {
                        Event: e,
                        DomObject: $self,
                        ViewType: "AlertSubscriptions",
                        DataContext: "GetSubscriptionItem",
                        SubscriptionId: $self.data("subscription-id"),
                        IsGroup: !!$self.data("is-group"),
                        CanManageGroupSubscriptions: appMain.checkUserPermissionForNetwork(Vault.getSessionItem("ActiveNetworkId"), "ManageGroupSubscription")
                    });

                    //subscription.remove();
                });
                // subscription.remove();

                NProgress.done();
            });
        }));
    };

    return {
        getUsersSubscriptions: getUsersSubscriptions
    };
})();

export default alertSubscriptions;

/*Helper to get a Hos[ital Number' from a Careflow patients Identifiers*/
export default {
    methods: {
        getPatientHospitalNumberFromIdentifiers(patientIdentifiers) {

            if (!patientIdentifiers) return null;
            //Pending network config implementation - need to get the Hospital number
            let identifier;
			if (patientIdentifiers.PrimaryIdentifier && patientIdentifiers.PrimaryIdentifier.Label && (patientIdentifiers.PrimaryIdentifier.Label.toLowerCase().includes('hospital') || patientIdentifiers.PrimaryIdentifier.Label.toLowerCase().includes('trust'))) {
                return identifier = patientIdentifiers.PrimaryIdentifier.Value || null;
            } else if (Array.isArray(patientIdentifiers.AlternativeIdentifiers)) {
                identifier = patientIdentifiers.AlternativeIdentifiers.map(x => {
					if (x.Label.toLowerCase().includes('hospital') || x.Label.toLowerCase().includes('trust')) return x.Value;
                    return null;
                }).filter(Boolean)[0] || null; //Take first result, or null
            }
            return identifier;
        }
    }
}

﻿//Parse template dates mixin
export default {
    filters: { //https://momentjs.com/
		formatDate(date) {
			return moment(date).local().format('DD-MMM-YYYY, HH:mm');
        },
        getShortDate(date) {
            return moment(date).format("DD-MMM-YYYY");
        },
        getLocalTime(date) {
            return moment(date).local().format('LT');
		},
		getDay(date) {
			return moment(date).local().format('DD'); //e.g 21, lowercase for 'fri'
		},
		getMonth(date) {
			return moment(date).local().format('MMM'); //e.g Oct, MMMM for 'October'
		},
		getYear(date) {
			return moment(date).local().format('YY'); //e.g 08, YYYY for full year
		}
    }
}

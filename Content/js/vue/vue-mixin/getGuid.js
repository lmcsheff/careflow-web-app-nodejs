// Mixin to generate a GUID on the client - https://caniuse.com/#search=crypto
// https://stackoverflow.com/questions/44042816/what-is-wrong-with-crypto-getrandomvalues-in-internet-explorer-11

export default {
    methods: {
        getGuid() {
            let crypto = window.crypto || window.msCrypto;
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            )
        }
    }
}

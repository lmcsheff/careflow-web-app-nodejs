// Mixin to get consultant / clinician data for UI select components. Import required modules into parent. Requires networkId.
import Vault from 'Endpoints/careflowApp/Vault';
/* API Endpoints */
import GetAllPopulationsForUser from 'Endpoints/populations/GetAllPopulationsForUser';
export default {
	methods: {
		getConsultantsData: function () {
			if (!this.networkId) return [];
			const networkAccessId = appMain.getAccessGroupExternalIdentifier(this.networkId, true); //from parent prop (required)
			let dataCheck = new Promise((resolve, reject) => {
				if (Vault.getItem('GetAllPopulationsForUser')) {
					//Check cache for data (set with getRequestingUserSummary)
					resolve(Vault.getItem('GetAllPopulationsForUser').Data.AvailablePopulations.filter(item => {
						if (item.NetworkExternalIdentifier === networkAccessId) return item;
						return;
					}))
				} else {
					GetAllPopulationsForUser.callApi().then(function(data, textStatus, jqXHR) {
						resolve(data.Data.AvailablePopulations);
					});
				}
			});

			return dataCheck.then(response => {
				return response[0].Clinicians.map((item, index, array) => {
					return {
						label: item.ClinicianName,
						value: item.ClinicianName,
						type: 'clinician'
					};
				});
			});
		}
	}
}

//Parse string case mixin
export default {
    filters: { //https://momentjs.com/
        toUpperCase(string) {
            if (!string) return ``;
            return string.toUpperCase();
        }
    }
}

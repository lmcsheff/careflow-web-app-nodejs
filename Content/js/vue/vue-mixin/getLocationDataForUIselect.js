// Mixin to get location data for UI select components. Import required modules into parent
import Vault from 'Endpoints/careflowApp/Vault';
/* API Endpoints */
import GetAllPopulationsForUser from 'Endpoints/populations/GetAllPopulationsForUser';
export default {
    methods: {
        getLocationData: function() {
            const networkAccessId = appMain.getAccessGroupExternalIdentifier(this.networkId, true); //from parent prop (required)
            let locationData = [];

            let dataCheck = new Promise((resolve, reject) => {
                if (Vault.getItem('GetAllPopulationsForUser')) {
                    //Check cache for data (set with getRequestingUserSummary)
                    resolve(
                        Vault.getItem('GetAllPopulationsForUser').Data.AvailablePopulations.filter(item => {
                            if (item.NetworkExternalIdentifier === networkAccessId) return item;
                            return;
                        })
                    );
                } else {
                    GetAllPopulationsForUser.callApi().then(function(data, textStatus, jqXHR) {
                        resolve(
                            data.Data.AvailablePopulations.filter(item => {
                                if (item.NetworkExternalIdentifier === networkAccessId) return item;
                                return;
                            })
                        );
                    });
                }
            });

            return dataCheck.then(response => {
                let x = response.map((item, index, array) => {
                    return item.Sites.map((item, index, array) => {
                        let siteName = item.SiteName;
                        return item.Areas.map((item, index, array) => {
                            return {
                                siteName: siteName,
                                areaIndex: index,
								areaName: item.AreaName,
								fullNameLabel: item.AreaName + " (" + siteName + ")",
                                label: item.AreaName,
                                value: item.AreaName,
								type: 'location',
								locationType: (item.AreaName) ? 'area' : 'site'
                            };
                        });
                    }).filter(Boolean);
                });

                //TODO: func to flatten deep multidimensional arrays

                let flattenedArray = [].concat(...x).filter(Boolean);

				flattenedArray.forEach(item => {
					if (!item.length) return;
                    item.unshift({
                        label: item[0] ? item[0].siteName + ' (All areas)' : null,
                        value: item[0] ? item[0].siteName : null,
						isSiteLabel: true,
						fullNameLabel: item[0] ? item[0].siteName + " (All areas)" : null,
						siteName: item[0] ? item[0].siteName : null,
						areaName: null,
                        type: 'location',
						locationType: 'site'
                    });
                });

                let xx = [].concat(...flattenedArray);

                return xx.filter(Boolean); //flatten and remove nulls
            });
        }
    }
}

import appMain from "../../app/app-main";

//Scroll to element
export default {
    methods: {
        scrollToEl(el) {
            el.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'end' });
            appMain.setElementsAsFullHeight(); //IE11 issues if not fired
            //https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollIntoView
        }
    }
};

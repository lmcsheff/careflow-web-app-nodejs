	//Allowed characters
	// ' " ` removed due to backend issues
	export const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@|#|$|%|^|&|*|\-|_|!|+|=|\\[|\]|\{|}|\||\|:|,|.|?|\/|~|\(|\)|;]).{8,16}$/; //https://gist.github.com/sofyanhadia/d144b37d390b666d4e3539407bd3aad6
	export const stringMatchCheckRegex = /[@|#|$|%|^|&|*|\-|_|!|+|=|\\[|\]|\{|}|\||\|:|,|.|?|\/|~|\(|\)|;]|[A-Z]|[a-z]|[0-9]/g; //Used to get non matched chars, hence prohibited. match() method
	export const stringMatchSymbolCheckRegex = /[@|#|$|%|^|&|*|\-|_|!|+|=|\\[|\]|\{|}|\||\|:|,|.|?|\/|~|\(|\)|;]/g; //ASCII symbols allowed
	export const allAsciiChars = ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';

	export const upperCase = /[A-Z]/;
	export const lowerCase = /[a-z]/;
	export const number = /[0-9]/;
	export const special = /[@|#|$|%|^|&|*|\-|_|!|+|=|\\[|\]|\{|}|\||\|:|,|.|?|\/|~|\(|\)|;]/g;
	//const prohibited = /[^\u0000-\u00ff]/; //Unicode characters.
	//const spaces = /\s/; //Spaces

	export function hasUnicode(str) {
	    for (var i = 0; i < str.length; i++) {
	        if (str.charCodeAt(i) > 127 || str.charCodeAt(i) === 32 || str.charCodeAt(i) === 34 || str.charCodeAt(i) === 39 || str.charCodeAt(i) === 96) return true; //not ASCII; is unicode or is a blank space (code 32) or "" (code 34) or  ` (code 96) or ' (code 39)
	    }
	    return false;
	}

export const template = `<p class="u-strong">We've sent you an email containing a verification link. Please locate the email, which will look like the image below, and follow the link to verify your account.</p>
<img alt="SystemC & Graphnet Care Alliance HPCA email" aria-label="SystemC & Graphnet Care Alliance" class="hpca-email-image pure-img" src="/Content/img/hpca_email.png" />
<p class="u-strong">
  If you cannot find this email please check your spam or junk folder.
</p>
<p>Please contact us if you need further assistance by emailing:
  <a href="mailto:support@careflowconnect.com">support@careflowconnect.com</a>
</p>`

﻿module.exports = {
    init(analyticsId) {
        window.ga = window.ga || function () { (ga.q = ga.q || []).push(arguments) }; ga.l = +new Date;
        ga('create', analyticsId, 'auto');
        ga('send', 'pageview');
    }
}
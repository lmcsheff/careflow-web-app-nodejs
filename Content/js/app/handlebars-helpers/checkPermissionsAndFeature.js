import CareflowFeatureFlags from '../../endPoints/careflowApp/FeatureFlags';

export default function(permissionArray, permissionValue, propString, options) {
    /*  Usage
     *  Pass in the Permissions array, Permission to check, then the feature (CareflowFeatureFlags)
     *  {{#checkPermissionsAndFeature  NetworkPermissions "ReadContent" "NetworkInbox"}}     
     *  http://stackoverflow.com/questions/6906108/in-javascript-how-can-i-dynamically-get-a-nested-property-of-an-object
     */
    if (!permissionArray || !permissionValue) return false;

    var __indexOf = [].indexOf || function (item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

    if (__indexOf.call(permissionArray, permissionValue) >= 0) {//Permission set, so check feature
        
        var fnTrue = options.fn, fnFalse = options.inverse;
        var obj = CareflowFeatureFlags; //Global app feature object

        if (!propString)
            return obj;

        var prop, props = propString.split('.');

        for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
            prop = props[i];

            var candidate = obj[prop];
            if (candidate !== undefined) {
                obj = candidate;
            } else {
                break;
            }
        }
        return (typeof obj[props[i]] === "undefined" || !obj[props[i]]) ? fnFalse(this) : fnTrue(this);

    } else {
        return options.inverse(this);
    }
};
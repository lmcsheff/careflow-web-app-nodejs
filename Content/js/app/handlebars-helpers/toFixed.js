import { Utils } from '../handlebars-helpers-utils';'

export default function(number, digits) {
    number = parseFloat(number);
    digits = Utils.isUndefined(digits) ? 0 : digits;
    return number.toFixed(digits);
};
export default function() {
    var values = [].slice.call(arguments),
        options = values.pop(),
        result = values.filter(Boolean).join(' - ');

    return result !== '' ? (' - ' + result ) : result;
};
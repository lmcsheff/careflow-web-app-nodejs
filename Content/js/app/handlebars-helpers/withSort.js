import { Utils } from '../handlebars-helpers-utils';

export default function(array, field, options) {
    var item, result, _i, _len;
    result = '';
    if (Utils.isUndefined(field)) {
        options = field;
        array = array.sort();
        for (_i = 0, _len = array.length; _i < _len; _i++) {
            item = array[_i];
            result += options.fn(item);
        }
    } else {
        array = array.sort(function(a, b) {
            return a[field] > b[field];
        });
        for (item in array) {
            result += options.fn(array[item]);
        }
    }
    return result;
};
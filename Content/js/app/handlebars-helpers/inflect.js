import { Utils } from '../handlebars-helpers-utils';

export default function(count, singular, plural, include) {
    var word;
    count = parseFloat(count);
    word = count > 1 || count === 0 ? plural : singular;
    if (Utils.isUndefined(include) || include === false) {
        return word;
    } else {
        return "" + count + " " + word;
    }
};
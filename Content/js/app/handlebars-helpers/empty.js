export default function(array, options) {
    if (!array || array.length <= 0) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
};
import CareflowFeatureFlags from '../../endPoints/careflowApp/FeatureFlags';

export default function(propString, options) {
    /*  http://stackoverflow.com/questions/6906108/in-javascript-how-can-i-dynamically-get-a-nested-property-of-an-object
         * 
         */
    
    var fnTrue = options.fn, fnFalse = options.inverse;
    var obj = CareflowFeatureFlags;

    if (!propString)
        return obj;

    var prop, props = propString.split('.');

    for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
        prop = props[i];

        var candidate = obj[prop];
        if (candidate !== undefined) {
            obj = candidate;
        } else {
            break;
        }
    }
    return (typeof obj[props[i]] === "undefined" || !obj[props[i]]) ? fnFalse(this) : fnTrue(this);
};
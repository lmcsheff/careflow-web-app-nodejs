import { Dates } from '../handlebars-helpers-utils';

export default function(date, format) {
    date = new Date(date);

    return Dates.format(date, format);
};
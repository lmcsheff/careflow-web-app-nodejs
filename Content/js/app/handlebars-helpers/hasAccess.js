export default function(val, options) {
    var fnTrue = options.fn, fnFalse = options.inverse;
    return val > 5 ? fnTrue() : fnFalse();
};
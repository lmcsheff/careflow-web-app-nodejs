export default function(value, divisor) {
    value = parseFloat(value);
    divisor = parseFloat(divisor);
    return value / divisor;
};
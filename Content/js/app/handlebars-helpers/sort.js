import { Utils } from '../handlebars-helpers-utils';'

export default function(array, field) {
    if (Utils.isUndefined(field)) {
        return array.sort();
    } else {
        return array.sort(function(a, b) {
            return a[field] > b[field];
        });
    }
};
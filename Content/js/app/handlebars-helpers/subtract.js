export default function(value, substraction) {
    value = parseFloat(value);
    substraction = parseFloat(substraction);
    return value - substraction;
};
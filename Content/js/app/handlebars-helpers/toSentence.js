export default function(context, block) {
    var ret = "";
    for (var i = 0, j = context.length; i < j; i++) {
        ret = ret + block(context[i]);
        if (i < j - 1) {
            ret = ret + ", ";
        };
    }
    return ret;
};
export default function(string) {
	// encoded is "&lt;p&gt;Example&lt;/p&gt" 
    var decoded = _.unescape(string);
    // decoded should now return <p>Example</p>
    return new Handlebars.SafeString(decoded);
};
import { Utils } from '../handlebars-helpers-utils';

export default function(array, count) {
    if (!Utils.isUndefined(count)) {
        count = parseFloat(count);
    }
    return array.slice(0, -count);
};
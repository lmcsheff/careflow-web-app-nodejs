export default function(value, block) {
    var values = value.split(' ');
    return values[block.data.index % (values.length + 1)];
};
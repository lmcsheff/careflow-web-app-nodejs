import { Dates, Utils } from '../handlebars-helpers-utils';

export default function(format) {
    var date;
    date = new Date();
    if (Utils.isUndefined(format)) {
        return date;
    } else {
        return Dates.format(date, format);
    }
};
import Handlebars from 'handlebars';

export default function(passedString, startstring, endstring) {
    var theString = passedString.substring(startstring, endstring);
    return new Handlebars.SafeString(theString);
};
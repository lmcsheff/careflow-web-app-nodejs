export default function(value, defaultValue) {
    return value || defaultValue;
};
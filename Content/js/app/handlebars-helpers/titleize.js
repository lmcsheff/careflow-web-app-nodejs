export default function(str) {
    var capitalize, title, word, words;
    title = str.replace(/[ \-_]+/g, ' ');
    words = title.match(/\w+/g) || [];
    capitalize = function(word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    };
    return ((function() {
        var _i, _len, _results;
        _results = [];
        for (_i = 0, _len = words.length; _i < _len; _i++) {
            word = words[_i];
            _results.push(capitalize(word));
        }
        return _results;
    })()).join(' ');
};
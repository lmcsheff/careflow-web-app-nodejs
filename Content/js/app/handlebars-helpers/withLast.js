import { Utils } from '../handlebars-helpers-utils';

export default function(array, count, options) {
    var item, result;
    if (!Utils.isUndefined(count)) {
        count = parseFloat(count);
    }
    if (Utils.isUndefined(count)) {
        options = count;
        return options.fn(array[array.length - 1]);
    } else {
        array = array.slice(-count);
        result = '';
        for (item in array) {
            result += options.fn(array[item]);
        }
        return result;
    }
};
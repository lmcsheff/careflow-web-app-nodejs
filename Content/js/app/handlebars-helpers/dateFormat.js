export default function(context, block) {
    if (window.moment) {
        var f = block.hash.format || "DD-MMM-YYYY";
        return moment(context).format(f);
        //return moment(Date(context)).format(f);
    } else {
        return context;   //  moment plugin not available. return data as is.
    };
};
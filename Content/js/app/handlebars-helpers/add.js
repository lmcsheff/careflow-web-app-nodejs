export default function(value, addition) {
    value = parseFloat(value);
    addition = parseFloat(addition);

    return value + addition;
};
import { Utils } from '../handlebars-helpers-utils';

export default function(number, precision) {
    number = parseFloat(number);
    precision = Utils.isUndefined(precision) ? 1 : precision;
    return number.toPrecision(precision);
};
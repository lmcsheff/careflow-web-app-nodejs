export default function(str) {
    var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

    return new Handlebars.SafeString(str.replace(urlRegex, function (url) {
        return '<a href="' + url + '">' + url + '</a>';
    }));
};
import { Utils } from '../handlebars-helpers-utils';

export default function(number, separator) {
    number = parseFloat(number);
    separator = Utils.isUndefined(separator) ? ',' : separator;
    return number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + separator);
};
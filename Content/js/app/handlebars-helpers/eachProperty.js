export default function(obj, options) {
    var key, result, value;
    result = '';
    for (key in obj) {
        value = obj[key];
        result += options.fn({
            key: key,
            value: value
        });
    }
    return result;
};
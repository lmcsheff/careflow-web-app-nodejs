export default function(value) {
    return moment(value).format("HH:mm");
};
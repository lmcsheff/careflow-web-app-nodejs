import { Utils } from '../handlebars-helpers-utils';'

export default function(str, replaceWith) {
    if (Utils.isUndefined(replaceWith)) {
        replaceWith = '-';
    }
    return str.replace(/[^a-z0-9]/gi, replaceWith);
};
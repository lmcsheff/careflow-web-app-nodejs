import { Utils } from '../handlebars-helpers-utils';

export default function(array, count, options) {
    var item, result;
    if (!Utils.isUndefined(count)) {
        count = parseFloat(count);
    }
    array = array.slice(count);
    result = '';
    for (item in array) {
        result += options.fn(array[item]);
    }
    return result;
};
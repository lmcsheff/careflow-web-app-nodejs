export default function() {
    var values = [].slice.call(arguments),
        options = values.pop(),
        collection = values.shift(),
        result = collection.map(function (item) {
            return '<span>' + values.reduce(function (a, c) {
                a.push(item[c]);
                return a;
            }, []).join(' ') + '</span>';
        }).join(', ');

    return new Handlebars.SafeString(result);
};
export default function(value, multiplier) {
    value = parseFloat(value);
    multiplier = parseFloat(multiplier);
    return value * multiplier;
};
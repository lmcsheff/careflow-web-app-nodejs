export default function(str) {
    return str.replace(/\r?\n|\r/g, '<br>');
};
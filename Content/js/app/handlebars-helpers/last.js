import { Utils } from '../handlebars-helpers-utils';

export default function(array, count) {
    if (!Utils.isUndefined(count)) {
        count = parseFloat(count);
    }
    if (Utils.isUndefined(count)) {
        return array[array.length - 1];
    } else {
        return array.slice(-count);
    }
};
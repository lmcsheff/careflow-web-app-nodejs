import { Utils } from '../handlebars-helpers-utils';'

export default function(number, fractions) {
    number = parseFloat(number);
    fractions = Utils.isUndefined(fractions) ? 0 : fractions;
    return number.toExponential(fractions);
};
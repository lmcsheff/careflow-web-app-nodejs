export default function(array, options) {
    var index, result, value, _i, _len;
    result = '';
    for (index = _i = 0, _len = array.length; _i < _len; index = ++_i) {
        value = array[index];
        result += options.fn({
            item: value,
            index: index
        });
    }
    return result;
};
import { Utils } from '../handlebars-helpers-utils';

export default function(str, length, omission) {
    if (Utils.isUndefined(omission)) {
        omission = '';
    }
    if (str.length > length) {
        return str.substring(0, length - omission.length) + omission;
    } else {
        return str;
    }
};
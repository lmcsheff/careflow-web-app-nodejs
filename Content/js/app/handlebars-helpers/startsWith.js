export default function(value, test, options) {
    if (_.startsWith(value, test)) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
};
import { __indexOf } from '../handlebars-helpers-utils';

export default function(array, value, options) {
    if (__indexOf.call(array, value) >= 0) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
};
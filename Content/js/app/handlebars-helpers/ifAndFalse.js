export default function(test, v1, v2, options) {
	if (v1 !== test && v2 !== test) {
        return options.fn(this);
    }
    return options.inverse(this);
};
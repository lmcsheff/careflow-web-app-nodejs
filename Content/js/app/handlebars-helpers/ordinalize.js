import { __indexOf } from '../handlebars-helpers-utils';

export default function(value) {
    var normal, _ref;
    value = parseFloat(value);
    normal = Math.abs(Math.round(value));
    if (_ref = normal % 100, __indexOf.call([11, 12, 13], _ref) >= 0) {
        return "" + value + "th";
    } else {
        switch (normal % 10) {
            case 1:
                return "" + value + "st";
            case 2:
                return "" + value + "nd";
            case 3:
                return "" + value + "rd";
            default:
                return "" + value + "th";
        }
    }
};
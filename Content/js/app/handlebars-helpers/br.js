import { Utils } from '../handlebars-helpers-utils';

export default function(count, options) {
    var br, i;
    br = '<br>';
    if (!Utils.isUndefined(count)) {
        i = 0;
        while (i < (parseFloat(count)) - 1) {
            br += '<br>';
            i++;
        }
    }
    return Utils.safeString(br);
};
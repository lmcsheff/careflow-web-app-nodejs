import { Utils } from '../handlebars-helpers-utils';

export default function(array, length, options) {
    if (!Utils.isUndefined(length)) {
        length = parseFloat(length);
    }
    if (array.length === length) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
};
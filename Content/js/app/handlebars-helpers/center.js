import { Utils } from '../handlebars-helpers-utils';

export default function(str, spaces) {
    var i, space;
    spaces = Utils.result(spaces);
    space = '';
    i = 0;
    while (i < spaces) {
        space += '&nbsp;';
        i++;
    }
    return "" + space + str + space;
};
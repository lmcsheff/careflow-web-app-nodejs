export default function(context, block) {
    var ret = "",
          offset = parseInt(block.hash.offset) || 0,
          limit = parseInt(block.hash.limit) || context.length,
          i = (offset < context.length) ? offset : 0,
          j = ((limit + offset) < context.length) ? (limit + offset) : context.length;


    for (i, j; i < j; i++) {
        ret += block.fn(context[i]);
    }
    return ret;
};
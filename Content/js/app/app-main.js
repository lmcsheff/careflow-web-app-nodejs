﻿/* Utils */
import CareflowApp from '../careflow-app';
import PubSub from '../endPoints/careflowApp/PubSub';
import Vault from '../endPoints/careflowApp/Vault';
import staticKeys from '../endPoints/careflowApp/Static';
import GetHandlebarsTemplate from '../endPoints/careflowApp/GetHandlebarsTemplate';
import AuthenticationSignOut from '../endPoints/careflowApp/SignOut';
import GetItSupportNetworks from '../endPoints/careflowApp/itSupport/getItSupportNetworks';
import googleAnalytics from '../app/google-analytics';
import * as NetworkAdministration from 'Views/NetworkAdministration'
//import * as WebSockets from 'AppJs/sockets.js'

/* API Endpoints */
//import SetDutyStatus from '../endPoints/users/SetDutyStatus';
import GetRequestingUserSummary from '../endPoints/users/GetRequestingUserSummary';
import GetUnreadItemCountsForSingleNetwork from '../endPoints/users/GetUnreadItemCountsForSingleNetwork';
import GetUnreadItemCountsForAllNetworks from '../endPoints/users/GetUnreadItemCountsForAllNetworks';
import GetMemberAreasUserCanStartAConversationIn from '../endPoints/conversation/GetMemberAreasUserCanStartAConversationIn';
import GetAllPopulationsForUser from '../endPoints/populations/GetAllPopulationsForUser';
import GetUsersNetworks from '../endPoints/networks/GetUsersNetworks';
import TasksMetaData from '../endPoints/tasks/TasksMetaData';
import RequestMembership from '../endPoints/groups/RequestMembership';

/* Views */
//import patientView from '../views/PatientView';
//import * as networkHome from '../views/NetworkHome';
//import groupHome from '../views/GroupHome';
//import feeds from '../views/Feeds';

/* Others */
import appRouter from './app-router';
import Vue from 'vue';
//import * as genericHelpers from './generic-helpers';

/*Components*/

var appMain = appMain || (function() {
    var _subscriptions = function() {
        var _randomTimeGenerator = function(min, max) {
            {
                return Math.floor(Math.random() * (max - min + 1) + min);
            }
        };

        PubSub.subscribe("Dispatch.Api.Error", function(obj) { //TODO: Global API error handling, handle error types
            //console.error("Dispatch.Api.Error", obj);
            //stopApiRequests();
            $.toast().reset('all');

            if (obj && obj.hasOwnProperty('Xhr') && obj.Xhr !== 401) {
                appMain.triggerAppNotification({
                    Heading: "An error occurred",
                    Text: "We were unable to process your request, please try again",
                    Icon: "error"
                });
            }
        });

        //Listeners for SessionStorage mechanism
        PubSub.subscribe("CareflowApp.ActiveNetworkChanged", function(obj) { //Triggered in Vault on key updates (actually, this triggers each route,via networkisvalid() method on router)
            if (!Vault.getSessionItem("ActiveNetworkId")) {
                Vault.deleteSessionItem("ActiveNetworkName");
                Vault.deleteSessionItem("ActiveNetworkMemberAreaId");
                Vault.deleteSessionItem("ActiveGroupMemberAreaId");
                Vault.deleteSessionItem("ActiveGroupName");
            } else {
                Vault.setSessionItem("ActiveNetworkName", appMain.getNetworkName(Vault.getSessionItem("ActiveNetworkId"), null));
                Vault.setSessionItem("ActiveNetworkMemberAreaId", appMain.getNetworkMemberAreaId(Vault.getSessionItem("ActiveNetworkId")));
                //appMain.getGlobalContextActionsUi();
            }

        });

        PubSub.subscribe("CareflowApp.ActiveGroupMemberAreaIdChanged", function(obj) { //Triggered in Vault on key updates
            if (!Vault.getSessionItem("ActiveGroupMemberAreaId")) {
                Vault.deleteSessionItem("ActiveGroupName");
            } else {
                Vault.setSessionItem("ActiveGroupName", appMain.getMemberAreaName(Vault.getSessionItem("ActiveGroupMemberAreaId")));
            }
        });

        //UI utils subscriptions
        PubSub.subscribe("CareflowApp.CloseAllOpenModalInstances", function(obj) {
            var current = $.featherlight.current();
            current.close();
        });

        //Listener for badge counts (TODO: websockets and Vueex)
        PubSub.subscribe("BadgeCounts.UnreadCountsForSingleNetworkUpdated", function(obj) {
            //Tell any Vue conmponents that may be listening
            VueEventBus.$emit('BadgeCounts.UnreadCountsForSingleNetworkUpdated', obj);
            Vault.setItem("UnreadCountsForSingleNetwork", obj);
            //setMainNavigationBadgeCounts(obj);
        });

        PubSub.subscribe("BadgeCounts.UnreadCountsForAllNetworksUpdated", function(obj) {
            //Tell any Vue conmponents that may be listening
            VueEventBus.$emit('BadgeCounts.UnreadCountsForAllNetworksUpdated', obj);

            Vault.setItem("UnreadCountsForAllNetworks", obj);
            //setMainNavigationBadgeCountsForAllUserNetworks(obj);
        });

        PubSub.subscribe("BadgeCounts.ClearAllBadgeCounts", function() {
            //CSS :empty will remove badges from UI
            Vault.deleteItem("UnreadCountsForAllNetworks");
            Vault.deleteItem("UnreadCountsForSingleNetwork");

            //Tell any Vue conmponents that may be listening
            VueEventBus.$emit('BadgeCounts.UnreadCountsForAllNetworksUpdated', {});
            VueEventBus.$emit('BadgeCounts.UnreadCountsForSingleNetworkUpdated', {});
        });

        $.jStorage.listenKeyChange("*", function(key, action) { //Listeners for Vault mechanism
            if (_appSignOutInitiatedFlag) return; //Logging out, do not fire change events

            if (key === staticKeys.getStaticKey("AccessTokenKey")) {
                if (Vault.getItem("AccessTokenKey")) {
                    Vault.setSessionItem(staticKeys.getStaticKey("AccessTokenKey"), Vault.getItem("AccessTokenKey")); //Set Token in session and delete local storage item
                    Vault.deleteItem("AccessTokenKey");
                }
            }

            if (key === staticKeys.getStaticKey("RefreshTokenKey")) {
                if (Vault.getItem("RefreshTokenKey")) {
                    Vault.setSessionItem(staticKeys.getStaticKey("RefreshTokenKey"), Vault.getItem("RefreshTokenKey")); //Set Token in session and delete local storage item
                    Vault.deleteItem("RefreshTokenKey");
                }
            }
        });

        // Tab unload event
        //window.onbeforeunload = function (e) {
        //    var dialogText = 'Dialog text here';
        //    e.returnValue = dialogText;
        //    return dialogText;
        //};
        //Remove navigation prompt
        //window.onbeforeunload = null;

        //SessionSharing
        if (crosstab.supported) {
            crosstab.on('RequestSessionTokens',
                function(data) {
                    //Dont broadcast if logging out.
                    if (_appSignOutInitiatedFlag) return false;

                    //Only allow the master tab to ever send tokens
                    var crossTabtabId = crosstab.id;
                    var isMasterTab = _.has(crosstab.util.tabs, "MASTER_TAB") ? crosstab.util.tabs.MASTER_TAB.id : null;
                    var userHasAnActiveSession = Vault.getItem("ActiveUserSessionSet");

                    //Check for an active session token, if so,is broadcast candidate
                    if (userHasAnActiveSession &&
                        Vault.getSessionItem('refresh_token') &&
                        Vault.getSessionItem('access_token')) {
                        //Broadcast to the requesting tab
                        if (crosstab.supported) {
                            crosstab.broadcast("SendSessionTokens", {
                                    RefreshToken: Vault.getSessionItem("refresh_token"),
                                    AccessToken: Vault.getSessionItem("access_token"),
                                    Date: moment().valueOf(), //Unix
                                    MasterTabId: crossTabtabId,
                                    CrossTabSessionRequesterTabId: data.data.CrossTabSessionRequesterTabId
                                },
                                data.data.CrossTabSessionRequesterTabId);
                        }
                    }
                });

            crosstab.on('SendSessionTokens',
                function(response) { //New tab, consume and set tokens sent by master tab
                    var crossTabtabId = crosstab.id;

                    if (crossTabtabId == response.data.CrossTabSessionRequesterTabId &&
                        !_appSignOutInitiatedFlag &&
                        !_sessionDataSetByParentTab
                    ) { //Verify this tab originated the session request chain, and we are not logging out
                        _sessionDataSetByParentTab =
                            true; //Set here to block other tabs, we have the session data needed.

                        if (!Vault.getSessionItem('refresh_token') && !Vault.getSessionItem('access_token')) { //If the target tab has no active session token
                            console.log("I am the newbie!! ", response.data.CrossTabSessionRequesterTabId);

                            if (crosstab.supported) {
                                crosstab.broadcast("SessionIsResumed", {
                                    //If any tab is timing out(modal countdown), resume the session
                                    SessionIsResumedInTabId: crossTabtabId
                                });
                            }

                            if (_.has(response.data, "RefreshToken") && _.has(response.data, "AccessToken")) {
                                Vault.setSessionItem("refresh_token", response.data.RefreshToken);
                                Vault.setSessionItem("access_token", response.data.AccessToken);

                                //crosstab.off("SendSessionTokens"); //Data set, so stop listening
                                //crosstab.off("RequestSessionTokens"); //Data set, so stop listening

                                //Use Pub/Sub, as we know target tab context is correct here
                                PubSub.publish("CareflowApp.SessionDataSet", {
                                    RequesterTabId: response.data.RequesterTabId
                                });
                            }
                        }
                    };
                });

            crosstab.once('SessionStartedInTab',
                function(response) { //A tab has logged in, so pass the session to all other actively open tabs in the browser
                    if (_appSignOutInitiatedFlag) return;

                    var crossTabtabId = crosstab.id;

                    if (!Vault.getSessionItem('refresh_token') &&
                        !Vault.getSessionItem('access_token') &&
                        crossTabtabId !== response.data.SessionStartedInTabId
                    ) { //If the target tab has no active session token, and is not originator
                        console.log("im in in!!!!");

                        if (crosstab.supported) {
                            crosstab.broadcast("SessionIsResumed", {
                                //If any tab is timing out(modal countdown), resume the session
                                SessionIsResumedInTabId: crossTabtabId
                            });
                        }

                        if (_.has(response.data, "RefreshToken") && _.has(response.data, "AccessToken")) {
                            Vault.setSessionItem("refresh_token", response.data.RefreshToken);
                            Vault.setSessionItem("access_token", response.data.AccessToken);

                            _sessionDataSetByParentTab = true;
                            window.location.href = "/"; //Hard start the app in tab
                        }
                    }
                });

            //User session has ended / logged out in another tab
            crosstab.on('EndTabSessions',
                function(response) {
                    var crossTabtabId = crosstab.id;

                    if (Vault.getSessionItem('refresh_token') &&
                        Vault.getSessionItem('access_token') &&
                        crossTabtabId !== response.data.SessionEndedInTabId) {
                        appMain.getAppTimeoutInstance().disableTimeout(); //Disable timeout
                        _appSignOutInitiatedFlag =
                            true; // Child Tab, not originator. Set flag, as triggered by parent tab as ALL need to log out
                        sessionStorage.clear(); //Clear session
                        // if (_tabCreatedId === data.TabId) Vault.flush(); //Parent Tab - do lastly after all tabs have fired

                        window.location.href = "/"; //Hard redirect, reset Application
                    }
                });

            //AccessTokenKeyRenewed in another tab. Other creds on local storage
            crosstab.on('CareflowAppAuth.AccessTokenKeyRenewed',
                function(response) {
                    var crossTabtabId = crosstab.id;
                    if (crossTabtabId !== response.data.AccessTokenKeyRenewedInTabId) { //Check this tab is not the sender
                        Vault.setSessionItem("access_token", response.data.AccessTokenKey);
                    }
                });

            crosstab.util.events.on(crosstab.util.eventTypes.tabUpdated,
                function() {});
            crosstab.util.events.on(crosstab.util.eventTypes.tabClosed,
                function() {});
            crosstab.util.events.on(crosstab.util.eventTypes.tabPromoted,
                function() {});
        } //End if crosstab supported

    }(); //Init, no params needed to be passed in

    //Shared utils globally
    var _currentEnvironment; //Environment flag
    var _currentApiVersion; //Api version flag
    var _cachedAppConfig; //Cache of app config, set on bootstrap
    var _xhrPool = []; //Track Ajax requests
    var _tabCreatedId = moment().valueOf(); //Allocate a UNIX id to the tab for tracking http://stackoverflow.com/questions/25499191/getting-current-unixtimestamp-using-moment-js
    var _sessionDataSetByParentTab; //Flag to identify tab as having session data set by a parent (previoulsly logged in)
    var _appTimeoutInstance;
    var _appSignOutInitiatedFlag; //Flag for logout procedure
    var _windowHeight; //Useful for UI
    var _windowWidth; //Useful for UI
    var _windowHeightMinusDeductableElements; //Useful for UI
    var _moduleRootVueInstance; //TODO: Summary as to how this app uses vue.js in a hybrid manner
    let _openedPatientContextWindows = []; //3rd party integration windows
    let _isRefreshingAccessToken = false;
    let _cachedMedwayPatient = null;
    let _openMedwayPatientApiRequests = new Set();

    //IE8/9 console
    if (!window.console) window.console = {};
    if (!window.console.log) window.console.log = function() {};

    //Methods

    var appOnLoadInit = function() { //First load / hard refresh
        "use strict"; //http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/

        //Ajax setup, tracking of requests globally (jquery - to deprecate with Axios)
        $.ajaxSetup({ cache: false });

        $(document).ajaxSend(function(e, jqXHR, options) {
            _xhrPool.push(jqXHR);
        });
        $(document).ajaxComplete(function(e, jqXHR, options) {
            _xhrPool = $.grep(_xhrPool, function(x) { return x != jqXHR; });
        });

        /*Some global plugin extends*/

        _.mixin(s.exports()); //Undercore.js string global - usage: s.method

        //Swag.registerHelpers(); //Swag Handlebars helpers

        NProgress.configure({ trickleRate: 0.02, trickleSpeed: 800, parent: ".js-app", showSpinner: false });

        $.extend($.featherlight.defaults, {
            closeSpeed: 10,
            openSpeed: 10,
            variant: null,
            background: null,
            beforeOpen: function(e) {
                //_modalIsOpen = true;
            },
            beforeContent: function(e) {
                cleanAllRenderedQtips();
                //NProgress.configure({ trickleRate: 0.02, trickleSpeed: 800, parent: ".featherlight-content" });
                $(document).find($(".featherlight")).addClass("appModal-wrapper js-appModal-wrapper").attr("data-modal-id", this.id); //Add global hook
            },
            afterContent: function(e) {
                $(document).find($(".featherlight-inner")).addClass("js-appModal-inner-content").attr("data-modal-id", this.id).css({ height: "100%" }); //Add global hook after content is loaded
                ajaxLoadingHelperComplete();
                setModalElementsAsFullHeight(this);
            },
            beforeClose: function() {
                cleanAllRenderedQtips();
                NProgress.done();
            },
            afterClose: function(e) {
                //_modalIsOpen = false;
                setElementsAsFullHeight();
                //NProgress.configure({ trickleRate: 0.02, trickleSpeed: 800, parent: ".js-app" });
            },
            onResize: function(e) {
                setModalElementsAsFullHeight(this);
            }
        });

        // make sure div stays full width/height on browser resize
        $(window).unbind('resize', setElementsAsFullHeight).on("resize", setElementsAsFullHeight);

        //Get custom jQuery plugins
        _initCustomJqueryPlugins();

        //Setup custom keyboard shortcuts
        _setCustomKeyboardShortCuts();

        //Create global Vue.js event bus for component messaging
        window.VueEventBus = new Vue({});

        //Mechanism to prevent a user timing out over an iFrame - js-iframe-parent-wrapper - (Rapport etc.)
        let windowBlurObj = { //Obj to track when window loses focus
            iFrameMouseOver: false
        }

        $(".js-app").on("mouseenter", ".js-iframe-parent-wrapper", function() { //Stop timeout when over some Medway iFrames
            windowBlurObj.iFrameMouseOver = true;
            appMain.getAppTimeoutInstance().disableTimeout();
            console.log("iframe detected");
        });

        $(".js-app").on("mouseleave", ".js-iframe-parent-wrapper", function() {
            windowBlurObj.iFrameMouseOver = false;
            appMain.getAppTimeoutInstance().restartTimeout(); //Restart timeout
            console.log("iframe out");
        });

        //Set static.js vars from appConfig.json, then fire up app
        getAppConfig().then(function(response) {
            staticKeys.setStaticKey("ApiRoute", response.careflowApi.url);
            staticKeys.setStaticKey("ApiVersion", response.careflowApi.version);
            staticKeys.setStaticKey("AppVersion", response.version);
            staticKeys.setStaticKey("AppEnvironment", response.environment);
            staticKeys.setStaticKey("ApiAuthRoute", response.careflowAuthApi.url);
            _currentEnvironment = response.environment;
            _currentApiVersion = response.careflowApi.version || 11;
            //Raygun API keys
            rg4js('enableCrashReporting', true);
            rg4js('apiKey', response.rayGunKey);

            if (response.hasOwnProperty("environment") && (response.environment !== "AppDev" && response.environment !== "Test")) { //Dev env settings - TODO: CI env vars
                Vue.config.devtools = false;
                Vue.config.debug = false;
                Vue.config.silent = true;
            }

            //Start App
            appRouter.getRouter();

            //Init GA
            let maxAnalyticCheckLoops = 0;
            let googleAnalyticsInitLoop = setInterval(() => { //Loop to ensure GA window object loads. We do not want ga script adding to bundle size with an import
                if (!checkIfFeatureIsEnabledInAppConfig("googleAnalyticsId") || maxAnalyticCheckLoops >= 3) { //Max 3 tries
                    clearInterval(googleAnalyticsInitLoop); //Max 3 tries
                    return false;
                }
                maxAnalyticCheckLoops++;

                let analyticsId = null;

                if (window.ga && response["networkConfig"][response.environment].hasOwnProperty("googleAnalyticsId")) analyticsId = response["networkConfig"][response.environment].googleAnalyticsId || null;

                if (analyticsId) {
                    googleAnalytics.init(analyticsId);
                    return clearInterval(googleAnalyticsInitLoop);
                }
            }, 250);
        });

        //Set up socket listeners
        //WebSockets.initClient();
    };

    var stopApiRequests = function() { //cancel unfinished ajax xhr requests
        $.each(_xhrPool, function(idx, jqXHR) {
            jqXHR.abort();
        });
        VueEventBus.$emit("Dispatch.CancelApiRequest", { //Axios
            Message: "Api request cancelled"
        });
        NProgress.done();
    };

    var setModuleRootVueInstance = function(instance) {
        _moduleRootVueInstance = instance;
    };

    var getModuleRootVueInstance = function() {
        if (_moduleRootVueInstance) return _moduleRootVueInstance;
        return false;
    };

    var destroyModuleRootVueInstance = function() {
        if (_moduleRootVueInstance && typeof _moduleRootVueInstance === "object") {
            _moduleRootVueInstance.$destroy();
            $(_moduleRootVueInstance.$el).remove();
            _moduleRootVueInstance = null;
        }
    };

    var isOldIe = function() { //Flag for Old IE
        if ($('html').is('.ie8, .ie9')) {
            return true;
        }
        return false;
    };

    var _setCustomKeyboardShortCuts = function() { //TODO:EHI DEMO - CREATE NEW MODULE //https://craig.is/killing/mice
        //Focus global network search
        Mousetrap.bind('s', function(e) {
            e.preventDefault();
            var searchInput = document.getElementsByClassName('js-appMainNavigation-patient-search-txt')[0];
            searchInput.focus();
            searchInput.value = "";
        });

        //Collapse slim patient list, if visible
        Mousetrap.bind("\\", function(e) { //https://stackoverflow.com/questions/3903488/javascript-backslash-in-variables-is-causing-an-error
            e.preventDefault();
            var patientSidePanel = document.getElementsByClassName('js-teamPatients-sidePanel-wrapper')[0];

            if (patientSidePanel) {
                var collapseButton = document.getElementsByClassName('js-sidePanel-collapse--teamPatientList')[0];

                var state = patientSidePanel.getAttribute("aria-expanded"); //Colapsed state
                if (state) { //expand
                    collapseButton.click();
                } else { //collapse
                    collapseButton.click();
                }
            }
        });
    };

    var _setEnvironmentBanner = function() {
        if (!staticKeys.getStaticKey("AppEnvironment")) return false;
        var text = staticKeys.getStaticKey("AppEnvironment").toLowerCase();

        var $banner = $(".js-appEnvironmentBanner");
        if ($banner.length && (text !== "demo" && text !== "live")) {
            return $banner.text(text);
        } else {
            return $banner.remove();
        }
    };

    var getCurrentEnvironment = function() {
        if (_currentEnvironment) return _currentEnvironment;
        return null;
    };

    var getCurrentApiVersion = function() {
        if (_currentApiVersion) return _currentApiVersion;
        return null;
    };

    var appTimeoutCounter = function() {
        isUserLoggedIn().then(response => { //Only start timeout if user is logged in
            if (response && !isTpiIntegrationSession()) { //No TPI Medway timeout
                let environment = (typeof getCurrentEnvironment() === "string") ? getCurrentEnvironment().toLowerCase() : "";
                return _appTimeoutInstance = $(document).idleTimeout({
                    redirectUrl: false,
                    activityEvents: 'click keypress touchmove',
                    idleTimeLimit: (environment === "demo") ? 3000 : 600, // 'No activity' time limit in seconds. Account for dialogDisplayLimit time. 300 = 5 Minutes
                    dialogDisplayLimit: 30, // Time to display the warning dialog before logout in seconds. 180 = 3 Minutes
                    idleCheckHeartbeat: 1, // Frequency to check for idle timeouts in seconds
                    sessionKeepAliveTimer: false,
                    customCallback: function() { //Called on signOut
                    }
                });
            } else {
                return _appTimeoutInstance = null;
            }
        });
    };

    var getAppTimeoutInstance = function() {
        if (_appTimeoutInstance) {
            return _appTimeoutInstance;
        }
        return appTimeoutCounter();
        //Usage eg: appMain.getAppTimeoutInstance().restartTimeout(); //Restart timeout
        //appMain.getAppTimeoutInstance().disableTimeout()
    };

    var isUserLoggedIn = function() { //Check is user is logged in across all window tabs
        return new Promise(function(resolve, reject) {
            if (Vault.getSessionItem("access_token") && Vault.getSessionItem("refresh_token") && Vault.getItem("GetRequestingUserSummary")) { //TODO: ADD REFRESH TIME CHECK WITH EXPIREs_IN
                resolve(true);
            } else {
                if (!_appSignOutInitiatedFlag) { //Only continue to check if logout flag is not actively set
                    var crossTabtabId = crosstab.id;
                    var crossTabMasterTabId = _.has(crosstab.util.tabs, "MASTER_TAB") ? crosstab.util.tabs.MASTER_TAB.id : false;

                    // if (!crossTabMasterTabId) return result.resolve(false); //No master tab, infer as first opened, bail out

                    PubSub.subscribeOnce("CareflowApp.SessionDataSet", function() {
                        resolve(true);
                    });

                    var data = {
                        TabId: _tabCreatedId,
                        CrossTabSessionRequesterTabId: crossTabtabId
                    };

                    //Has a session set flag been set? If so, broadcast and request it
                    var newtabs = JSON.parse(JSON.stringify(crosstab.util.tabs));
                    newtabs = crosstab.util.filter(newtabs, function(value, key) {
                        return key !== 'MASTER_TAB';
                    });
                    var countOfTabs = _.size(newtabs);

                    //Check this is not the only open tab (don't rely on ActiveUserSessionSet as user may have closed tabs but not logged out, thus not clearing storage item)
                    if (countOfTabs === 1) {
                        Vault.deleteItem("ActiveUserSessionSet"); //Important to remove this item! - clears any lingering values as referenced in above comment, user is logged out
                        resolve(false);
                    } else if (Vault.getItem("ActiveUserSessionSet") && countOfTabs > 1) {
                        if (crosstab.supported) {
                            crosstab.broadcast("RequestSessionTokens", data);
                        }
                    } else {
                        resolve(false);
                    }
                } else {
                    resolve(false);
                }
            }
        });
    };

    var userLogOut = function(e, wasTimedOut) {
        _appSignOutInitiatedFlag = true; //Set logout procedure flag
        if (appMain.getAppTimeoutInstance()) appMain.getAppTimeoutInstance().disableTimeout(); //Disable timeout on instance if set
        appMain.closeOpenedPatientContextWindow(); //Close any patient context windows
        var userKey = Vault.getItem("ApplicationUserID");
        AuthenticationSignOut.callApi().then(function(response) {
            if (response.status === 200) {
                Vault.deleteItem("ActiveUserSessionSet");
                _appSignOutInitiatedFlag = true;
                var obj = {
                    Date: moment().valueOf(),
                    TabId: _tabCreatedId
                };
                var tabId = (crosstab.supported) ? crosstab.id : null;
                //Broadcast to all session tabs to end
                if (crosstab.supported) {
                    crosstab.broadcast("EndTabSessions", {
                        SessionEndedInTabId: tabId
                    });
                }
                let networkId = Vault.getSessionItem("ActiveNetworkId"); //Take reference before flushed for appUrl if needed
                sessionStorage.clear(); //Clear session
                Vault.flush(); //Logout initiated here (this tab called userLogOut()), clear data for all tabs (localstorage)

                //If modal timed out, set the last location for 10 mins to help with a re-log in
                if ($.jStorage.get("appUrl")) $.jStorage.deleteKey("appUrl"); //Remove for a regular logout

                if (wasTimedOut) {
                    $.jStorage.set("appUrl", {
                        url: appRouter.careflowAppRouter.getLocation(),
                        key: userKey,
                        networkId: networkId
                    });
                }

                window.location.href = "/"; //Hard redirect, reset Application
                //Vault.setItem("UserSessionEnded", obj, { TTL: 3000 });
            } else {
                _appSignOutInitiatedFlag = true;
                stopApiRequests();
                sessionStorage.clear();
                window.location.replace("/");
            }
        }).catch(function(error) {
            _appSignOutInitiatedFlag = true;
            stopApiRequests();
            sessionStorage.clear();
            window.location.replace("/");
        });
    };

    var setElementsAsFullHeight = function() {
        //if ($.featherlight.current()) { //Modal in view, bail out
        //    return;
        //}

        /*Full page app setup - fired on view load, window resize and when templates are updated, inserted etc. Ensures that the full window is used by setting heights and scrolls on all elements contained within the view. Min width of 1024 for site. Responsive above */

        //Set environment banner
        _setEnvironmentBanner();

        //var appHeaderHeight = ($(".js-appHeader").is(":visible")) ? parseInt($(".js-appHeader").outerHeight(true)) : 0;
        var globalNavigationHeight = ($(".js-appMainNavigation--template").is(":visible")) ? parseInt($(".js-appMainNavigation--template").outerHeight(true)) : 0;

        //deduct app environmennt banner too
        var globalEnvironmentHeaderHeight = ($(".js-appEnvironmentBanner").is(":visible")) ? parseInt($(".js-appEnvironmentBanner").outerHeight(true)) : 0;

        //if (globalHeaderHeight > globalNavigationHeight) {
        //    globalNavigationHeight = globalHeaderHeight;
        //}

        _windowHeight = parseInt($(window).outerHeight(true)) - (globalNavigationHeight + globalEnvironmentHeaderHeight);
        var windowMinimumBreakpoint = 1024;
        appMain.windowHeight = _windowHeight; //set global API
        _windowWidth = $(window).outerWidth(true);

        //If its in a modal context, if so return
        //if (_modalIsOpen) return;

        var fullHeightElement = $(".js-fullHeight"); //Body element set full height, not accounting for siblings
        var pageGlobalDeductableElement = $("[data-full-height-deduct-global]"); //Wherever this element is on the page, account for its height (e.g. Patient Banner on Patient/)

        var mainNavWidth = 0;

        if ($(".js-appMainNavigation--template").is(":visible")) { //Set width of main navigation
            var $el = $(".js-appMainNavigation--template");
            // mainNavWidth = $el.outerWidth(true);
            mainNavWidth = _windowWidth;
            $el.css({ "width": mainNavWidth, "min-width": windowMinimumBreakpoint });
        }

        $(".js-app").css({ "min-width": windowMinimumBreakpoint });
        $(".js-app-body").css({ "width": _windowWidth, "min-width": windowMinimumBreakpoint }); //Set width of main view body, to account for varying layouts.

        /*Parse the global deductable elements*/
        var pageGlobalDeductableElements = 0;
        pageGlobalDeductableElement.each(function() {
            if ($(this).is(":visible")) pageGlobalDeductableElements += parseInt($(this).outerHeight(true));
        });

        /*Global*/
        fullHeightElement.each(function() {
            var $self = $(this);

            if ($self.data("scroll")) {
                $self.css({ 'height': _windowHeight, 'max-height': _windowHeight, 'overflow-y': 'auto' });
            } else {
                $self.css({ 'height': _windowHeight, 'max-height': _windowHeight, 'overflow': 'hidden' });
            }
        });

        /*Relative*/
        $(".js-scaffold-block").each(function() { //Calculate the height relative only to the parent container (.js-scaffold-block is the structural region container), to account for a variety of layouts
            var that = $(this);
            var fullHeightBodyElement = $(this).find(".js-fullHeight-bodyElement"); //Element within body thay may need to account for siblings height
            var fullHeightBodyElementdeductable = $(this).find("[data-full-height-deduct]"); //Page elements (visible) where height needs to be deducted (relative to scaffold block)
            //var heightTillBottomDeduct = $(this).find('[data-height-till-bottom-deduct]');

            /*Page body elements to account for sibling elements on page*/
            var deductableHeightElements = 0;
            fullHeightBodyElementdeductable.each(function() {
                if ($(this).is(":visible")) deductableHeightElements += parseInt($(this).outerHeight(true));
            });

            //var bottomDeductableHeightElemenets = 0;
            // heightTillBottomDeduct.each(function () {
            //    if ($(this).is(":visible")) bottomDeductableHeightElemenets += parseInt($(this).outerHeight(true));
            // });

            var calculatedFullHeight = _windowHeight - deductableHeightElements;

            fullHeightBodyElement.each(function() {
                //Check if the full height element has margins
                var $self = $(this);
                var topMargin = 0;
                var bottomMargin = 0;
                var additionalElementsHeightToInclude = 0;
                var additionalElementsHeightToExclude = 0;

                if (checkIfElementHasDataKeySet($self, "full-height-deduct-include")) { //An optional class list of additional element height(s) to dedcut for complex views
                    var additionalIncludeClassElements = $self.data("full-height-deduct-include").split(" ");

                    if (additionalIncludeClassElements.length) {
                        additionalIncludeClassElements.forEach(function(item) {
                            var el = $("." + item);
                            var height = parseInt(el.outerHeight(true));

                            if (el.is(":visible")) additionalElementsHeightToInclude += height; //Increment
                        });
                    }
                }

                if (checkIfElementHasDataKeySet($self, "full-height-deduct-exclude")) { //An optional class list of additional element height(s) to dedcut for complex views
                    var additionalExcludeClassElements = $(this).data("full-height-deduct-exclude").split(" ");

                    if (additionalExcludeClassElements.length) {
                        additionalExcludeClassElements.forEach(function(item) {
                            var el = $("." + item);
                            var height = parseInt(el.outerHeight(true));

                            if (el.is(":visible")) additionalElementsHeightToExclude += height; //Decrease
                        });
                    }
                }

                if (additionalElementsHeightToInclude > 0) calculatedFullHeight += additionalElementsHeightToInclude; //Add this height to the element
                if (additionalElementsHeightToExclude > 0) calculatedFullHeight -= additionalElementsHeightToExclude; //Deduct this height from the element

                topMargin += ($self.css("margin-top").replace('px', '') !== "auto") ? parseInt($self.css("margin-top").replace('px', ''), 10) : 0; /*IE8 Fix for "auto" */
                bottomMargin += ($self.css("margin-bottom").replace('px', '') !== "auto") ? parseInt($self.css("margin-bottom").replace('px', ''), 10) : 0; /*IE8 Fix for "auto" */

                var margins = (topMargin + bottomMargin) + pageGlobalDeductableElements;

                if ($self.data("inherit-el-height")) { //Expects a js-class hook
                    let height;
                    let elHeight = $("." + $self.data("inherit-el-height")).outerHeight(true);
                    height = (elHeight > (calculatedFullHeight - margins)) ? calculatedFullHeight - margins : elHeight;

                    $self.css({ 'height': 'auto', 'overflow-y': 'auto' });
                } else if ($self.data("scroll") && !$self.data("inherit-el-height")) {
                    $self.css({ 'height': calculatedFullHeight - margins, 'max-height': calculatedFullHeight - margins, 'overflow': 'auto' });
                } else {
                    $self.css({ 'height': calculatedFullHeight - margins, 'max-height': calculatedFullHeight - margins, 'overflow': 'hidden' });
                }

                //On scrol to bottom, ensure any waypoints within this container are calibrated
                // if (!$(this).data("waypoint-scroll-calibrate")) {
                //var throttled = _.throttle(scrollToBottomCheck, 1000);

                // if (!$self.data("waypoint-scroll-calibrate")) {
                //     $self.on("scroll", scrollToBottomCheck);
                //     $self.data("waypoint-scroll-calibrate", true);
                // }
                // }

                // function scrollToBottomCheck() {
                //     if ($self[0].scrollHeight - $self.scrollTop() === $self.outerHeight()) {
                //         Waypoint.refreshAll();
                //     };
                // };

                //  if ($(this).data('heightTillBottom')) {
                //     var newHeight = _windowHeight - $(this).position().top - bottomDeductableHeightElemenets;
                //     $(this).css({ 'height': newHeight, 'max-height': newHeight });
                // }
            });
        });

        //Set useful API to get height available on view with all deuctable / static elements accounted for
        var deductableValue = pageGlobalDeductableElements;

        _windowHeightMinusDeductableElements = _windowHeight - deductableValue;
        appMain.windowHeightMinusDeductableElements = _windowHeightMinusDeductableElements; //set global API

        //Text clamp elements
        //$("[data-clamp-overflow]").each(function () {
        //    var $self = $(this);

        //    if (!$self.data("clamp-title")) $self.data("clamp-title", $self.text());

        //    $self.trunk8({
        //        onTruncate: function () {
        //            $self.data("is-truncated", true);
        //        }
        //    });
        //});
        //Waypoint.refreshAll(); //Any waypoints will need refreshing

        //Both needed here due to 'clamping' of the names, based on screen size
        //_setNetworkTitleInUi();
        //_setGroupTitleInUi();

        //updateNavigationCounts(); //Commented out until websocket implementation
    };

    var _setNetworkTitleInUi = function() {
        // if (!Vault.getSessionItem("ActiveNetworkName") || !$(".js-networkLabel").length) return;

        // var $networkLabel = $(".js-networkLabel");
        // $networkLabel.text(Vault.getSessionItem("ActiveNetworkName"));

        // $networkLabel.trunk8({
        //     // width: "70",
        //     lines: 1,
        //     onTruncate: function() {
        //         $networkLabel.data("is-truncated", true);
        //     }
        // });

        // $networkLabel.unbind("mouseenter").on("mouseenter", function(e) {
        //     var $self = $(this);
        //     if ($self.data("is-truncated") && ($self.text() !== $self.data("clamp-title"))) {
        //         appMain.drawMouseEnteredPopoutBubble({
        //             DomObject: $self,
        //             Content: $self.data("clamp-title")
        //         });
        //     }
        // });
    };

    var _setGroupTitleInUi = function() {
        // var $teamLabel = $(".js-appMainNavigation-team-link-ddl");
        // $teamLabel.trunk8({
        //     // width: "70",
        //     lines: 1,
        //     onTruncate: function() {
        //         $teamLabel.data("is-truncated", true);
        //     }
        // });

        // $teamLabel.unbind("mouseenter").on("mouseenter", function(e) {
        //     var $self = $(this);
        //     if ($self.data("is-truncated") && ($self.text() !== $self.data("clamp-title"))) {
        //         return appMain.drawMouseEnteredPopoutBubble({
        //             DomObject: $self,
        //             Content: $self.data("clamp-title")
        //         });
        //     }
        // });
    };

    var setModalElementsAsFullHeight = function(element) {
        if ($.featherlight.current()) { //Only if modal active
            var $modal;
            var $modalInnerHtml;
            if (element) {
                $modal = $(".js-appModal-wrapper[data-modal-id=" + element.id + "]").find(".featherlight-content");
                $modalInnerHtml = $(".js-appModal-wrapper[data-modal-id=" + element.id + "]").find(".featherlight-inner");
            } else {
                $modal = $(".js-appModal-wrapper").find(".featherlight-content");
                $modalInnerHtml = $(".js-appModal-wrapper").find(".featherlight-inner");
            }

            var modalHeight = parseInt($modal.height());
            //var $modalHeader = $modal.find(".js-appModal-header");
            var $modalBody = $modal.find(".js-appModal-body");
            //var $modalFooter = $modal.find(".js-appModal-footer");
            var isFullHeightModal = ($modalInnerHtml.data("full-height-modal") === true) ? true : false;

            //Find deductable elements
            var $globalDeducatables = $modal.find("[data-full-height-deduct-global]");
            var $blockDeducatables = $modal.find("[data-full-height-deduct]");

            //FullHeight modal Body. Should only appply one per modal
            var $fullHeightModalBodyElement = $modal.find(".js-fullHeight-modalBodyElement"); //Element within body thay may need to account for siblings height

            var deductableModalHeight = 0;

            $globalDeducatables.each(function() {
                deductableModalHeight += parseInt($(this).outerHeight(true));
            });

            $blockDeducatables.each(function() {
                deductableModalHeight += parseInt($(this).outerHeight(true));
            });

            if (isFullHeightModal) {
                var windowHeight = parseInt($(window).outerHeight(true));
                $modal.css({ "height": windowHeight - 90, "min-height": windowHeight - 90 });
            }

            $modalBody.css({ "height": modalHeight - deductableModalHeight + "px", "max-height": modalHeight - deductableModalHeight + "px", "overflow-y": "hidden" });

            if ($fullHeightModalBodyElement) $fullHeightModalBodyElement.css({ "height": modalHeight - deductableModalHeight + "px", "max-height": modalHeight - deductableModalHeight + "px", "overflow-y": "auto" });

            //Set any textarea to grow
            var textarea = $modal.find('textarea');
            //autosize.update(textarea);
        }
    };

    var cleanUpUiElementsOnRouteChange = function() { //Generic UI cleanups called via router on every route change
        NProgress.done();
        NProgress.remove();
		appMain.ajaxLoadingHelperComplete();
        appMain.stopApiRequests();
        ajaxLoadUnMask();
        //Waypoint.destroyAll(); //Clear any existing waypoints
        $.featherlight.close();

        //Remove any Keen-ui Vue elements
        $(".drop.drop-element, .tooltip, [data-tether-id]").remove();

        //Destroy / cleanup any Module Vue instances (as we do not use Vue native router) :(
        //destroyModuleRootVueInstance();

        Vault.deleteSessionItem("ActiveGroupMemberAreaId"); //Remove group context. Will be reset on new view via permissions (isMemberAreaRouteValidForUser) check if applicable
    };

    var checkIfElementHasDataKeySet = function(element, key) { //http://stackoverflow.com/questions/12161132/is-there-a-way-that-i-can-check-if-a-data-attribute-exists
        return (typeof element.data(key) != 'undefined'); //If undefined false, if not true
    };

    var isNetworkValidForUser = function(networkId, justCheckNetworkValidity) { //Is user authenticated to navigate to the network requested? *****DEPRECATED
        networkId = parseInt(networkId);
        var isItSupportUser = _isUserItSupport();
        if (!Vault.getItem("GetRequestingUserSummary")) return false;
        var userSummary = Vault.getItem("GetRequestingUserSummary");

        if (justCheckNetworkValidity) { //Just check, don't set up the UI
            for (var i in userSummary.Data.MemberAreasAndPermissions) {
                if ((userSummary.Data.MemberAreasAndPermissions[i].NetworkId === networkId && userSummary.Data.MemberAreasAndPermissions[i].IsNetwork) || isItSupportUser) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    };

    var isNetworkRouteValidForUser = function(networkId, permission) { //Is user authenticated to navigate to the network requested? Called before all network based routes via app router
        //Handles single permission string - TODO: handle array
        return new Promise(function(resolve, reject) {
            networkId = parseInt(networkId);
            var isItSupportUser = _isUserItSupport();
            if (!Vault.getItem("GetRequestingUserSummary")) return reject(false);
            var userSummary = Vault.getItem("GetRequestingUserSummary");

            if (Vault.getSessionItem("ActiveNetworkId") === networkId && !isItSupportUser) { //No need to continue if same network already set. If session tampered, API will return 403, not fussed if UI breaks as a result.
                //Continue to check cache though if isItSupportUser
                //Vault.setSessionItem("ActiveNetworkId", networkId); //Always set the network session to trigger nav UI updates etc. which will change context menus etc. for networks
                return resolve(true);
            }

            //Network has changed
            if (Vault.getSessionItem("ActiveNetworkId") !== networkId) PubSub.publish("BadgeCounts.ClearAllBadgeCounts"); //Clear badge counts - only double checking isItSupportUser at this point (see above)

            //Handle IT support users
            if (isItSupportUser) {
                if (Vault.getItem("ItSupportActiveNetworkId") === networkId && Vault.getItem("ItSupportActiveNetworkTeams")) { //Use Cache
                    Vault.setSessionItem("ActiveNetworkId", networkId); //Set the network session
                    resolve(true);
                    return;
                } else {
                    return GetUsersNetworks.callApi(networkId).then(function(response, textStatus, jqXHr) {
                        Vault.setSessionItem("ActiveNetworkId", networkId); //Set the network session
                        Vault.setItem("ItSupportActiveNetworkTeams", response.Data.Groups);
                        Vault.setItem("ItSupportActiveNetworkId", networkId);
                        resolve(true);
                    }).fail(function() {});
                }
            } else {
                for (var i = 0; i < userSummary.Data.MemberAreasAndPermissions.length; i++) {
                    if (userSummary.Data.MemberAreasAndPermissions[i].NetworkId === networkId && userSummary.Data.MemberAreasAndPermissions[i].IsNetwork) {
                        if (permission) { //Check network permission against pooled team permissions
                            if (getUserPermissionsForAllUsersMemberAreas(networkId).indexOf(permission) !== -1) {
                                Vault.setSessionItem("ActiveNetworkId", networkId); //Set the network session
                                resolve(true);
                                return;
                            } else { //Permission check failed
                                reject(false);
                                return; //return stops execution and exits the function. return always exits its function immediately, with no further execution if it's inside a loop.
                            }
                        } else { //No permission to check
                            Vault.setSessionItem("ActiveNetworkId", networkId); //Set the network session
                            resolve(true);
                            return;
                        }
                    }
                }
            }
            return reject(false);
        }); //Promise end
    };

    var isMemberAreaValidForUser = function(memberAreaId, permission, justCheckMemberAreaValidity) { //Is user authenticated to navigate to the member area requested, with permissions option / array
        if (!Vault.getItem("GetRequestingUserSummary")) return false;

        var userSummary = Vault.getItem("GetRequestingUserSummary");
        var isItSupportUser = _isUserItSupport();

        if (justCheckMemberAreaValidity) { //Only check validity bool,see if user is a member and has permissions obj
            for (var i in userSummary.Data.MemberAreasAndPermissions) {
                if ((userSummary.Data.MemberAreasAndPermissions[i].MemberAreaId === memberAreaId && !userSummary.Data.MemberAreasAndPermissions[i].IsNetwork) || isItSupportUser) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    };

    var isMemberAreaRouteValidForUser = function(memberAreaId, permission, networkId = Vault.getSessionItem('ActiveNetworkId')) { //Used by router
        return new Promise(function(resolve, reject) {
            if (!Vault.getItem("GetRequestingUserSummary") || !memberAreaId) return reject(false);

            const isItSupportUser = _isUserItSupport();
            const memberAreaPermissions = appMain.getUserPermissionsForMemberArea(memberAreaId) || [];

            const isValid = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions.map(x => {

                if (x.MemberAreaId !== memberAreaId) return false;
                if ((isItSupportUser && networkId === x.NetworkId) || (!permission && x.MemberAreaId === memberAreaId && !x.IsNetwork && networkId === x.NetworkId)) return x;

                if (permission && !Array.isArray(permission)) {

                    if (memberAreaPermissions.includes(permission) && (x.MemberAreaId === memberAreaId && !x.IsNetwork && networkId === x.NetworkId)) return x;

                    return null;
                }

                if (permission && Array.isArray(permission)) {
                    if (arrayContainsArray(memberAreaPermissions, permission) && x.MemberAreaId === memberAreaId && !x.IsNetwork && networkId === x.NetworkId) return x;
                    return null;
                }
                return null;


            }).filter(Boolean).length > 0;

            if (isValid) {
                if (Vault.getSessionItem("ActiveGroupMemberAreaId") !== memberAreaId) Vault.setSessionItem("ActiveGroupMemberAreaId", memberAreaId);
                return resolve(isValid)
            }
            return reject(isValid);


            /*Helper if array passed*/
            function arrayContainsArray(superset, subset) {
                if (0 === subset.length) {
                    return false;
                }
                return subset.every(function(value) {
                    return (superset.indexOf(value) >= 0);
                });
            }


            // for (var i = 0; i < userSummary.Data.MemberAreasAndPermissions.length; i++) {
            //     if (isItSupportUser || userSummary.Data.MemberAreasAndPermissions[i].MemberAreaId === memberAreaId && !userSummary.Data.MemberAreasAndPermissions[i].IsNetwork) {
            //         //Area matched, is a permission check required?
            //         if (permission) {
            //             if (isItSupportUser || checkUserPermissionForMemberArea(memberAreaId, permission)) {
            //                 Vault.setSessionItem("ActiveGroupMemberAreaId", memberAreaId);
            //                 resolve(true);
            //                 return;
            //             } else {
            //                 reject(false); //Permission failed
            //                 return;
            //             }
            //         } else { //No permission check required
            //             Vault.setSessionItem("ActiveGroupMemberAreaId", memberAreaId); //Set the member area session
            //             resolve(true);
            //             return;
            //         }
            //     }
            // }

            // reject(false);
            // return;
        });
    };

    var getRequestToJoinTeamModal = function(obj, lastLocation) { //When hitting a URL a user is not a team member of, modal is thrown
        let text = (obj.MembershipPolicy !== "Private") ? "Join " + obj.Name + " now?" : "Request membership of " + obj.Name + "?";
        let title = "You are not a member of " + obj.Name;

        var content = $('<div />', {
                'text': text,
                'class': 'pure-u u-modalPrompt__wrapper js-request-team-membership-prompt-wrapper'
            }),
            ok = $('<button />', {
                'text': (obj.MembershipPolicy !== "Private") ? "Join " + obj.Name + "" : "Request membership of " + obj.Name + "",
                'class': "btn u-pull-left js-prompt-confirm"
            }),
            cancel = $('<button/>', {
                'text': 'Cancel',
                'class': "btn--plain--block-pad u-pull-right js-prompt-cancel"
            });

        var callBack = function(event, api) {
            $(".js-prompt-confirm").unbind().on("click", function(e) {
                var $self = $(this);
                $self.ajaxActiveButtonUi({ ActionState: "start", DisableNoInputs: true });

                RequestMembership.callApi(obj.MemberAreaID).then((data, textStatus, jQXHr) => { //Rebuild permissions
                    let responseCode = data.Messages[0].Code;

                    appMain.getRequestingUserSummary().then(response => {
                        $self.ajaxActiveButtonUi({ ActionState: "stop", DisableNoInputs: true });
                        var text = "";
                        var $confirmBtn = $(".js-prompt-confirm");
                        var $cancelBtn = $(".js-prompt-cancel");
                        $confirmBtn.unbind();
                        $cancelBtn.remove();

                        //1 = The membership cannot be requested as you are already a member.
                        if (responseCode === 1) return appRouter.careflowAppRouter.setLocation('/#/' + Vault.getSessionItem("ActiveNetworkId") + '/Teams/' + obj.MemberAreaID + '/TeamUpdates');

                        if (responseCode === 2) {
                            text = "You have successfully joined " + obj.Name;
                            $confirmBtn.text("Go to " + obj.Name + "").on("click", function() {
                                api.destroy(e);
                                var routeLocation = appRouter.careflowAppRouter.getLocation(); //The current URL in browser, behind modal.

                                //If route is in URL bar, force a reload
                                if (routeLocation === '/#/' + Vault.getSessionItem("ActiveNetworkId") + '/Teams/' + obj.MemberAreaID + '/TeamUpdates') {
                                    return appRouter.careflowAppRouter.refresh();
                                } else {
                                    return appRouter.careflowAppRouter.setLocation('/#/' + Vault.getSessionItem("ActiveNetworkId") + '/Teams/' + obj.MemberAreaID + '/TeamUpdates');
                                }
                            });
                        }
                        if (responseCode === 3) {
                            text = "Membership has been requested for " + obj.Name + ". The team admins have been notified of your request.";
                            $confirmBtn.text("Close").on("click", function() {
                                api.destroy(e);

                                //If the user has deep lined here, bounce to Home page as nothing to see
                                if (lastLocation) {
                                    var routeLocation = appRouter.careflowAppRouter.getLocation();
                                    if (routeLocation === lastLocation) { //User has deep linked here (no history) or hard refreshed, lastLocation is only one set
                                        return window.location.href = "/";
                                    } else {
                                        //Bounce back to last location, this view is not valid for the user
                                        return appRouter.careflowAppRouter.setLocation(lastLocation);
                                    }
                                }
                                return window.location.href = "/";
                            });
                        }
                        if (responseCode === 4) {
                            text = "You have previously requested membership of " + obj.Name + ". Please contact your administrator for more information.";
                            $confirmBtn.text("Close").on("click", function() {
                                api.destroy(e);

                                //If the user has deep lined here, bounce to Home page as nothing to see
                                if (lastLocation) {
                                    var routeLocation = appRouter.careflowAppRouter.getLocation();
                                    if (routeLocation === lastLocation) { //User has deep linked here (no history) or hard refreshed, lastLocation is only one set
                                        return window.location.href = "/";
                                    } else {
                                        //Bounce back to last location, this view is not valid for the user
                                        return appRouter.careflowAppRouter.setLocation(lastLocation);
                                    }
                                }
                                return window.location.href = "/";
                            });
                        }

                        $(".js-request-team-membership-prompt-wrapper").text(text);
                    }).fail((error) => {
                        //Bounce back to last location on error
                        if (lastLocation) return appRouter.careflowAppRouter.setLocation(lastLocation);
                        return window.location.href = "/";
                    });
                });
            });

            $(".js-prompt-cancel").on("click", function(e) {
                api.destroy(e);

                //If the user has deep lined here, bounce to Home page as nothing to see
                if (lastLocation) {
                    var routeLocation = appRouter.careflowAppRouter.getLocation();

                    if (routeLocation === lastLocation) { //User has deep linked here (no history) or hard refreshed
                        return window.location.href = "/";
                    } else {
                        //Bounce back to last location, this view is not valid for the user
                        return appRouter.careflowAppRouter.setLocation(lastLocation);
                    }
                }

                return window.location.href = "/";
            });
        };
        drawPromptConfirmDialog(content.add(ok).add(cancel), title, callBack);
    };

    var setPageLayout = function(layout, url, skipEmptyAppBody) {
        var targetUrl = url; //The URL view being built

        return new Promise((resolve, reject) => {
            var routeLocation = appRouter.careflowAppRouter.getLocation().toLowerCase().split("/")[2] + " " + appRouter.careflowAppRouter.getLocation().toLowerCase().split("/")[3]; //Get slug
            var appHeader = $(".js-appHeaderNavigation--template");
            var appMainNavigation = $(".js-appMainNavigation--template");
            layout = (layout) ? layout : "default-view";
            routeLocation = routeLocation.replace(/undefined/i, '');

            if (!skipEmptyAppBody) $(".js-app-body").empty(); //Empty prior to view loading (e.g. Group Home sometimes we don't want to empty the DOM to retain navigation elemement)

            if (layout === "blank-1") { //Blue bar across header and blank
                $("body").removeClass().addClass("view--blank-1 " + routeLocation + "");
                if (!skipEmptyAppBody) $(".js-app-body").empty();
                appHeader.add(appMainNavigation).hide(0, function() {
                    appHeader.empty();
                    //appMainNavigation.empty();
                    setElementsAsFullHeight();
                    return resolve();
                });
            } else if (layout === "blank-hero") { //Typically signIn page, blank with bg of #EEEEEE;
                $("body").removeClass().addClass("layout-master-page--blank " + routeLocation + "");
                if (!skipEmptyAppBody) $(".js-app-body").empty();
                appHeader.add(appMainNavigation).hide(0, function() {
                    appHeader.empty();
                    //appMainNavigation.empty();
                    setElementsAsFullHeight();
                    return resolve();
                });
            } else { //default-view"
                $("body").removeClass().addClass(routeLocation);
                if (!skipEmptyAppBody) $(".js-app-body").empty();
                appHeader.add(appMainNavigation).show(0, function() {
                    return resolve();
                });
                setElementsAsFullHeight();
            }
        });

        //appHeader.add(appMainNavigation).show(0, function() {
        //    appMain.setElementsAsFullHeight();
        //    $(".js-app-body").empty();
        //    $("body").removeClass();
        //    result.resolve();
        //});
    };

    var getRefreralStatusUpdatedText = function(referralStatusId) {
        switch (referralStatusId) {
            case "1":
                return "Pending";

            case "2":
                return "Accepted";

            case "3":
                return "Declined";

            case "4":
                return "Cancelled";
        }
    };
    var formatTemplateDates = function() { //Requires a UTC date stamp string
        $(".js-dateFormat").each(function() {
            var $self = $(this);
            if ((!$self.attr("data-dateparsed"))) {
                var utcDate = moment.utc($self.text().trim()); //Grab the UTC date as returned by Careflow webservice
                var localDate = moment(utcDate).local(); //Adjust to local timezone (GMT / BST etc.)

                $self.text(moment(localDate).format("DD-MMM-YYYY, HH:mm"));
                $self.attr("data-dateparsed", true);
            }
        });
    };

    var formatDate = function(date) {
        return moment(date).local().format('DD-MMM-YYYY, HH:mm');
    };

    var getFormattedDate = function(date) {;
        return moment(date).format("DD-MMM-YYYY, HH:mm");
    };

    var getUtcDate = function() {
        return moment()["_d"];
    }

    var getAccessGroupExternalIdentifier = function(identifier, isNetwork) { //Get and return the external ID (AccessGroupExternalIdentifier) of an area / network from member area ID. networkId arc can be NetworkId or MemberAreaID.
        if (_isUserItSupport() && Vault.getItem("ItSupportNetworks") && isNetwork) {
            return _.filter(Vault.getItem("ItSupportNetworks"), function(network) {
                if (network.NetworkId === identifier) return network;
            })[0]["Identifier"];
        }

        if (Vault.getItem("GetRequestingUserSummary")) {
            var getRequestingUserSummary = Vault.getItem("GetRequestingUserSummary");
            var dataCheck;

            if (isNetwork) {
                dataCheck = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
                    "NetworkId": identifier,
                    "IsNetwork": true
                });
            } else {
                dataCheck = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
                    "MemberAreaId": identifier,
                    "IsNetwork": false
                });
            }
            if (dataCheck.length === 1) {
                return dataCheck[0].AccessGroupExternalIdentifier;
            }
            return false;
        } else {
            return false;
        }
    };

    var getAccessGroupId = function(memberAreaId, isNetwork) { //Parse AccessGroupId from a memberAreaId

        if (!Vault.getItem("GetRequestingUserSummary") || !memberAreaId) return false;

        let data = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions;

        return data.map(x => {
            if (x.MemberAreaId === parseInt(memberAreaId)) return x.AccessGroupID;
            return null;
        }).filter(Boolean)[0] || null;

        // var accessGroupId;
        // _.filter(Vault.getItem("GetRequestingUserSummary")["Data"]["MemberAreasAndPermissions"], function(item) {
        //     if (isNetwork) {
        //         if (item.NetworkId === parseInt(memberAreaId, 10)) {
        //             return accessGroupId = item.AccessGroupID;
        //         }
        //     } else {
        //         if (item.MemberAreaId === parseInt(memberAreaId, 10)) {
        //             return accessGroupId = item.AccessGroupID;
        //         }
        //     }
        // });
        // return parseInt(accessGroupId, 10);
    };

    var getMemberAreaIdFromAccessGroupId = function(accessGroupId) { //Can be network or team. Filters by active network ID. Pass an AccessGroupId

        if (!Vault.getItem("ActiveNetworkGroupMemberships") || !accessGroupId) return null;

        let data = Vault.getItem("ActiveNetworkGroupMemberships").Groups;

        return data.map(x => {
            if (x.AccessGroupID === accessGroupId) return x.MemberAreaID;
            return null;
        }).filter(Boolean)[0] || null;

        // let data = _.flatten(_.map(Vault.getItem("ActiveNetworkGroupMemberships")["Groups"], (item) => {
        //     return _.map(item.Groups, function(el, i, array) {
        //         return (el.AccessGroupID === accessGroupId) ? el.MemberAreaID : null;
        //     });
        // }));
        // console.log(data);
        // if (data.length) return data.filter(Boolean)[0];
        // return null;
    };

    var getMemberAreaName = function(memberAreaId, externalIdentifier, accessGroupId, returnNameAndId) { //Get and return the member area name from either memberAreaId, externalIdentifier or accessGroupId.

        if (!memberAreaId && !externalIdentifier && !accessGroupId) return null;

        if (!Vault.getItem("ActiveNetworkGroupMemberships")) return null;

        let data = Vault.getItem("ActiveNetworkGroupMemberships").Groups;

        let networkid = Vault.getItem("ActiveNetworkGroupMemberships").NetworkId || null;

        let mappedData = data.map(x => {
            if (memberAreaId && memberAreaId === x.MemberAreaID) {
                return {
                    MemberAreaName: x.Name,
                    MemberAreaID: x.MemberAreaID,
                    AccessGroupExternalIdentifier: x.GroupExternalIdentifier,
                    NetworkID: networkid
                }
            } else if (externalIdentifier && externalIdentifier === x.GroupExternalIdentifier) {
                return {
                    MemberAreaName: x.Name,
                    MemberAreaID: x.MemberAreaID,
                    AccessGroupExternalIdentifier: x.GroupExternalIdentifier,
                    NetworkID: networkid
                }
            } else if (accessGroupId && accessGroupId === x.AccessGroupID) {
                return {
                    MemberAreaName: x.Name,
                    MemberAreaID: x.MemberAreaID,
                    AccessGroupExternalIdentifier: x.GroupExternalIdentifier,
                    NetworkID: networkid
                }
            }
            return null;
        }).filter(Boolean);

        if (!returnNameAndId && mappedData.length > 0) return mappedData[0].MemberAreaName || null;
        return (mappedData.length) ? mappedData : null;


        // var data = _.flatten(_.map(memberAreas.Data.Groups, function(currentDummyData) {
        //     networkId = currentDummyData.NetworkId;
        //     return _.map(currentDummyData.Groups, function(el, i) {
        //         return { Name: el.Name, MemberAreaID: el.MemberAreaID, NetworkID: networkId, AccessGroupExternalIdentifier: el.GroupExternalIdentifier, AccessGroupID: el.AccessGroupID };
        //     });
        // }));

        // console.log("********", data);

        // _.filter(data, function(team) {
        //     if (memberAreaId) {
        //         if (team.MemberAreaID === memberAreaId) {
        //             memberAreaName = team.Name;
        //             memberAreaId = team.MemberAreaID;
        //             accessGroupExternalIdentifier = team.AccessGroupExternalIdentifier;
        //             networkId = team.NetworkID;
        //         }
        //     } else if (externalIdentifier) {
        //         if (team.AccessGroupExternalIdentifier === externalIdentifier) {
        //             memberAreaName = team.Name;
        //             memberAreaId = team.MemberAreaID;
        //             accessGroupExternalIdentifier = team.AccessGroupExternalIdentifier;
        //             networkId = team.NetworkID;
        //         }
        //     } else if (accessGroupId) {
        //         if (team.AccessGroupID === parseInt(accessGroupId)) {
        //             memberAreaName = team.Name;
        //             memberAreaId = team.MemberAreaID;
        //             accessGroupExternalIdentifier = team.AccessGroupExternalIdentifier;
        //             networkId = team.NetworkID;
        //         }
        //     }

        //     return false;
        // });

        // if (!returnNameAndId) {
        //     return memberAreaName;
        // } else {
        //     return {
        //         "MemberAreaName": memberAreaName,
        //         "MemberAreaID": memberAreaId,
        //         "AccessGroupExternalIdentifier": accessGroupExternalIdentifier,
        //         "NetworkID": networkId
        //     };
        // }
    };

    let getRequestingUserSummary = function(networkId = Vault.getSessionItem("ActiveNetworkId")) { //Get the logged in users member areas and permissions and cache in local storage. Default the networkId param. Used to setup user

        let _checkUserForItSupport = function() {
            return new Promise(function(resolve, reject) {
                if (_isUserItSupport()) {
                    //Get netowrks for IT support
                    axios.get('/config/a/appConfig.json')
                        .then(function(response) {
                            if (_.has(response.data, "permissionList") && _.isArray(response.data.permissionList)) {
                                Vault.setItem("ItSupportPermissions", response.data.permissionList);
                            }
                        }).then(function() {
                            return _getItSupportNetworks().then(function(response) { //Returns promise
                                Vault.setItem("ItSupportNetworks", response);
                            });
                        }).then(function(response) {
                            resolve();
                        }).catch(function(error) {});
                } else {
                    resolve();
                }
            });
        };
        let _getItSupportNetworks = function() {
            return new Promise(function(resolve, reject) {
                //Get all teams for the IT support user
                var skip = 0;
                var take = 100;
                var cacheData = [];

                function getItSupportTeams() {
                    GetItSupportNetworks.callApi(skip, take).then((response, statusText, request) => {
                        response.Data.forEach(function(item) {
                            if (item.NetworkId && typeof item.NetworkId === "number") {
                                cacheData.push(item);
                            }
                        });

                        if (response.Metadata.Pagination.ThereAreMore) {
                            skip += take;
                            getItSupportTeams();
                        } else {
                            resolve(cacheData);
                        }
                    });
                }

                //Get it support teams
                getItSupportTeams(); //Init
            });
        };
        let _getRequestingUserSummary = function() { //Critical method
            //Lock UI globally on method call to stop user moving away before all data set (until websockets, we rebuild user data on this view) -- https://doccom.atlassian.net/browse/WEB-854
            //document.body.addEventListener("click", blockAnchorLinks, true);

            VueEventBus.$emit('GetRequestingUserSummary.IsUpdating', true)

            return new Promise(function(resolve, reject) {
                GetRequestingUserSummary.callApi(true).then((data, textStatus, jqXHr) => { //Get users active teams and permissions
                    if (!/^2/.test(jqXHr.status)) {
                        throw new Error("GetRequestingUserSummary not returned"); // rejects the promise
                    }

                    if (networkId && !data.Data.MemberAreasAndPermissions.filter(x => x.IsNetwork && x.NetworkId === parseInt(networkId)).length) {
                        //No longer in network
                        networkId = null
                    }

                    //Set active network Id
                    if (!networkId && !Vault.getSessionItem("ActiveNetworkId")) {
                        if (data.Data.hasOwnProperty("PreferredNetwork") && data.Data.PreferredNetwork) {
                            networkId = data.Data.PreferredNetwork.NetworkId;
                        } else if (data.Data.MemberAreasAndPermissions.length) {
                            networkId = data.Data.MemberAreasAndPermissions[0].NetworkId;
                        } else {
                            networkId = null;
                        }
                    }

                    if (!networkId) return reject("User not authenticated"); // rejects the promise

                    //Set the User status in Vault endpoint - signed in or out
                    Vault.setItem("GetRequestingUserSummary", data);
                    Vault.setItem("LoggedInUserName", data.Data.FullName.FirstName + " " + data.Data.FullName.LastName);
                    Vault.setItem("LoggedInUserFirstName", data.Data.FullName.FirstName);
                    Vault.setItem("LoggedInUserLastName", data.Data.FullName.LastName);
                    Vault.setItem("LoggedInUserAvatar", data.Data.ProfileImageUrl);
                    Vault.setItem("OnDutyStatus", data.Data.OnDuty);
                    Vault.setItem("LoggedInUserExternalId", data.Data.ExternalUserID);
                    Vault.setItem("ApplicationUserID", data.Data.FullName.ApplicationUserID);


                    if (!data.Data.MemberAreasAndPermissions.length) { //User has no networks on signin
                        Vault.setItem("NoActiveUserNetworks", true);
                        return resolve();
                    }

                    //Set a 'UserPermissionsForAllUsersMemberAreas' cache of all user permissions for the network and teams
                    if (data.Data.MemberAreasAndPermissions.length) {

                        let userSummary = data.Data.MemberAreasAndPermissions.map((x) => {
                            if (x.NetworkId === networkId) return x.Permissions;
                            return null;
                        }).filter(Boolean);

                        let storageObj = {
                            NetworkId: networkId || null,
                            Permissions: [...new Set([].concat.apply([], userSummary))]
                        }
                        Vault.setItem("UserPermissionsForAllUsersMemberAreas", storageObj); //https://davidwalsh.name/array-unique
                    }


                    _checkUserForItSupport().then(response => {
                        return Promise.all([_getRequestingUsersTeams(networkId), _getNetworkTeamsAllowingTasks(networkId), _getAllPopulationsForUser(networkId)]).then(responses => {

                            return resolve();
                        }).catch((error) => { //All es6 promises

                            reject(error);
                        });
                    }).catch((error) => { //All es6 promises

                        reject(error);
                    });

                }).fail((error) => {

                    reject(error);
                });
            });
        };
        let _getMemberAreasUserCanStartAConversationIn = function() {
            //Get the logged in users member areas and permissions and cache in local storage
            return GetMemberAreasUserCanStartAConversationIn.callApi().then((data, textStatus, jQXHr) => {
                if (data.Data.ConversationMemberAreas.length) {
                    Vault.setItem("GetMemberAreasAUserCanStartAConversationIn", data);
                    return data;
                }
            }).fail(() => {});
        };

        let _getAllPopulationsForUser = function(networkId) {
            if (!networkId) return Promise.reject();
            //Load all the users population data
            return new Promise((resolve, reject) => {
                GetAllPopulationsForUser.callApi(networkId).then((response, textStatus, jqXHR) => {
                    if (/^2/.test(jqXHR.status)) {
                        Vault.setItem("GetAllPopulationsForUser", response);
                        resolve(response);
                    } else {
                        reject("No population data set");
                    }
                }).fail(() => {
                    reject("No population data set");
                });

            });
        };

        let _getRequestingUsersTeams = function(networkId) { //Get all teams, not just teams user is a member of (filter by active network only)
            return new Promise(function(resolve, reject) {

                if (!networkId) {
                    console.error("networkId required");
                    return reject();
                }

                let networkTeams = {
                    Network: appMain.getNetworkName(networkId),
                    NetworkId: networkId,
                    CountOfTeams: null,
                    Groups: [],
                    TeamsUserIsMemeberOf: []
                };

                GetUsersNetworks.callApi(networkId).then((response, textStatus, jqXHr) => {

                    if (/^2/.test(jqXHr.status)) {
                        networkTeams.Groups = response.Data.Groups;
                        networkTeams.CountOfTeams = response.Data.TotalNumberOfGroups;
                        networkTeams.TeamsUserIsMemeberOf = response.Data.Groups.filter(x => x.MembershipStatus === 'Member');
                    }

                    Vault.setItem("ActiveNetworkGroupMemberships", networkTeams); //Set Local Storage
                    resolve(networkTeams);
                }).fail(function() {
                    reject();
                });
            });
        };

        let _getNetworkTeamsAllowingTasks = function(networkId) {
            //Tasks Lookup. In the absence of a 'Team' level permission, per iOs, we call TaskMeta Data to see if the Team should have task features enabled. API returns GroupsAllowingTasks property. https://doccom.atlassian.net/browse/WEB-865

            let networkPermissions = getUserPermissionsForAllUsersMemberAreas(networkId);

            return new Promise((resolve, reject) => {
                if (!networkPermissions.includes('ViewUserTasks')) {
                    Vault.setItem("NetworkTeamsAllowingTasks", []);
                    return resolve([]);
                }; //Non task network, will return 403 if API hit

                TasksMetaData.callApi(networkId).then((response, textStatus, jqXHr) => {
                    //if (response.Data.GroupsAllowingTasks.length) Vault.setItem("NetworkTeamsAllowingTasks", response.Data.GroupsAllowingTasks); //Only set if returned
                    Vault.setItem("NetworkTeamsAllowingTasks", response.Data.GroupsAllowingTasks);
                    return resolve();
                }).fail((error) => {
                    Vault.setItem("NetworkTeamsAllowingTasks", []);
                    return resolve([]); //Non critical, if errors (403 etc can return empty array) and resolve
                });
            });
        };

        //Make the calls
        return Promise.all([_getRequestingUserSummary(), _getMemberAreasUserCanStartAConversationIn()]).then(responses => {
            let a = responses[0];
            let b = responses[1];
            let c = responses[1];

            VueEventBus.$emit('GetRequestingUserSummary.IsUpdating', false)
            VueEventBus.$emit('GetRequestingUserSummary.IsUpdated', true)
            return Promise.resolve();

        }).catch(error => {
            VueEventBus.$emit('GetRequestingUserSummary.IsUpdating', false)
			VueEventBus.$emit('GetRequestingUserSummary.IsUpdated', false)
			            //Send Raygun error
						rg4js('send', {
							error: 'GetRequestingUserSummary call failed for user',
							customData: [{ user: Vault.getItem('LoggedInUserExternalId'), error:error }]
						  });

            //Sign out - critical issue in building GetRequestingUserSummary
            appMain.triggerAppNotification({
                Text: "There was a problem processing your request, please sign in again",
                Heading: "Error processing request",
                Icon: "error"
            });
            if (appRouter.careflowAppRouter.getLocation() !== '/#/SignIn') setTimeout(() => { appMain.userLogOut(); }, 1000);
            return Promise.reject();
        });
    };

    var _isUserItSupport = function() {
        if (!Vault.getItem("GetRequestingUserSummary")) return false;
        return (_.has(Vault.getItem("GetRequestingUserSummary")["Data"], "IsItSupport") && Vault.getItem("GetRequestingUserSummary")["Data"]["IsItSupport"]) ? true : false;
    };

    var getItSupportUserPermissions = function() {
        if (_isUserItSupport()) return Vault.getItem("ItSupportPermissions") || []; //Array
        return [];
    };

    var getMemberAreaIdFromNamePassedAsString = function(memberAreaName) { //Until GetPatientFeedItems returns a memberAreaId as well as MemberAreaName, this is the best I can do to set the memberarea context.

        if (!Vault.getItem("ActiveNetworkGroupMemberships") || !memberAreaName) return null;

		let data = Vault.getItem("ActiveNetworkGroupMemberships").Groups;

		const item = data.find(x => x.Name.toLowerCase() === memberAreaName.toLowerCase());

		if (item) return item.MemberAreaID;
		return false;
    };

    var getNetworkName = function(networkId, memberAreaId) { //get network name via NetworkId or MemberAreaId

        if (!networkId && !memberAreaId) return false;

        if (_isUserItSupport() && Vault.getItem("ItSupportNetworks") && networkId) {
            return _.filter(Vault.getItem("ItSupportNetworks"), function(network) {
                if (network.NetworkId === networkId) return network;
            })[0]["Name"] || null;
        }

        if (!Vault.getItem("GetRequestingUserSummary")) return false;

        let data = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions;

        return data.map(x => {
            if (networkId && x.NetworkId === networkId) return x.NetworkName;
            if (memberAreaId && x.MemberAreaId === memberAreaId) return x.NetworkName;
            return null;
        }).filter(Boolean)[0];

        // if (Vault.getItem("GetRequestingUserSummary")) {
        //     var getRequestingUserSummary = Vault.getItem("GetRequestingUserSummary");

        //     var dataCheck = false;

        //     if (networkId) {
        //         dataCheck = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
        //             "NetworkId": parseInt(networkId),
        //             "IsNetwork": true
        //         });
        //     } else if (memberAreaId) {
        //         dataCheck = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
        //             "MemberAreaId": parseInt(memberAreaId),
        //             "IsNetwork": true
        //         });
        //     }
        //     if (dataCheck.length === 1) {
        //         return dataCheck[0].NetworkName;
        //     } else {
        //         return false;
        //     }
        // }
    };

    var getNetworkMemberAreaId = function(networkId, externalIdentifier) {

        if ((!networkId && !externalIdentifier) || !Vault.getItem("GetRequestingUserSummary")) return false;

        let data = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions;

        return data.map(x => {
            if (networkId && x.NetworkId === parseInt(networkId)) return x.MemberAreaId;
            if (externalIdentifier && x.AccessGroupExternalIdentifier === externalIdentifier) return x.MemberAreaId;
            return null;
        }).filter(Boolean)[0];

        // if (Vault.getItem("GetRequestingUserSummary")) {
        //     var getRequestingUserSummary = Vault.getItem("GetRequestingUserSummary");
        //     var dataCheck;

        //     if (networkId) {
        //         dataCheck = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
        //             "NetworkId": networkId,
        //             "IsNetwork": true
        //         });
        //     } else if (externalIdentifier) {
        //         dataCheck = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
        //             "AccessGroupExternalIdentifier": externalIdentifier,
        //             "IsNetwork": true
        //         });
        //     }

        //     if (dataCheck.length === 1) {
        //         return dataCheck[0].MemberAreaId;
        //     } else {
        //         return false;
        //     }
        // } else {
        //     return false;
        // }
    };

    var getNetworkExternalNetworkId = function(networkId) {
        if (!Vault.getItem("GetRequestingUserSummary") || !networkId) return false;

        let data = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions;

        let externalIds = data.map(x => {if (x.IsNetwork && x.NetworkId === networkId) return x.AccessGroupExternalIdentifier;
            return null;
        }).filter(Boolean);

		if (!externalIds.length) return null;
        return externalIds[0];

    };
    var getMemberAreaExternalNetworkId = function(memberAreaId) {

        if (!memberAreaId || !Vault.getItem("ActiveNetworkGroupMemberships")) return false;

        let data = Vault.getItem("ActiveNetworkGroupMemberships").Groups;

        let mappedData = data.map(x => {
            if (memberAreaId === x.MemberAreaID) {
                return x.GroupExternalIdentifier
            }
            return null;
        }).filter(Boolean);
        if (!mappedData.length) return null;
        return mappedData[0];
    };

    //var getGroupListsPatientIsOn = function (groupListExternalIdentifiers) {
    //    if (Vault.getItem("GetRequestingUserSummary")) {
    //        var getRequestingUserSummary = Vault.getItem("GetRequestingUserSummary");
    //        var groupsPatientIsOn = {};
    //        groupsPatientIsOn.Data = [];

    //        $.each(groupListExternalIdentifiers, function () {
    //            var id = this.toString();
    //            var dataCheck = {};

    //            dataCheck.Data = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
    //                "AccessGroupExternalIdentifier": id
    //            });

    //            if (dataCheck.Data.length) {
    //                $.each(dataCheck.Data, function (key, val) {
    //                    groupsPatientIsOn.Data.push({
    //                        "MemberAreaName": this.MemberAreaName,
    //                        "MemberAreaId": this.MemberAreaId,
    //                        "Permissions": this.Permissions
    //                    });
    //                });
    //            }
    //        });

    //        return groupsPatientIsOn;
    //    } else {
    //        return false;
    //    }
    //};

    var getGroupListsPatientCanBeAddedTo = function(groupListExternalIdentifiersPatientIsAlreadyOn, patientGroupLists, networkId = Vault.getSessionItem("ActiveNetworkId")) {
        //Return a list of group patient lists the patient can be added to, permission checked for user and excluding lists the patient is already a added to. Use GetRequestingUserSummary, as user must have permissions for the teams. Response is network filtered.

        if (!Vault.getItem("GetRequestingUserSummary")) return [];

        //Filter groups that the patient is already on.
        var teamData = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions;

        let patientListData = teamData.map(x => {
            if (x.IsNetwork || x.NetworkId !== networkId) return null;
            x.PatientIsOnGroupList = groupListExternalIdentifiersPatientIsAlreadyOn.includes(x.AccessGroupExternalIdentifier); //Is in team
            return x;
        }).filter(Boolean);

        return _.sortBy(patientListData, function(item) { return item.MemberAreaName.toLowerCase(); });

    };

    var getUserPermissionsForNetwork = function(networkId = Vault.getSessionItem("ActiveNetworkId")) {
        if (!Vault.getItem("GetRequestingUserSummary")) return false;

        if (_isUserItSupport()) return getItSupportUserPermissions(); //Check if IT support

        let userSummary = Vault.getItem("GetRequestingUserSummary")["Data"]["MemberAreasAndPermissions"].map((item) => {
            if (item.NetworkId === networkId && item.IsNetwork) return item.Permissions;
            return "";
        });

        //return userSummary.filter(Boolean); //Remove falseys
        return [].concat(...userSummary.filter(Boolean)); //http://www.jstips.co/en/javascript/flattening-multidimensional-arrays-in-javascript/
    };

    var getUserPermissionsForMemberArea = function(memberAreaId) { //Get users permissions for an MemberAreaID (not network), return an array
        if (!Vault.getItem("GetRequestingUserSummary")) return false;

        if (_isUserItSupport()) return getItSupportUserPermissions(); //Check if IT support

        let userSummary = Vault.getItem("GetRequestingUserSummary")["Data"]["MemberAreasAndPermissions"].map((item, index, items) => {
            if (item.MemberAreaId === memberAreaId && !item.IsNetwork) return item.Permissions;
            return "";
        });

        //return userSummary.filter(Boolean); //Remove falseys
        return [].concat(...userSummary.filter(Boolean)); //http://www.jstips.co/en/javascript/flattening-multidimensional-arrays-in-javascript/
    };

    var getUserPermissionsForAllUsersMemberAreas = function(networkId = Vault.getSessionItem("ActiveNetworkId")) { //Pool all of the users team/network permissions in a network; useful for the context actions (fab) when viewing a patient in 'network' view

        if (!Vault.getItem("UserPermissionsForAllUsersMemberAreas")) return [];

        if (_isUserItSupport()) return getItSupportUserPermissions(); //Check if IT support

        let data = Vault.getItem("UserPermissionsForAllUsersMemberAreas");

        if (data && data.NetworkId === networkId) return data.Permissions;

        //Fallback here - hit if no 'UserPermissionsForAllUsersMemberAreas' permissions cached yet

        if (!Vault.getItem("GetRequestingUserSummary") || !networkId) return [];

        if (_isUserItSupport()) return getItSupportUserPermissions(); //Check if IT support

        data = Vault.getItem("GetRequestingUserSummary")["Data"]["MemberAreasAndPermissions"]

        let userSummary = data.map((x) => {
            if (networkId && x.NetworkId === networkId) return x.Permissions;
            return null;
        }).filter(Boolean);

        userSummary = [...new Set([].concat.apply([], userSummary))]; //https://davidwalsh.name/array-unique

        return userSummary;

    };

    var getUserPermissionsForAllNetworks = function() { //User permissions across all networks
        if (!Vault.getItem("GetRequestingUserSummary")) return [];

        if (_isUserItSupport()) return getItSupportUserPermissions(); //Check if IT support

        let permissions = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions.map((item, index, array) => {
            if (item.IsNetwork) return item.Permissions || false;
            return false;
        }) || [];

        permissions = [...new Set([].concat.apply([], permissions))]; //https://davidwalsh.name/array-unique

        return permissions.flatten().filter(Boolean);
    };

    var checkUserPermissionForMemberArea = function(memberAreaId, permissionsArrayToCheck) { //Check a single permission or array in an area for a user. Must match case also. Can check network or Team (memberareaId) - TODO: DEPRECATE
        if (!memberAreaId) {
            console.error('No memberArea');
            return false;
        }

        if (Vault.getItem("GetRequestingUserSummary")) { //Set on sign In to app
            //if (_isUserItSupport()) return getItSupportUserPermissions(); //Check if IT support
            if (_isUserItSupport()) return true; //Check if IT support

            var getRequestingUserSummary = Vault.getItem("GetRequestingUserSummary");
            memberAreaId = parseInt(memberAreaId);

            if (_.isArray(permissionsArrayToCheck)) { //Check against multiple permissions
                var arrayLength = permissionsArrayToCheck.length;

                var i = 0; //Set a counter var
                var permissionCheckResult;
                $.each(permissionsArrayToCheck, function(index, val) {
                    if (i++ <= arrayLength) {
                        if (!checkUserPermission(val)) {
                            permissionCheckResult = false;
                            return false; //Break out and return false
                        } else {
                            permissionCheckResult = true; //All passed, no breaks, return true
                        }
                    }
                });

                return permissionCheckResult;
            } else {
                return checkUserPermission(permissionsArrayToCheck);
            }

            function checkUserPermission(permission) {
                var dataCheck = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
                    //Creates an array
                    //get the specified member area objet
                    "MemberAreaId": memberAreaId,
                    "NetworkId": Vault.getSessionItem("ActiveNetworkId") //Check and filter only relative to active network
                });

                if (dataCheck.length) { //If memberarea matched, check the permissions against string supplied
                    return _.contains(dataCheck[0].Permissions, permission); //Returns bool
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    };

    var checkUserPermissionForNetwork = function(networkId, permissionsArrayToCheck) { //Check a single permission or array in an area for a user. Must match case also. Can check network or Team (memberareaId) - TODO: DEPRECATE
        if (!networkId) {
            console.error('No networkId');
            return false;
        }

        if (Vault.getItem("GetRequestingUserSummary")) { //Set on sign In to app
            //if (_isUserItSupport()) return getItSupportUserPermissions(); //Check if IT support
            if (_isUserItSupport()) return true; //Check if IT support

            var getRequestingUserSummary = Vault.getItem("GetRequestingUserSummary");
            networkId = parseInt(networkId, 10);

            if (_.isArray(permissionsArrayToCheck)) { //Check against multiple permissions
                var arrayLength = permissionsArrayToCheck.length;

                var i = 0; //Set a counter var
                var permissionCheckResult;
                $.each(permissionsArrayToCheck, function(index, val) {
                    if (i++ <= arrayLength) {
                        if (!checkUserPermission(val)) {
                            permissionCheckResult = false;
                            return false; //Break out and return false
                        } else {
                            permissionCheckResult = true; //All passed, no breaks, return true
                        }
                    }
                });

                return permissionCheckResult;
            } else {
                return checkUserPermission(permissionsArrayToCheck);
            }

            function checkUserPermission(permission) {
                var dataCheck = _.where(getRequestingUserSummary.Data.MemberAreasAndPermissions, {
                    "IsNetwork": true,
                    "NetworkId": networkId
                });

                if (dataCheck.length) { //If memberarea matched, check the permissions against string supplied
                    var result;
                    result = _.contains(dataCheck[0].Permissions, permission); //Returns true
                    return result; //return boolean
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    };

    var getTeamsUserIsMemberOf = function(networkId = Vault.getSessionItem("ActiveNetworkId")) { //Return a list of a users teams/groups of which they are a member, and permissions from network, filtered by network

        if (!Vault.getItem("ActiveNetworkGroupMemberships")) return [];

        let data = Vault.getItem("ActiveNetworkGroupMemberships");

        if (networkId !== data.NetworkId) return [];
        return data.TeamsUserIsMemeberOf || [];

        // if (!Vault.getItem("GetRequestingUserSummary")) return [];
        // let data = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions;


        // return data.map(x=>{
        // 	if (!x.IsNetwork && x.NetworkId === networkId && (x.Permissions && x.Permissions.length > 1)) return x; //All users assigned 'UseClinicalWorkspace'
        // 	return null;
        // }).filter(Boolean);

    };

    let getActiveUsersNetworks = () => {

        if (!Vault.getItem("GetRequestingUserSummary")) return [];

        let getRequestingUserSummary = Vault.getItem("GetRequestingUserSummary").Data.MemberAreasAndPermissions;

        return getRequestingUserSummary.filter(x => x.IsNetwork);

    }

    var getNetworksUserIsMemberOf = function() { //TODO - DEPRECATE for getActiveUsersNetworks
        if (!Vault.getItem("GetRequestingUserSummary")) return [];

        let getRequestingUserSummary = Vault.getItem("GetRequestingUserSummary");

        let filteredData = getRequestingUserSummary.Data.MemberAreasAndPermissions.map(x => {

            if (x.IsNetwork) return {
                NetworkName: x.NetworkName,
                Name: x.NetworkName,
                NetworkId: x.NetworkId,
                NetworkMemberAreaId: x.MemberAreaId,
                NetworkExternalIdentifier: x.AccessGroupExternalIdentifier,
                Permissions: x.Permissions, //NetworkPermissions
                Teams: getTeamsUserIsMemberOf(x.NetworkId) //Get each Networks Teams
            }
            return null;

        }).filter(Boolean) || [];

        let returnObj = {
            Networks: filteredData,
            CountOfNetworksUserIsIn: filteredData.length
        };
        return returnObj;
    };

    var ajaxLoadingHelper = function(obj) { //Provides UI for a div / layout container or block subject to an Ajax update / load
        var options = {
            Padding: "1em",
            DisableInputsContainer: false,
            DomElement: false, //the continer / block to target (JQ object)
            DisableNoInputs: false,
            LoaderMessage: ""
        };

        if (obj && typeof obj === "object") $.extend(options, obj);
        var text;

        //  text = '<div class="u-ajax-loader u-clearfix"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw btn--locked-img" style="padding:' + settings.Padding + ';float:left"></i></span> <span style="padding: 1.5em 0 0 0;display: inline-block;" class="">' + settings.LoadingMessage + '</span></div>';
        if (options.LoaderMessage) {
            text = '<div style="padding:' + options.Padding + '" class="u-ajax-loader u-clearfix"><i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw u-ajax-loader__icon"></i> <span style="display: inline-block;" class="u-ajax-loader__label">' + options.LoaderMessage + '</span></div>';
        } else {
            text = '<div style="padding:' + options.Padding + '" class="u-ajax-loader u-clearfix"><span style="display: inline-block;" class="u-ajax-loader__label">' + options.LoaderMessage + '</span></div>';
        }

        if (options.DomElement) {
            options.DomElement.html(text);

            if (options.DisableInputsContainer && !options.DisableNoInputs) { //Disable selected
                options.DisableInputsContainer.find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
            } else if (!options.DisableInputsContainer && !options.DisableNoInputs) { //Disable all
                if ($.featherlight.current()) {
                    $(".js-appModal-wrapper").find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
                } else {
                    $(".js-app-body").find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
                }
            }
        } else if (!options.DomElement && !options.DisableNoInputs) { //No selector passed, so globally disable all inputs as fallback
            $(".js-app-body").find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
        }
    };

    var ajaxLoadingHelperComplete = function(obj) { //Provides UI for a div container / block subject to an Ajax update
        var options = {
            DisableInputsContainer: false, //the continer / block to target
            DisableNoInputs: false
        };

        if (obj && typeof obj === "object") $.extend(options, obj);

        if (options.DisableInputsContainer && !options.DisableNoInputs) {
            options.DisableInputsContainer.find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", false).removeClass("disabled");
            ///Revert any locked buttons
            options.DisableInputsContainer.find(".btn--locked-parent").each(function() {
                var $self = $(this);
                $($self).html($($self).attr('data-btn-text')).removeClass("btn--locked-parent");
            });
        } else if (!options.DisableInputsContainer && !options.DisableNoInputs) {
            $(".js-app-body").find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", false).removeClass("disabled");
            //Revert any locked buttons
            $(".btn--locked-parent").each(function() {
                var $self = $(this);
                $self.html($self.attr('data-btn-text')).removeClass("btn--locked-parent").prop("disabled", false);
            });
        }

        //If a modal is open
        if ($.featherlight.current() && !options.DisableNoInputs) {
            $(".js-appModal-wrapper").find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input', '.tt-hint').prop("disabled", false).removeClass("disabled");
            //Revert any locked buttons
            $(".btn--locked-parent").each(function() {
                var $self = $(this);
                $self.html($self.attr('data-btn-text')).removeClass("btn--locked-parent").prop("disabled", false);
            });
        }
    };

    var ajaxLoadMask = function(obj) {
        //Default Options
        obj = obj || {};

        var label = '<div style="class="u-ajax-loader u-clearfix"><i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw u-ajax-loader__icon"></i> <span style="display: inline-block;" class="u-ajax-loader__label">Please wait...</span></div>';

        if (_.has(obj, "ContainerDomObject")) {
            obj.ContainerDomObject.mask();
            obj.ContainerDomObject.find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled"); //Also disable inputs
        } else {
            $(document.body).mask(label);
            $(document.body).find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled"); //Also disable inputs for visual effect
        }

        $(document.body).on("click", "a", blockAnchorLinks); //Pause all links
        $ //(".js-feedBody-createContent-context-btn").prop("disabled", true).qtip("hide"); //Lock global content '+'
    };

    var ajaxLoadUnMask = function(obj) {
        //Default Options
        obj = obj || {};

        if (_.has(obj, "ContainerDomObject")) {
            obj.ContainerDomObject.unmask();
            obj.ContainerDomObject.find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", false).removeClass("disabled"); //Also disable inputs for visual effect
        } else {
            $(document.body).unmask();
            $(document.body).find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", false).removeClass("disabled"); //Also disable inputs for visual effect
        }

        $(document.body).off("click", "a", blockAnchorLinks); //Pause all links
        //$(".js-feedBody-createContent-context-btn").prop("disabled", false).qtip("hide"); //UnLock global content '+'
    };

    var blockAnchorLinks = function(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    };

    var closeAllqTips = function(excludedTips) { //Close all Qtips that match the selector. Suppy an array for elements not to close
        // if (excludedTips) {
        //     for (var i = 0; i < excludedTips.length; i++) {
        //         if (!$(".qtip:visible").hasClass(excludedTips[i])) {
        //             $(this).qtip("hide");
        //             break;
        //         }
        //     }
        // } else {
        //     $(".qtip:visible").qtip("hide");
        // }
    };

    var cleanAllRenderedQtips = function(domElement) { //Clean all rendered Qtips, useful for cleaning DOM on route change. TODO: REWRITE - DOES NOT WORK
        // if (domElement) { //JQ object
        //     domElement.find($("[data-hasqtip]")).qtip('destroy');
        //     domElement.find($(".qtip")).remove();
        // } else {
        //     $("[data-hasqtip]").qtip('destroy');
        //     $(".qtip").remove();
        // }
    };

    var destroyWaypointOnElement = function(primaryClassName) {
        // // console.log("^^^^^^^^^",primaryClassName)
        // var elToLookup = document.querySelectorAll("." + primaryClassName);

        // if (!elToLookup.length) return console.warn("no waypoint element in dom");

        // var classListToLookup = elToLookup[0].className;
        // var element = document.getElementsByClassName(classListToLookup); //Returns collection
        // var context = Waypoint.Context.findByElement(element);

        // if (context instanceof Waypoint.Context) {
        // }

        // // console.log(element);
        // //  console.log("878789789789798", context);
        // if (typeof context === "object") context.destroy();
        // // console.log("878789789789798", context);
        // return context;
    };

    var setDutyStatusTogglebtn = function(e) {
        // var $self = $(this);
        // var statusToSet = $self.attr("data-dutystatus") === "true" ? true : false;

        // SetDutyStatus.callApi(statusToSet).then(function(data, textStatus, jqXHr) {
        //     if (jqXHr.status === 204) {
        //         if (statusToSet) {
        //             $(".js-dutyStatusToggle-status").text("You are on duty");
        //             $self.attr("data-dutystatus", false).text("Go off duty");
        //             Vault.setItem("OnDutyStatus", false);
        //         } else {
        //             $(".js-dutyStatusToggle-status").text("You are off duty");
        //             $self.attr("data-dutystatus", true).text("Go on duty");
        //             Vault.setItem("OnDutyStatus", true);
        //         }

        //         PubSub.publish("SetDutyStatus.StatusUpdated", {
        //             OnDuty: statusToSet
        //         });
        //     }
        // }).fail();
    };

    var getPresenceIndicatorClass = function(lastOnlineTime) {
        if (!lastOnlineTime) {
            return "presenceStatus--offline";
        } else {
            lastOnlineTime = moment(lastOnlineTime).utc().local();
            var now = moment.utc().local();

            var lastOnlineMinutesAgo = parseInt(now.diff(lastOnlineTime, 'minutes'), 10); //[days, years, months, seconds, ...]

            if (lastOnlineMinutesAgo < 60) {
                return "presenceStatus--online";
            } else if (lastOnlineMinutesAgo < 420) { //7 hours
                return "presenceStatus--offline-less-seven";
            } else {
                return "presenceStatus--offline";
            }
        }
    };

    var expandInputDownOnFocus = function(e) {
        var originalInputHeight;

        $(".js-input-expand--down").focus(function(e) {
            originalInputHeight = $(this).outerHeight();
            $(this).animate({
                height: "5em"
            }, {
                easing: "",
                duration: 200,
                complete: function() { setElementsAsFullHeight(); }
            });
        });

        $(".js-input-expand--down").blur(function(e) {
            $(this).animate({
                height: originalInputHeight
            }, {
                easing: "",
                duration: 200,
                complete: function() { setElementsAsFullHeight(); }
            });
        });
    };

    //var startNewUserToUserMessage = function (messageText, recipientListIds) {
    //    return StartConversation.callApi(messageText, recipientListIds).done(StartConversation.responseHandler);
    //};

    //var replyToUserToUserMessage = function () {
    //    var formElement = $('.js-messageReply-form');
    //    formElement.parsley().validate();

    //    if (!formElement.parsley().isValid()) {
    //        return false;
    //    }

    //    ajaxLoadingHelper("js-post-message--submitBtn");

    //    var conversationTrackableItemId = $(this).attr("data-conversationtrackableitemid");
    //    var messageText = $(".js-post-message-body-txt").val();

    //    var replyToUserToUserMessageResult = AddMessageToConversation.callApi(conversationTrackableItemId, messageText).done(AddMessageToConversation.responseHandler).fail();

    //    replyToUserToUserMessageResult.then(function () {
    //        $(".js-post-message-body-txt").val("");
    //        $(".js-messagesInbox-item[data-activeinboxitem='true']").triggerHandler("click"); //refresh the active conversation
    //        ajaxLoadingHelperComplete();
    //    });
    //};

    var urlifyText = function(text) { //Check for URL in text, parse for display
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(urlRegex, function(url) {
            return '<a href="' + url + '">' + url + '</a>';
        });
    };

    var checkShims = function() { //Collection of shims to check when bootstrapping app
        //https://developer.mozilla.org/en-US/docs/Web/HTTP/Browser_detection_using_the_user_agent
        var result = new $.Deferred();

        if (!Modernizr.borderimage || !Modernizr.placeholder || !Modernizr.flexbox) { //http://blog.greggant.com/posts/2016/11/30/modernizr-quick-detect-for-ie10.html
            GetHandlebarsTemplate.callApi("Content/js/handlebarsTemplates/utils/SupportedBrowsersContent.htm", null, null).then(function(html) {
                getFatalErrorUserNotificationModal(html, "Close");
                result.reject();
            });
        } else {
            result.resolve();
        }

        return result;
    };

    var drawMouseEnteredPopoutBubble = function(obj) {
        // if (obj.DomObject.data("rendered") || obj.DomObject.data("hasqtip")) {
        //     obj.DomObject.qtip("destroy");
        //     obj.DomObject.data({ "rendered": false, "hasqtip": false });
        //     return;
        // }

        // var options = {
        //     Content: obj.Content,
        //     DomObject: obj.DomObject,
        //     QtipViewport: (obj.QtipViewport) ? obj.QtipViewport : $(window),
        //     Classes: obj.Classes,
        //     ShowCallBack: obj.ShowCallBack,
        //     HideCallBack: obj.HideCallBack
        // };
        // var api;

        // var tooltip = options.DomObject.qtip({
        //     content: {
        //         text: options.Content
        //     },
        //     position: {
        //         target: 'mouse', // Track the mouse as the positioning target
        //         adjust: {
        //             x: 5, // Offset it slightly from under the mouse
        //             y: 5,
        //             screen: true,
        //             resize: true,
        //             mouse: false
        //         },
        //         viewport: options.QtipViewport,
        //     },
        //     hide: {
        //         fixed: true,
        //         event: "mouseleave click"
        //     },
        //     overwrite: false, // Don't overwrite tooltips already bound
        //     show: {
        //         event: 'mouseenter',
        //         delay: 20, //.20 milliseconds
        //         ready: true, //Fire immediately
        //         solo: false
        //     },
        //     events: {
        //         hide: function() {
        //             if (typeof options.HideCallBack === "function") options.HideCallBack(api);
        //             obj.DomObject.data({ "rendered": false, "hasqtip": false });
        //             api.destroy();
        //         },
        //         show: function() {
        //             if (typeof options.ShowCallBack === "function") options.ShowCallBack(api);
        //             obj.DomObject.data({ "rendered": true, "hasqtip": true });
        //         }
        //     },
        //     style: {
        //         classes: options.Classes,
        //         tip: true
        //     },
        //     // prerender: true
        // }, options.DomObject);
        // api = tooltip.qtip('api');

        //_.delay(function() {//Catch any misfired events (IE especially!) - wait double the 251ms show delay
        //    if (!obj.DomObject.data("rendered") || !obj.DomObject.data("hasqtip")) {
        //        api.show();
        //    }
        //}, 251);
    };

    var drawClickToOpenPopoutBubble = function(obj) {
        if (obj.DomObject.data("rendered")) {
            obj.DomObject.qtip("destroy");
            obj.DomObject.data("rendered", false);
            return;
        }

        var options = {
            ShowEvent: (obj.ShowEvent) ? obj.ShowEvent : "click",
            Content: obj.Content,
            DomObject: obj.DomObject,
            QtipContainer: (obj.QtipContainer) ? obj.QtipContainer : $(document.body),
            QtipViewport: (obj.QtipViewport) ? obj.QtipViewport : $(window),
            ShowCallBack: obj.ShowCallBack,
            HideCallBack: obj.HideCallBack,
            Classes: obj.Classes,
            CloseEvents: (obj.CloseEvents) ? obj.CloseEvents : "click unfocus",
            HideDistance: null,
            Tip: (_.has(obj, "Tip") && typeof obj.Tip === "boolean") ? obj.Tip : true,
            PositionMy: (obj.PositionMy) ? obj.PositionMy : "top center", // Position my top left...
            PositionAt: (obj.PositionAt) ? obj.PositionAt : "bottom center", // at the bottom right of...
            AdjustX: (obj.AdjustX) ? obj.AdjustX : 0,
            AdjustY: (obj.AdjustY) ? obj.AdjustY : 0,
            PositionTarget: (obj.PositionTarget) ? obj.PositionTarget : false
        };
        var api;

        console.log(options);

        var tooltip = options.DomObject.qtip({
            content: {
                text: options.Content
            },
            position: {
                container: options.QtipContainer,
                viewport: options.QtipViewport,
                my: options.PositionMy,
                at: options.PositionAt,
                adjust: {
                    x: options.AdjustX,
                    y: options.AdjustY, // Minor x/y adjustments
                    scroll: true,
                    screen: true,
                    resize: true
                },
                target: options.PositionTarget // Defaults to target element
            },
            show: {
                event: options.ShowEvent,
                ready: true,
                solo: false
            },
            hide: {
                event: options.CloseEvents,
                distance: options.HideDistance
            },
            events: {
                hide: function() {
                    options.DomObject.data("rendered", false);
                    if (typeof options.HideCallBack === "function") options.HideCallBack(api);
                    api.destroy();
                },
                show: function() {
                    options.DomObject.data("rendered", true);
                    if (typeof options.ShowCallBack === "function") options.ShowCallBack(api);
                }
            },
            style: {
                classes: options.Classes,
                tip: options.Tip
            },
        }, options.DomObject);
        api = tooltip.qtip('api');
    };

    var drawPromptConfirmDialog = function(content, title, callBack) { // Alert/Confirm/Prompt
        $('<div />').qtip({
            content: {
                text: content,
                title: title
            },
            position: {
                my: 'center',
                at: 'center',
                target: $(window)
            },
            show: {
                ready: true,
                modal: {
                    on: true,
                    blur: false,
                    escape: false
                }
            },
            hide: false,
            style: 'dialogue',
            events: {
                render: function(event, api) {
                    if (typeof callBack === "function") callBack(event, api);
                },
                hide: function(event, api) { api.destroy(); }
            }
        });
    };

    var getFatalErrorUserNotificationModal = function(message, buttonText, variant) { //A modal for fatal app errors, this will log user out
        GetHandlebarsTemplate.callApi("Content/js/handlebarsTemplates/utils/FatalErrorUserNotificationModal.htm", null, { Message: message, ButtonText: buttonText }, null).then(GetHandlebarsTemplate.onTemplateInserted).then(function($html) {
            $.featherlight($html, {
                closeOnEsc: false,
                closeIcon: "",
                variant: variant || false,
                /* Class that will be added to change look of the lightbox */
                closeOnClick: false,
                type: "html",
                beforeOpen: function(e) {
                    $.proxy($.featherlight.defaults.beforeOpen, this, e)();
                },
                beforeClose: function(e) {
                    $.proxy($.featherlight.defaults.beforeClose, this, e)();
                },
                afterClose: function(e) {
                    $.proxy($.featherlight.defaults.afterClose, this, e)();
                    appMain.userLogOut(); //By default
                },
                afterContent: function(e) {
                    $.proxy($.featherlight.defaults.afterContent, this, e)();
                    $(".js-user-notification-fatal-close-btn").on("click", function() {
                        PubSub.publish("CareflowApp.CloseAllOpenModalInstances");
                    });
                }
            });
        });
    };

    var toggleActionButton = function() { //Typically used on patient lists for '...' action button TODO:Complete
        //if (Modernizr.touch) {
        //} else {
        //}

        // var $theRow = $(".js-action-element-row");
        // var $buttons = $(".js-action-element-btn");

        // //Actions link visibility on hover
        // $theRow.unbind("mouseenter").on("mouseenter", function(e) {
        //     var $self = $(this);
        //     var $actionsBtn = $self.find(".js-action-element-btn");

        //     $buttons.each(function() {
        //         var $self = $(this);

        //         if (!$self.attr("data-hasqtip")) {
        //             $self.addClass("u-no-display");
        //         }
        //     });

        //     //$(".js-action-element-btn[data-action-element-active!='true']").addClass("u-no-display");

        //     if (!$actionsBtn.attr("data-hasqtip") && !$actionsBtn.is(":visible")) {
        //         $actionsBtn.removeClass("u-no-display");
        //     }
        // });

        // $theRow.unbind("mouseleave").on("mouseleave", function(e) {
        //     var $self = $(this);
        //     var $actionsBtn = $self.find(".js-action-element-btn");

        //     $buttons.each(function() {
        //         var $self = $(this);

        //         if (!$self.attr("data-hasqtip")) {
        //             $self.addClass("u-no-display");
        //         }
        //     });

        //     //$(".js-action-element-btn[data-action-element-active!='true']").addClass("u-no-display");

        //     if (!$actionsBtn.attr("data-hasqtip") && !$actionsBtn.is(":visible")) {
        //         $actionsBtn.addClass("u-no-display");
        //     }
        // });
    };

    var generateDefaultContentContainerMessage = function(obj) { //Append a custom message to a container
        var hbTemplate = CareflowApp.Templates['Content/js/handlebarsTemplates/utils/ContainerDefaultMessage.htm'],
            htmlContent = hbTemplate(obj);
        return $(htmlContent);
    };

    var getGlobalContextActionsUi = function() { //Helper to get the iOs style '+' icon across views to create content
        // if ($(".js-feedBody-createContent-context-btn").length) $(".js-feedBody-createContent-context-btn").remove(); //Reset / remove bindings on each call

        // var _getPatientViewModule = $.Deferred();
        // var _getGroupHomeViewModel = $.Deferred();

        // if (typeof patientView === "undefined") {
        //     //$LAB.script("/Content/js/views/min/PatientView.js").wait(function () {
        //     _getPatientViewModule.resolve();
        //     //});
        // } else {
        //     _getPatientViewModule.resolve();
        // }

        // if (typeof groupHome === "undefined") {
        //     //$LAB.script("/Content/js/views/min/GroupHome.js").wait(function () {
        //     _getGroupHomeViewModel.resolve();
        //     //});
        // } else {
        //     _getGroupHomeViewModel.resolve();
        // }

        // $.when(_getPatientViewModule, _getGroupHomeViewModel).then(function() {
        //     var networkId = Vault.getSessionItem("ActiveNetworkId");
        //     var teamId = Vault.getSessionItem("ActiveGroupMemberAreaId");
        //     //var patientData = patientView.getCachedPatient();
        //     var templateData = {};
        //     var templatePath = "Content/js/handlebarsTemplates/utils/GlobalCreateContentAction.htm";
        //     var domSelectorToInsertTemplateTo = $(".js-app");

        //     if (networkId && teamId) { //Team loaded
        //         GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, templateData, "append").done(GetHandlebarsTemplate.onTemplateInserted).then(function() {
        //             var $contentActionsBtn = $(".js-feedBody-createContent-context-btn");
        //             var $svgIcon = $(".js-createContent-svg");

        //             console.log('@@@@@@@@@@@1');
        //             //subject to change - plus button
        //             if (!patientView.getCachedPatient()) $contentActionsBtn.hide(); //https://doccom.atlassian.net/browse/WEB-748
        //             //subject to change - /plus button

        //             $contentActionsBtn.unbind().on("click", function(e) {
        //                 //if ($(".js-feedBody-createContent-context-btn").data("hasqtip")) {
        //                 //    $(".js-feedBody-createContent-context-btn").qtip("destroy");
        //                 //    $(".js-feedBody-createContent-context-btn").data("rendered", false);
        //                 //    //Add the open state SVG
        //                 //    $(".js-createContent-svg").attr("src", "/Content/img/utils/careflow-plus-blue.svg").data("open", false);
        //                 //    return;
        //                 //}

        //                 $svgIcon.attr("src", "/Content/img/utils/careflow-plus-red.svg").data("open", true); //Add the 'close' svg

        //                 if (!patientView.getCachedPatient()) {
        //                     return PubSub.publish("GroupHome.GetContextActions", {
        //                         Event: e,
        //                         DomObject: $contentActionsBtn,
        //                         DataContext: "TeamContext",
        //                         ControlId: 1, //We know a team context is set, pass memberAreaiD
        //                         QtipContainer: $("document"),
        //                         QtipViewport: $(window),
        //                         QtipContainerMy: "bottom right",
        //                         QtipContainerAt: "bottom left",
        //                         Classes: "teamContextActions__qTip js-teamContextActions-qTip",
        //                         Tip: false,
        //                         IsGlobalActionsEvent: true,
        //                         Permissions: [].concat(appMain.getUserPermissionsForMemberArea(Vault.getSessionItem("ActiveGroupMemberAreaId")), appMain.getUserPermissionsForNetwork(Vault.getSessionItem("ActiveNetworkId"))), //appMain.getUserPermissionsForMemberArea(Vault.getSessionItem("ActiveGroupMemberAreaId")),
        //                         IsPatientNetwork: appMain.checkUserPermissionForNetwork(Vault.getSessionItem("ActiveNetworkId"), "ViewPatients"),
        //                         IsMemberAreaContext: true,
        //                         MemberAreaId: Vault.getSessionItem("ActiveGroupMemberAreaId"),
        //                         IsPatientListLoadedStatus: groupHome.getLoadedPatientListAndType(),
        //                         IsGlobalContextOptionsMenuClick: true //Flag allows for links to be set depending on source, patient banner '..' or global '+'
        //                     });
        //                 } else { //Render standard patient actions - patient loaded in cache
        //                     var patientId = patientView.getCachedPatient().Data.PatientExternalIdentifier;
        //                     return PubSub.publish("PatientView.GetPatientActions", {
        //                         PatientId: patientId,
        //                         ContentId: null,
        //                         Event: e,
        //                         ControlId: 3, //Patient Update in Team context
        //                         DomObject: $contentActionsBtn,
        //                         DataContext: "TeamContextPatientView",
        //                         QtipContainer: $("document"),
        //                         QtipViewport: $(window),
        //                         QtipContainerMy: "bottom right",
        //                         QtipContainerAt: "bottom left",
        //                         Classes: "teamContextActions__qTip js-patientContextActions-qTip",
        //                         Tip: false,
        //                         IsGlobalActionsEvent: true,
        //                         Permissions: [].concat(appMain.getUserPermissionsForMemberArea(Vault.getSessionItem("ActiveGroupMemberAreaId")), appMain.getUserPermissionsForNetwork(Vault.getSessionItem("ActiveNetworkId"))), //appMain.getUserPermissionsForMemberArea(Vault.getSessionItem("ActiveGroupMemberAreaId")),
        //                         IsPatientNetwork: appMain.checkUserPermissionForNetwork(Vault.getSessionItem("ActiveNetworkId"), "ViewPatients"),
        //                         IsMemberAreaContext: true,
        //                         MemberAreaId: Vault.getSessionItem("ActiveGroupMemberAreaId"),
        //                         IsPatientListLoadedStatus: groupHome.getLoadedPatientListAndType(),
        //                         FeedIsLoadedInDom: feeds.getLastLoadedFeedTypeAndDomStatus().FeedIsLoadedInDom,
        //                         IsPatientDashboardLoadedInDom: groupHome.isPatientDashboardLoadedInDom(),
        //                         IsGlobalContextOptionsMenuClick: true
        //                     });
        //                 }
        //             });
        //         });
        //     } else if (networkId && !teamId) { //Network view only
        //         //if ($(".js-feedBody-createContent-context-btn").length) $(".js-feedBody-createContent-context-btn").remove();

        //         GetHandlebarsTemplate.callApi(templatePath, domSelectorToInsertTemplateTo, templateData, "append").done(GetHandlebarsTemplate.onTemplateInserted).then(function() {
        //             var $contentActionsBtn = $(".js-feedBody-createContent-context-btn");
        //             var $svgIcon = $(".js-createContent-svg");

        //             console.log('@@@@@@@@@@@2');
        //             //subject to change - plus button
        //             if (!patientView.getCachedPatient()) $contentActionsBtn.hide(); //https://doccom.atlassian.net/browse/WEB-748
        //             //subject to change - /plus button

        //             $contentActionsBtn.unbind().on("click", function(e) {
        //                 //console.log(e)

        //                 //if ($(e.target).is('.js-feedBody-createContent-context-btn')) {
        //                 //    if ($(".js-feedBody-createContent-context-btn").data("rendered")) {
        //                 //        $(".js-feedBody-createContent-context-btn").qtip("destroy");
        //                 //        $(".js-feedBody-createContent-context-btn").data("rendered", false);
        //                 //        //Add the open state SVG
        //                 //        $(".js-createContent-svg").attr("src", "/Content/img/utils/careflow-plus-blue.svg").data("open", false);
        //                 //        return;
        //                 //    }
        //                 //}

        //                 $svgIcon.attr("src", "/Content/img/utils/careflow-plus-red.svg").data("open", true); //Add the 'close' svg

        //                 //Check if a patient is loaded
        //                 if (patientView.getCachedPatient()) {
        //                     console.log("$$$$$", [].concat(appMain.getUserPermissionsForMemberArea(Vault.getSessionItem("ActiveGroupMemberAreaId")), appMain.getUserPermissionsForNetwork(Vault.getSessionItem("ActiveNetworkId"))));

        //                     var patientId = patientView.getCachedPatient().Data.PatientExternalIdentifier;
        //                     PubSub.publish("PatientView.GetPatientActions", {
        //                         PatientId: patientId,
        //                         ContentId: null,
        //                         Event: e,
        //                         DomObject: $contentActionsBtn,
        //                         DataContext: "TeamContextPatientView",
        //                         QtipContainer: $("document"),
        //                         QtipViewport: $(window),
        //                         QtipContainerMy: "bottom right",
        //                         QtipContainerAt: "bottom left",
        //                         Classes: "teamContextActions__qTip js-patientContextActions-qTip",
        //                         Tip: false,
        //                         IsGlobalActionsEvent: true,
        //                         Permissions: getUserPermissionsForAllUsersMemberAreas(networkId),
        //                         IsPatientNetwork: appMain.checkUserPermissionForNetwork(Vault.getSessionItem("ActiveNetworkId"), "ViewPatients"),
        //                         IsMemberAreaContext: false,
        //                         MemberAreaId: null,
        //                         IsPatientListLoadedStatus: groupHome.getLoadedPatientListAndType(),
        //                         FeedIsLoadedInDom: feeds.getLastLoadedFeedTypeAndDomStatus().FeedIsLoadedInDom,
        //                         isPatientDashboardLoadedInDom: groupHome.isPatientDashboardLoadedInDom(),
        //                         IsGlobalContextOptionsMenuClick: true
        //                     });
        //                 } else { //If not, get Group actions
        //                     PubSub.publish("GroupHome.GetContextActions", {
        //                         Event: e,
        //                         DomObject: $contentActionsBtn,
        //                         DataContext: "TeamContext",
        //                         ControlId: 1, //Patient Update - Select a team available.
        //                         QtipContainer: $("document"),
        //                         QtipViewport: $(window),
        //                         QtipContainerMy: "bottom right",
        //                         QtipContainerAt: "bottom left",
        //                         Classes: "teamContextActions__qTip js-teamContextActions-qTip",
        //                         Tip: false,
        //                         IsGlobalActionsEvent: true,
        //                         Permissions: [].concat(appMain.getUserPermissionsForMemberArea(Vault.getSessionItem("ActiveGroupMemberAreaId")), appMain.getUserPermissionsForNetwork(Vault.getSessionItem("ActiveNetworkId"))), //appMain.getUserPermissionsForNetwork(Vault.getSessionItem("ActiveNetworkId")),
        //                         IsPatientNetwork: appMain.checkUserPermissionForNetwork(Vault.getSessionItem("ActiveNetworkId"), "ViewPatients"),
        //                         IsMemberAreaContext: true,
        //                         MemberAreaId: null,
        //                         IsGlobalContextOptionsMenuClick: true
        //                     });
        //                 }
        //             });
        //         });
        //     }
        // });

        // if (!PubSub.checkSubscription("GroupHome.GetContextActions.ActionsUiClosed")) {
        //     PubSub.subscribe("GroupHome.GetContextActions.ActionsUiClosed", function(obj) {
        //         if ($(".js-feedBody-createContent-context-btn").data("hasqtip")) {
        //             $(".js-feedBody-createContent-context-btn").qtip("destroy");
        //         }
        //         //Add the open state SVG
        //         $(".js-createContent-svg").attr("src", "/Content/img/utils/careflow-plus-blue.svg").data("open", false);
        //     });
        // }
    };

    var removeGlobalContextActionsUi = function() {
        // if ($(".js-feedBody-createContent-context-btn").length) $(".js-feedBody-createContent-context-btn").remove();
        // return;
    };

    var updateNavigationCounts = function() {
        // var counts = $.jStorage.get('counts', {});
        // $('.js-newContent-counter-alerts').text(counts.alerts || 0);
    };

    var sortPatientListByLocation = function(patientDataSet, sortDataByLocationIndex) {
        //Add a sort key for location ordering - see https://doccom.atlassian.net/browse/WEB-309
        //https://stackoverflow.com/questions/45162140/pass-array-to-function-by-value

        if (!patientDataSet || !Array.isArray(patientDataSet)) return console.error("No parseable array supplied");

        //Group all by site
        var sortedArray = _.groupBy(patientDataSet, function(item) {
            //Create sort key properties
            item.SortKeySiteName = item.SiteName;
            item.SortKeyAreaName = item.AreaName;
            item.SortKeyBaySort = item.BaySort;
            item.SortKeyBedSort = item.BedSort;

            if (!item.SortKeySiteName) item.SortKeySiteName = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
            if (!item.SortKeyAreaName) item.SortKeyAreaName = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

            if (!item.SortKeyBaySort && !item.SortKeyBedSort) {
                item.SortKeyBedSort = "000000000000000BED-99ZZ";
                item.SortKeyBaySort = "000000000000000BAY-99ZZ";
            }

            if (!item.SortKeyBaySort) item.SortKeyBaySort = "000000000000000BAY-99ZZ";

            if (!item.SortKeyBedSort) item.SortKeyBedSort = "000000000000000BED-99ZZ";

            item.SortKeySiteName = item.SortKeySiteName.toLowerCase();
            item.SortKeyAreaName = item.SortKeyAreaName.toLowerCase();
            item.SortKeyBaySort = item.SortKeyBaySort.toLowerCase();
            item.SortKeyBedSort = item.SortKeyBedSort.toLowerCase();

            return item.SortKeySiteName.replace(/ /g, "").toLowerCase().trim();
        });

        Object.keys(sortedArray).forEach(function(key) {
            // do something with obj[key]
            sortedArray[key] = _.sortBy(sortedArray[key], 'SortKeyBedSort');
        });

        Object.keys(sortedArray).forEach(function(key) {
            sortedArray[key] = _.sortBy(sortedArray[key], 'SortKeyBaySort');
        });

        Object.keys(sortedArray).forEach(function(key) {
            sortedArray[key] = _.sortBy(sortedArray[key], 'SortKeyAreaName');
        });

        sortedArray = _.sortBy(_.pairs(sortedArray), function(o) { return o[0]; }); //Convert an object into a list of [key, value] pairs.

        //Add a location sort key
        var sortCounter = 0;

        sortedArray.forEach(function(a) {
            a[1].forEach(function(b) {
                b.LocationSortKey = sortCounter;
                sortCounter += 1;

                //Add key to _patientsData.Data
                _.find(patientDataSet, function(patient) { //Looks through each value in the list, returning the first one that passes a truth test
                    if (patient.PatientExternalIdentifier === b.PatientExternalIdentifier) {
                        patient.LocationSortKey = b.LocationSortKey;
                    }
                });
            });
        });

        //Optional sort
        if (sortDataByLocationIndex) patientDataSet = _.sortBy(patientDataSet, 'LocationSortKey');

        //Clear data
        sortedArray = null;

        return patientDataSet;
    };
    var triggerAppNotification = function(obj) {
        if (appRouter.careflowAppRouter.getLocation() === '/#/SignIn') return;
        if (!obj || typeof obj !== "object") obj = {};

        // merge objects
        var options = Object.assign({
            Text: "",
            Icon: "info",
            Heading: "",
            ShowHideTransition: "fade",
            BeforeShow: $.noop(),
            AfterShown: $.noop(),
            BeforeHide: $.noop(),
            AfterHidden: $.noop()
        }, obj);

        return $.toast({
            text: options.Text, //Can be HTML
            icon: options.Icon, //error, success, warning, info
            heading: options.Heading, // Optional heading to be shown on the toast
            showHideTransition: options.showHideTransition, // fade, slide or plain
            hideAfter: 15000,
            allowToastClose: true,
            position: obj.Position || 'top-right',
            stack: 2, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
            beforeShow: (typeof options.BeforeShow === "function") ? options.BeforeShow(e) : $.noop(), // will be triggered before the toast is shown
            afterShown: (typeof options.AfterShown === "function") ? options.AfterShown(e) : $.noop(), // will be triggered after the toat has been shown
            beforeHide: (typeof options.BeforeHide === "function") ? options.BeforeHide(e) : $.noop(), // will be triggered before the toast gets hidden
            afterHidden: (typeof options.AfterHidden === "function") ? options.AfterHidden(e) : $.noop() // will be triggered after the toast has been hidden
        });
    };

    var getAppConfig = function() {
        return new Promise(function(resolve, reject) {
            axios.get('/config/appConfig.json')
                .then(function(response) {
                    if (response.data && typeof response.data === "object") {
                        _cachedAppConfig = response.data; //Set cache on each call
                        return resolve(response.data);
                    }
                    return reject();
                }).catch(function(error) {
                    reject();
                });
        });
    };

    var getCachedAppConfig = function() {
        if (_cachedAppConfig) return _cachedAppConfig;
        return null;
    };

    var isHpcaAuthTokenType = function(token) {
        //https://doccom.atlassian.net/browse/WEB-642

        if (!token) return false;
        token = JSON.parse(token);

        if (typeof token === "object" && (_.has(token, "Provider") && token.Provider.toLowerCase() === "hpca")) {
            return true;
        }
        return false;
    };

    var getSafeString = function(str) { //https://stackoverflow.com/questions/14129953/how-to-encode-a-string-in-javascript-for-displaying-in-html
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    };

    //Badge count update methods, called via cache and API response
    let getUnreadCountsForAllNetworks = function(includeSubCounts, types) {
        return GetUnreadItemCountsForAllNetworks.callApi(includeSubCounts, types).then(
            (response, textStatus, jQXHr) => {
                if (/^2/.test(jQXHr.status)) {
                    _.delay(function() {
                            PubSub.publish("BadgeCounts.UnreadCountsForAllNetworksUpdated", {
                                Data: response
                            });
                        },
                        3000);
                }
                return response;
            });
    };

    let getUnreadCountsForSingleNetwork = function(includeSubCounts, networkId, types) {
        return GetUnreadItemCountsForSingleNetwork.callApi(includeSubCounts, networkId, types).then(
            (response, textStatus, jQXHr) => {
                if (/^2/.test(jQXHr.status)) {
                    _.delay(function() {
                        PubSub.publish("BadgeCounts.UnreadCountsForSingleNetworkUpdated", {
                            Data: response
                        });
                    }, 3000);
                }
                return response;
            });
    };

    var setMainNavigationBadgeCounts = function(obj) {
        // //Update main nav, if in DOM. This method is called each time main nav is built (cache check) and whenever the counts APIs are called and broadcast Pub / Sub (BadgeCounts.*)
        // if (!$(".js-appMainNavigation--template").length) return;

        // //Update main nav 'teams' dropdown
        // $(".js-appMainNavigation--template").find(".js-newContent-counter-network-teams").each(
        //     function(index, val) {
        //         var $el = $(this);
        //         let count = [];
        //         let accessGroupId = appMain.getAccessGroupId(parseInt($el.data("groupid"), 10));
        //         if (obj.Data.Data.Groups) {
        //             count = obj.Data.Data.Groups.filter(function(item) {
        //                 return item.AccessGroupID === accessGroupId;
        //             });
        //         }
        //         if (count.length) {
        //             $el.text(_parseCountValues(count[0].Updates)).removeClass("u-no-display");
        //         } else {
        //             $el.text("").addClass("u-no-display");
        //         }
        //     });

        // //Alerts
        // if (obj.Data.Data.Alerts) {
        //     $(".js-newContent-counter-alerts").text(_parseCountValues(obj.Data.Data.Alerts)).removeClass("u-no-display").attr("data-count", _parseCountValues(obj.Data.Data.Alerts));
        // } else {
        //     $(".js-newContent-counter-alerts").text("").addClass("u-no-display");
        // }
    };

    var setMainNavigationBadgeCountsForAllUserNetworks = function(obj) {
        // //Update main nav, if in DOM. This method is called each time main nav is built (cache check) and whenever the counts APIs are called and broadcast Pub / Sub (BadgeCounts.*)
        // if (!$(".js-appMainNavigation--template").length) return;

        // //Update networks nav list
        // if (obj.Data.Data.Networks) {
        //     obj.Data.Data.Networks.forEach(function(item) {
        //         //All Updates, inc Alerts which are filtered by network
        //         if (item.NetworkID !== Vault.getSessionItem("ActiveNetworkId")) { //Do not show on 'active' network
        //             $(".js-appMainNavigation--template").find(".js-newContent-counter-other-network[data-networkid=" + item.NetworkID + "]").text((item.Total) ? _parseCountValues(item.Total) : "").removeClass("u-no-display");
        //         } else {
        //             $(".js-appMainNavigation--template").find(".js-newContent-counter-other-network[data-networkid=" + item.NetworkID + "]").text("").addClass("u-no-display");
        //         }
        //     });
        // }

        // //User to User messages (only returned in GetUnreadItemCountsForAllNetworks, messages are non-network specific)
        // if (obj.Data.Data.UserToUser) {
        //     $(".js-appMainNavigation--template").find(".js-newContent-counter-messages").text(_parseCountValues(obj.Data.Data.UserToUser)).removeClass("u-no-display").attr("data-count", _parseCountValues(obj.Data.Data.UserToUser));
        // } else {
        //     $(".js-appMainNavigation--template").find(".js-newContent-counter-messages").text("").addClass("u-no-display");
        // }

        // //Count all updates, excluding active and U2U (non network specific),to display on burger icon. See comment here - https://doccom.atlassian.net/browse/WEB-273
        // let _countOfAllOtherNetworkUpdates = 0;
        // let _activeNetwork = (obj.Data.Data.Networks) ? obj.Data.Data.Networks.filter(function(item) {
        //     return item.NetworkID === Vault.getSessionItem("ActiveNetworkId");
        // }) : null;

        // if (obj.Data.Data.hasOwnProperty("AllNetworkUpdatesTotal")) _countOfAllOtherNetworkUpdates += obj.Data.Data.AllNetworkUpdatesTotal;
        // if (obj.Data.Data.hasOwnProperty("AllNetworksAlertsTotal")) _countOfAllOtherNetworkUpdates += obj.Data.Data.AllNetworksAlertsTotal;

        // if (_activeNetwork && _activeNetwork[0]) {
        //     _countOfAllOtherNetworkUpdates -= _activeNetwork[0].Total;
        // }

        // if (_countOfAllOtherNetworkUpdates) {
        //     $(".js-appMainNavigation--template").find(".js-newContent-counter-all-other-networks").text(_parseCountValues(_countOfAllOtherNetworkUpdates)).removeClass("u-no-display").attr("data-count", _parseCountValues(_countOfAllOtherNetworkUpdates));
        // } else {
        //     $(".js-appMainNavigation--template").find(".js-newContent-counter-all-other-networks").text("").addClass("u-no-display");
        // }
    };

    let _parseCountValues = function(value) {
        if (!value || typeof value !== "number") return value;

        if (value > 99) return "99+";
        return value;
    };

    var _initCustomJqueryPlugins = function() { //Custom app jquery functions
        //Make ajaxActiveButtonUi() method available as basic jQuery plugin, for simple instances where button locking is required
        $.fn.ajaxActiveButtonUi = function(options) {
            if (!options) options = {};
            var settings = $.extend({
                // These are the defaults.
                ActionState: "start",
                DisableAllInputs: true,
                DisableContainerInputs: false,
                DisableNoInputs: false,
                DisableThis: true,
                LockedText: (this.data("locked-txt")) ? this.data("locked-txt") : "Please wait...",
                DefaultText: this.text(),
                Class: ""
            }, options);

            //start loading animation
            if (settings.ActionState.toLowerCase() === "start") {
                if (settings.DisableThis) this.prop("disabled", true).attr('data-btn-text', this.text());

                //Reference default text value
                this.data({ "default-text": settings.DefaultText, "active-button-ui": true });

                //binding spinner element to button and changing button text
                this.html('<span class="btn--locked">' + settings.LockedText + ' <i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw btn--locked-img"></i></span>').addClass("btn--locked-parent");
                //Custom classes
                if (options.Class) {
                    this.data("original-class", this.attr("class").split());
                    this.data("custom-class", options.Class); //Reference for removal on "stop"
                    this.removeClass().addClass(options.Class);
                }
                //this.html('<i class="fa fa-spinner fa-spin fa-fw"></i> ' + settings.LockedText + '<span class="sr-only">Loading...</span>');

                if (settings.DisableContainerInputs && !settings.DisableNoInputs) {
                    settings.DisableContainerInputs.find(':input', ':button', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
                    return;
                } else if (settings.DisableAllInputs && !settings.DisableNoInputs && !settings.DisableContainerInputs) {
                    $(".js-app-body").find(':input', ':button', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
                    return;
                }
            }
            //stop loading animation
            if (settings.ActionState.toLowerCase() != "start") {
                this.html(this.data("default-text")).removeClass("btn--locked-parent"); //Set on start method

                //Custom classes
                if (this.data("custom-class")) {
                    this.removeClass().addClass(this.data("original-class"));
                    this.data("custom-class", false);
                    this.data("original-class", false);
                }

                //enable buttons after finish loading
                this.prop("disabled", false).data("active-button-ui", false);

                if (settings.DisableContainerInputs) {
                    settings.DisableContainerInputs.find(':input', ':button', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", false).removeClass("disabled");
                    return;
                } else {
                    $(".js-app-body").find(':input', ':button', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", false).removeClass("disabled");
                    return;
                }
            }

            return this;
        };

        //plugin that change element type, preserving attributes. Pass a new type string
        $.fn.changeElementType = function(newType, callback) {
            this.each(function() {
                var attrs = {};

                $.each(this.attributes, function(idx, attr) {
                    attrs[attr.nodeName] = attr.nodeValue;
                });

                $(this).replaceWith(function() {
                    var $newEl = $("<" + newType + "/>", attrs).append($(this).contents());
                    if (typeof callback === "function") callback($newEl); //Execute any passed callback()
                    return $newEl;
                });
            });
        };

        //Div / contailer loading helper
        $.fn.ajaxLoadingHelper = function(options) {
            var settings = $.extend({
                Padding: "1em",
                DisableInputsContainer: false,
                DomElement: false, //the continer / block to target (JQ object)
                DisableNoInputs: false,
                LoadingMessage: ""
            }, options);

            var text;

            if (settings.LoadingMessage) {
                text = '<div style="padding:' + settings.Padding + '" class="u-ajax-loader u-clearfix"><i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw u-ajax-loader__icon"></i> <span style="display: inline-block;" class="u-ajax-loader__label">' + settings.LoadingMessage + '</span></div>';
            } else {
                text = '<div style="padding:' + settings.Padding + '" class="u-ajax-loader u-clearfix"><span style="display: inline-block;" class="u-ajax-loader__label">' + settings.LoadingMessage + '</span></div>';
            }

            this.html(text);

            if (settings.DisableInputsContainer && !settings.DisableNoInputs) { //Disable selected
                settings.DisableInputsContainer.find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
            } else if (!settings.DisableInputsContainer && !settings.DisableNoInputs) { //Disable all
                if ($.featherlight.current()) {
                    $(".js-appModal-wrapper").find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
                } else {
                    $(".js-app-body").find(':input', 'textarea', 'select', '[data-ajaxload-disable]', '.tt-input').prop("disabled", true).addClass("disabled");
                }
            }
        };

        $.fn.cfTickbox = function(options) {
            this.each(function() {
                var instance = $(this),
                    settings = $.extend({
                        ticked: instance.attr('data-is-ticked') === 'true',
                        parentTrigger: instance.attr('data-parent-trigger') === 'true',
                        reloadAppearance: false
                    }, options);

                if (instance.attr('data-init') !== 'true') {
                    instance.attr('data-is-ticked', settings.ticked);

                    instance.append('<i class="fa ' + (settings.ticked ? 'fa-check-square-o' : 'fa-square-o') + '" aria-hidden="true"></i>');

                    instance.on('click', function(e) {
                        e.stopPropagation();

                        var self = $(this),
                            tickElement = $(':first-child', self),
                            isTicked = self.attr('data-is-ticked') === 'true';

                        if (self.is(':disabled')) return;

                        if (isTicked) {
                            tickElement.removeClass('fa-check-square-o');
                            tickElement.addClass('fa-square-o');
                        } else {
                            tickElement.removeClass('fa-square-o');
                            tickElement.addClass('fa-check-square-o');
                        }

                        self.attr('data-is-ticked', !isTicked);

                        self.trigger('tick');
                    });

                    if (settings.parentTrigger) {
                        instance.parent().on('click', function(e) {
                            instance.trigger('click');
                        });
                    }

                    instance.attr('data-init', 'true');
                }

                if (settings.reloadAppearance) {
                    instance.attr('data-is-ticked', settings.ticked);

                    var tickElement = $(':first-child', instance);

                    if (settings.ticked) {
                        tickElement.removeClass('fa-square-o');
                        tickElement.addClass('fa-check-square-o');
                    } else {
                        tickElement.removeClass('fa-check-square-o');
                        tickElement.addClass('fa-square-o');
                    }
                }
            });

            return this;
        };

        $.fn.cfSelectCb = function(options) {
            options = options || {};

            if (this.data('cfSelectCbInit')) {
                if (options === 'getValues') {
                    return this.data('selectedValues') || [];
                } else if (options === 'getText') {
                    return this.data('selectedText') || [];
                } else if (options === 'empty') {
                    //TODO
                }

                return this;
            }

            var $self = this,
                $trigger = $('<button><span>All</span><i class="fa fa-sort-desc" style="float: right" aria-hidden="true"></i></button>'),
                $dropdown = $('<div style="z-index: 99"></div>'),
                $search = $('<input type="text" placeholder="Search...">'),
                $reset = $('<button type="button">Reset to default</button>'),
                $itemsContainer = $('<div></div>'),
                preselectedValues = options.selected || [];

            $dropdown.append($search, $reset, $itemsContainer);

            $self.hide();
            $dropdown.hide();

            $self.after($trigger, $dropdown);

            if (options.triggerClass) $trigger.addClass(options.triggerClass);
            if (options.searchClass) $search.addClass(options.searchClass);
            if (options.resetClass) $reset.addClass(options.resetClass);
            if (options.optionsClass) $itemsContainer.addClass(options.optionsClass);

            $self.find('option').each(function() {
                var $selfOption = $(this),
                    newOption = $('<div><label><input type="checkbox" value="' + $selfOption.val() + '"><span></span></label></div>');

                newOption.find('span').text($selfOption.text());

                newOption.find('input').on('change', function(e) {
                    var checkedItems = $itemsContainer.find('input:checked'),
                        selectedText = checkedItems.map(function() {
                            return $(this).parent('label').find('span').text();
                        }).toArray();

                    $self.data('selectedValues', checkedItems.map(function() {
                        return $(this).val();
                    }).toArray());

                    $self.data('selectedText', selectedText);

                    $trigger.find('span').text(selectedText.length === 0 ? 'All' : selectedText.join(', '));

                    $self.trigger('change');
                });

                $itemsContainer.append(newOption);
            });

            preselectedValues.forEach(function(value) {
                $itemsContainer
                    .find('input[value="' + value + '"]')
                    .prop('checked', true)
                    .trigger('change');
            });

            $trigger.on('click', function(e) {
                e.stopPropagation();
                e.preventDefault();

                if ($dropdown.is(':hidden')) {
                    var position = $trigger.position() || {};

                    $dropdown.css({ position: 'absolute', left: position.left, top: parseFloat(position.top) + $trigger.outerHeight() });
                }

                $dropdown.toggle();
            });

            $search.on('input', function(e) {
                var searchValue = $(this).val().trim().toLowerCase();

                $itemsContainer
                    .find('span')
                    .each(function() {
                        var $self = $(this);
                        $self.closest('div')[$self.text().toLowerCase().indexOf(searchValue) === -1 ? 'hide' : 'show']();
                    });
            });

            $reset.on('click', function(e) {
                //TODO: optimize using options.empty
                $itemsContainer
                    .find('input')
                    .each(function() {
                        var $self = $(this);

                        $self
                            .prop('checked', preselectedValues.indexOf($self.val()) !== -1)
                            .trigger('change');
                    });

                $search.val('').trigger('input');
            });

            $dropdown.on('click', function(e) {
                e.stopPropagation();
            });

            $(document).on('click', function(e) {
                $dropdown.hide();
            });

            $self.data('cfSelectCbInit', true);

            return $self;
        };

        //Plugin to determine how many characters will fill a space. IE9 does support canvas https://stackoverflow.com/questions/6172637/why-does-my-ie9-does-not-support-canvas
        $.fn.textSize = function() {
            var $self = this;

            function getCharWidth() {
                var canvas = getCharWidth.canvas || (getCharWidth.canvas = $("<canvas>")[0]),
                    context = canvas.getContext("2d");

                context.font = [$self.css('font-size'), $self.css('font-family')].join(' ');
                var metrics = context.measureText("3");
                return metrics.width;
            };

            var lineHeight = parseFloat(getComputedStyle($self[0]).lineHeight);
            return {
                x: Math.floor($self.width() / getCharWidth()),
                y: Math.floor($self.height() / lineHeight)
            };
        };

        //Text overflow helper - return bool
        $.fn.overflown = function() {
            var e = this[0];
            return e.scrollHeight > e.clientHeight || e.scrollWidth > e.clientWidth;
        };
    };

    /*Helpers*/
    /*A collection of helpers - will be replaced and deprecated in full by APIs, mainly AppConfig API*/

    let checkIfFeatureIsEnabledInAppConfig = function(featureKey, networkId = Vault.getSessionItem("ActiveNetworkId")) { //Must match case etc.
        if (!appMain.getCachedAppConfig()) return false;
        let appConfigData = appMain.getCachedAppConfig();
        let environment = appConfigData.environment; //dynamically replaced on deploy (Nik G)

        //Check env prop
        if (appConfigData.networkConfig[environment].hasOwnProperty(featureKey) && appConfigData.networkConfig[environment][featureKey]) return appConfigData.networkConfig[environment][featureKey]; //If env has the prop (googleAnalytics for eg)
        //Check network props
        if (!_.has(appConfigData.networkConfig, environment)) return false;
        let item = _.filter(appConfigData.networkConfig[environment].networks, function(item) {
            return item.networkId === networkId;
        })[0];

        if (!item || !item.hasOwnProperty(featureKey) || !item[featureKey]) return false;

        return item[featureKey]; //return requested propery value if found
    };

    let setOpenedPatientContextWindow = function(window) {
        if (!window) return;
        //If instance exists, close old and set new
        //console.log(_openedPatientContextWindows)
        //alert(window.name)
        return _openedPatientContextWindows.push(window);
    };

    let closeOpenedPatientContextWindow = function(window) {
        if (isTpiIntegrationSession) appApi.callCommand('closeContextWindows', null); //Close TPI shell windows
        if (!_openedPatientContextWindows.length) return;
        if (window) return window.close();
        return _openedPatientContextWindows.forEach(x => {
            x.close();
        });
    };

	let isTpiIntegrationSession = function () {
        return Vault.getItem('IsAppIntegrationSession') || false;
    };

    let getIsRefreshingAccessToken = function() {
        return _isRefreshingAccessToken;
    }

    let setIsRefreshingAccessToken = function(bool) {
        if (typeof bool !== "boolean") return;
        _isRefreshingAccessToken = bool;
    };

    let awaitIsRefreshingAccessToken = function() {
        return new Promise((resolve, reject) => {
            //Refresh token updated, trigger resolution / rejection
            VueEventBus.$on('CareflowApp.RefreshTokenHeartbeatRefreshed', (value) => {
                if (typeof value === "boolean" && value) {
                    resolve("awaitIsRefreshingAccessToken is now resolved")
                } else {
                    reject("awaitIsRefreshingAccessToken is now rejected")
                }
            });
        });
    };

    let setLastCachedValidPatientMedwayLookup = function(medwayPatient, careflowPatientId) {
        if (!medwayPatient || typeof medwayPatient !== "object" || !careflowPatientId) return false;
        _cachedMedwayPatient = medwayPatient;
        _cachedMedwayPatient.careflowPatientExternalId = careflowPatientId; //Add the careflow patient ID for lookup
        //Return a promise for async / await (queued patient lookup requests for same patient)
        VueEventBus.$emit('CareflowApp.GetPatientMedwayGuidResolved', careflowPatientId);
    };

    let getLastCachedValidPatientMedwayLookup = function(careflowPatientId) {
        if (!careflowPatientId || !_cachedMedwayPatient) return false;
        if (careflowPatientId === _cachedMedwayPatient.careflowPatientExternalId) return _cachedMedwayPatient;
        return false;
    };

    let userHasNetworkAdministrationViewAccess = function() {
        return NetworkAdministration.userCanAccessNetworkAdministration();
    }

    let checkOpenPatientMedwayApiRequest = function(careflowPatientId) {

        if (_openMedwayPatientApiRequests.has(careflowPatientId)) return true;
        return false;

    }

    let setOpenPatientMedwayApiRequest = function(careflowPatientId) {

        if (!_openMedwayPatientApiRequests.has(careflowPatientId)) return _openMedwayPatientApiRequests.add(careflowPatientId);
        return false;

    }

    let clearOpenPatientMedwayApiRequest = function(careflowPatientId, isError = false) {

        if (isError && careflowPatientId) {
            VueEventBus.$emit('CareflowApp.GetPatientMedwayGuidRequestFail', careflowPatientId); //API fail or non-matched patient
        }
        return _openMedwayPatientApiRequests.clear();
    }

    let awaitOpenPatientMedwayApiRequest = function(careflowPatientId) {

        return new Promise((resolve, reject) => {
            if (!careflowPatientId) return reject();
            //Refresh token updated, trigger resolution / rejection
            VueEventBus.$on('CareflowApp.GetPatientMedwayGuidResolved', (value) => {
                if (value && value === careflowPatientId) {
                    return resolve();
                } else {
                    return reject();
                }
            });
            VueEventBus.$on('CareflowApp.GetPatientMedwayGuidRequestFail', (value) => { //API fail or non-matched patient
                if (value && value === careflowPatientId) {
                    return reject("CareflowApp.GetPatientMedwayGuidRequestFail is now rejected");
                }
            });
        });
    }


    return {
        checkUserPermissionForMemberArea: checkUserPermissionForMemberArea,
        checkUserPermissionForNetwork: checkUserPermissionForNetwork,
        windowHeight: _windowHeight,
        windowHeightMinusDeductableElements: _windowHeightMinusDeductableElements,
        isOldIe: isOldIe,
        stopApiRequests: stopApiRequests,
        appOnLoadInit: appOnLoadInit,
        userLogOut: userLogOut,
        isUserLoggedIn: isUserLoggedIn,
        setPageLayout: setPageLayout,
        setElementsAsFullHeight: setElementsAsFullHeight,
        setModalElementsAsFullHeight: setModalElementsAsFullHeight,
        appTimeoutCounter: appTimeoutCounter,
        getAppTimeoutInstance: getAppTimeoutInstance,
        getRequestingUserSummary: getRequestingUserSummary,
        getNetworksUserIsMemberOf: getNetworksUserIsMemberOf,
        getActiveUsersNetworks: getActiveUsersNetworks,
        getTeamsUserIsMemberOf: getTeamsUserIsMemberOf,
        getUserPermissionsForNetwork: getUserPermissionsForNetwork,
        getUserPermissionsForAllNetworks: getUserPermissionsForAllNetworks,
        getUserPermissionsForMemberArea: getUserPermissionsForMemberArea,
        getUserPermissionsForAllUsersMemberAreas: getUserPermissionsForAllUsersMemberAreas,
        getItSupportUserPermissions: getItSupportUserPermissions,
        getNetworkName: getNetworkName,
        getNetworkMemberAreaId: getNetworkMemberAreaId,
        getAccessGroupId: getAccessGroupId,
        getMemberAreaIdFromAccessGroupId: getMemberAreaIdFromAccessGroupId,
        getNetworkExternalNetworkId: getNetworkExternalNetworkId,
        getMemberAreaExternalNetworkId: getMemberAreaExternalNetworkId,
        expandInputDownOnFocus: expandInputDownOnFocus, //Maybe delete
        checkIfElementHasDataKeySet: checkIfElementHasDataKeySet,
        formatTemplateDates: formatTemplateDates,
        formatDate: formatDate,
        getFormattedDate: getFormattedDate,
        getUtcDate: getUtcDate,
        cleanUpUiElementsOnRouteChange: cleanUpUiElementsOnRouteChange,
        closeAllqTips: closeAllqTips,
        cleanAllRenderedQtips: cleanAllRenderedQtips,
        destroyWaypointOnElement: destroyWaypointOnElement,
        setDutyStatusTogglebtn: setDutyStatusTogglebtn,
        getPresenceIndicatorClass: getPresenceIndicatorClass,
        ajaxLoadMask: ajaxLoadMask,
        ajaxLoadUnMask: ajaxLoadUnMask,
        ajaxLoadingHelper: ajaxLoadingHelper,
        ajaxLoadingHelperComplete: ajaxLoadingHelperComplete,
        blockAnchorLinks: blockAnchorLinks,
        getMemberAreaName: getMemberAreaName,
        getMemberAreaIdFromNamePassedAsString: getMemberAreaIdFromNamePassedAsString,
        getAccessGroupExternalIdentifier: getAccessGroupExternalIdentifier,
        isMemberAreaValidForUser: isMemberAreaValidForUser,
        isNetworkValidForUser: isNetworkValidForUser,
        isNetworkRouteValidForUser: isNetworkRouteValidForUser,
        isMemberAreaRouteValidForUser: isMemberAreaRouteValidForUser,
        getRequestToJoinTeamModal: getRequestToJoinTeamModal,
        getRefreralStatusUpdatedText: getRefreralStatusUpdatedText,
        urlifyText: urlifyText,
        getGroupListsPatientCanBeAddedTo: getGroupListsPatientCanBeAddedTo,
        checkShims: checkShims,
        drawMouseEnteredPopoutBubble: drawMouseEnteredPopoutBubble,
        drawClickToOpenPopoutBubble: drawClickToOpenPopoutBubble,
        drawPromptConfirmDialog: drawPromptConfirmDialog,
        getFatalErrorUserNotificationModal: getFatalErrorUserNotificationModal,
        toggleActionButton: toggleActionButton,
        generateDefaultContentContainerMessage: generateDefaultContentContainerMessage,
        getGlobalContextActionsUi: getGlobalContextActionsUi,
        removeGlobalContextActionsUi: removeGlobalContextActionsUi,
        updateNavigationCounts: updateNavigationCounts,
        sortPatientListByLocation: sortPatientListByLocation,
        triggerAppNotification: triggerAppNotification,
        getSafeString: getSafeString,
        getAppConfig: getAppConfig,
        getCachedAppConfig: getCachedAppConfig,
        getCurrentEnvironment: getCurrentEnvironment,
        getCurrentApiVersion: getCurrentApiVersion,
        setModuleRootVueInstance: setModuleRootVueInstance,
        getModuleRootVueInstance: getModuleRootVueInstance,
        destroyModuleRootVueInstance: destroyModuleRootVueInstance,
        isHpcaAuthTokenType: isHpcaAuthTokenType,
        getUnreadCountsForAllNetworks: getUnreadCountsForAllNetworks,
        getUnreadCountsForSingleNetwork: getUnreadCountsForSingleNetwork,
        checkIfFeatureIsEnabledInAppConfig: checkIfFeatureIsEnabledInAppConfig,
        setOpenedPatientContextWindow: setOpenedPatientContextWindow,
        closeOpenedPatientContextWindow: closeOpenedPatientContextWindow,
        isTpiIntegrationSession: isTpiIntegrationSession,
        setIsRefreshingAccessToken: setIsRefreshingAccessToken,
        getIsRefreshingAccessToken: getIsRefreshingAccessToken,
        awaitIsRefreshingAccessToken: awaitIsRefreshingAccessToken,
        setLastCachedValidPatientMedwayLookup: setLastCachedValidPatientMedwayLookup,
        getLastCachedValidPatientMedwayLookup: getLastCachedValidPatientMedwayLookup,
        userHasNetworkAdministrationViewAccess: userHasNetworkAdministrationViewAccess,
        checkOpenPatientMedwayApiRequest: checkOpenPatientMedwayApiRequest,
        setOpenPatientMedwayApiRequest: setOpenPatientMedwayApiRequest,
        clearOpenPatientMedwayApiRequest: clearOpenPatientMedwayApiRequest,
        awaitOpenPatientMedwayApiRequest: awaitOpenPatientMedwayApiRequest,
    };
}());

export default appMain;

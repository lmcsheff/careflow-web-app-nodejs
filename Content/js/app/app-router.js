﻿/* Utils */
import PubSub from 'Endpoints/careflowApp/PubSub';
import Vault from 'Endpoints/careflowApp/Vault';

/* API Endpoints */

/* Views */
import * as NetworkHome from 'Views/NetworkHome';
//import groupHome from 'Views/GroupHome';
import * as TeamHome from  'Views/TeamHome'
import * as PatientSearch from 'Views/PatientSearch';
import alertSubscriptions from 'Views/AlertSubscriptions';
import * as Alerts from 'Views/Alerts';
import Messages from 'Views/Messages';
import * as MessagesNew from 'Views/Messages__NEW';
import * as Tasks from 'Views/Tasks';
import Profiles from 'Views/Profiles';
import ItSupport from 'Views/ItSupport';
import * as SignIn from 'Views/SignIn';
import * as AppIntegrationApi from 'Views/AppIntegrationApi'
import * as NetworkAdministration from 'Views/NetworkAdministration'
import * as NetworkUpdates from 'Views/NetworkUpdates'
import * as AppMainNavigation from 'Views/AppMainNavigation'
import biReports from 'Views/BiReports';
import appMain from './app-main';
//import { url } from 'inspector';
//import * as teamReferrals from '../views/TeamReferrals';

let appRouter = appRouter || (function() {
    //Shared
    let _careflowAppRouter;
    let _routeHistory = []; //Maintain a crude history of view slugs (controllers)
    let _urlHistory = []; //maintain alert track off all views user visited in session
    let nonAuthRoutes = ['/#/SignIn', '/#/Integration', '/#/Register'] //Routes that we want to handle outside of usual callback

    let _initAppRouter = function() { //Self executing
        return $.Deferred(function() {
            let self = this;

            _careflowAppRouter = $.sammy(function() {
                let that = this;
                this.element_selector = ".js-app-body"; //The element sammy will render for content (context)
                this.app_container = $(".js-app"); //Useful app wrapper API

                this.use('Handlebars', 'htm'); // include handlebars for templating via router

                let userIsChecked = false; //When app starts or a browser hard refresh refresh occurs, set false. Else set for session

                function checkLoggedIn(callback) {

                    let routeLocation = that.getLocation().toLowerCase();

                    let checkNonAuthRoute = function(url) { //Just a loose check
                        return nonAuthRoutes.filter(x => url.toLowerCase().includes(x.toLowerCase())).length > 0;
                    }

                    //Non auth required routes
                    if (checkNonAuthRoute(routeLocation)) {

                        if (routeLocation.includes('/#/signin')) {
                            return appMain.isUserLoggedIn().then(result => {
                                if (result) { //User is logged in
                                    userIsChecked = true;
                                    appMain.appTimeoutCounter(); //Start the timeout heartbeat
                                    return _careflowAppRouter.setLocation("/#/Home"); //Will call around() again
                                } else {
                                    appMain.checkShims().then(() => { //Fire the router, based on location set as /#/SignIn and a logged out user
                                        var userTimedOutFlag;
                                        if ($.jStorage.get("appUrl")) userTimedOutFlag = $.jStorage.get("appUrl"); //Retain flag if they timed out
                                        Vault.flush();
                                        if (userTimedOutFlag) $.jStorage.set("appUrl", userTimedOutFlag);
                                        _urlHistory.push(that.getLocation());
                                        return callback(); //Fire the router, already at /#/signin
                                        //  appMain.getGlobalContextActionsUi();
                                    });
                                }
                            });
                        }

                        return appMain.isUserLoggedIn().then(result => {
                            if (result) { //User is logged in
                                userIsChecked = true;
                                appMain.appTimeoutCounter(); //Start the timeout heartbeat
                                return callback(); //Fire the route
                            } else {
                                return callback(); //Fire the route
                            }
                        });
                    }

                    //Regular routes
                    if (!userIsChecked) {
                        return appMain.isUserLoggedIn().then(result => {
                            if (result) { //User is logged in
                                userIsChecked = true;
                                appMain.appTimeoutCounter(); //Start the timeout heartbeat
                                appMain.checkShims().then(function() {
                                    AppMainNavigation.initAppMainNavigation();
                                    callback(); //Fire the router
                                    _urlHistory.push(_careflowAppRouter.getLocation());
                                    _routeHistory.push(_getRouteSlug(_careflowAppRouter.getLocation().toLowerCase(), 2)); //Can be overwritten at view level
                                    //  appMain.getGlobalContextActionsUi();
                                });
                            } else {
                                return window.location = "/";
                            }
                        });
                    } else { //User is checked
                        return appMain.isUserLoggedIn().then(result => { //Always check logged in - if user deletes session items in browser, this will catch it
                            if (result) { //User is logged in
                                if (routeLocation === "/#/signin") {
                                    _careflowAppRouter.setLocation("/#/Home");
                                } else { //Run requested route
                                    appMain.checkShims().then(function() {
                                        AppMainNavigation.initAppMainNavigation();
                                        callback(); //Fire the router
                                        _urlHistory.push(_careflowAppRouter.getLocation());
                                        _routeHistory.push(_getRouteSlug(_careflowAppRouter.getLocation().toLowerCase(), 2)); //Can be overwritten at view level
                                        // appMain.getGlobalContextActionsUi();
                                    });
                                }
                            } else {
                                window.location = "/";
                            }
                        });
                    }
                };

                this.around(checkLoggedIn); //Fired around every route http://sammyjs.org/docs/api/0.6.3/all#Sammy.Application-around

                //Helpers
                this.helpers({
                    // define a helper to check if a member can join another group
                    checkIfUserCanJoinTeam: function(networkId, memberAreaId) {
                        //Get the last location before memberarea id requested
                        console.log(this);

                        var lastLocation = null;

                        if (this.getLastURL()) {
                            lastLocation = this.getLastURL();
                            //Silently set the url back to the last location
                            //history.replaceState({}, '', lastLocation);
                        }

                        //Firstly, check the network is valid for the user
                        if (!appMain.isNetworkValidForUser(networkId, true)) {
                            _careflowAppRouter.setLocation("#/Home");
                            return false;
                        }

                        let allNetworkGroups = Vault.getItem("ActiveNetworkGroupMemberships")["Groups"] || [];

                        //Check if its a team the user is not a member of
                        var i;
                        for (i = 0; i < allNetworkGroups.length; ++i) {
                            if (allNetworkGroups[i].MemberAreaID === memberAreaId && allNetworkGroups[i].MembershipStatus !== 'Member') {
                                return appMain.getRequestToJoinTeamModal(allNetworkGroups[i], lastLocation); //match found, break loop
                            }
                        }
                        //Nothing found, bounce the user home as memberarea ID passed is invalid for user
                        _careflowAppRouter.setLocation("#/Home");
                    },

                    getLastURL: function() {
                        var lastRouteCount = _urlHistory.length;
                        return _urlHistory[lastRouteCount - 2] || false; //Take prvious to newly pushed (current). 0 index, so -2
                    },
                    getSpecificURL: function(urlIndex) {
                        var lastRouteCount = _urlHistory.length;
                        return _urlHistory[lastRouteCount - urlIndex] || false;
                    },
                    getRoute: function() {
                        return _careflowAppRouter.getLocation();
                    }
                });

                /*Routes*/
                //APP_EVENTS: ['run', 'unload', 'lookup-route', 'run-route', 'route-found', 'event-context-before', 'event-context-after', 'changed', 'error', 'check-form-submission', 'redirect', 'location-changed'],

                this.bind('run-route', function(context) { //Clean up UI before ALL new routes
					VueEventBus.$emit('Router.NewRouteSet',context);
                    appMain.closeOpenedPatientContextWindow(); //Close any patient context windows

                    appMain.cleanUpUiElementsOnRouteChange();
                    PubSub.publish("CareflowApp.Router.RouteCleanup", { Data: context, Route: _careflowAppRouter.getLocation().toLowerCase() });

                    if (window.ga) { //https://www.miguel.nz/blog/how-to-use-google-analytics-with-vue-cli-and-webpack/
                        ga('send', {
                            hitType: 'pageview',
                            page: window.location.hash.slice(1),
                            location: window.location.hash.slice(1),
                            title: window.location.hash.slice(1)
                        });
                        //ga('set', 'page', window.location.hash.slice(1));
                        //ga('send', 'pageview');
                    }

                    //Specific route actions (local array)
                    //var lastRouteCount = _routeHistory.length;
                    //var lastRoute = _routeHistory[lastRouteCount - 1]; //Take prvious to newly pushed (current)

                    //if (lastRoute === "patient") { //TODO: REFACTOR PER NETWORKHOME CLEANUP METHODS
                    //    patientView.viewCleanup();
                    //}
                });

                this.bind('event-context-after', function(context) {
                    if (!userIsChecked) return;
                });

                this.bind('location-changed', function(context) {
                    PubSub.publish("CareflowAppRouter.LocationChanged", { Data: context });
                });

                this.notFound = function() {

                    appMain.isUserLoggedIn().then(function(isLoggedIn) {
                        if (isLoggedIn) {
                            console.error("route not found");
                            _careflowAppRouter.setLocation("#/Home");
                        } else {
                            window.location.href = "/";
                        }
                    });
                };

                this.get('/#/:networkId/Teams/:id/TeamTags', function(context) {
                    let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "ManageGroupClinicalTags")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, null ,'Manage team tags', 'TeamTags')
						} else {

							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level

								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, null, 'Manage team tags', 'TeamTags');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });
                });

                this.get('/#/:networkId/Teams/:id/TeamSmartLists', function(context) {
                    let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "ModifyAutoPopDefinitions")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, null ,'Manage smart lists', 'TeamSmartLists')
						} else {

							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level

								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, null, 'Manage smart lists', 'TeamSmartLists');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });
                });

                this.get('/#/:networkId/Teams/:id/Patients(/:patientid)?', function(context) { //Slim view of patient list in team context
                    var optionalParams = this.params['splat']; //http://stackoverflow.com/questions/15876112/sammyjs-optional-parameters
					var patientId = optionalParams[0] || null;
					let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "SearchForPatient")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, patientId ,false, 'PatientList')
						} else {
							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level
								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, patientId, false, 'PatientList');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });


                });



                this.get('/#/:networkId/Teams/:id/PatientListHandovers(/:patientid)?', function(context) {
                    var optionalParams = this.params['splat']; //http://stackoverflow.com/questions/15876112/sammyjs-optional-parameters

					let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "SearchForPatient")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, null ,false, 'PatientHandoverList')
						} else {

							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level

								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, null, false, 'PatientHandoverList');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });
                });

                this.get('/#/:networkId/Teams/:id/TeamProfiles', function(context) {

					let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "ViewProfilesForTeam")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, null ,'Manage list profiles', 'TeamProfiles')
						} else {

							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level

								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, null, 'Manage list profiles', 'TeamProfiles');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });
                });

                this.get('/#/:networkId/Teams/:id/TeamReferrals', function(context) {
					let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "ReadContent")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, null ,'Referrals', 'TeamReferrals')
						} else {

							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level

								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, null, 'Referrals', 'TeamReferrals');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });
                });

                this.get('/#/:networkId/Teams/:id/Tasks', function(context) {
					let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "ReadContent")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, null ,false, 'TeamTasks')
						} else {

							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level

								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, null, false, 'TeamTasks');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });
                });

                this.get('/#/:networkId/Teams/:id/TeamUpdates', function(context) {
                    let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "ReadContent")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, null ,'Team updates', 'TeamUpdates')
						} else {

							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level

								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, null, 'Team updates', 'TeamUpdates');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });
                });

                this.get('/#/:networkId/Teams/:id/TeamDirectory', function(context) {
					let networkId = parseInt(this.params['networkId']);
					let memberAreaId = parseInt(this.params['id']);

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId), appMain.isMemberAreaRouteValidForUser(memberAreaId, "ReadContent")]).then(responses => {
						let a = responses[0], b = responses[1];
						//Check master view status
						let vue = TeamHome.getViewStatus();
						if (vue) {
							return vue.setActiveView(networkId, memberAreaId, null ,'Team updates', 'TeamDirectory')
						} else {

							//Init View
							appMain.setPageLayout().then(() => { //Set view layout View level

								context.partial('/Content/js/handlebarsTemplates/teamHome/TeamHomeMaster.htm').then(() => {
									TeamHome.init(networkId, memberAreaId, null, 'Team updates', 'TeamDirectory');
								});

                            });

						}
                    }).catch(error => {
                        this.checkIfUserCanJoinTeam(networkId, memberAreaId);
                    });
                });

                this.get('/#/:networkId/Teams/:id', function(context) { //Group Home
                    var networkId = parseInt(this.params['networkId']);
                    var memberAreaId = parseInt(this.params['id']);
					return _careflowAppRouter.setLocation("#/" + networkId + "/Teams/" + memberAreaId + "/TeamUpdates");
                });

                this.get(/\#\/(.*)\/PatientSearch\/(.*)/, function(context) {
                    const result = this.params['splat'];
                    const networkId = parseInt(result[0]) || null;
                    const optionalParams = result[1].split('/') || [];
                    const searchTerm = optionalParams[0] || null;
                    const patientId = optionalParams[1] || null;
                    const configObj = optionalParams[2] || null; //Base64 expected - optional config, e.g. Handover Audit fab link to deep link summary tab view

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId, "SearchForPatient")]).then((responses) => {
                        var a = responses[0];
                        appMain.setPageLayout().then(function() { //Set view layout View level
                            context.partial('/Content/js/handlebarsTemplates/patientSearch/NetworkPatientSearchWrapper.htm').then(function() {
                                PatientSearch.init(networkId, searchTerm, patientId, configObj); //Remove the '/'
                            });
                        });
                    }).catch(function(error) {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                this.get('/#/:networkId/Alerts', function(context) {
                    var networkId = parseInt(this.params['networkId']);
                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then(function(responses) {
                        var a = responses[0];
                        appMain.setPageLayout().then(function() { //Set view layout View level
                            // render the template view and pass it through handlebars
                            context.partial('/Content/js/handlebarsTemplates/alerts/AlertsWrapper.htm').then(function() {
                                Alerts.init(networkId);
                            });
                        });
                    }).catch(function(error) {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                this.get('/#/:networkId/AlertSubscriptions', function(context) {
                    var networkId = parseInt(this.params['networkId']);
                    var that = this;
                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then(function(responses) {
                        var a = responses[0];
                        appMain.setPageLayout().then(function() { //Set view layout View level
                            // render the template view and pass it through handlebars
                            context.NetworkId = networkId;
                            context.partial('/Content/js/handlebarsTemplates/views/AlertSubscriptions.htm').then(function() {
                                alertSubscriptions.getUsersSubscriptions();
                            });
                        });
                    }).catch(function(error) {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                this.get("/#/(:networkid/)?Home", function(context) {
                        //var optionalParams = this.params['splat']; //http://stackoverflow.com/questions/15876112/sammyjs-optional-parameters
                        let that = this;
                        let networkId = (this.params.networkid) ? parseInt(this.params['networkid']) : null;

                        var checkNetwork = new Promise((resolve, reject) => {
                            if (networkId) {
                                appMain.isNetworkRouteValidForUser(networkId, null).then((response) => {
                                    resolve();
								}).catch(error => {
									//TODO: Bounce home? check user flows here - maybe null networkId and send to NetworkHome.initUserNetworkHome()?
                                    return appMain.getFatalErrorUserNotificationModal("You are not a member of any networks or teams, please contact your system administrator.");
                                });
                            } else {
                                resolve();
                            }
                        });

                        checkNetwork.then(() => {
                            appMain.setPageLayout().then(function() {
                                // render the template view and pass it through handlebars
                                context.partial('/Content/js/handlebarsTemplates/networkHome/NetworkHomeView.htm').then(() => {
                                    const userJustSignedIn = that.getLastURL() && that.getLastURL().toLowerCase() === "/#/signin";
                                    //NetworkHome model handles isNetworkRouteValidForUser() network checks and routing
                                    NetworkHome.initUserNetworkHome(networkId, !userJustSignedIn);
                                });
                            });

                        });
                    }),

					this.get("/#/:networkId/Network/Updates(/:contentItemId)?", function(context) {//https://stackoverflow.com/questions/15876112/sammyjs-optional-parameters

						let networkId = (this.params && this.params['networkId']) ? parseInt(this.params['networkId']) : Vault.getSessionItem('ActiveNetworkId');
						let contentItemId = (this.params && this.params['splat'].length) ? parseInt(this.params['splat'][0]) : null;
                        //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                        Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then((responses) => {
                            var a = responses[0];

                            if (this.getLastURL() && this.getLastURL().toLowerCase().includes('network/') && appMain.getModuleRootVueInstance()) {
                                let vue = appMain.getModuleRootVueInstance();
                                return vue.setRoute(networkId, "Updates from your organisation", 'updates');
                            }

                            appMain.setPageLayout().then(function() { //Set view layout View level
                                context.partial('/Content/js/handlebarsTemplates/views/NetworkUpdatesWrapper.htm').then(() => {
                                    NetworkUpdates.init(networkId, "Updates from your organisation", 'updates', contentItemId);
                                });
                            });
                        }).catch(error => {
                            console.error(error);
                            _careflowAppRouter.setLocation("/#/Home");
                        });
                    });

                this.get("/#/(:networkId/)?Network/Directory", function(context) {

                    let networkId = (this.params && this.params.networkId) ? parseInt(this.params.networkId) : Vault.getSessionItem('ActiveNetworkId');
                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then((responses) => {
                        var a = responses[0];

                        if (this.getLastURL() && this.getLastURL().toLowerCase().includes('network/') && appMain.getModuleRootVueInstance()) {
                            let vue = appMain.getModuleRootVueInstance();
                            return vue.setRoute(networkId, "Updates from your organisation", 'directory');
                        }

                        appMain.setPageLayout().then(function() { //Set view layout View level
                            // render the template view and pass it through handlebars
                            context.partial('/Content/js/handlebarsTemplates/views/NetworkUpdatesWrapper.htm').then(() => {
                                NetworkUpdates.init(networkId, '', 'directory');
                            });
                        });
                    }).catch(error => {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                // this.get("/#/:networkId/Directory", function(context) {
                //     var that = this;
                //     var networkId = parseInt(this.params['networkId']);
                //     //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                //     Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then(function(responses) {
                //         var a = responses[0];
                //         appMain.setPageLayout().then(function() { //Set view layout View level
                //             // render the template view and pass it through handlebars
                //             context.partial('/Content/js/handlebarsTemplates/directory/DirectoryWrapper.htm').then(function() {
                //                 Directory.initDirectory({ NetworkId: networkId });
                //             });
                //         });
                //     }).catch(function(error) {
                //         console.error(error);
                //         _careflowAppRouter.setLocation("#/Home");
                //     });
                // });

                this.get("/#/(:networkId/)?Messages", function(context) {
                    let networkId = (this.params && this.params.networkId) ? parseInt(this.params.networkId) : Vault.getSessionItem('ActiveNetworkId');
                    var that = this;
                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then(function(responses) {
                        var a = responses[0];
                        appMain.setPageLayout().then(function() { //Set view layout View level
                            // render the template view and pass it through handlebars
                            context.partial('/Content/js/handlebarsTemplates/views/Messages.htm').then(function() {
                                Messages.init();
                            });
                        });
                    }).catch(function(error) {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                this.get("/#/(:networkId/)?MessagesNew", function(context) { //Vue rebuild
                    let networkId = (this.params && this.params.networkId) ? parseInt(this.params.networkId) : Vault.getSessionItem('ActiveNetworkId');

                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then((responses) => {
                        let a = responses[0];
                        // render the template view and pass it through handlebars
                        context.partial('/Content/js/handlebarsTemplates/messages/MessagesWrapper.htm').then(function() {
                            MessagesNew.initMessages(networkId);

                        });
                    }).catch(function(error) {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                this.get("/#/(:networkId/)?MyTasks", function(context) {
                    let networkId = (this.params && this.params.networkId) ? parseInt(this.params.networkId) : Vault.getSessionItem('ActiveNetworkId');
                    //Promise.all is all or nothing. It resolves once all promises in the array resolve, or reject as soon as one of them rejects.
                    Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then(responses => {
                        appMain.setPageLayout().then(function() { //Set view layout View level
                            // render the template view and pass it through handlebars
                            context.partial('/Content/js/handlebarsTemplates/tasks/MyTasksWrapper.htm').then(function() {
                                Tasks.init(networkId, null, null, null);
                            });
                        });
                    }).catch(function(error) {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                this.get("/#/SignIn(/:guid)?", function(context) {
                    appMain.setPageLayout("blank-hero").then(() => {
                        // render the template view and pass it through handlebars
                        let guid = (this.params && this.params.guid) ? this.params.guid.substring(1) : null;
                        context.partial('/Content/js/handlebarsTemplates/auth/AuthAndRegisterWrapper.htm').then(function() {
                            SignIn.initSignIn(guid, false);
                        });
                    });
                });

                this.get("/#/Register(/:guid)?", function(context) {
                    appMain.setPageLayout("blank-hero").then(() => {
                        // render the template view and pass it through handlebars
                        let guid = (this.params && this.params.guid) ? this.params.guid.substring(1) : null;
                        context.partial('/Content/js/handlebarsTemplates/auth/AuthAndRegisterWrapper.htm').then(function() {
                            SignIn.initSignIn(guid, true);
                        });
                    });
                });

                this.get("/#/Integration", context => {
                    appMain.isUserLoggedIn().then(result => { //Always check logged in - if user deletes session items in browser, this will catch it
                        if (result) { //User is logged in
                            _careflowAppRouter.setLocation("/#/Home");
                        } else {
                            appMain.setPageLayout("blank-hero").then(() => {
                                context.partial('/Content/js/handlebarsTemplates/integrations/IntegrationSplashPageWrapper.htm').then(function() {
                                    AppIntegrationApi.init();
                                });
                            });
                        }
                    });
                });

                this.get('/#/Profile(/:userId)?', function(context) {
                    var userId = parseInt(this.params['userId'].slice(1));

                    appMain.setPageLayout().then(function() { //Set view layout View level
                        // render the template view and pass it through handlebars
                        context.partial('/Content/js/handlebarsTemplates/views/Profiles.htm').then(function() {
                            Profiles.init(userId);
                        });
                    });
                });
                /*Network Admin*/
                this.get("/#/(:networkId/)?Administration", (context) => {
                    let networkId = (this.params && this.params.networkId) ? parseInt(this.params.networkId) : Vault.getSessionItem('ActiveNetworkId');

                    appMain.isNetworkRouteValidForUser(networkId, null).then(response => {
                        appMain.setPageLayout().then(() => { //Set view layout View level

                            if (!appMain.userHasNetworkAdministrationViewAccess()) return _careflowAppRouter.setLocation("#/Home");

                            context.partial('/Content/js/handlebarsTemplates/views/NetworkAdministrationWrapper.htm').then(function() {
                                NetworkAdministration.init(networkId);
                            });
                        });
                    }).catch((error) => {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                this.get("/#/:networkId/ITsupport", function(context) {

                    var networkId = parseInt(this.params['networkId']) || Vault.getSessionItem("ActiveNetworkId");

                    Promise.all([appMain.isNetworkRouteValidForUser(networkId, null)]).then((responses) => {
                        if (Vault.getItem("GetRequestingUserSummary") && _.has(Vault.getItem("GetRequestingUserSummary")["Data"], "IsItSupport") && Vault.getItem("GetRequestingUserSummary")["Data"]["IsItSupport"]) {
                            appMain.setPageLayout().then(function() {
                                context.partial('/Content/js/handlebarsTemplates/itSupport/itSupportView.htm').then(function() {
                                    ItSupport.init();
                                });
                            });
                        } else {
                            window.location = "/";
                        }
                    }).catch(function(error) {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });

                /*Power BI test -for demo only - (ehi ONLY)*/
                this.get("/#/:networkId/Dashboards(/:reportId)?", function(context) {

                    var optionalParams = this.params['splat'];
                    var networkId = parseInt(this.params['networkId']) || Vault.getSessionItem("ActiveNetworkId");
                    var reportId = optionalParams[0] || null;
                    let userHasDashboardsPermission = appMain.getUserPermissionsForNetwork(networkId).includes('InviteMemberFromCareflowDirectory'); //Temp permission - API pending
                    let env = appMain.getCurrentEnvironment().toLowerCase();

                    appMain.isNetworkRouteValidForUser(networkId, null).then(appMain.setPageLayout()).then(function() {
                        context.UserHasDashboardsPermission = userHasDashboardsPermission;
                        context.Environment = env;
                        context.partial('/Content/js/handlebarsTemplates/views/BIreports.htm').then(function() {
                            if (userHasDashboardsPermission) {
                                biReports.init(reportId, networkId);
                            } else {
                                _careflowAppRouter.setLocation("#/Home");
                            }
                        });
                    }).catch(function(error) {
                        console.error(error);
                        _careflowAppRouter.setLocation("#/Home");
                    });
                });


            }); //End router setup

            self.resolve(); //Return deferred
        });
    }();

    var _getRouter = function() {
        return $.Deferred(function() {
            var self = this;
            _careflowAppRouter.run("/#/SignIn"); //Start the application
            self.resolve();
        });
    };

    var _getRoute = function() {
        return _careflowAppRouter.getLocation();
    };
    var _getRouteSlug = function(route, slugLocation) {
        var fullRoute = _careflowAppRouter.getLocation().toLowerCase();
        var splitRoute = fullRoute.split('/');
        return splitRoute[slugLocation];
    };

    return {
        careflowAppRouter: _careflowAppRouter, //Router instance
        getRouter: _getRouter, //Start the application
        getRoute: _getRoute
    };
}());

export default appRouter;

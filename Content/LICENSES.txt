{
    "featherlight@1.5.0": {
        "licenses": [
            "MIT"
        ]
    },
    "datatables.net@undefined": {
        "licenses": [
            "MIT"
        ],
        "homepage": "https://datatables.net"
    },
    "imagesloaded@undefined": {
        "licenses": [
            "MIT"
        ],
        "homepage": "http://imagesloaded.desandro.com"
    },
    "handlebars@1.3.0": {
        "licenses": "UNKNOWN"
    },
    "jquery@1.11.1": {
        "licenses": [
            "MIT"
        ]
    },
    "parsleyjs@undefined": {
        "licenses": [
            "MIT"
        ]
    },
    "nprogress@0.2.0": {
        "licenses": [
            "MIT"
        ]
    },
    "moment@undefined": {
        "licenses": [
            "MIT*"
        ]
    },
    "underscore@1.8.3": {
        "licenses": [
            "MIT*"
        ]
    },
    "At.js@1.5.1": {
        "licenses": [
            "MIT"
        ],
        "repository": "git+https://github.com/ichord/At.js"
    },
    "waypoints@undefined": {
        "licenses": [
            "MIT*"
        ]
    },
    "bootstrap-tagsinput@0.5.0": {
        "licenses": [
            "MIT"
        ],
        "homepage": "https://github.com/TimSchlechter/bootstrap-tagsinput",
        "repository": "https://github.com/timschlechter/bootstrap-tagsinput"
    },
    "Caret.js@0.2.2": {
        "licenses": [
            "MIT"
        ]
    },
    "dragula.js@3.7.1": {
        "licenses": [
            "MIT"
        ],
        "homepage": "https://github.com/bevacqua/dragula",
        "repository": "git+https://github.com/bevacqua/dragula"
    },
    "x-editable@1.5.1": {
        "licenses": "UNKNOWN"
    },
    "ev-emitter@undefined": {
        "licenses": [
            "MIT"
        ],
        "homepage": "https://github.com/metafizzy/ev-emitter",
        "repository": "git+https://github.com/metafizzy/ev-emitter"
    },
    "sammy@0.7.6": {
        "licenses": [
            "MIT"
        ],
        "homepage": "http://sammyjs.org"
    },
    "qtip2@3.0.3": {
        "licenses": [
            "MIT"
        ],
        "repository": "https://github.com/qTip2/qTip2",
        "homepage": "http://qtip2.com"
    },
    "select2@undefined": {
        "licenses": [
            "MIT"
        ],
        "repository": "https://github.com/select2/select2"
    },
    "swag@0.7.0": {
        "licenses": [
            "MIT"
        ],
        "repository": "git+https://github.com/elving/swag"
    },
    "typeahead.js@0.11.1": {
        "licenses": [
            "MIT*"
        ],
        "repository": "git+https://github.com/twitter/typeahead.js"
    },
    "underscore.string@3.3.4": {
        "licenses": [
            "MIT"
        ],
        "repository": "git+https://github.com/epeli/underscore.string",
        "homepage": "http://epeli.github.com/underscore.string/"
    },
    "json2@undefined": {
        "licenses": "UNKNOWN",
        "homepage": "https://github.com/douglascrockford/JSON-js"
    },
    "handlebars.js@4.0.5": {
        "licenses": [
            "MIT"
        ],
        "homepage": "https://github.com/wycats/handlebars.js",
        "repository": "git+https://github.com/wycats/handlebars.js"
    },
    "modernizr@3.3.1": {
        "licenses": [
            "MIT"
        ],
        "homepage": "https://github.com/Modernizr/Modernizr",
        "repository": "https://github.com/Modernizr/Modernizr"
    },
    "jStorage@0.4.12": {
        "licenses": [
            "MIT*"
        ]
    }
}
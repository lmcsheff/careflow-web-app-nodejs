﻿const webpack = require('webpack');
const path = require('path');
const pkg = require('./package.json');
//const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const { VueLoaderPlugin } = require('vue-loader')
//const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
// const extractLess = new ExtractTextPlugin({
//     filename: 'main.[chunkhash].css',
// });
const stylus_plugin = require('stylus');
//const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require("clean-webpack-plugin");

//let PROD = JSON.parse(process.env.PROD_ENV || '0');//Uses .env file
let PROD = false;
const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
    //mode: 'production',
    mode: 'development',
    entry: ["babel-polyfill", path.resolve('./Content/js/entry.js')],
    resolve: {
        alias: {
            jquery: 'jquery/src/jquery',
            sammy: 'sammy/lib/min/sammy-latest.min.js',
            select2: 'select2/dist/js/select2.full.js',
            modernizr$: path.resolve(__dirname, ".modernizrrc"),
            waypoints: 'waypoints/lib/jquery.waypoints.js',
            handlebars: 'handlebars/dist/handlebars.min.js',
            vue: 'vue/dist/vue.js',
            AppJs: path.resolve(__dirname, 'Content/js/app'),
            Endpoints: path.resolve(__dirname, 'Content/js/endPoints'),
            Views: path.resolve(__dirname, 'Content/js/views'),
            Less: path.resolve(__dirname, 'Content/less'),
            VueComponents: path.resolve(__dirname, 'Content/js/vue/components'),
            VueMixins: path.resolve(__dirname, 'Content/js/vue/vue-mixin')
        },
        extensions: ['*', '.js', '.vue', '.json']
    },
    devtool: 'source-map',
    output: {
        path: path.resolve('./dist'),
        //filename: 'main.bundle.js'
        filename: 'main.bundle.[chunkhash].js'
	},
	node: {
		__dirname:false,
	},
    module: {
        rules: [{
                test: /\.vue$/,
                loader: 'vue-loader',
                //options: {
                    //hotReload: true, // disables Hot Reload
                    //loaders: {}
                    // other vue-loader options go here
                //}
            },

            {
                test: /\.js$/,
				loader: 'babel-loader',
                exclude: [
                    path.resolve(__dirname, "node_modules"),
                    path.resolve(__dirname, "Content/js/app/plugins")
                ],

                loader: "babel-loader"
            },
            {
                test: /\.modernizrrc$/,
                loader: "modernizr-loader!json-loader"
            },
            {
                test: /\.htm/,
                loader: 'handlebars-loader',
                query: {
                    partialResolver: function(partial, callback) {
                        callback(null, path.resolve('./Content/js/handlebarsTemplates/_' + partial + '.htm'));
                    },
                    helperDirs: [path.resolve('./Content/js/app/handlebars-helpers')],
                }
            },
            // {
            //     test: /\.css/,
            //     use: extractLess.extract({
            //         use: [{
            //             loader: 'css-loader?url=false',
            //         }, {
            //             loader: 'less-loader'
            //         }],
            //         fallback: 'style-loader',
            //     })
            // },
            {
                test: /\.less$/,
                use: ['style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    //'postcss-loader',
                    'less-loader'
                ]

            },

            {
                test: /\.css/,
                use: [MiniCssExtractPlugin.loader,
                    'css-loader?url=false',
                    //'postcss-loader',
                    'less-loader',
                ]
            },

            // {
            //     test: /\.less$/,
            //     use: extractLess.extract({
            //         use: [{
            //             loader: 'css-loader?url=false'
            //         }, {
            //             loader: 'less-loader'
            //         }],
            //         fallback: 'style-loader'
            //     })
            // },
            //{
            //    test: /\.styl$/i,
            //    use: [
            //        'style-loader',
            //        'css-loader?url=false',
            //        'stylus-loader'
            //    ]
            //},
            //{
            //    test: /\.scss/,
            //    loader: 'sass-resources-loader',
            //    options: {
            //        resources: path.resolve('./Content/sass/variables.scss')
            //    }
            //},
            // All font files will be handled here
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [{
                    loader: "file-loader"
                }]
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        useRelativePath: true,
                        limit: 8000, // Convert images < 8kb to base64 strings
                        name: '[name].[ext]' //Working dir is dist
                    }
                }]
            }
        ],
    },
    //plugins: [extractLess],
    plugins: PROD ? [
        // Clean dist folder.
        new CleanWebpackPlugin(["./dist"], {
            "verbose": true // Write logs to console.
        }),
        new MiniCssExtractPlugin({
            filename: 'main.[chunkhash].css',
        }),
        stylus_plugin,
        new VueLoaderPlugin(),
        //pluginsWebpack,
        // Ignore all locale files of moment.js - https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

        // new UglifyJsPlugin({
        //     "uglifyOptions": {
        //         compress: {
        //             drop_console: true,
        //             warnings: false,
        //             //screw_ie8: true,
        //             conditionals: true,
        //             unused: true,
        //             comparisons: true,
        //             sequences: true,
        //             dead_code: true,
        //             evaluate: true,
        //             if_return: true,
        //             join_vars: true
        //         },
        //         sourceMap: false
        //     }
        // }),


        // new webpack.optimize.UglifyJsPlugin({
        //     compress: {

        //     },
        //     output: {
        //         comments: false
        //     }
        // }),
        new HtmlWebpackPlugin({
            inject: "body",
            filename: __dirname +'/views/workspaceindex.html',
            template: __dirname +'/app/views/workspaceindex_template.html'
        })
    ] : [
        // new webpack.optimize.CommonsChunkPlugin({
        // 	name: 'vendor',
        // 	minChunks: ({ resource }) => /node_modules/.test(resource),
        //   })
        // Clean dist folder.
        new CleanWebpackPlugin(["./dist"], {
            "verbose": true // Write logs to console.
        }),
        new MiniCssExtractPlugin({
            filename: 'main.[chunkhash].css',
        }),
        stylus_plugin,
        new VueLoaderPlugin(),
        //new BundleAnalyzerPlugin(), //Analyze bundle
        //pluginsWebpack,
        // Ignore all locale files of moment.js - https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new webpack.optimize.ModuleConcatenationPlugin(),
        // new UglifyJsPlugin({
        //     "uglifyOptions": {
        //         compress: {
        //             drop_console: false,
        //             warnings: true,
        //             //screw_ie8: true
        //         },
        //         sourceMap: false
        //     }
        // }),
        new HtmlWebpackPlugin({
            inject: "body",
			filename: path.resolve(__dirname+'/views/workspaceindex.html'),
            template: path.resolve(__dirname+'/app/views/workspaceindex_template.html'),
            minify: {
                collapseWhitespace: true,
                collapseInlineTagWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true
            }
        })
    ],

};

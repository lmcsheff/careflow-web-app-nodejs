const crypto = require('crypto');
const axios = require('axios');
const querystring = require('querystring');
const express = require('express');
const bodyParser = require('body-parser'); //EXPRESS MIDDLEWARE TO PARSE POST JSON - https://github.com/expressjs/body-parser#bodyparserjsonoptions
const csurf = require('csurf');
const cookieParser = require('cookie-parser');
const app = express();
const es6Renderer = require('express-es6-template-engine');
const ENCRYPTION_KEY = process.env.ENCRYPTION_KEY || 'XwPp9xazJ0ku5CZnlmgAx2Dld8SHkAeT'; // Must be 256 bytes (32 characters) - TODO: LOCAL dotenv file for dev
const HCPA_API_URL = process.env.HCPA_API_URL || 'https://coret1.syhapp.com/vlive/hpca/oauth/token'; 
const API_AUTH_BASE_URL = process.env.API_AUTH_BASE_URL || 'https://appdevauth.careflowapp.com';


app.engine('html', es6Renderer); //HTML TEMPLATE RENDERING ENGINE
app.set('views', __dirname + '/views'); //render express html templates views directory
app.set('view engine', 'html');

//app.use(express.static('dist'))
//app.use(express.static(__dirname + 'dist'))
app.use('/dist', express.static(__dirname + '/dist'));
app.use('/config', express.static(__dirname + '/config'));
app.use('/Content', express.static(__dirname + '/Content'));
//app.use('../../Content', express.static(__dirname + '/Content'))
//app.use('/Content/', express.static(__dirname + '/Content'))
const csrfMiddleware = csurf({
    cookie: true
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ //The 'body-parser' middleware only handles JSON and urlencoded data, not multipart (e.g regular POST forms)
    extended: true
}));
// parse application/json
app.use(bodyParser.json()) //https://github.com/expressjs/body-parser#bodyparserjsonoptions

app.use(cookieParser());
app.use(csrfMiddleware);

var server = require('http').createServer(app);
var io = require('socket.io').listen(server, { resource: '/appDirectory/socket.io' });

var port = process.env.PORT || 3000;

// var server = http.createServer(function(request, response) {
//   response.writeHead(200, { 'Content-Type': 'text/plain' });
//   response.end('Hi again there CF World! ' + port);
//   console.log('HELLO!');
//   console.log('1. Server running at http://localhost:%s', port);
// });


app.get('/', function(req, res) {
    //res.sendFile(__dirname + '/index.html');
    res.render('workspaceindex', { locals: { csrfToken: req.csrfToken() } }); //rendere for templates, sendFile just to return html, send returns inline template
});

app.post('/entry', (req, res) => { //POST endpoint
    console.log(`Message received: ${req.body.message}`);
    res.send(`CSRF token used: ${req.body._csrf}, Message received: ${req.body.message}`);
});

app.post('/authenticate', (req, res) => {
    console.log(`POST received: ${req.body.emailAddress}`);
    //res.json(req.body);
    //Call careflow auth
    axios.post(`${API_AUTH_BASE_URL}/Home/Login`, querystring.stringify({
            response_type: req.body.response_type,
            redirect_uri: req.body.redirect_uri,
            client_id: req.body.client_id,
            emailAddress: req.body.emailAddress,
            password: req.body.password
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cache-Control': 'no-cache'
            }
        })
        .then(response => {           
            
            res.set(response.headers)
            res.set('refresh_token', encrypt(response.headers['refresh_token']))
            res.status(response.status).send(response.data);
        })
        .catch(err => {
            console.log(err.response.data);
            res.status(err.response.status).send(err.response.data);

        });

    //res.send('POST request to the homepage')
})


app.post('/refreshtoken', (req, res) => {
    console.log(`POST received: ${req.body.lastName}`);
    //res.json(req.body);
    //Call careflow auth
    axios.post(`${API_AUTH_BASE_URL}/Home/Login/RefreshToken`, querystring.stringify({
            refresh_token: '{"Token":"41c3e78bb4c74c2291116c0b43472e8d","Provider":"HPCA"}'
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cache-Control': 'no-cache'
            }
        })
        .then(response => {
            //console.log(`statusCode: ${response.statusCode}`)
            //console.log(response.data);
            console.log(response.headers['refresh_token'])
            res.set(response.headers)
            res.set('refresh_token', encrypt(response.headers['refresh_token']))
            res.status(response.status).send(response.data);
        })
        .catch(err => {
            console.log(err.response.data);
            res.status(err.response.status).send(err.response.data);

        });

    //res.send('POST request to the homepage')
})

app.post('/handovertoken', (req, res) => {
	console.log(`handovertoken POST received: ${req.body.token}`);
	console.log(querystring.stringify(req.body));
	axios.post(`${HCPA_API_URL}`, querystring.stringify({
		grant_type: req.body.type,
		handover_token: req.body.token
	}), {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Cache-Control': 'no-cache'
			}
		})
		.then(response => {
			console.log(response.data);
			res.set(response.headers)		
			res.set('refresh_token', encrypt(response.data.refresh_token)); 
			res.set('access_token', JSON.stringify({Token: response.data.access_token, Provider: "HPCA" })); 
			res.status(response.status).send(response.data);
		})
		.catch(err => {
			// console.log(err);
			// console.log(err.response);
			// console.log(err.response.data);
			res.status(err.response.status).send(err.response.data);

		});
});

io.on('connection', function(socket) {
    io.set('transports', ['websocket']);
    console.log('new connection on socket.io');
    socket.emit("welcome", "SOCKET.IO: ENCRYPT: " + encrypt('welcome'));
    socket.on('chat message', function(msg) {
        console.log(port);
        console.log('message from client: ' + msg);
        console.log('decrypt: ' + decrypt('dd995288bb1faf7665f9a8e02a2704ca:3fe2106e26521f761c8b0b10a05925ba'));
        socket.emit("messageback", 'i, the server,  received your message thanks - it was this: ' + msg);
    });
});

server.listen(port);
console.log(__dirname) //LOGS: D:\home\site\wwwroot
console.log('2. Server running at http://localhost:%s', port);



/*Encryption module*/
const IV_LENGTH = 16; // For AES, this is always 16


function encrypt(text) {
    let iv = crypto.randomBytes(IV_LENGTH);
    let cipher = crypto.createCipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
    let encrypted = cipher.update(text);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decrypt(text) {
    let textParts = text.split(':');
    let iv = new Buffer(textParts.shift(), 'hex');
    let encryptedText = new Buffer(textParts.join(':'), 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
    let decrypted = decipher.update(encryptedText);

    decrypted = Buffer.concat([decrypted, decipher.final()]);

    return decrypted.toString();
}





// var express = require('express');
// var app = express();
// var server = require('http').createServer(app);
// io = require('socket.io').listen(server, { resource : '/appDirectory/socket.io' });
// var port = process.env.PORT || 3000;
// app.use(express.static('public'));

// app.get('/', function(req, res) {
//  res.sendFile(__dirname + '/public/default.html');
// });

// io.on('connection', function(socket) {
//     io.set('transports', ['websocket']);
// 	console.log('new connection on socket.io');
// 	socket.emit("welcome", "welcome");
// 	socket.on('chatMessage', function (msg) {
// 				console.log(port);
// 				console.log('message: ' + msg);
// 			  });


//     socket.on('move', function(msg) {
//         socket.broadcast.emit('move', msg);
//     });
// });

// server.listen(port, function () {
//   console.log('Server listening at port %d', port);
// });
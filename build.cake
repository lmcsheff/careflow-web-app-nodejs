﻿#addin "Cake.Npm"
#addin "nuget:https://www.nuget.org/api/v2?package=Cake.Webpack"
///////////////////////////////////////////////////////////////////////////////
// ARGUMENTS
///////////////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var checkoutDir = Argument("checkoutDir", @"C:\Git\CareflowNodeApp");
var gitSha = Argument("gitSha", "gitShaHash");
var releaseVersion = Argument("releaseVersion","0.0.0.0");
var buildNumber = Argument("buildNumber","0");
var octopusServerBaseUrl = Argument("octopusServerBaseUrl","https://octopus.careflowapp.com");
var octopusAPIKey = Argument("octopusAPIKey","");
var environmentList = Argument("EnvironmentList","AppDev,Prerelease,Demo,Live"); 
var isLocalBuild = Argument("isLocalBuild", true); 
var nugetCliPath = Argument("nugetCliPath", @"c:\tools\nuget.exe");
///////////////////////////////////////////////////////////////////////////////
// GLOBAL VARIABLES
///////////////////////////////////////////////////////////////////////////////
var packageVersion = releaseVersion+"-build."+buildNumber+".sha."+gitSha.Substring(0,7);
var artifactDirectory = $"{checkoutDir}/deploymentPackages";
///////////////////////////////////////////////////////////////////////////////
// SETUP / TEARDOWN
///////////////////////////////////////////////////////////////////////////////

Setup(ctx =>
{
	// Executed BEFORE the first task.
	Information("Running tasks...");
	ctx.Tools.RegisterFile(nugetCliPath);
	EnsureDirectoryExists(artifactDirectory);
});

if (TeamCity.IsRunningOnTeamCity)
{
  // This block makes the teamcily log collapsible by Task
  TaskSetup(ctx => TeamCity.WriteStartBlock(ctx.Task.Name));
  TaskTeardown(ctx => 
  {
      TeamCity.WriteEndBlock(ctx.Task.Name);
      // This service message makes the Tasks duration visible as a statisticValue
      var duration = ctx.Duration.TotalMilliseconds.ToString("0");
      Information("##teamcity[buildStatisticValue key='Block." + ctx.Task.Name + ".Duration' value='" + duration + "']");
  });
} 

Teardown(ctx =>
{
	// Executed AFTER the last task.
	Information("Finished running tasks.");
});

///////////////////////////////////////////////////////////////////////////////
// TASKS
///////////////////////////////////////////////////////////////////////////////
Task("Install Node Modules")
    .Does(() =>
{
  Information("Npm i");
  // https://cake-contrib.github.io/Cake.Npm/docs/usage/examples
  NpmInstall();
});

Task("Run Webpack")
	.Does(() =>{
		Information("Running webpack");
		Webpack.Global();	
	});


Task("Build Package")
	.Does(() =>{
		 var nuGetPackSettings   = new NuGetPackSettings {
                                     Id                      = "Careflow.Website",
                                     Version                 = packageVersion,
                                     Title                   = "The tile of the package",
                                     Authors                 = new[] {"John Doe"},
                                     Owners                  = new[] {"Contoso"},
                                     Description             = "The description of the package",
                                     Summary                 = "Excellent summary of what the package does",
                                     ProjectUrl              = new Uri("https://github.com/SomeUser/TestNuget/"),
                                     IconUrl                 = new Uri("http://cdn.rawgit.com/SomeUser/TestNuget/master/icons/testnuget.png"),
                                     LicenseUrl              = new Uri("https://github.com/SomeUser/TestNuget/blob/master/LICENSE.md"),
                                     Copyright               = "Some company 2015",
                                     ReleaseNotes            = new [] {"Bug fixes", "Issue fixes", "Typos"},
                                     Tags                    = new [] {"Cake", "Script", "Build"},
                                     RequireLicenseAcceptance= false,
                                     Symbols                 = false,
                                     NoPackageAnalysis       = true,
                                     BasePath                = checkoutDir,
                                     OutputDirectory         = artifactDirectory
                                 };
		NuGetPack("./Careflow.Website.nuspec",nuGetPackSettings);
	});


Task("Default")
  .IsDependentOn("Install Node Modules")
  .IsDependentOn("Run Webpack")
  .IsDependentOn("Build Package")
  .Does(() =>
{
  Information("Executions Completed");
});

RunTarget(target);